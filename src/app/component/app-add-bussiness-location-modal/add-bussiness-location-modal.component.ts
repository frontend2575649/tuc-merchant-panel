import { AfterViewInit, ChangeDetectorRef, Component, OnInit, Output, EventEmitter } from '@angular/core';
import * as Feather from 'feather-icons';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Select2OptionData } from 'ng2-select2';
import { Address } from "ngx-google-places-autocomplete/objects/address";
import { Observable } from 'rxjs/internal/Observable';
import { Location } from '@angular/common';
import { HelperService } from 'src/app/service/helper.service';
import { OResponse, OSelect } from 'src/app/service/object.service';
import { DataHelperService } from 'src/app/service/datahelper.service';
declare var $: any;

@Component({
  selector: 'app-add-bussiness-location-modal',
  templateUrl: './add-bussiness-location-modal.component.html',
  styleUrls: ['./add-bussiness-location-modal.component.css']
})
export class AddBussinessLocationModalComponent implements OnInit, AfterViewInit {
  @Output() dismissBusinessLocationModal = new EventEmitter<boolean>();
  public exampleData: Array<Select2OptionData>  = [
    {
      id: 'opt1',
      text: 'Options 1'
    },
    {
      id: 'opt2',
      text: 'Options 2'
    },
    {
      id: 'opt3',
      text: 'Options 3'
    },
    {
      id: 'opt4',
      text: 'Options 4'
    }
  ];
  public options: Select2Options = {
    multiple: true,
    theme: 'classic',
    closeOnSelect: false
  }
  public data =
    {
      Address: null,
      Latitude: null,
      Longitude: null,
      CityAreaId: null,
      CityAreaCode: null,
      CityAreaName: null,
      CityId: null,
      CityCode: null,
      CityName: null,
      StateId: null,
      StateCode: null,
      StateName: null,
      CountryId: null,
      CountryCode: null,
      CountryName: null,
      PostalCode: null,
      MapAddress: null,
      ReferralCode: null,
      draggable: true
    };
  addBusinessLocationForm: FormGroup;
  public displayMap = false;
  IsAddressSet: boolean = false;
  ShowState: boolean = true;
  ShowCity: boolean = true;
  accountId;
  accountKey;
  public Countries: any = [
    {
      ReferenceId: 1,
      ReferenceKey: "nigeria",
      Name: "Nigeria",
      IconUrl: "assets/images/countries/nigeria_icon.png",
      Isd: "234",
      Iso: "ng",
      CurrencyName: "Naira",
      CurrencyNotation: "NGN",
      CurrencySymbol: "₦",
      CurrencyHex: "&#8358;",
      NumberLength: 10
    }
  ]
  public HCXLocManager_S2States_Option: Select2Options;
  public HCXLocManager_S2Country_Option: Select2Options = {
    placeholder: this._HelperService.GetStorage("hca").UserCountry.CountryName,
    multiple: false,
    disabled: true,
  };
  constructor(
    private fb: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    private router: Router
  ) { }
  ngOnInit(): void {
    this.accountId = this._HelperService.GetStorage("hca").UserAccount.AccountId
    this.accountKey = this._HelperService.GetStorage("hca").UserAccount.AccountKey
    this.addBusinessLocationForm = this.fb.group({
      'streetAddress': [null, Validators.required],
      'city': [null, Validators.compose([Validators.required])],
      'state': [null, Validators.compose([Validators.required])],
      'lat': [null],
      'lng': [null],
      // 'countryName': [null, Validators.required]
    })

    // this.data.CountryId = 1;
    // this.data.CountryCode = "nigeria";
    // this.data.CountryName = "Nigeria";
    this.data.CountryId = this._HelperService.UserCountry.CountryId;
      this.data.CountryCode = this._HelperService.UserCountry.CountryKey;
      this.data.CountryName = this._HelperService.UserCountry.CountryName;
    this.HCXLoc_Manager_GetStates();

  }
  ngAfterViewInit(): void {
    Feather.replace();
  }

  HCXLocManager_AddressChange(address: Address) {

    this.data.Latitude = address.geometry.location.lat();
    this.data.Longitude = address.geometry.location.lng();
    this.data.MapAddress = address.formatted_address;
    this.data.Address = address.formatted_address;
    this.addBusinessLocationForm.patchValue({ "lat": this.data.Latitude })
    this.addBusinessLocationForm.patchValue({ "lng": this.data.Longitude })
    var tAddress = this._HelperService.GoogleAddressArrayToJson(address.address_components);

    if (tAddress.postal_code != undefined && tAddress.postal_code != null && tAddress.postal_code != "") {
      this.data.PostalCode = tAddress.postal_code;
    }
    if (tAddress.country != undefined && tAddress.country != null && tAddress.country != "") {
      this.data.CountryName = tAddress.country;
    }
    if (tAddress.administrative_area_level_1 != undefined && tAddress.administrative_area_level_1 != null && tAddress.administrative_area_level_1 != "") {
      this.data.StateName = tAddress.administrative_area_level_1;
    }
    if (tAddress.locality != undefined && tAddress.locality != null && tAddress.locality != "") {
      this.data.CityName = tAddress.locality;
    }
    if (tAddress.administrative_area_level_2 != undefined && tAddress.administrative_area_level_2 != null && tAddress.administrative_area_level_2 != "") {
      this.data.CityAreaName = tAddress.administrative_area_level_2;
    }
    if (tAddress.administrative_area_level_2 != undefined && tAddress.administrative_area_level_2 != null && tAddress.administrative_area_level_2 != "") {
      this.data.CityAreaName = tAddress.administrative_area_level_2;
    }
    if (this.data.CountryName != this._HelperService.UserCountry.CountryName) {
      this._HelperService.NotifyError('Currently we’re not serving in this area, please add locality within ' + this._HelperService.UserCountry.CountryName);
      setTimeout(() => {
        this.HCXLocManager_OpenUpdateManager_Clear();
      }, 1);
  }
    // if (this.data.CountryName != "Nigeria") {
    //   this.displayMap = false
    //   this._HelperService.NotifyError('Currently we’re not serving in this area');

    //   setTimeout(() => {
    //     this.HCXLocManager_OpenUpdateManager_Clear();
    //   }, 1);
    // }
    else {
      this.displayMap = true
      // this.data.CountryId = 1;
      // this.data.CountryCode = "nigeria";
      // this.data.CountryName = "Nigeria";
      this.data.CountryId = this._HelperService.UserCountry.CountryId;
      this.data.CountryCode = this._HelperService.UserCountry.CountryKey;
      this.data.CountryName = this._HelperService.UserCountry.CountryName;
      this.addBusinessLocationForm.patchValue({ "streetAddress": this.data.MapAddress })
      this.HCXLoc_Manager_GetStates();
    }
  }
  HCXLocManager_OpenUpdateManager_Clear() {
    this.IsAddressSet = false;
    this.data =
    {
      Latitude: 0,
      Longitude: 0,

      Address: null,
      MapAddress: null,

      CountryId: 0,
      CountryCode: null,
      CountryName: null,

      StateId: 0,
      StateName: null,
      StateCode: null,

      CityId: 0,
      CityCode: null,
      CityName: null,

      CityAreaId: 0,
      CityAreaName: null,
      CityAreaCode: null,

      PostalCode: null,
      ReferralCode: null,
      draggable: true
    }
  }
  public HCXLocManager_S2States_Data: Array<Select2OptionData>;
  HCXLoc_Manager_GetStates() {
    this._HelperService.IsFormProcessing = true;
    this.ShowState = false;
    var PData =
    {
      Task: this._HelperService.AppConfig.Api.Core.getstates,
      ReferenceKey: this.data.CountryCode,
      ReferenceId: this.data.CountryId,
      Offset: 0,
      Limit: 1000,
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.State, PData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          if (_Response.Result.Data != undefined) {
            var _Data = _Response.Result.Data as any[];
            if (_Data.length > 0) {
              var finalCat = [];
              this.ShowState = false;
              _Data.forEach(element => {
                var Item = {
                  id: element.ReferenceId,
                  key: element.ReferenceKey,
                  text: element.Name,
                  additional: element,
                };
                if (this.data.StateName != undefined || this.data.StateName != null || this.data.StateName != '') {
                  if (element.Name == this.data.StateName) {
                    this.data.StateId = Item.id;
                    this.data.StateCode = Item.key;
                    this.data.StateName = Item.text;
                    this.addBusinessLocationForm.patchValue({ "state": this.data.StateName })
                    this.ShowCity = false;
                    this.HCXLoc_Manager_City_Load();
                    setTimeout(() => {
                      this.ShowCity = true;
                    }, 1);
                  }
                }
                finalCat.push(Item);
              });
              if (this.data.StateId > 0) {
                this.HCXLocManager_S2States_Option = {
                  placeholder: this.data.StateName,
                  multiple: false,
                  allowClear: false,
                };
                setTimeout(() => {
                  this.ShowState = true;
                }, 500);
              }
              else {
                this.HCXLocManager_S2States_Option = {
                  placeholder: "Select state",
                  multiple: false,
                  allowClear: false,
                };
                setTimeout(() => {
                  this.ShowState = true;
                }, 500);
              }
              this.HCXLocManager_S2States_Data = finalCat;
              this.ShowState = true;
              if (this.data.CityName != undefined && this.data.CityName != null && this.data.CityName != "") {
                this.HCXLoc_Manager_GetStateCity(this.data.CityName);
              }
            }
            else {
              this.HCXLocManager_S2States_Option = {
                placeholder: "Select state",
                multiple: false,
                allowClear: false,
              };
              setTimeout(() => {
                this.ShowState = true;
              }, 500);
            }
          }
          else {
            this.HCXLocManager_S2States_Option = {
              placeholder: "Select state",
              multiple: false,
              allowClear: false,
            };
            setTimeout(() => {
              this.ShowState = true;
            }, 500);
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        this._HelperService.ToggleField = false;

      });
  }
  public HCXLoc_Manager_City_Option: Select2Options;
  HCXLoc_Manager_City_Load() {
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.getcities,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.City,
      ReferenceId: this.data.StateId,
      ReferenceKey: this.data.StateCode,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "Name",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
      ],
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    if (this.data.CityName != undefined || this.data.CityName != null && this.data.CityName != '') {
      this.addBusinessLocationForm.patchValue({ "city": this.data.CityName })
      this.HCXLoc_Manager_City_Option = {
        placeholder: this.data.CityName,
        ajax: _Transport,
        multiple: false,
        allowClear: false,
      };
    }
    else {
      this.HCXLoc_Manager_City_Option = {
        placeholder: "Select City",
        ajax: _Transport,
        multiple: false,
        allowClear: false,
      };
    }

  }
  HCXLoc_Manager_GetStateCity(CityName) {
    this._HelperService.IsFormProcessing = true;
    var PData =
    {
      Task: this._HelperService.AppConfig.Api.Core.getcities,
      ReferenceKey: this.data.StateCode,
      ReferenceId: this.data.StateId,
      SearchCondition: this._HelperService.GetSearchConditionStrict('', 'Name', 'text', CityName, '='),
      Offset: 0,
      Limit: 1,
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.City,PData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          if (_Response.Result.Data != undefined) {
            var _Result = _Response.Result.Data;
            if (_Result != undefined && _Result != null && _Result.length > 0) {
              var Item = _Result[0];
              this.data.CityId = Item.ReferenceId;
              this.data.CityCode = Item.ReferenceKey;
              this.data.CityName = Item.Name;
              this.addBusinessLocationForm.patchValue({ "city": this.data.CityName })
              // this.ShowCity = false;
              this.HCXLoc_Manager_City_Load();
              setTimeout(() => {
                this.ShowCity = true;
              }, 1);
            }
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        this._HelperService.ToggleField = false;

      });
  }
  HCXLoc_Manager_City_Change(Items: any) {
    if (Items != undefined && Items.value != undefined && Items.data.length > 0) {
      this.displayMap = false
      this.data.CityId = Items.data[0].ReferenceId;
      this.data.CityCode = Items.data[0].ReferenceKey;
      this.data.CityName = Items.data[0].Name;
      this.addBusinessLocationForm.patchValue({ "city": this.data.CityName })
    }
  }
  HCXLoc_Manager_StateSelected(Items) {
    if (this.data.MapAddress) {
      this.addBusinessLocationForm.patchValue({ "streetAddress": null })
      this.addBusinessLocationForm.controls['streetAddress'].markAsUntouched()
    }
    if (Items != undefined && Items.value != undefined && Items.data.length > 0 && Items.data[0].selected) {
      this.displayMap = false
      this.data.StateId = Items.data[0].id;
      this.data.StateCode = Items.data[0].key;
      this.data.StateName = Items.data[0].text;
      this.addBusinessLocationForm.patchValue({ "state": this.data.StateName })
      this.data.CityId = null;
      this.data.CityCode = null;
      this.data.CityName = null;
      this.data.Address = null;
      this.data.MapAddress = null;
      this.data.CityAreaName = null
      this.data.Latitude = 0;
      this.data.Longitude = 0;
      this.ShowCity = false;
      this.addBusinessLocationForm.patchValue({ "city": this.data.CityName })

      this.HCXLoc_Manager_City_Load();
      setTimeout(() => {
        this.ShowCity = true;
      }, 1);
    }
  }
  markerDragEnd(e) {
    this.data.Latitude = e.coords.lat;
    this.data.Longitude = e.coords.lng;
    this.displayMap = false
    this.addBusinessLocationForm.patchValue({ "lat": this.data.Latitude })
    this.addBusinessLocationForm.patchValue({ "lng": this.data.Longitude })
  }
  public _S2Categories_Data: Array<Select2OptionData>;
  GetDealCategories_Selected(event: any) {
    if (event.value != "0") {
        // this._SelectedSubCategory.ReferenceId = event.data[0].additional.ReferenceId;
        // this._SelectedSubCategory.ReferenceKey = event.data[0].additional.ReferenceKey;
        // this._SelectedSubCategory.Name = event.data[0].additional.Name;
        // this._SelectedSubCategory.IconUrl = event.data[0].additional.IconUrl;
        // this._SelectedSubCategory.Fee = event.data[0].additional.Fees;
        // this._AmountDistribution.TUCPercentage = event.data[0].additional.Fees;
        // this._SelectedSubCategory.ParentCategoryId = event.data[0].additional.ParentCategoryId;
        // this._SelectedSubCategory.ParentCategoryKey = event.data[0].additional.ParentCategoryKey;
        // this._SelectedSubCategory.ParentCategoryName = event.data[0].additional.ParentCategoryName;
        this.addBusinessLocationForm.patchValue(
            {
                SubCategoryKey: event.data[0].additional.ParentCategoryKey
            }
        );
        // this.ProcessAmounts();
    }
}
  HCXLoc_Manager_Validator() {
    if (this.addBusinessLocationForm.value.streetAddress == null) {
      this._HelperService.NotifyError("Please enter address")
    }
    else if (this.addBusinessLocationForm.value.city == null) {
      this._HelperService.NotifyError("Please select city")
    }
    else if (this.addBusinessLocationForm.value.state == null) {
      this._HelperService.NotifyError("Please select state")
    }
    else if (this.addBusinessLocationForm.valid) {
      var _PostData = {
        Task: "ob_merchant_requestupdatebusinessDetails",
        ReferenceId: this.accountId,
        ReferenceKey: this.accountKey,
        Address: {
          Latitude: this.addBusinessLocationForm.value.lat,
          Longitude: this.addBusinessLocationForm.value.lng,
          Address: this.addBusinessLocationForm.value.streetAddress,
          MapAddress: this.data.MapAddress,
          CountryId: this.data.CountryId,
          CountryCode: this.data.CountryCode,
          CountryName: this.data.CountryName,
          StateId: this.data.StateId,
          StateName: this.addBusinessLocationForm.value.state,
          StateCode: this.data.StateCode,
          CityId: this.data.CityId,
          CityCode: this.data.CityCode,
          CityName: this.data.CityName,
          CityAreaId: 0,
          CityAreaName: this.data.CityAreaName,
          CityAreaCode: 0,
          PostalCode: this.data.PostalCode
        },
      }

      this._HelperService.IsFormProcessing = true;
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.MOnBoardV2, _PostData);
      _OResponse.subscribe(
        _Response => {
          this._HelperService.IsFormProcessing = false;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.NotifySuccess('Business Details added successfully');
            localStorage.removeItem('addbusinesslocation');
            this._HelperService.CloseAllModal();
            this.dismissBusinessLocationModal.emit(true);
          }
          else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        });
    }
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { AgmCoreModule } from '@agm/core';
import { Select2Module } from 'ng2-select2';
@NgModule({
    declarations: [
      AddBussinessLocationModalComponent,
    ],
    imports: [
        TranslateModule,
        CommonModule,
        FormsModule,
        Select2Module,
        ReactiveFormsModule,
        GooglePlaceModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
    ],
    providers: [],
    exports: [AddBussinessLocationModalComponent],
    // bootstrap: [TUConsoleComponent]
})
export class AddBussinessLocationModalModule { }