import { AgmCoreModule } from '@agm/core';
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { Select2Module } from "ng2-select2";
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { ImageCropperModule } from "ngx-image-cropper";
import { InputFileConfig, InputFileModule } from 'ngx-input-file';
import { NgxPaginationModule } from "ngx-pagination";
import { TUProductComponent } from "./tuproduct.component";

const config: InputFileConfig = {
    fileAccept: '*',
    fileLimit: 1,
};

const routes: Routes = [
    {
        path: "",
        component: TUProductComponent,
        children: [
            { path: '', data: { 'permission': 'salehistory', PageName: 'System.Menu.Store' }, loadChildren: '../../../../panel/merchant/tucmall/tuvariants/tuvariants.module#TUVariantsModule' },
            // { path: 'inventory/:referencekey/:referenceid', data: { 'permission': 'salehistory', PageName: 'System.Menu.Product' }, loadChildren: '../../../../panel/merchant/tucmall/tuinventory/tuinventory.module#TUInventoryModule' },
            // { path: 'dashboard/:referencekey/:referenceid', data: { 'permission': 'salehistory', PageName: 'System.Menu.Product' }, loadChildren: '../../../../panel/merchant/tucmall/tuproductoverview/dashboard.module#TUDashboardModule' },
            { path: 'variants/:referencekey/:referenceid', data: { 'permission': 'salehistory', PageName: 'System.Menu.Product' }, loadChildren: '../../../../panel/merchant/tucmall/tuvariants/tuvariants.module#TUVariantsModule' },

        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUProductRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        TUProductRoutingModule,
        GooglePlaceModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
        ImageCropperModule,
        InputFileModule.forRoot(config),
    ],
    declarations: [TUProductComponent]
})
export class TUProductModule { }
