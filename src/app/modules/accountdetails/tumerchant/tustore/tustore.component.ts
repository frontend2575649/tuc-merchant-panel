import { ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from 'feather-icons';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { Observable, Subscription } from "rxjs";
import { DataHelperService, HelperService, OResponse, OSelect, OUserDetails, FilterHelperService } from "../../../../service/service";
declare let $: any;
import swal from 'sweetalert2';
import { HCXAddress, HCXAddressConfig, locationType } from "../../../../component/hcxaddressmanager/hcxaddressmanager.component";

@Component({
  selector: "tu-store",
  templateUrl: "./tustore.component.html"
})
export class TUStoreComponent implements OnInit, OnDestroy {
  isLoaded: boolean = true;

  public ShowCategorySelector: boolean = true;
  public _StoreAddress: any = {};
  public _StoreContactPerson: any = {};

  public _AddressShow = true;
  public _Address: HCXAddress = {};
  public _AddressConfig: HCXAddressConfig =
    {
      locationType: locationType.form
    };

  AddressChange(Address) {
    this._Address = Address;
  }
  //#region subscriptions 

  subscription: Subscription;
  ReloadSubscription: Subscription;

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.ReloadSubscription.unsubscribe();
  }

  //#endregion

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {

  }

  //#region ToogleDetailView 

  HideStoreDetail() {
    var element = document.getElementById("StoresHide");
    element.classList.add("Hm-HideDiv");
    element.classList.remove("Hm-ShowStoreDetail");
  }

  ShowStoreDetail() {
    var element = document.getElementById("StoresHide");
    element.classList.add("Hm-ShowStoreDetail");
    element.classList.remove("Hm-HideDiv");
  }
  //#endregion

  BackDropInit(): void {
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.onclick = () => {
      $(this.divView.nativeElement).removeClass('show');
      backdrop.classList.remove("show");
    };
  }

  ngOnInit() {
    //#region UIInit 
    Feather.replace();
    this._HelperService.ContainerHeight = window.innerHeight;
    this._HelperService.AppConfig.ShowHeader = true;
    this.BackDropInit();
    //#endregion
    //#region Subscriptions 
    this.subscription = this._HelperService.isprocessingtoogle
      .subscribe((item) => {
        this._ChangeDetectorRef.detectChanges();
      });

    this.ReloadSubscription = this._HelperService.ReloadEventEmit.subscribe((number) => {

    });

    //#endregion

    var StorageDetails = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.ActiveStore);
    if (StorageDetails != null) {
      this._HelperService.AppConfig.ActiveReferenceKey = StorageDetails.ReferenceKey;
      this._HelperService.AppConfig.ActiveReferenceId = StorageDetails.ReferenceId;
      this._HelperService.AppConfig.ActiveReferenceDisplayName = StorageDetails.DisplayName;
      this._HelperService.AppConfig.ActiveReferenceAccountTypeCode = StorageDetails.AccountTypeCode;
    }

    this._HelperService.Get_UserAccountDetails(false);

    //#region DropdownInit 

    this.GetSoresDetails();


    //#endregion

    this.FormA_EditUser_Load();
    this.FormB_EditUser_Load();
    this.FormD_EditUser_Load();
    this.GetBusinessCategories();

  }

  //#region OffCanvasNBackdrop 
  @ViewChild("offCanvas") divView: ElementRef;

  ShowOffCanvas() {
    $(this.divView.nativeElement).addClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.add("show");
  }
  RemoveOffCanvas() {
    $(this.divView.nativeElement).removeClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.remove("show");
  }
  //#endregion

  //#region StoreDetails 

  public _StoreDetails: any =
    {
      ManagerName: null,
      BranchName: null,
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Description: null,
      Address: null,
      AddressComponent: {
        Latitude: 0,
        Longitude: 0
      },
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      SmsText: null,
      Comment: null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,
      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,
    }
  GetSoresDetails() {
    this._AddressShow = false;
    this._HelperService.IsFormProcessing = true;
    this.isLoaded = false;

    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetStore,
      AccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveReferenceId
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this.isLoaded = true;

          this._StoreDetails = _Response.Result;
          this._StoreDetails = _Response.Result;
          
          if (this._StoreDetails.AddressComponent != undefined && this._StoreDetails.AddressComponent != null) {
            this._Address = this._StoreDetails.AddressComponent;
          }
          this._AddressShow = true;



          // 
          if (this._StoreDetails != undefined && this._StoreDetails.ContactNumber != undefined && this._StoreDetails.ContactNumber != null) {
            if (this._StoreDetails.ContactNumber.startsWith("234")) {
              this._StoreDetails.ContactNumber = this._StoreDetails.ContactNumber.substring(3, this._StoreDetails.length);
            }
          }
          // 


          this._StoreDetails.UpdateCategories = [];
          for (let index = 0; index < this._StoreDetails.Categories.length; index++) {
            const element = this._StoreDetails.Categories[index];
            this._StoreDetails.UpdateCategories.push({
              Name: element.Name,
              ReferenceKey: element.ReferenceKey,
              ReferenceId: element.ReferenceId
            });

          }

          this._StoreDetails.CreateDate = this._HelperService.GetDateS(this._StoreDetails.CreateDate);
          this._StoreDetails.ModifyDateS = this._HelperService.GetDateS(this._StoreDetails.ModifyDate);
          this._StoreDetails.StatusI = this._HelperService.GetStatusIcon(this._StoreDetails.StatusCode);
          this._StoreDetails.StatusB = this._HelperService.GetStatusBadge(this._StoreDetails.StatusCode);
          this._StoreDetails.StatusC = this._HelperService.GetStatusColor(this._StoreDetails.StatusCode);
          this._StoreAddress = this._StoreDetails.Address;
          this._StoreContactPerson = this._StoreDetails.ContactPerson;
          if (this._StoreContactPerson != undefined && this._StoreContactPerson.MobileNumber != undefined && this._StoreContactPerson.MobileNumber != null) {
            if (this._StoreContactPerson.MobileNumber.startsWith("234")) {
              this._StoreContactPerson.MobileNumber = this._StoreContactPerson.MobileNumber.substring(3, this._StoreContactPerson.length);
            }
          }
          this._ChangeDetectorRef.detectChanges();
          //#region ResponseInit 
          //#endregion

          //#region Patch Profile
          //#endregion

        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }


  AccoutSettings() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Settings
    ]);
  }

  //#endregion

  //#region EditStore

  //#region EditUserFormA 

  FormA_EditUser: FormGroup;

  FormA_EditUser_Show() {
    this._HelperService.OpenModal("FormA_EditUser_Content");
  }
  FormA_EditUser_Close() {
    var backdrop: HTMLElement = document.getElementById("backdrop");
    $(this.divView.nativeElement).removeClass('show');
    backdrop.classList.remove("show");
  }
  FormA_EditUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.FormA_EditUser = this._FormBuilder.group({
      OperationType: 'new',
      AccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveReferenceId,
      Task: this._HelperService.AppConfig.Api.ThankUCash.updatestore,
      DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
      Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
      ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(18)])],
      EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])]
    });
  }

  FormA_EditUser_Process(_FormValue: any) {
    if (this._Address.CityId < 1) {
      this._HelperService.NotifyError("Please enter store location");
    }
    else {
      this._HelperService.IsFormProcessing = true;
      let _OResponse: Observable<OResponse>;

      var request = this.CreateRequestJson();
      _OResponse = this._HelperService.PostData(
        this._HelperService.AppConfig.NetworkLocation.V3.Account,
        request
      );
      _OResponse.subscribe(
        _Response => {
          this.toogleIsFormProcessing(false);
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.NotifySuccess("Store details updated successfully");
            this.GetSoresDetails();
            this.Forms_EditUser_Close();
          } else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this.toogleIsFormProcessing(false);
          this._HelperService.HandleException(_Error);
        }
      );
    }
  }

  //#endregion

  //#region EditUserFormB 

  FormB_EditUser: FormGroup;
  FormB_EditUser_Address: string = null;
  FormB_EditUser_Latitude: number = 0;
  FormB_EditUser_Longitude: number = 0;


  FormB_EditUser_Show() {
    this._HelperService.OpenModal("FormB_EditUser_Content");
  }

  FormB_EditUser_Close() {
    var backdrop: HTMLElement = document.getElementById("backdrop");
    $(this.divView.nativeElement).removeClass('show');
    backdrop.classList.remove("show");
  }
  FormB_EditUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.FormB_EditUser = this._FormBuilder.group({
      // OperationType: 'new',
      // Task: this._HelperService.AppConfig.Api.ThankUCash.updatestore,

      MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
      FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      LastName: [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      CEmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
    });
  }

  FormB_EditUser_Process(_FormValue: any) {

    var request = this.CreateRequestJson();

    this.toogleIsFormProcessing(true);
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V3.Account,
      request
    );
    _OResponse.subscribe(
      _Response => {
        this.toogleIsFormProcessing(false);
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Account updated successfully");
          if (_FormValue.OperationType == "edit") {
            this.Forms_EditUser_Close();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this.toogleIsFormProcessing(false);
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion

  //#region EditUserFormC 


  //#endregion

  //#endregion


  //#region EditUserFormB 

  FormD_EditUser: FormGroup;
  FormD_EditUser_Address: string = null;
  FormD_EditUser_Latitude: number = 0;
  FormD_EditUser_Longitude: number = 0;


  FormD_EditUser_Show() {
    this._HelperService.OpenModal("FormB_EditUser_Content");
  }

  FormD_EditUser_Close() {
    var backdrop: HTMLElement = document.getElementById("backdrop");
    $(this.divView.nativeElement).removeClass('show');
    backdrop.classList.remove("show");
  }
  FormD_EditUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.FormD_EditUser = this._FormBuilder.group({
      // OperationType: 'new',
      // Task: this._HelperService.AppConfig.Api.ThankUCash.updatestore,

      Username: [null],
      Password: [null],

    });
  }

  FormD_EditUser_Process(_FormValue: any) {

    var request = this.CreateRequestJson();

    this.toogleIsFormProcessing(true);
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V3.Account,
      request
    );
    _OResponse.subscribe(
      _Response => {
        this.toogleIsFormProcessing(false);
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Account updated successfully");
          if (_FormValue.OperationType == "edit") {
            this.Forms_EditUser_Close();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this.toogleIsFormProcessing(false);
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion



  CreateRequestJson(): any {


    var AccountDetail = this.FormA_EditUser.value;
    AccountDetail.Categories = [];
    if (this.SelectedBusinessCategories != undefined && this.SelectedBusinessCategories.length > 0) {
      this.SelectedBusinessCategories.forEach(element => {
        if (!this.DoesCatAlreadyExist(element.id)) {
          AccountDetail.Categories.push(
            {
              ReferenceKey: element.key,
              ReferenceId: element.id,
              Name: element.Name
            }
          )
        }
      });
    }

    AccountDetail.Categories = AccountDetail.Categories.concat(this._StoreDetails.UpdateCategories);

    var ContactPersonDetail = this.FormB_EditUser.value;

    var credentialsdetails = this.FormD_EditUser.value;

    AccountDetail.Address = this._Address.Address;
    AccountDetail.AddressComponent = this._Address;

    AccountDetail.Username = credentialsdetails.Username;
    AccountDetail.Password = credentialsdetails.Password;


    AccountDetail.ContactPerson = ContactPersonDetail;

    AccountDetail.AccountKey = this._StoreDetails.ReferenceKey;
    AccountDetail.AccountId = this._StoreDetails.ReferenceId;
    return AccountDetail;

  }

  toogleIsFormProcessing(value: boolean): void {
    this._HelperService.IsFormProcessing = value;
  }

  Forms_EditUser_Close() {
    // this.GetMerchantDetails();
    var backdrop: HTMLElement = document.getElementById("backdrop");
    $(this.divView.nativeElement).removeClass('show');
    backdrop.classList.remove("show");
  }

  Form_EditUser_Block() {

  }

  public BusinessCategories = [];
  public S2BusinessCategories = [];

  GetBusinessCategories() {
    this._HelperService.ToggleField = true;

    var PData =
    {
      Task: this._HelperService.AppConfig.Api.Core.getcategories,
      SearchCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
      SortExpression: 'Name asc',
      Offset: 0,
      Limit: 1000,
    }
    PData.SearchCondition = this._HelperService.GetSearchConditionStrict(
      PData.SearchCondition,
      "TypeCode",
      this._HelperService.AppConfig.DataType.Text,
      this._HelperService.AppConfig.HelperTypes.MerchantCategories,
      "="
    );
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Op, PData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          if (_Response.Result.Data != undefined) {
            this.BusinessCategories = _Response.Result.Data;

            this.ShowCategorySelector = false;
            this._ChangeDetectorRef.detectChanges();
            for (let index = 0; index < this.BusinessCategories.length; index++) {
              const element = this.BusinessCategories[index];
              this.S2BusinessCategories.push(
                {
                  id: element.ReferenceId,
                  key: element.ReferenceKey,
                  text: element.Name
                }
              );
            }
            this.ShowCategorySelector = true;
            this._ChangeDetectorRef.detectChanges();


            this._HelperService.ToggleField = false;
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        this._HelperService.ToggleField = false;
      });
  }
  public SelectedBusinessCategories = [];
  CategoriesSelected(Items) {

    if (Items != undefined && Items.value != undefined && Items.value.length > 0) {
      this.SelectedBusinessCategories = Items.data;
    }
    else {
      this.SelectedBusinessCategories = [];
    }

  }
  SaveMerchantBusinessCategory(item) {
    if (item != '0') {
      var Setup =
      {
        Task: this._HelperService.AppConfig.Api.Core.SaveUserParameter,
        TypeCode: this._HelperService.AppConfig.HelperTypes.MerchantCategories,
        // UserAccountKey: this._UserAccount.ReferenceKey,
        CommonKey: item,
        StatusCode: 'default.active'
      };
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, Setup);
      _OResponse.subscribe(
        _Response => {
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.NotifySuccess('Business category assigned to merchant');
            // this.BusinessCategories = [];
            // this.GetMerchantBusinessCategories();
          }
          else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        });
    }
  }

  removeCategory(cat: any): void {
    this._StoreDetails.UpdateCategories.splice(cat, 1);
  }

  DoesCatAlreadyExist(Id: number): boolean {
    for (let index = 0; index < this._StoreDetails.UpdateCategories.length; index++) {
      const element = this._StoreDetails.UpdateCategories[index];
      if (element.ReferenceId == Id) {
        return true;
      }
    }
    return false;
  }

  unclick() {
    $(this.divView.nativeElement).removeClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.remove("show");
  }

  BlockStore() {
    this._HelperService.OpenModal("BlockPos");
  }
  public Block(): void {
    this._HelperService.IsFormProcessing = true;
    this._HelperService.AppConfig.ShowHeader = true;
    var pData = {
      Task: "disablestore",
      AccountId: this._StoreDetails.ReferenceId,
      AccountKey: this._StoreDetails.ReferenceKey,
      StatusCode: "default.blocked",
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifySuccess("Store Disabled Successfully ");
          this._HelperService.CloseModal("BlockPos");
          this.GetSoresDetails();

        } else {
          this._HelperService.IsFormProcessing = false;
          // this._HelperService.NotifyError(_Response.Message);
          this._HelperService.NotifyError("Store Already blocked, Please refresh page and try again");
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }
  public UnBlock(): void {
    this._HelperService.IsFormProcessing = true;
    this._HelperService.AppConfig.ShowHeader = true;
    var pData = {
      Task: "enablestore",
      AccountId: this._StoreDetails.ReferenceId,
      AccountKey: this._StoreDetails.ReferenceKey,
      StatusCode: "default.blocked",
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifySuccess("Store Enabled Successfully ");
          this._HelperService.CloseModal("BlockPos");
          this.GetSoresDetails();

        } else {
          this._HelperService.IsFormProcessing = false;
          //this._HelperService.NotifyError(_Response.Message);
          this._HelperService.NotifyError("Store Already Unblocked, Please refresh page and try again");
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }


  //#region Expand/Collapse Details 

  ExpandedView: any = false;

  ToogleExpandedView(): void {
    this.ExpandedView = !this.ExpandedView;
  }

  //#endregion
}

export class OAccountOverview {
  public Merchants: number;
  public Stores: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
}
