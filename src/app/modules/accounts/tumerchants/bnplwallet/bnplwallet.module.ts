import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BnplwalletComponent } from './bnplwallet.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { MainPipe } from '../../../../service/main-pipe.module'
import { ImageCropperModule } from 'ngx-image-cropper';

const routes: Routes = [
  {
    path: "",
    component: BnplwalletComponent,
    children: [
      { path: "redeem", data: { accounttypecode: "merchant" }, loadChildren: "./bnpl-redeemwallet/bnpl-redeemwallet.module#BnplRedeemWalletModule" },
    ]
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class BnplWalletRoutingModule { }
@NgModule({
  declarations: [BnplwalletComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    MainPipe,
    ImageCropperModule,
    BnplWalletRoutingModule
  ]
})
export class BnplwalletModule { }
