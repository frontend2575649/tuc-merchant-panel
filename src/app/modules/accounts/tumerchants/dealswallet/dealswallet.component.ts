import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { DataHelperService } from 'src/app/service/datahelper.service';
import { HelperService } from 'src/app/service/helper.service';

@Component({
  selector: 'app-dealswallet',
  templateUrl: './dealswallet.component.html',
})
export class DealswalletComponent implements OnInit {

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService
  ) { }

  ngOnInit() {
  }

}
