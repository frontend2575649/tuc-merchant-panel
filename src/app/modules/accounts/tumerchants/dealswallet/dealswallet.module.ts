import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { MainPipe } from '../../../../service/main-pipe.module'
import { ImageCropperModule } from 'ngx-image-cropper';
import { DealswalletComponent } from "./dealswallet.component";

const routes: Routes = [
  {
    path: "",
    component: DealswalletComponent,
    children: [
      { path: "redeem", data: { accounttypecode: "merchant" }, loadChildren: "./deals-redeemwallet/deals-redeemwallet.module#DealsRedeemWalletModule" },
    ]
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class DealsWalletRoutingModule { }

@NgModule({
  declarations: [DealswalletComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    Ng2FileInputModule,
    MainPipe,
    ImageCropperModule,
    DealsWalletRoutingModule
  ]
})
export class DealswalletModule { }
