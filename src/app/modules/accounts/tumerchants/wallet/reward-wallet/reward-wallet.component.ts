import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import * as Feather from 'feather-icons';
import { ChangeContext } from 'ng5-slider';
import { Observable, Subscription } from 'rxjs';
import swal from 'sweetalert2';
import { DataHelperService, FilterHelperService, HelperService, OList, OResponse, OSelect } from '../../../../../service/service';
declare var moment: any;
declare var $: any;
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-reward-wallet',
  templateUrl: './reward-wallet.component.html',
})
export class RewardWalletComponent implements OnInit {

  public ResetFilterControls: boolean = true;
  public _ObjectSubscription: Subscription = null;
  public displayDetails: boolean = false
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewCashierBtn = false;
    this._HelperService.showAddNewSubAccBtn = false;


  }
  @ViewChild("offCanvas") divView: ElementRef;
  isValidAmount: boolean = true
  isBalanceZero: boolean = false
  maxTransferAmount: boolean = true
  _rewardWalletOverview = {
    balance: null,
    userRewards: ""
  }

  _LoyaltyredeemWalletOverview = {
    amount: null
  }

  _DealsredeemWalletOverview = {
    amount: null
  }

  _BnplredeemWalletOverview = {
    amount: null
  }
  isBnplRedeemWallet: boolean = false
  isDealsRedeemWallet: boolean = false
  redeemFrom = null
  ngOnInit() {
    this._HelperService.StopClickPropogation();
    Feather.replace();
    var StorageDetails = this._HelperService.GetStorage("hcuat");

    if (StorageDetails) {
      if (StorageDetails.length == 0) {
        this.isDealsRedeemWallet = true
      } else {
        for (let i = 0; i < StorageDetails.length; i++) {
          if (StorageDetails[i].TypeCode.includes("deal")) {
            this.isDealsRedeemWallet = true
          }
          if (StorageDetails[i].TypeCode.includes("bnpl")) {
            this.isBnplRedeemWallet = true
          }
        }
      }
    }
    this.getRewardWalletOverview()
    this.getLoyaltyRedeemWalletBalance()
    this.getDealsRedeemWalletBalance()
    this.getBnplRedeemWalletBalance()
    this.CashoutList_Setup();
    this.CashoutList_Filter_Stores_Load();
    this.CashoutList_Filter_Providers_Load();
    this.InitColConfig();
    this.CashoutList_Filter_Banks_Load();
    this.Form_AddUser_Load();
    this.TUTr_Filter_Banks_Load();
    this.InitBackDropClickEvent();
    this.GetCashOutConfiguration();

    // this._ObjectSubscription = this._HelperService.ObjectCreated.subscribe(value => {
    //   this.CashoutList_GetData();
    // });
  }
  InitBackDropClickEvent(): void {
    var backdrop: HTMLElement = document.getElementById("backdrop");

    backdrop.onclick = () => {
      $('.df-settings').show();
      $(this.divView.nativeElement).removeClass('show');
      backdrop.classList.remove("show");
    };
  }
  clicked() {
    $('.df-settings').hide();
    $(this.divView.nativeElement).addClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.add("show");
  }
  unclick() {
    $('.df-settings').show();
    $(this.divView.nativeElement).removeClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.remove("show");
  }

  ngOnDestroy(): void {
    try {
      this._ObjectSubscription.unsubscribe();
    } catch (error) {
    }
  }

  Form_AddUser_Open() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer
        .MerchantOnboarding,
    ]);
  }

  //#region columnConfig

  TempColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  ColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  InitColConfig() {
    var MerchantTableConfig = this._HelperService.GetStorage("BMerchantTable");
    var ColConfigExist: boolean =
      MerchantTableConfig != undefined && MerchantTableConfig != null;
    if (ColConfigExist) {
      this.ColumnConfig = MerchantTableConfig.config;
      this.TempColumnConfig = this._HelperService.CloneJson(
        MerchantTableConfig.config
      );
    }
  }

  OpenEditColModal() {
    this._HelperService.OpenModal("EditCol");
  }

  SaveEditCol() {
    this.ColumnConfig = this._HelperService.CloneJson(this.TempColumnConfig);
    this._HelperService.SaveStorage("BMerchantTable", {
      config: this.ColumnConfig,
    });
    this._HelperService.CloseModal("EditCol");
  }

  //#endregion

  getRewardWalletOverview() {
    this._HelperService.IsFormProcessing = true;
    var Data = {
      Task: 'gettopupoverview',
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      TotalRecords: 0,
      Offset: 0,
      Limit: 10,
      RefreshCount: true,
      SearchCondition: "",
      SortExpression: "StartDate desc",
      Type: "all",
      ReferenceKey: null,
      ReferenceId: 0,
      SubReferenceId: 0,
      IsDownload: false
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Subscription, Data);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._rewardWalletOverview.balance = _Response.Result.TotalAmount;
          this._rewardWalletOverview.userRewards = _Response.Result.RewardPercentage;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  getLoyaltyRedeemWalletBalance() {
    this._HelperService.IsFormProcessing = true;
    var Data = {
      Task: 'getredeemwalletoverview',
      ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
      ReferenceId: this._HelperService.AppConfig.ActiveOwnerId,
      RedeemFrom: 775
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Payments, Data);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._LoyaltyredeemWalletOverview.amount = _Response.Result.TotalAmount;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  getDealsRedeemWalletBalance() {
    this._HelperService.IsFormProcessing = true;
    var Data = {
      Task: 'getredeemwalletoverview',
      ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
      ReferenceId: this._HelperService.AppConfig.ActiveOwnerId,
      RedeemFrom: 776
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Payments, Data);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._DealsredeemWalletOverview.amount = _Response.Result.TotalAmount;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  getBnplRedeemWalletBalance() {
    this._HelperService.IsFormProcessing = true;
    var Data = {
      Task: 'getredeemwalletoverview',
      ReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
      ReferenceId: this._HelperService.AppConfig.ActiveOwnerId,
      RedeemFrom: 777
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Payments, Data);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._BnplredeemWalletOverview.amount = _Response.Result.TotalAmount;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }
  //#region merchantlist

  optionSelected(attr) {
    if (attr == "walletL" && this._LoyaltyredeemWalletOverview.amount <= 0) {
      this.isBalanceZero = true
    } else if (attr == "walletD" && this._DealsredeemWalletOverview.amount <= 0) {
      this.isBalanceZero = true
    } else if (attr == "walletB" && this._BnplredeemWalletOverview.amount <= 0) {
      this.isBalanceZero = true
    } else {
      this.Form_AddTopup.get('control').setValue(attr)
      this.Form_AddTopup.get('control').updateValueAndValidity()
      this.isBalanceZero = false
    }
  }

  public CashoutList_Config: OList;
  CashoutList_Setup() {
    this.CashoutList_Config = {
      Id: null,
      // Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetRewardWalletHistory,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Payments,
      Title: "Available Top History",
      StatusType: "default",
      Type: this._HelperService.AppConfig.TerminalTypes.all,
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      // DefaultSortExpression: "CreateDate desc",
      IsDownload: true,
      Sort:
      {
        SortDefaultName: null,
        SortDefaultColumn: 'TransactionDate',
        SortName: null,
        SortColumn: null,
        SortOrder: 'desc',
        SortOptions: [],
      },
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '=='),

      TableFields: [
        {
          DisplayName: '#',
          SystemName: 'ReferenceId',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Show: false,
          Search: false,
          Sort: false,
        },
        {
          DisplayName: 'Date',
          SystemName: 'TransactionDate',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Show: true,
          Search: true,
          Sort: true,
          IsDateSearchField: true,
        },
        {
          DisplayName: 'Time',
          SystemName: '',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Show: true,
          Search: false,
          Sort: false
        },

        {
          DisplayName: "Store Name",
          SystemName: "",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
        },

        {
          DisplayName: 'Amount',
          SystemName: 'Amount',
          DataType: this._HelperService.AppConfig.DataType.Decimal,
          Show: true,
          Search: false,
          Sort: true,
        },
        {
          DisplayName: "Transaction Type",
          SystemName: "PaymentMethodName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
        },
        {
          DisplayName: "Transaction ID",
          SystemName: "PaymentReference",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
        },


      ],
    };
    this.CashoutList_Config = this._DataHelperService.List_Initialize(
      this.CashoutList_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Cashout,
      this.CashoutList_Config
    );

    this.CashoutList_GetData();
  }
  CashoutList_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.CashoutList_Config.Sort.SortOptions.length; index++) {
        const element = this.CashoutList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.CashoutList_Config


    );

    this.CashoutList_Config = this._DataHelperService.List_Operations(
      this.CashoutList_Config,
      event,
      Type
    );

    if (
      (this.CashoutList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.CashoutList_GetData();
    }

  }

  timeout = null;
  CashoutList_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.CashoutList_Config.Sort.SortOptions.length; index++) {
          const element = this.CashoutList_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.CashoutList_Config


      );

      this.CashoutList_Config = this._DataHelperService.List_Operations(
        this.CashoutList_Config,
        event,
        Type
      );

      if (
        (this.CashoutList_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.CashoutList_GetData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);


  }

  public OverviewData: any = {
    Total: 0,
    Active: 0,
    Idle: 0,
    Dead: 0,
    Unused: 0
  };

  CashoutList_GetData() {
    // this.GetOverviews(this.CashoutList_Config, this._HelperService.AppConfig.Api.ThankUCash.TransactionOverviews.getterminalsoverview);
    var TConfig = this._DataHelperService.List_GetData(
      this.CashoutList_Config
    );
    this.CashoutList_Config = TConfig;
  }
  CurrentData: any = {

  }

  GoBack() {
    this.displayDetails = false
  }

  DownloadPdf() {
    html2canvas(document.querySelector("#pdfData"), {
      onclone: function (clonedDoc) {
        clonedDoc.getElementById('pdfData').style.display = 'block';
      }
    }).then(canvas => {
      var imgWidth = 208;
      var pageHeight = 295;
      var imgHeight = canvas.height * imgWidth / canvas.width;
      var heightLeft = imgHeight;
      const contentDataURL = canvas.toDataURL('image/png');
      let pdf1 = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF
      var position = 0;
      pdf1.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
      pdf1.save('TransactionDetails' + '.pdf'); // Generated PDF
    });
  }

  CashoutList_RowSelected(ReferenceData) {
    // this.displayDetails = true
    this._HelperService.SaveStorage(
      "cashoutdetails",
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.PosTerminal,
      }
    );

    this._HelperService.AppConfig.ActiveReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;
    this.CurrentData = ReferenceData
    // console.log("this.CurrentData", this.CurrentData);
    this.CurrentData.StatusB = this._HelperService.GetStatusBadge(this.CurrentData.StatusCode);
    this.clicked()
  }

  //#endregion

  //#region Store

  public CashoutList_Filter_Store_Option: Select2Options;
  public CashoutList_Filter_Store_Selected = 0;
  CashoutList_Filter_Stores_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      AccountId: this._HelperService.UserAccount.AccountId,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
        // {
        //   SystemName: "AccountTypeCode",
        //   Type: this._HelperService.AppConfig.DataType.Text,
        //   SearchCondition: "=",
        //   SearchValue: this._HelperService.AppConfig.AccountType.Store
        // }
      ],
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.CashoutList_Filter_Store_Option = {
      placeholder: "Filter by Store",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  CashoutList_Filter_Stores_Change(event: any) {

    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.CashoutList_Config,
      this._HelperService.AppConfig.OtherFilters.Terminal.Store
    );

    this.StoreEventProcessing(event);

  }

  StoreEventProcessing(event: any): void {
    if (event.value == this.CashoutList_Filter_Store_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "StoreReferenceId",
        this._HelperService.AppConfig.DataType.Number,
        this.CashoutList_Filter_Store_Selected,
        "="
      );
      this.CashoutList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.CashoutList_Config.SearchBaseConditions
      );
      this.CashoutList_Filter_Store_Selected = 0;
    } else if (event.value != this.CashoutList_Filter_Store_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "StoreReferenceId",
        this._HelperService.AppConfig.DataType.Number,
        this.CashoutList_Filter_Store_Selected,
        "="
      );
      this.CashoutList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.CashoutList_Config.SearchBaseConditions
      );
      this.CashoutList_Filter_Store_Selected = event.value;
      this.CashoutList_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "StoreReferenceId",
          this._HelperService.AppConfig.DataType.Number,
          this.CashoutList_Filter_Store_Selected,
          "="
        )
      );
    }
    this.CashoutList_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion

  //#region Provider

  public CashoutList_Filter_Provider_Option: Select2Options;
  public CashoutList_Filter_Provider_Selected = 0;
  CashoutList_Filter_Providers_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.PosAccount,
        },
      ],
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.CashoutList_Filter_Provider_Option = {
      placeholder: "Filter by PTSP",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  CashoutList_Filter_Providers_Change(event: any) {

    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.CashoutList_Config,
      this._HelperService.AppConfig.OtherFilters.Terminal.Provider
    );

    this.ProvderEventProcessing(event);

  }

  ProvderEventProcessing(event): void {
    if (event.value == this.CashoutList_Filter_Provider_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ProviderReferenceId",
        this._HelperService.AppConfig.DataType.Number,
        this.CashoutList_Filter_Provider_Selected,
        "="
      );
      this.CashoutList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.CashoutList_Config.SearchBaseConditions
      );
      this.CashoutList_Filter_Provider_Selected = 0;
    } else if (event.value != this.CashoutList_Filter_Provider_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ProviderReferenceId",
        this._HelperService.AppConfig.DataType.Number,
        this.CashoutList_Filter_Provider_Selected,
        "="
      );
      this.CashoutList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.CashoutList_Config.SearchBaseConditions
      );
      this.CashoutList_Filter_Provider_Selected = event.value;
      this.CashoutList_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "ProviderReferenceId",
          this._HelperService.AppConfig.DataType.Number,
          this.CashoutList_Filter_Provider_Selected,
          "="
        )
      );
    }
    this.CashoutList_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion
  //#region Bank 

  public CashoutList_Filter_Bank_Option: Select2Options;
  public CashoutList_Filter_Bank_Selected = 0;
  CashoutList_Filter_Banks_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.Acquirer
        }
      ]
    };
    // S2Data.SearchBaseCondition = this._HelperService.GetSearchConditionStrict('', 'TypeCode', this._HelperService.AppConfig.DataType.Text, Condition, '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.CashoutList_Filter_Bank_Option = {
      placeholder: 'Filter by Bank',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  CashoutList_Filter_Banks_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.CashoutList_Config,
      this._HelperService.AppConfig.OtherFilters.Terminal.Bank
    );

    this.BankEventProcessing(event);
  }

  BankEventProcessing(event): void {
    if (event.value == this.CashoutList_Filter_Bank_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquierReferenceId', this._HelperService.AppConfig.DataType.Number, this.CashoutList_Filter_Bank_Selected, '=');
      this.CashoutList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.CashoutList_Config.SearchBaseConditions);
      this.CashoutList_Filter_Bank_Selected = 0;
    }
    else if (event.value != this.CashoutList_Filter_Bank_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict('', 'AcquierReferenceId', this._HelperService.AppConfig.DataType.Number, this.CashoutList_Filter_Bank_Selected, '=');
      this.CashoutList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.CashoutList_Config.SearchBaseConditions);
      this.CashoutList_Filter_Bank_Selected = event.value;
      this.CashoutList_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'AcquierReferenceId', this._HelperService.AppConfig.DataType.Number, this.CashoutList_Filter_Bank_Selected, '='));
    }
    this.CashoutList_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }

  //#endregion

  SetOtherFilters(): void {
    this.CashoutList_Config.SearchBaseConditions = [];
    // this.CashoutList_Config.SearchBaseCondition = null;

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
      // this.CashoutList_Filter_Owners_Selected = null;
      // this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }
  ToogleType(type: string): void {
    this.CashoutList_Config.Type = type;
    this.CashoutList_GetData();
  }
  // ToogleStatusType(type: any): void {
  //   console.log(type.value);
  //   switch (type.value) {
  //     case '0': this.CashoutList_Config.Type = this._HelperService.AppConfig.TerminalTypes.all

  //       break;
  //     case '547': this.CashoutList_Config.Type = this._HelperService.AppConfig.TerminalTypes.active

  //       break;
  //     case '548': {
  //       this.CashoutList_Config.Type = this._HelperService.AppConfig.TerminalTypes.idle
  //     }
  //       break;
  //     case '549': this.CashoutList_Config.Type = this._HelperService.AppConfig.TerminalTypes.dead

  //       break;
  //     case '550': this.CashoutList_Config.Type = this._HelperService.AppConfig.TerminalTypes.unused

  //       break;

  //     default:
  //       break;
  //   }

  //   // this.CashoutList_Config.Type = type;
  //   // this.CashoutList_GetData();
  // }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.CashoutList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.CashoutList_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_NoSortRewardmWallet(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.CashoutList_Config);

    this.SetOtherFilters();

    this.CashoutList_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        maxLength: "32",
        minLength: "4",
      },
      inputValidator: function (value) {
        if (value === '' || value.length < 4) {
          return 'Enter filter name length greater than 4!'
        }
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Cashout
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }
  Delete_Confirm() {
    swal({
      position: 'top',
      title: this._HelperService.AppConfig.CommonResource.DeleteBankTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteBankHelp,
      // input: 'password',
      // inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      // inputAttributes: {
      //   autocapitalize: 'off',
      //   autocorrect: 'off',
      //   maxLength: "4",
      //   minLength: "4"
      // },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService.IsFormProcessing = true;
        var pData = {
          Task: this._HelperService.AppConfig.Api.ThankUCash.deletebankaccount,
          AccountId: this._HelperService.AppConfig.ActiveOwnerId,
          AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
          ReferenceId: this.CurrentData.ReferenceId,
          ReferenceKey: this.CurrentData.ReferenceKey,
          // AuthPin: result.value
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Bank, pData);
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess(_Response.Message);
              this.unclick();
              this.CashoutList_Setup();
            }
            else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {

            this._HelperService.HandleException(_Error);
          });
      }
    });
  }


  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Cashout
        );
        this._FilterHelperService.SetMerchantConfig(this.CashoutList_Config);
        this.CashoutList_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.CashoutList_GetData();

    if (ButtonType == 'Sort') {
      $("#CashoutList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#CashoutList_fdropdown").dropdown('toggle');
    }
    this.CashoutList_GetData();
    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.CashoutList_Config);
    this.SetOtherFilters();

    this.CashoutList_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }
  public TUTr_Filter_Bank_Option: Select2Options;
  TUTr_Filter_Banks_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.getbankaccounts,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Bank,
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "Name",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "BankName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        // {
        //   SystemName: "Code",
        //   Type: this._HelperService.AppConfig.DataType.Text,
        //   Id: false,
        //   Text: true
        // }
      ]

    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Bank_Option = {
      placeholder: 'Select Bank',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TUTr_Filter_Banks_Change(event: any) {
    // console.log("dddd",event.data[0])
    this.Form_AddUser.patchValue(
      {
        BankId: event.data[0].ReferenceId,
        BankKey: event.data[0].ReferenceKey,
        // BankAccountNumber:event.data[0].AccountNumber,
      });
  }

  validateInput(e) {
    if (e.target.value > 20000) {
      this.maxTransferAmount = false
      this.isValidAmount = true
    }
    else if (this.redeemFrom == 775 && e.target.value > this._LoyaltyredeemWalletOverview.amount) {
      this.isValidAmount = false
      this.maxTransferAmount = true
    }
    else if (this.redeemFrom == 776 && e.target.value > this._DealsredeemWalletOverview.amount) {
      this.isValidAmount = false
      this.maxTransferAmount = true
    }
    else if (this.redeemFrom == 777 && e.target.value > this._BnplredeemWalletOverview.amount) {
      this.isValidAmount = false
      this.maxTransferAmount = true
    } else {
      this.isValidAmount = true
      this.maxTransferAmount = true
    }
  }

  Form_AddUser: FormGroup;
  Form_AddTopup: FormGroup;
  _AmountTranserFromWallet: Number
  Form_AddUser_Show() {
    this._HelperService.OpenModal("Form_AddUser_Content");

  }

  Form_TopUp_Show() {
    this._HelperService.OpenModal("Form_AddTopup_Content");
  }

  Form_TopUp_Close() {
    this._HelperService.CloseModal("Form_AddTopup_Content");
  }
  Form_AddUser_Close() {
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService.CloseModal("Form_AddUser_Content");
  }
  Form_AddUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_AddUser = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.ThankUCash.CashoutInitialize,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      BankId: [null, Validators.required],
      BankKey: [null, Validators.required],
      // BankAccountNumber: [null, Validators.required],

      StatusCode: this._HelperService.AppConfig.Status.Active,
      Amount: [null, Validators.compose([Validators.required, Validators.min(1), Validators.maxLength(20)])],

    });

    this.Form_AddTopup = this._FormBuilder.group({
      control: ['', Validators.required]
    });
  }
  Form_AddUser_Clear() {
    this.Form_AddUser.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_AddUser_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }

  public topupRedeemWalletConfig = {
    minAmount: 1,
    maxAmout: 20000,
    charge: 0
  }
  Form_AddTopup_Process(_FormValue: any) {

    if (_FormValue.control == "walletL") {
      this.redeemFrom = 775
    } else if (_FormValue.control == "walletD") {
      this.redeemFrom = 776
    } else if (_FormValue.control == "walletB") {
      this.redeemFrom = 777
    }
    if (_FormValue.control.includes("wallet")) {
      this.Form_TopUp_Close()
      this._HelperService.OpenModal("Form_TopupWallet_Content");

    } else if (_FormValue.control == "bank") {
      this.Form_TopUp_Close()
      this.OpenPaymentOptions()
    }

  }
  Form_TopupWallet_Close() {
    this._HelperService.CloseModal("Form_TopupWallet_Content");
    this._AmountTranserFromWallet = null
    this.isValidAmount = true
    this.maxTransferAmount = true

  }

  Form_TopupWallet_Process() {
    
    // console.log("Furrther processing");
    this._HelperService.IsFormProcessing = true;
    var PostData = {
      Task: "redeemfromwallet",
      OperationType: "new",
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      StatusCode: "default.active",
      Amount: this._AmountTranserFromWallet,
      RedeemFrom: this.redeemFrom
    };
    this.Form_TopupWallet_Close()
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Payments, PostData);
    _OResponse.subscribe(
      _Response => {
        this.getRewardWalletOverview()
        this.CashoutList_GetData()
        this.getLoyaltyRedeemWalletBalance()
        this.getDealsRedeemWalletBalance()
        this.getBnplRedeemWalletBalance()
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {

          this._HelperService.NotifySuccess(_Response.Message);
          this._AmountTranserFromWallet = null;

        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );

  }


  Form_AddUser_Process(_FormValue: any) {

    swal({
      position: 'top',
      title: this._HelperService.AppConfig.CommonResource.RequestCashOut,
      text: this._HelperService.AppConfig.CommonResource.RequestCashoutHelp,
      // input: 'password',
      // inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
      // inputAttributes: {
      //   autocapitalize: 'off',
      //   autocorrect: 'off',
      //   maxLength: "4",
      //   minLength: "4"
      // },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {


        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        if (true) {
          _OResponse = this._HelperService.PostData(
            this._HelperService.AppConfig.NetworkLocation.V3.Payments,
            _FormValue
          );
          _OResponse.subscribe(
            _Response => {
              this._HelperService.IsFormProcessing = false;
              if (_Response.Status == this._HelperService.StatusSuccess) {
                this._HelperService.FlashSwalSuccess("New Cashout initialized successfully",
                  "You have initialized New Cashout");
                this._HelperService.ObjectCreated.next(true);
                this.Form_AddUser_Clear();
                this.Form_AddUser_Close();
                if (_FormValue.OperationType == "close") {
                  this.Form_AddUser_Close();
                }
                // this._HelperService.GetAccountCount(this._HelperService.AppConfig.ActiveOwnerKey, this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.DefaultStartTimeAll, this._HelperService.AppConfig.DefaultEndTimeToday)

              } else {
                this._HelperService.NotifyError(_Response.Message);
              }
            },
            _Error => {
              this._HelperService.IsFormProcessing = false;
              this._HelperService.HandleException(_Error);
            }
          );
          return;
        }


        else if (this._HelperService.AppConfig.TerminalPermission.MinimumLimit <= this._HelperService.AppConfig.TerminalsCount && this._HelperService.AppConfig.TerminalPermission.MaximumLimit > this._HelperService.AppConfig.TerminalsCount) {
          _OResponse = this._HelperService.PostData(
            this._HelperService.AppConfig.NetworkLocation.V3.Account,
            _FormValue
          );
          _OResponse.subscribe(
            _Response => {
              this._HelperService.IsFormProcessing = false;
              if (_Response.Status == this._HelperService.StatusSuccess) {
                this._HelperService.FlashSwalSuccess("New pos terminal has been added successfully",
                  "You have successfully created new POS terminal");
                this._HelperService.ObjectCreated.next(true);
                this.Form_AddUser_Clear();
                this.Form_AddUser_Close();
                if (_FormValue.OperationType == "close") {
                  this.Form_AddUser_Close();
                }
                this._HelperService.GetAccountCount(this._HelperService.AppConfig.ActiveOwnerKey, this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.DefaultStartTimeAll, this._HelperService.AppConfig.DefaultEndTimeToday)

              } else {
                this._HelperService.NotifyError(_Response.Message);
              }
            },
            _Error => {
              this._HelperService.IsFormProcessing = false;
              this._HelperService.HandleException(_Error);
            }
          );
        } else {
          this._HelperService.NotifyError('Upgrade Your Subscription')
          this.Form_AddUser_Close();
          this._Router.navigate(['m' + '/' + this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Upgrade]);
          return;
        }
      }
    });
  }

  GetOverviews(ListOptions: any, Task: string): any {
    this._HelperService.IsFormProcessing = true;

    ListOptions.SearchCondition = '';
    ListOptions = this._DataHelperService.List_GetSearchCondition(ListOptions);
    if (ListOptions.ActivePage == 1) {
      ListOptions.RefreshCount = true;
    }
    var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;;

    if (ListOptions.Sort.SortDefaultName) {
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
    }

    if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
      if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
        SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
      }
      else {
        SortExpression = ListOptions.Sort.SortColumn + ' desc';
      }
    }

    // var SearchCondition = '';
    // if (ListOptions.SearchCondition.includes('StatusCode')) {
    //   SearchCondition = '';
    // }else{
    //   SearchCondition = '';
    // }


    var pData = {
      Task: Task,
      TotalRecords: ListOptions.TotalRecords,
      Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
      Limit: ListOptions.PageRecordLimit,
      RefreshCount: ListOptions.RefreshCount,
      SearchCondition: ListOptions.SearchCondition,
      SortExpression: SortExpression,
      Type: ListOptions.Type,
      ReferenceKey: ListOptions.ReferenceKey,
      StartDate: ListOptions.StartDate,
      EndDate: ListOptions.EndDate,
      ReferenceId: ListOptions.ReferenceId,
      SubReferenceId: ListOptions.SubReferenceId,
      SubReferenceKey: ListOptions.SubReferenceKey,
      AccountId: ListOptions.AccountId,
      AccountKey: ListOptions.AccountKey,
      ListType: ListOptions.ListType,
      IsDownload: false,
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.OverviewData = _Response.Result.Data as any;


        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });


  }

  public _TransactionReference = null;
  public _AccountBalanceCreditAmount = 0;
  generateRandomNumber() {
    var TrRef = Math.floor(100000 + Math.random() * (999999 + 1 - 100000));
    return TrRef;
  }
  OpenPaymentOptions() {
    this._AccountBalanceCreditAmount = 0;
    var Ref = this.generateRandomNumber();
    this._TransactionReference = 'Tp' + '_' + this._HelperService.UserAccount.AccountId + '_' + Ref;
    this._HelperService.OpenModal('Form_TopUpBank_Content');
  }
  paymentDone(ref: any) {
    this.TransactionId = ref.trans
    if (ref != undefined && ref != null && ref != '') {
      if (ref.status == 'success') {
        this.CreditAmount();
      }
      else {
        this._HelperService.NotifyError('Payment failed');
      }
    }
    else {
      this._HelperService.NotifyError('Payment failed');
    }
    // this.title = 'Payment successfull';
    // console.log(this.title, ref);
  }

  public TransactionId
  public CreditAmount() {
    this._HelperService.IsFormProcessing = true;
    var PostData = {
      Task: "topupaccount",
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      Amount: this._AccountBalanceCreditAmount,
      PaymentReference: this._TransactionReference,
      TransactionId: this.TransactionId
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Subscription, PostData);
    _OResponse.subscribe(
      _Response => {
        this.getRewardWalletOverview()
        this.CashoutList_GetData()
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess("Your reward wallet has successfully been credited with N" + this._AccountBalanceCreditAmount);
          this._AccountBalanceCreditAmount = 0;
          this._TransactionReference = null;

        } else {
          this._HelperService.NotifySuccess(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }
  paymentCancel() {
    console.log('payment failed');
  }

  //Method to findout Total amount in RequestCashout
  TotalAmount: number
  CaculateTotalAmount(event: any) {
    this.TotalAmount = parseInt(event.target.value) + (this._CashOutConfiguration.Charge)
  }
  public _CashOutConfiguration: any = {}
  GetCashOutConfiguration() {

    this._HelperService.IsFormProcessing = true;
    var Data = {
      Task: 'getcashoutconfiguration',
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,


    };
    console.log(this._HelperService.AppConfig.ActiveBankCode)

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Payments, Data);
    _OResponse.subscribe(
      _Response => {



        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._CashOutConfiguration = _Response.Result;

          // Open To See Dummy Data

          // this._LoyalityOverview = {
          //     NewCustomers: 0,
          //     RepeatingCustomers: 0,
          //     VisitsByRepeatingCustomers: 0,
          //     NewCustomerInvoiceAmount: 0.0,
          //     RepeatingCustomerInvoiceAmount: 0.0,
          //     Transaction: 520.0,
          //     TransactionInvoiceAmount: 0.0,

          //     RewardTransaction: 210.0,
          //     RewardAmount: 0.0,
          //     NonRewardTransaction: 0.0,

          //     TucRewardTransaction: 510.0,
          //     TucPlusRewardTransaction: 0,
          //     TucPlusRewardClaimTransaction: 0,

          //     RedeemTransaction: 0,

          //     RewardTransactionPerc: 210.0,
          //     NonRewardTransactionPerc: 0.0,
          // }



        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
        //#endregion



      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

}
