import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { Angular4PaystackModule } from 'angular4-paystack';
import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { RewardWalletComponent } from "./reward-wallet.component";
import { Ng5SliderModule } from "ng5-slider";
import { MainPipe } from "src/app/service/main-pipe.module";
const routes: Routes = [
    {
        path: "",
        component: RewardWalletComponent,

    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RewardWalletRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        RewardWalletRoutingModule,
        CommonModule,
        Ng5SliderModule,
        MainPipe,
        Angular4PaystackModule
    ],
    declarations: [RewardWalletComponent]
})
export class RewardWalletModule { }
