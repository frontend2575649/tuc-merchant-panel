import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";
import { MainPipe } from '../../../../service/main-pipe.module'
import { ImageCropperModule } from 'ngx-image-cropper';
import { WalletComponent } from "./wallet.component";

const routes: Routes = [
    {
        path: "",
        component: WalletComponent,
        children: [
            { path: "reward", data: {accounttypecode: "merchant" }, loadChildren: "./reward-wallet/reward-wallet.module#RewardWalletModule" },
            { path: "redeem", data: { accounttypecode: "merchant" }, loadChildren: "./redeem-wallet/redeem-wallet.module#RedeemWalletModule" },


        ]
    }];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class WalletRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        WalletRoutingModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        MainPipe,
        ImageCropperModule
    ],
    declarations: [WalletComponent]
})
export class WalletModule { }
