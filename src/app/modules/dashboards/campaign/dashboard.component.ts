import { Component, OnDestroy, OnInit, ViewChildren } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { BaseChartDirective, Label } from 'ng2-charts';
import { Observable } from 'rxjs';
import { DataHelperService, HelperService, OResponse } from '../../../service/service';
declare var moment: any;
import * as Feather from 'feather-icons';
@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styles: [`
    agm-map {
      height: 300px;
    }
`]
})
export class TUDashboardComponent implements OnInit, OnDestroy {



  @ViewChildren(BaseChartDirective) components: BaseChartDirective[];

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
  ) {
    this._HelperService.ShowDateRange = true;
  }

  public _DateSubscription = null;

  StartTime = null;
  EndTime = null;

  //#region BarChartConfig 

  public BarChartOptions: any = {
    cornerRadius: 20,
    responsive: true,
    legend: {
      display: false,
      position: 'right',
    },
    ticks: {
      autoSkip: false
    },
    scales: {
      xAxes: [
        {
          gridLines: {
            stacked: true,
            display: false
          },
          ticks: {
            autoSkip: false,
            fontSize: 11
          }
        }
      ],
      yAxes: [
        {

          gridLines: {
            stacked: true,
            display: true
          },
          ticks: {
            beginAtZero: true,
            fontSize: 11
          }
        }
      ]
    },
    annotation: {
      annotations: [{
        type: 'line',
        mode: 'horizontal',
        scaleID: 'y-axis-0',
        // value: 20,
        borderColor: 'rgb(75, 192, 192)',
        borderWidth: 4,
        label: {
          enabled: false,
          content: 'Test label'
        }
      }]
    },
    plugins: {
      datalabels: {
        backgroundColor: "#ffffff47",
        color: "#798086",
        borderRadius: "2",
        borderWidth: "1",
        borderColor: "transparent",
        anchor: "end",
        align: "end",
        padding: 2,
        font: {
          size: 10,
          weight: 500
        },
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          if (label != undefined) {
            return value;
          } else {
            return value;
          }
        }
      },
    },
    emptyOverlay: {
      fillStyle: 'rgba(255,0,0,0.4)',
      fontColor: 'rgba(255,255,255,1.0)',
      fontStrokeWidth: 0,
      enabled: true
    }
  }
public barChartLabels: Label[] = [];
public barChartColors = [{ backgroundColor: ['#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC'] }, { backgroundColor: ['#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A'] }, { backgroundColor: ['#F10875', '#F10875', '#F10875', '#F10875', '#F10875', '#F10875', '#F10875'] }, { backgroundColor: ['#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA'] }];
public barChartType = 'bar';
public barChartLegend = true;
public barChartData = [
  { data: [0, 0, 0, 0, 0, 0, 0], label: 'Active' },
  { data: [0, 0, 0, 0, 0, 0, 0], label: 'Idle' },
  { data: [0, 0, 0, 0, 0, 0, 0], label: 'Dead' },
  { data: [0, 0, 0, 0, 0, 0, 0], label: 'Inactive' }
];

  //#endregion

  ngOnDestroy() {
    this._DateSubscription.unsubscribe();
  }

  ngOnInit() {
    Feather.replace();
    this._HelperService.ResetDateRange();

    this._DateSubscription = this._HelperService.RangeAltered.subscribe(value => {
      // this.GetAccountOverviewLite();
      this.GetTerminalsDetails();
      // this.GetThisMonthData();
    });

    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
      this._HelperService.AppConfig.ActiveReferenceId = params["referenceid"];
      if (this._HelperService.AppConfig.ActiveReferenceKey == null) {
        this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound]);
      } else {
        this._HelperService.FullContainer = false;

        //#region InitDates 

        this.StartTime = moment().startOf('day');
        this.EndTime = moment().startOf('day');

        // this.MonthStartTime = moment().startOf('month').startOf('day');
        // this.MonthEndTime = moment().startOf('day');

        // this._HelperService.DateRangeStart = moment(this.MonthStartTime);
        // this._HelperService.DateRangeEnd = moment(this.MonthEndTime);

        // this._HelperService.DateRangeStartO = moment(this.MonthStartTime);
        // this._HelperService.DateRangeEndO = moment(this.MonthEndTime);


        //this.barChartLabels = this._HelperService.CalculateIntermediateDate(moment(this.MonthStartTime), moment(this.MonthEndTime));

        this.barChartLabels = this._HelperService.CalculateIntermediateDate(moment(this.StartTime), moment(this.EndTime));

        //#endregion

        // this.LoadData();
      }
    });

  }

  StartTimeS = null;
  EndTimeS = null;

  LoadData() {
    this.StartTimeS = this._HelperService.GetDateS(this.StartTime);
    this.EndTimeS = this._HelperService.GetDateS(this.EndTime);
  }


  //#region AccountOverview 

  public _AccountOverview: OAccountOverview =
    {
      ActiveMerchants: 0,
      ActiveMerchantsDiff: 0,
      ActiveTerminals: 0,
      ActiveTerminalsDiff: 0,
      CardRewardPurchaseAmount: 0,
      CardRewardPurchaseAmountDiff: 0,
      CashRewardPurchaseAmount: 0,
      CashRewardPurchaseAmountDiff: 0,
      Merchants: 0,
      PurchaseAmount: 0,
      PurchaseAmountDiff: 0,
      Terminals: 0,
      Transactions: 0,
      TransactionsDiff: 0,
      UnusedTerminals: 0,
      IdleTerminals: 0,
      DeadTerminals: 0,
      TotalTransactions: 0,
      TotalSale: 0,
      AverageTransactionAmount: 0,
    }


  //#endregion

  //#region TerminalDetails 

  public _TerminalDetails:any =
    {
      ProviderIconUrl: null,
      MerchantIconUrl: null,
      ReferenceId: null,
      ReferenceKey: null,
      TypeCode: null,
      TypeName: null,
      SubTypeCode: null,
      SubTypeName: null,
      UserAccountKey: null,
      UserAccountDisplayName: null,
      Name: null,
      Description: null,
      StartDate: null,
      StartDateS: null,
      EndDate: null,
      EndDateS: null,
      SubTypeValue: null,
      MinimumInvoiceAmount: null,
      MaximumInvoiceAmount: null,
      MinimumRewardAmount: null,
      MaximumRewardAmount: null,
      ManagerKey: null,
      ManagerDisplayName: null,
      SmsText: null,
      Comment: null,
      CreateDate: null,
      CreatedByKey: null,
      CreatedByDisplayName: null,
      ModifyDate: null,
      ModifyByKey: null,
      ModifyByDisplayName: null,
      StatusId: null,
      StatusCode: null,
      StatusName: null,
      CreateDateS: null,
      ModifyDateS: null,
      StatusI: null,
      StatusB: null,
      StatusC: null,
    }

  GetTerminalsDetails() {

    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetTerminal,
      AccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveReferenceId
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.IsFormProcessing = false;
          this._TerminalDetails = _Response.Result;

          //#region InitResponse 

          this._TerminalDetails.EndDateS = this._HelperService.GetDateS(
            this._TerminalDetails.EndDate
          );
          this._TerminalDetails.CreateDateS = this._HelperService.GetDateTimeS(
            this._TerminalDetails.CreateDate
          );
          this._TerminalDetails.ModifyDateS = this._HelperService.GetDateTimeS(
            this._TerminalDetails.ModifyDate
          );
          this._TerminalDetails.StatusI = this._HelperService.GetStatusIcon(
            this._TerminalDetails.StatusCode
          );
          this._TerminalDetails.StatusB = this._HelperService.GetStatusBadge(
            this._TerminalDetails.StatusCode
          );
          this._TerminalDetails.StatusC = this._HelperService.GetStatusColor(
            this._TerminalDetails.StatusCode
          );

          //#endregion

        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion

  //#region ThisMonthData 

  public MonthStartTime = null;
  public MonthEndTime = null;

  public _ThisMonthData = {
    List: [],
    InactiveDays: 0
  };

 
  ClearMonthData() {
    this._ThisMonthData = {
      List: [],
      InactiveDays: 0
    };
  }

  //#endregion

}


export class OAccountOverview {
  public DeadTerminals?: number;
  public TotalTransactions?: number;
  public TotalSale?: number;


  public AverageTransactionAmount?: number;
  public IdleTerminals?: number;
  public UnusedTerminals?: number;
  public Merchants: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
}