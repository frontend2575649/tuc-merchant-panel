import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { InputFileComponent, InputFile } from 'ngx-input-file';
import * as Feather from "feather-icons";
import swal from "sweetalert2";
import * as XLSX from 'xlsx';
declare var $: any;

import {
  DataHelperService,
  HelperService,
  OList,
  OSelect,
  FilterHelperService,
  OResponse,
} from "../../../../service/service";
import { Observable } from 'rxjs';
import { ChangeContext } from 'ng5-slider';
import { debug } from "console";
import { CustomerImport } from "src/app/pages/useronboarding/bulkcustomer/customerupload.component";

@Component({
  selector: "tu-tucustomers",
  templateUrl: "./tucustomers.component.html",
  styles: [`
    .user-icon{
        width: 50px;
        height: 50px;
        border: 0.6px solid #979797;
        border-radius: 50%;
        display: flex;
        justify-content: center;
        align-items: center;
        margin: 20px auto;
    }
  `]
})
export class TuCustomersComponent implements OnInit, OnDestroy {
  public ResetFilterControls: boolean = true;

  ngOnDestroy(): void {
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService.Icon_Crop_Clear();

  }

  storeId = null;
  stores = [];

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = true;
    this._HelperService.showAddNewCashierBtn = false;
    this._HelperService.showAddNewSubAccBtn = false;


  }

  @ViewChild("customer")
  private InputFileComponent_Customer: InputFileComponent;

  CurrentImagesCount: number = 0;
  ngOnInit() {

    if (this._HelperService.isRewarCustomer == true) {
      this.Form_RewardCustomer_Show();
      this._HelperService.isRewarCustomer = false;
    }
    this._HelperService.isRewarCustomer = false;
    this._HelperService.StopClickPropogation();
    Feather.replace();
    this.store_load();
    this.stores = this._HelperService.getStoresList();
    this.Customers_Setup();
    this.Customers_Filter_Owners_Load();
    this.Form_AddCustomer_Load();
    this.InitColConfig();
    // this.TUTr_Filter_Stores_Load();
    this.GetLoyaltyConfiguration();
    // this._HelperService.StopClickPropogation();
    this._HelperService.Icon_Crop_Clear();
    this._HelperService.setTheme();
  }

  _Transport;
  store_load() {
    this.TUTr_Filter_Store_Option = {
      placeholder: 'Filter Store',
      ajax: this._Transport,
      multiple: false,
      allowClear: true
    };
  }

  Form_AddUser_Open() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer
        .MerchantOnboarding,
    ]);
  }

  ToogleType(type: string): void {
    this.Customers_Config.Type = type;
    this.Customers_GetData();
  }

  //#region columnConfig

  TempColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  ColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  InitColConfig() {
    var MerchantTableConfig = this._HelperService.GetStorage("BMerchantTable");
    var ColConfigExist: boolean =
      MerchantTableConfig != undefined && MerchantTableConfig != null;
    if (ColConfigExist) {
      this.ColumnConfig = MerchantTableConfig.config;
      this.TempColumnConfig = this._HelperService.CloneJson(
        MerchantTableConfig.config
      );
    }
  }

  OpenEditColModal() {
    this._HelperService.OpenModal("EditCol");
  }

  SaveEditCol() {
    this.ColumnConfig = this._HelperService.CloneJson(this.TempColumnConfig);
    this._HelperService.SaveStorage("BMerchantTable", {
      config: this.ColumnConfig,
    });
    this._HelperService.CloseModal("EditCol");
  }

  public NameFile: string = null;
  title = 'XlsRead'
  file: File;
  arrayBuffer: any;
  filelist: any;
  addfile(event) {
    this.file = event.target.files[0];
    this.NameFile = this.file.name;
    let fileReader = new FileReader();
    fileReader.readAsArrayBuffer(this.file);
    fileReader.onload = (e) => {
      this.arrayBuffer = fileReader.result;
      var data = new Uint8Array(this.arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, { type: "binary" });
      var first_sheet_name = workbook.SheetNames[0];
      var worksheet = workbook.Sheets[first_sheet_name];
      this.filelist = XLSX.utils.sheet_to_json(worksheet, { raw: true });
    }
  }

  IsUploading = false;
  UploadCount = 0;
  TotalCount = 0;
  CustomersList: CustomerImport[] = [];
  data: any = [];
  wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'array' };
  fileName: string = 'SheetJS.xlsx';
  StartMerchantUpload() {
    if (this.filelist == undefined) {
      this._HelperService.NotifyError("Please select file to upload");
    }
    else if (this.filelist.length == 0) {
      this._HelperService.NotifyError("Please select file with data to upload");
    }
    else {
      this.IsUploading = true;
      // this.filelist[index].Status = "processing";
      // this.filelist[index].Message = "sending data";
      var PostItem = {
        Task: this._HelperService.AppConfig.Api.Core.uploadcustomerrewards,
        AccountId: this._HelperService.AppConfig.ActiveOwnerId,
        AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
        FileName: this.NameFile,
        Customers: this.filelist,
      };
      this._HelperService.IsFormProcessing = true;
      PostItem.FileName = this.NameFile;
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Operations, PostItem);
      _OResponse.subscribe(
        _Response => {
          this._HelperService.IsFormProcessing = false;
          this.UploadCount = this.UploadCount + 1;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this.filelist.Status = "success";
            this.filelist.Message = "Customer added";
            this._HelperService.NotifySuccess('Import Successfully');
            this._HelperService.CloseModal('SampleSheet');
            this.ngOnInit();
          }
          else {
            this.filelist.Status = "failed";
            this.filelist.Message = "Customer already present";
          }
          if (this.filelist == (this.filelist.length - 1)) {
            this.IsUploading = false;
            this._HelperService.NotifySuccess('Customer import Successfully');
          }


        },
        _Error => {
          this.UploadCount = this.UploadCount + 1;
          if (this.filelist == (this.filelist.length - 1)) {
            this.IsUploading = false;
            this._HelperService.NotifySuccess('Customer import Successfully');
          }
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        });
    }

  }


  private InitImagePicker(InputFileComponent: InputFileComponent) {
    if (InputFileComponent != undefined) {
      this.CurrentImagesCount = 0;
      this._HelperService._InputFileComponent = InputFileComponent;
      InputFileComponent.onChange = (files: Array<InputFile>): void => {
        if (files.length >= this.CurrentImagesCount) {
          this._HelperService._SetFirstImageOrNone(InputFileComponent.files);
        }
        this.CurrentImagesCount = files.length;
      };
    }
  }

  Icon_Crop_Clear() {
    this.InputFileComponent_Customer.files.pop();
    this._HelperService.Icon_Crop_Clear()
  }
  SetSearchRanges(): void {
    //#region Invoice 
    this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('TotalInvoiceAmount', this.Customers_Config.SearchBaseConditions);
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'TotalInvoiceAmount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
    if (this.TUTr_InvoiceRangeMinAmount == this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit && this.TUTr_InvoiceRangeMaxAmount == this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit) {
      this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Customers_Config.SearchBaseConditions);
    }
    else {
      this.Customers_Config.SearchBaseConditions.push(SearchCase);
    }

    //#endregion      

    //#region reward 
    this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('TotalRewardAmount', this.Customers_Config.SearchBaseConditions);
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'TotalRewardAmount', this.TUTr_RewardRangeMinAmount, this.TUTr_RewardRangeMaxAmount);
    if (this.TUTr_RewardRangeMinAmount == this._HelperService.AppConfig.RangeRewardAmountMinimumLimit && this.TUTr_RewardRangeMaxAmount == this._HelperService.AppConfig.RangeRewardAmountMaximumLimit) {
      this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Customers_Config.SearchBaseConditions);
    }
    else {
      this.Customers_Config.SearchBaseConditions.push(SearchCase);
    }

    //#endregion

    //Redeem
    this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('RedeemAmount', this.Customers_Config.SearchBaseConditions);
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'RedeemAmount', this.TUTr_RedeemRangeMinAmount, this.TUTr_RedeemRangeMaxAmount);
    if (this.TUTr_RedeemRangeMinAmount == this._HelperService.AppConfig.RangeRedeemAmountMinimumLimit && this.TUTr_RedeemRangeMaxAmount == this._HelperService.AppConfig.RangeRedeemAmountMaximumLimit) {
      this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Customers_Config.SearchBaseConditions);
    }
    else {
      this.Customers_Config.SearchBaseConditions.push(SearchCase);
    }
    //endregion
  }

  SetSalesRanges(): void {
    this.TUTr_InvoiceRangeMinAmount = this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit;
    this.TUTr_InvoiceRangeMaxAmount = this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit;
    this.TUTr_RewardRangeMinAmount = this._HelperService.AppConfig.RangeRewardAmountMinimumLimit;
    this.TUTr_RewardRangeMaxAmount = this._HelperService.AppConfig.RangeRewardAmountMaximumLimit;
    this.TUTr_RedeemRangeMinAmount = this._HelperService.AppConfig.RangeRedeemAmountMinimumLimit;
    this.TUTr_RedeemRangeMaxAmount = this._HelperService.AppConfig.RangeRedeemAmountMaximumLimit;
  }

  //#endregion

  //#region merchantlist

  public Customers_Config: OList;
  Customers_Setup() {
    this.Customers_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetCustomers,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      Title: "All Customers",
      StatusType: "default",
      IsDownload: false,
      Type: this._HelperService.AppConfig.CustomerTypes.all,
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      SubReferenceId: null,
      SubReferenceKey: null,
      DefaultSortExpression: "CreateDate desc",
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '=='),
      TableFields: [
        {
          DisplayName: " Customer Name",
          SystemName: "DisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Mobile No",
          SystemName: "MobileNumber",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-center",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Email Address",
          SystemName: "EmailAddress",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Total Spend",
          SystemName: "TotalInvoiceAmount",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        // {
        //   DisplayName: "Total Reward Amount",
        //   SystemName: "TotalRewardAmount",
        //   DataType: this._HelperService.AppConfig.DataType.Number,
        //   Class: "",
        //   DefaultValue: '0',
        //   Show: true,
        //   Search: false,
        //   Sort: true,
        //   ResourceId: null,
        //   NavigateField: "ReferenceKey"
        // },
        // {
        //   DisplayName: "Tuc Plus Reward",
        //   SystemName: "TucPlusRewardAmount",
        //   DataType: this._HelperService.AppConfig.DataType.Number,
        //   Class: "",
        //   Show: false,
        //   Search: false,
        //   Sort: false,
        //   DefaultValue: '0',
        //   ResourceId: null,
        //   NavigateField: "ReferenceKey"
        // },
        // {
        //   DisplayName: "Redeem Amount",
        //   SystemName: "RedeemAmount",
        //   DataType: this._HelperService.AppConfig.DataType.Number,
        //   Class: "",
        //   Show: false,
        //   Search: false,
        //   Sort: true,
        //   ResourceId: null,
        //   NavigateField: "ReferenceKey"
        // },
        {
          DisplayName: "Total Transaction",
          SystemName: "TotalTransaction",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        // {
        //   DisplayName: 'Last Transaction',
        //   SystemName: "LastTransactionDate",
        //   DataType: this._HelperService.AppConfig.DataType.Date,
        //   Class: "td-date text-right",
        //   Show: true,
        //   Search: false,
        //   Sort: true,
        //   ResourceId: null,
        //   IsDateSearchField: true,
        //   NavigateField: "ReferenceKey",
        // }
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: "CreateDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: false,
          Search: false,
          Sort: false,
          ResourceId: null,
          IsDateSearchField:true,
          NavigateField: "ReferenceKey",
        },
      ]
    };
    this.Customers_Config = this._DataHelperService.List_Initialize(
      this.Customers_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Customer,
      this.Customers_Config
    );

    this.Customers_GetData();
  }

  Customers_Filter(event: any, Type: any) {




    if (event != null) {
      for (let index = 0; index < this.Customers_Config.Sort.SortOptions.length; index++) {
        const element = this.Customers_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.Customers_Config


    );

    this.Customers_Config = this._DataHelperService.List_Operations(
      this.Customers_Config,
      event,
      Type
    );
    this.Customers_GetData();



  }

  Customers_ToggleOption(event: any, Type: any) {

    if (Type == this._HelperService.AppConfig.ListToggleOption.SalesRange) {
      // if (this.TUTr_InvoiceRangeMinAmount == null || this.TUTr_InvoiceRangeMaxAmount == null || this.TUTr_RewardRangeMinAmount == null ||
      //   this.TUTr_RewardRangeMaxAmount == null || this.TUTr_RedeemRangeMinAmount == null || this.TUTr_RedeemRangeMaxAmount == null){
      //     this._HelperService.NotifyError("Please enter values in the min and max sales")
      //   }
      event.data = {
        SalesMin: this.TUTr_InvoiceRangeMinAmount,
        SalesMax: this.TUTr_InvoiceRangeMaxAmount,
        RewardMin: this.TUTr_RewardRangeMinAmount,
        RewardMax: this.TUTr_RewardRangeMaxAmount,
        RedeemMin: this.TUTr_RedeemRangeMinAmount,
        RedeemMax: this.TUTr_RedeemRangeMaxAmount
      }
    }
    if (event != null) {
      for (let index = 0; index < this.Customers_Config.Sort.SortOptions.length; index++) {
        const element = this.Customers_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }
    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.Customers_Config


    );

    this.Customers_Config = this._DataHelperService.List_Operations(
      this.Customers_Config,
      event,
      Type
    );

    if (
      (this.Customers_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.Customers_GetData();
    }

  }

  timeout = null;
  Customers_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.Customers_Config.Sort.SortOptions.length; index++) {
          const element = this.Customers_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.Customers_Config


      );

      this.Customers_Config = this._DataHelperService.List_Operations(
        this.Customers_Config,
        event,
        Type
      );

      if (
        (this.Customers_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.Customers_GetData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);

  }

  Customers_GetData() {
    this.GetOverviews(this.Customers_Config, this._HelperService.AppConfig.Api.ThankUCash.TransactionOverviews.getcustomersoverview);
    var TConfig = this._DataHelperService.List_GetData(
      this.Customers_Config
    );
    this.Customers_Config = TConfig;
  }
  Customers_RowSelected(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveCustomer,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Customer,
      }
    );

    this._HelperService.AppConfig.ActiveReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Customer
        .Dashboard, ReferenceData.ReferenceKey, ReferenceData.ReferenceId
    ]);


  }


  StoreId: any = null;
  StoreKey: any = null
  public ToggleStoreSelect: boolean = false;
  public TUTr_Filter_Store_Selected: any;
  public TUTr_Filter_Store_Option: Select2Options;
  // TUTr_Filter_Stores_Load() {
  //   var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
  //   var _Select: OSelect = {
  //     Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
  //     Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
  //     SearchCondition: "",
  //     SortCondition: [],
  //     Fields: [
  //       {
  //         SystemName: "ReferenceKey",
  //         Type: this._HelperService.AppConfig.DataType.Text,
  //         Id: true,
  //         Text: false,
  //       },
  //       {
  //         SystemName: "DisplayName",
  //         Type: this._HelperService.AppConfig.DataType.Text,
  //         Id: false,
  //         Text: true
  //       },
  //       {
  //         SystemName: "AccountTypeCode",
  //         Type: this._HelperService.AppConfig.DataType.Text,
  //         SearchCondition: "=",
  //         SearchValue: this._HelperService.AppConfig.AccountType.Store
  //       }
  //     ]
  //   };

  //   var OwnerKey = this._HelperService.UserAccount.AccountId;
  //   if (this._HelperService.UserAccount.AccountTypeCode != this._HelperService.AppConfig.AccountType.Merchant) {
  //     OwnerKey = this._HelperService.UserOwner.AccountId;
  //   }
  //   _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, OwnerKey, '=');
  //   var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
  //   this.TUTr_Filter_Store_Option = {
  //     placeholder: 'Select Store',
  //     ajax: _Transport,
  //     multiple: false,
  //     allowClear: true,
  //   };
  // }
  TUTr_Filter_Stores_Change(event: any) {


    this.Customers_Config.SubReferenceId = event.data[0].ReferenceId,
      this.Customers_Config.SubReferenceKey = event.data[0].ReferenceKey

    if (this.Customers_Config.SubReferenceKey != null) {
      this.Customers_GetData();

    }



  }

  //#endregion
  TUTr_InvoiceRangeMinAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit;
  TUTr_InvoiceRangeMaxAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit;
  TUTr_InvoiceRange_OnChange(changeContext: ChangeContext): void {
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'TotalInvoiceAmount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
    this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Customers_Config.SearchBaseConditions);
    this.TUTr_InvoiceRangeMinAmount = changeContext.value;
    this.TUTr_InvoiceRangeMaxAmount = changeContext.highValue;
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'TotalInvoiceAmount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
    if (this.TUTr_InvoiceRangeMinAmount == this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit && this.TUTr_InvoiceRangeMaxAmount == this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit) {
      this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Customers_Config.SearchBaseConditions);
    }
    else {
      this.Customers_Config.SearchBaseConditions.push(SearchCase);
    }
    this.Customers_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }

  TUTr_RedeemRangeMinAmount: number = this._HelperService.AppConfig.RangeRedeemAmountMinimumLimit;
  TUTr_RedeemRangeMaxAmount: number = this._HelperService.AppConfig.RangeRedeemAmountMaximumLimit;
  TUTr_RedeemRange_OnChange(changeContext: ChangeContext): void {
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'RedeemAmount', this.TUTr_RedeemRangeMinAmount, this.TUTr_RedeemRangeMaxAmount);
    this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Customers_Config.SearchBaseConditions);
    this.TUTr_RedeemRangeMinAmount = changeContext.value;
    this.TUTr_RedeemRangeMaxAmount = changeContext.highValue;
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'RedeemAmount', this.TUTr_RedeemRangeMinAmount, this.TUTr_RedeemRangeMaxAmount);
    if (this.TUTr_RedeemRangeMinAmount == this._HelperService.AppConfig.RangeRedeemAmountMinimumLimit && this.TUTr_RedeemRangeMaxAmount == this._HelperService.AppConfig.RangeRedeemAmountMaximumLimit) {
      this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Customers_Config.SearchBaseConditions);
    }
    else {
      this.Customers_Config.SearchBaseConditions.push(SearchCase);
    }
    this.Customers_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }


  TUTr_RewardRangeMinAmount: number = this._HelperService.AppConfig.RangeRewardAmountMinimumLimit;
  TUTr_RewardRangeMaxAmount: number = this._HelperService.AppConfig.RangeRewardAmountMaximumLimit;
  TUTr_RewardRange_OnChange(changeContext: ChangeContext
  ): void {
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'TucRewardAmount', this.TUTr_RewardRangeMinAmount, this.TUTr_RewardRangeMaxAmount);
    this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Customers_Config.SearchBaseConditions);
    this.TUTr_RewardRangeMinAmount = changeContext.value;
    this.TUTr_RewardRangeMaxAmount = changeContext.highValue;
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'TucRewardAmount', this.TUTr_RewardRangeMinAmount, this.TUTr_RewardRangeMaxAmount);
    if (this.TUTr_RewardRangeMinAmount == this._HelperService.AppConfig.RangeRewardAmountMinimumLimit && this.TUTr_RewardRangeMaxAmount == this._HelperService.AppConfig.RangeRewardAmountMaximumLimit) {
      this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Customers_Config.SearchBaseConditions);
    }
    else {
      this.Customers_Config.SearchBaseConditions.push(SearchCase);
    }
    this.Customers_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }


  //#region OwnerFilter

  public Customers_Filter_Owners_Option: Select2Options;
  public Customers_Filter_Owners_Selected = null;
  Customers_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
      ],
    };
    // _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
    //   [
    //     this._HelperService.AppConfig.AccountType.Merchant,
    //     this._HelperService.AppConfig.AccountType.Acquirer,
    //     this._HelperService.AppConfig.AccountType.PGAccount,
    //     this._HelperService.AppConfig.AccountType.PosAccount
    //   ]
    //   , '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.Customers_Filter_Owners_Option = {
      placeholder: "Sort by Referrer",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  Customers_Filter_Owners_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.Customers_Config,
      this._HelperService.AppConfig.OtherFilters.Merchant.Owner
    );

    this.OwnerEventProcessing(event);

  }

  OwnerEventProcessing(event: any): void {
    if (event.value == this.Customers_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.Customers_Filter_Owners_Selected,
        "="
      );
      this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.Customers_Config.SearchBaseConditions
      );
      this.Customers_Filter_Owners_Selected = null;
    } else if (event.value != this.Customers_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.Customers_Filter_Owners_Selected,
        "="
      );
      this.Customers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.Customers_Config.SearchBaseConditions
      );
      this.Customers_Filter_Owners_Selected = event.data[0].ReferenceKey;
      this.Customers_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "ReferenceKey",
          this._HelperService.AppConfig.DataType.Text,
          this.Customers_Filter_Owners_Selected,
          "="
        )
      );
    }

    this.Customers_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion

  SetOtherFilters(): void {
    this.Customers_Config.SearchBaseConditions = [];
    // this.Customers_Config.SearchBaseCondition = null;

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
      this.Customers_Filter_Owners_Selected = null;
      this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.Customers_Config);
    //#region setOtherFilters
    this.SetOtherFilters();
    this.SetSalesRanges();
    //#endregion
    this.Customers_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Merchant(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.Customers_Config);
    this.SetOtherFilters();
    this.SetSalesRanges();
    this.Customers_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        maxLength: "20",
        minLength: "4",
      },
      inputValidator: function (value) {
        if (value === '' || value.length < 4) {
          return 'Enter filter name length greater than 4!'
        }
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Customer
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Customer
        );
        this._FilterHelperService.SetMerchantConfig(this.Customers_Config);
        this.Customers_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    if (this.TUTr_InvoiceRangeMinAmount && this.TUTr_InvoiceRangeMaxAmount && this.TUTr_InvoiceRangeMinAmount > this.TUTr_InvoiceRangeMaxAmount) {
      this._HelperService.NotifyError("Minimum Sale Amount should be less than Maximum Sale Amount");
      return;
    }
    if (this.TUTr_InvoiceRangeMinAmount < 0 || this.TUTr_InvoiceRangeMaxAmount < 0) {
      this._HelperService.NotifyError("Sale Amount should not be negative");
      return;
    }

    if (this.TUTr_RewardRangeMinAmount && this.TUTr_RewardRangeMaxAmount && this.TUTr_RewardRangeMinAmount > this.TUTr_RewardRangeMaxAmount) {
      this._HelperService.NotifyError("Minimum Reward Amount should be less than Maximum Reward Amount");
      return;
    }
    if (this.TUTr_RewardRangeMinAmount < 0 || this.TUTr_RewardRangeMaxAmount < 0) {
      this._HelperService.NotifyError("Reward Amount should not be negative");
      return;
    }

    if (this.TUTr_RedeemRangeMinAmount && this.TUTr_RedeemRangeMaxAmount && this.TUTr_RedeemRangeMinAmount > this.TUTr_RedeemRangeMaxAmount) {
      this._HelperService.NotifyError("Minimum Redeem Amount should be less than Maximum Redeem Amount");
      return;
    }
    if (this.TUTr_RedeemRangeMinAmount < 0 || this.TUTr_RedeemRangeMaxAmount < 0) {
      this._HelperService.NotifyError("Redeem Amount should not be negative");
      return;
    }
    this.SetSearchRanges();
    this._HelperService.MakeFilterSnapPermanent();
    this.Customers_GetData();

    if (ButtonType == 'Sort') {
      $("#Customers_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#Customers_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.Customers_Config);
    this.SetOtherFilters();
    this.SetSalesRanges();
    this.Customers_GetData();
    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();
    this.Customers_Filter_Owners_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }


  //#region SalesOverview 
  public OverviewData = { Total: 0, New: 0, Repeating: 0, Lost: 0 };
  GetOverviews(ListOptions: any, Task: string): any {
    this._HelperService.IsFormProcessing = true;

    ListOptions.SearchCondition = '';
    ListOptions = this._DataHelperService.List_GetSearchCondition(ListOptions);
    if (ListOptions.ActivePage == 1) {
      ListOptions.RefreshCount = true;
    }
    var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;

    if (ListOptions.Sort.SortDefaultName) {
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
    }

    if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
      if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
        SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
      }
      else {
        SortExpression = ListOptions.Sort.SortColumn + ' desc';
      }
    }


    var pData = {
      Task: Task,
      TotalRecords: ListOptions.TotalRecords,
      Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
      Limit: ListOptions.PageRecordLimit,
      RefreshCount: ListOptions.RefreshCount,
      SearchCondition: ListOptions.SearchCondition,
      SortExpression: SortExpression,
      Type: ListOptions.Type,
      ReferenceKey: ListOptions.ReferenceKey,
      StartDate: ListOptions.StartDate,
      EndDate: ListOptions.EndDate,
      ReferenceId: ListOptions.ReferenceId,
      SubReferenceId: ListOptions.SubReferenceId,
      SubReferenceKey: ListOptions.SubReferenceKey,
      AccountId: ListOptions.AccountId,
      AccountKey: ListOptions.AccountKey,
      ListType: ListOptions.ListType,
      IsDownload: false,
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Account, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.OverviewData = _Response.Result as any;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });


  }
  //#endregion
  CreateCashierRequest(_FormValue: any): void {

    // _FormValue.Address = {
    //   Latitude: this.Form_AddStore_Latitude,
    //   Longitude: this.Form_AddStore_Longitude,
    //   Address: this.Form_AddStore_Address,
    //   CityName: this._CurrentAddress.sublocality_level_2,
    //   StateName: this._CurrentAddress.sublocality_level_1,
    //   CountryName: this._CurrentAddress.country
    // }

    // _FormValue.Latitude = undefined;
    // _FormValue.Longitude = undefined;
    // _FormValue.MapAddress = undefined;

    var IconContent: any = undefined;

    if (this._HelperService._Icon_Cropper_Data.Content != null) {
      IconContent = this._HelperService._Icon_Cropper_Data;
    }

    if (this._HelperService.AppConfig.ActiveOwnerIsTucPlusEnabled) {
      _FormValue.IsTucPlus = true
    }
    else {
      _FormValue.IsTucPlus = false

    }

    _FormValue.IconContent = IconContent;

    return _FormValue;
  }
  Form_AddCustomer: FormGroup;
  Filer_Sales: FormGroup;
  Form_AddCustomer_Show() {
    this._HelperService.Icon_Crop_Clear();
    this.InitImagePicker(this.InputFileComponent_Customer);
    this._HelperService.OpenModal("Form_AddCustomer_Content");
  }
  Form_AddCustomer_Close() {
    this._HelperService._FileSelect_Icon_Reset();
    this.InputFileComponent_Customer.files.pop();
    this._HelperService.CloseModal("Form_AddCustomer_Content");
  }
  Form_AddCustomer_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_AddCustomer = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.ThankUCash.onboardcustomer,
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      StatusCode: this._HelperService.AppConfig.Status.Active,
      FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256), Validators.pattern("^[a-zA-Z _-]*$")])],
      LastName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128), Validators.pattern("^[a-zA-Z _-]*$")])],
      MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
      EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
      GenderCode: this._HelperService.AppConfig.Gender.Male,
      countryId: this._HelperService.UserCountry.CountryId,
      CountryKey: this._HelperService.UserCountry.CountryKey
    });
  }
  // FiterSaleValidation(){
  //   this.Filer_Sales = this._FormBuilder.group({

  //   InvoiceRangeMinAmount: [null, Validators.required],
  //   InvoiceRangeMaxAmount: [null, Validators.required],
  //   RewardRangeMinAmount: [null, Validators.required],
  //   RewardRangeMaxAmount: [null, Validators.required],
  //   RedeemRangeMinAmount: [null, Validators.required],
  //   RedeemRangeMaxAmount : [null, Validators.required]

  // });
  // }

  Form_AddCustomer_Clear() {
    this.Form_AddCustomer.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_AddCustomer_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_AddCustomer_Process(_FormValue: any) {
    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
    this._HelperService.IsFormProcessing = true;

    var Request = this.CreateCashierRequest(_FormValue);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(
      this._HelperService.AppConfig.NetworkLocation.V3.Cop,
      Request
    );
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.FlashSwalSuccess("New customer has been added successfully",
            "Done! You have successfully created new customer");
          this._HelperService.ObjectCreated.next(true);
          this.Form_AddCustomer_Clear();
          this.Form_AddCustomer_Close();
          this.Customers_Setup();
          if (_FormValue.OperationType == "close") {
            this.Form_AddCustomer_Close();
          }
        } else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }

  _RewardInitialize =
    {
      MobileNumber: null,
      AccountNumber: null,
      IconUrl: null,
      IsNewCustomer: false,
      AllowCustomReward: false,
      ReferenceId: 0,
      ReferenceKey: null,
      RewardPercentage: null,
    }

  _RewardConfirm =
    {
      ReferenceId: 0,
      ReferenceKey: null,
      Name: null,
      MobileNumber: null,
      EmailAddress: null,
      DisplayName: null,
      IconUrl: null,
      StatusId: null,
      AccountBalance: null,
      AccountNumber: null,
      Comment: null,
      RewardAmount: null,
      InvoiceAmount: null,
      RewardPercentage: null,
      TransactionDate: null,
      TransactionReferenceId: null,
      StatusName: null
    }

  _RewardRequest =
    {
      InvoiceAmount: null,
      RewardAmount: null,
      ReferenceNumber: null,
    }

  Form_RewardCustomer_Bulk() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.Partner.bulkcustomerrewards
    ]);
  }

  Form_RewardCustomer_Show() {
    this._RewardConfirm =
    {
      ReferenceId: 0,
      ReferenceKey: null,
      Name: null,
      MobileNumber: null,
      EmailAddress: null,
      DisplayName: null,
      IconUrl: null,
      StatusId: null,
      AccountBalance: null,
      AccountNumber: null,
      Comment: null,
      RewardAmount: null,
      InvoiceAmount: null,
      RewardPercentage: null,
      TransactionDate: null,
      TransactionReferenceId: null,
      StatusName: null
    }
    this._RewardInitialize =
    {
      MobileNumber: null,
      AccountNumber: null,
      IconUrl: null,
      IsNewCustomer: false,
      AllowCustomReward: false,
      ReferenceId: 0,
      ReferenceKey: null,
      RewardPercentage: null,
    }
    this._RewardRequest =
    {
      InvoiceAmount: null,
      RewardAmount: null,
      ReferenceNumber: null,
    }
    this._HelperService.OpenModal("Form_RewardCustomer_Content");
  }
  Form_RewardCustomer_Close() {

    this._HelperService.CloseModal("Form_RewardCustomer_ContentEx");
    this._HelperService.CloseModal("Form_RewardCustomer_Content");
    this._RewardConfirm =
    {
      ReferenceId: 0,
      ReferenceKey: null,
      Name: null,
      MobileNumber: null,
      EmailAddress: null,
      DisplayName: null,
      IconUrl: null,
      StatusId: null,
      AccountBalance: null,
      AccountNumber: null,
      Comment: null,
      RewardAmount: null,
      InvoiceAmount: null,
      RewardPercentage: null,
      TransactionDate: null,
      TransactionReferenceId: null,
      StatusName: null
    }
    this._RewardInitialize =
    {
      MobileNumber: null,
      AccountNumber: null,
      IconUrl: null,
      IsNewCustomer: false,
      AllowCustomReward: false,
      ReferenceId: 0,
      ReferenceKey: null,
      RewardPercentage: null,
    }
    this._RewardRequest =
    {
      InvoiceAmount: null,
      RewardAmount: null,
      ReferenceNumber: null,
    }
  }

  Form_RewardCustomer_Pre(Item) {
    this._RewardInitialize.MobileNumber = Item.MobileNumber;
    this.InitializeReward(true);
  }

  InitializeReward(ShowPopup: boolean) {
    if (this._RewardInitialize.MobileNumber == undefined || this._RewardInitialize.MobileNumber == null || this._RewardInitialize.MobileNumber == "") {
      this._HelperService.NotifyError("Enter mobile number");
    }
    else {
      var _Request =
      {
        Task: "rewardinitialize",
        MobileNumber: this._RewardInitialize.MobileNumber,
        AccountId: this._HelperService.AppConfig.ActiveOwnerId,
        AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      }
      this._HelperService.IsFormProcessing = true;
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.App, _Request);
      _OResponse.subscribe(
        _Response => {
          this._HelperService.IsFormProcessing = false;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            if (ShowPopup == true) {
              this._RewardConfirm =
              {
                ReferenceId: 0,
                ReferenceKey: null,
                Name: null,
                MobileNumber: null,
                EmailAddress: null,
                DisplayName: null,
                IconUrl: null,
                StatusId: null,
                AccountBalance: null,
                AccountNumber: null,
                Comment: null,
                RewardAmount: null,
                InvoiceAmount: null,
                RewardPercentage: null,
                TransactionDate: null,
                TransactionReferenceId: null,
                StatusName: null
              }
              this._RewardInitialize =
              {
                MobileNumber: null,
                AccountNumber: null,
                IconUrl: null,
                IsNewCustomer: false,
                AllowCustomReward: false,
                ReferenceId: 0,
                ReferenceKey: null,
                RewardPercentage: null,
              }
              this._RewardRequest =
              {
                InvoiceAmount: null,
                RewardAmount: null,
                ReferenceNumber: null,
              }
              this._RewardInitialize = _Response.Result;
              this._HelperService.OpenModal("Form_RewardCustomer_ContentEx");
            }
            else {
              this._RewardInitialize = _Response.Result;

            }
          } else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        }
      );
    }
  }
  ConfirmReward() {
    if (this._RewardInitialize.MobileNumber == undefined || this._RewardInitialize.MobileNumber == null || this._RewardInitialize.MobileNumber == "") {
      this._HelperService.NotifyError("Enter mobile number");
    }
    else if (this._RewardRequest.InvoiceAmount == undefined || this._RewardRequest.InvoiceAmount == null || this._RewardRequest.InvoiceAmount == "") {
      this._HelperService.NotifyError("Enter invoice amount");
    }
    else if (isNaN(this._RewardRequest.InvoiceAmount)) {
      this._HelperService.NotifyError("Enter valid invoice amount");
    }
    else if (this._RewardRequest.RewardAmount == undefined || this._RewardRequest.RewardAmount == null || this._RewardRequest.RewardAmount == "") {
      this._HelperService.NotifyError("Enter reward amount");
    }
    else if (this._RewardInitialize.AllowCustomReward == true && (this._RewardRequest.InvoiceAmount == undefined || this._RewardRequest.InvoiceAmount == null || this._RewardRequest.InvoiceAmount == "")) {
      this._HelperService.NotifyError("Enter reward amount");
    }
    else if (this._RewardInitialize.AllowCustomReward == true && isNaN(this._RewardRequest.RewardAmount)) {
      this._HelperService.NotifyError("Enter valid reward amount");
    }
    else {
      var _Request =
      {
        Task: "rewardconfirm",
        MobileNumber: this._RewardInitialize.MobileNumber,
        AccountId: this._HelperService.AppConfig.ActiveOwnerId,
        AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
        ReferenceId: this._RewardInitialize.ReferenceId,
        ReferenceKey: this._RewardInitialize.ReferenceKey,
        InvoiceAmount: this._RewardRequest.InvoiceAmount,
        RewardAmount: this._RewardRequest.RewardAmount,
        ReferenceNumber: this._RewardRequest.ReferenceNumber
      }
      this._HelperService.IsFormProcessing = true;
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.App, _Request);
      _OResponse.subscribe(
        _Response => {
          this._HelperService.IsFormProcessing = false;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._RewardConfirm = _Response.Result;
            this._RewardConfirm.TransactionDate = this._HelperService.GetDateTimeS(this._RewardConfirm.TransactionDate);
            this.Customers_GetData()
          } else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        }
      );
    }
  }






  public _LoyaltyCustomer =
    {
      IsTucPlus: false,
      IsTucGold: false,
      Reward:
      {

        RewardPercentage: 0,


      }
    };
  GetLoyaltyConfiguration() {

    var pData = {
      Task: 'getloyaltyconfiguration',
      StartDate: null,
      EndDate: null,
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      ConfigurationKey: this._HelperService.AppConfig.ConfigurationKey
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Operations, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._LoyaltyCustomer = _Response.Result;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });

  }

  Amount: number
  CalculateRewardAmount(event: any) {
    if (this._HelperService.AppConfig.ActiveOwnerId == 583505) {
    }
    else {
      this.Amount = parseInt(event.target.value)
      this._RewardRequest.RewardAmount = this._HelperService.GetAmountFromPercentage(this.Amount, this._RewardInitialize.RewardPercentage)
    }
  }


}
