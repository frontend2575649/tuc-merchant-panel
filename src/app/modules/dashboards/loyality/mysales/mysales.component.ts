import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChildren } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { DataHelperService, HelperService, OResponse, OSelect, OSalesTrendData, OSalesTrend, OLoyalityHistory, OLoyalityHistoryData } from '../../../../service/service';
declare var moment: any;
import * as Feather from "feather-icons";
import { ChartDataSets } from 'chart.js';
import * as cloneDeep from 'lodash/cloneDeep';
import { Color } from 'ng2-charts';
import { I } from '@angular/cdk/keycodes';

@Component({
  selector: 'app-mysales',
  templateUrl: './mysales.component.html'
})
export class MysalesComponent implements OnInit {
  stores = [];
  storeId = null;
  public DailyChartData: ChartDataSets[] = [
    { data: [], borderDash: [10, 5], label: 'Sales amount', hidden: false }
  ];
  chartLabels = ['00:00', '00:01', '00:02', '00:03', '00:04', '00:05', '00:06', '00:07', '00:08', '00:09', '00:10', '00:11', '00:12', '00:13', '00:14', '00:15', '00:16', '00:17',
    '00:18', '00:19', '00:20', '00:21', '00:22', '00:23'
  ]

  public lineChartColors: Color[] = [

    {
      borderColor: '#FFC20A',
      backgroundColor: 'rgba(255, 255, 255, 0)',
    }
  ]

  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef

  ) {
    this._HelperService.ShowDateRange = true;
  }
  ngOnInit() {
    window.addEventListener('scroll', this.scroll, true);
    Feather.replace();
    this._HelperService.setTheme();
    // this.TUTr_Filter_Stores_Load();
    this.InitializeDates();
    this.store_load();
    this.stores = this._HelperService.getStoresList();
    // this.ChartLabels = this._HelperService.CalculateIntermediateDate(moment(this._Daily.ActualStartDate), moment(this._Daily.ActualEndDate).endOf('day'));
    this.GetSalesOverview();
    this._dailyReportData();
  }

  ngOnDestroy() {
    window.removeEventListener('scroll', this.scroll, true);
  }

  _Transport;
  store_load() {
    this.TUTr_Filter_Store_Option = {
      placeholder: 'Filter Store',
      ajax: this._Transport,
      multiple: false,
      allowClear: true
    };
  }

  scroll = (event): void => {
    $(".daterangepicker").hide();
    // $(".cancelBtn").trigger("click")
    $(".form-daterangepicker").blur();
  };

  showDatePicker() {
    $(".cancelBtn").trigger("click")
    $(".daterangepicker").show();

  }

  InitializeDates(): void {
    this._Daily.ActualStartDate = moment().startOf('day');
    this._Daily.ActualEndDate = moment().endOf('day');
  }

  public _Daily: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,

    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,

    ActualSalesAmount: 0,
    CompareSalesAmount: 0,
    SalesAmountDifference: 0
  }


  CurrentDate: any;
  isCustomDate:boolean = false
  label = 'Today'
  //#region DateChangeHandler
  DateChanged(event: any, Type: any): void {
    
    this.CurrentDate = cloneDeep(event);
    this.label = event.label
    this.isCustomDate = this.CurrentDate.label == 'Custom Range'
    

    //#region Monthly 
    this._Daily.ActualStartDate = moment(this.CurrentDate.start).startOf('day');
    this._Daily.ActualEndDate = moment(this.CurrentDate.end).endOf('day');
    this._Daily.CompareStartDate = moment(this.CurrentDate.start).startOf('day');
    this._Daily.CompareEndDate = moment(this.CurrentDate.end).endOf('day');
    //#endregion
    this.GetSalesOverview();
  }

  public ToggleStoreSelect: boolean = false;
  public TUTr_Filter_Store_Selected: any;
  public TUTr_Filter_Store_Option: Select2Options;
  // TUTr_Filter_Stores_Load() {
  //   var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
  //   var _Select: OSelect = {
  //     Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
  //     Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
  //     SearchCondition: "",
  //     SortCondition: [],
  //     Fields: [
  //       {
  //         SystemName: "ReferenceKey",
  //         Type: this._HelperService.AppConfig.DataType.Text,
  //         Id: true,
  //         Text: false,
  //       },
  //       {
  //         SystemName: "DisplayName",
  //         Type: this._HelperService.AppConfig.DataType.Text,
  //         Id: false,
  //         Text: true
  //       },
  //       {
  //         SystemName: "AccountTypeCode",
  //         Type: this._HelperService.AppConfig.DataType.Text,
  //         SearchCondition: "=",
  //         SearchValue: this._HelperService.AppConfig.AccountType.Store
  //       }
  //     ]
  //   };

  //   var OwnerKey = this._HelperService.UserAccount.AccountId;
  //   if (this._HelperService.UserAccount.AccountTypeCode != this._HelperService.AppConfig.AccountType.Merchant) {
  //     OwnerKey = this._HelperService.UserOwner.AccountId;
  //   }
  //   _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, OwnerKey, '=');
  //   var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
  //   this.TUTr_Filter_Store_Option = {
  //     placeholder: 'Select Store',
  //     ajax: _Transport,
  //     multiple: false,
  //     allowClear: true,
  //   };
  // }

  private pData = {
    Task: 'getsaleshistory',
    StartDate: null,
    EndDate: null,
    AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
    AccountId: this._HelperService.AppConfig.ActiveOwnerId,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
    Type: null
  };

  private pLoyalityData = {
    Task: 'getloyaltyvisithistory',
    StartDate: null,
    EndDate: null,
    AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
    AccountId: this._HelperService.AppConfig.ActiveOwnerId,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
    Type: null
  };


  TUTr_Filter_Stores_Change(event: any) {
    if (event.value == this.TUTr_Filter_Store_Selected) {
      this.pData.StoreReferenceId = 0;
      this.pData.StoreReferenceKey = null;

      this.pLoyalityData.StoreReferenceId = 0;
      this.pLoyalityData.StoreReferenceKey = null;

      this.TUTr_Filter_Store_Selected = 0;
    }
    else if (event.value != this.TUTr_Filter_Store_Selected) {
      this.pData.StoreReferenceId = event.data[0].ReferenceId;
      this.pData.StoreReferenceKey = event.data[0].ReferenceKey;

      this.pLoyalityData.StoreReferenceId = event.data[0].ReferenceId;
      this.pLoyalityData.StoreReferenceKey = event.data[0].ReferenceKey;


      this.TUTr_Filter_Store_Selected = event.value;
    }
    this.GetSalesOverview();
    this._dailyReportData();
    setTimeout(() => {
      this._HelperService.ToggleField = false;
    }, 500);
  }

  public _dailyReportData() {
    this.DailyChartData[0].data = [];

    this._Daily.ActualData = this.GetSalesReport(this._Daily.ActualStartDate, this._Daily.ActualEndDate, this._Daily.ActualData, 'hour', 'actual');
  }

  GetSalesReport(StartDateTime, EndDateTime, Data: OSalesTrendData[], Type, LineType: string) {

    this._HelperService.IsFormProcessing = true;

    this.pData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pData.EndDate = this._HelperService.DateInUTC(EndDateTime);
    this.pData.Type = Type;

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, this.pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          Data = _Response.Result as OSalesTrendData[];

          var TempArray = [];
          var SalesAmount = 0;
          var NewCustomer = 0;
          var RepeatingCustomer = 0;



          for (let index = 0; index < Data.length; index++) {
            const element: OSalesTrendData = Data[index];
            TempArray.push(element.TotalInvoiceAmount);
            SalesAmount = SalesAmount + element.TotalInvoiceAmount;
            NewCustomer = NewCustomer + element.NewCustomer;
            RepeatingCustomer = RepeatingCustomer + element.RepeatingCustomer;


          }

          if (Type == 'hour') {
            let invoiceAmount = [];
            for (let index = 0; index < Data.length; index++) {
              const element = Data[index];

              if (element) {
                invoiceAmount.push(element.TotalInvoiceAmount);
              }
              else {
                invoiceAmount.push(0.0);
              }
            }
            this.DailyChartData[0].data = invoiceAmount;

          }

          return Data;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
          return Data;
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        return Data;
      });
  }

  _SalesSummary: any;

  GetSalesOverview() {
    let pData = {
      Task: 'getsalesoverview',
      StartDate: this._HelperService.DateInUTC(this._Daily.ActualStartDate),
      EndDate: this._HelperService.DateInUTC(this._Daily.ActualEndDate),
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      StoreReferenceId: this.pData.StoreReferenceId,
      StoreReferenceKey: this.pData.StoreReferenceKey,
      AmountDistribution: true
    };

    this._HelperService.IsFormProcessing = true;

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {

          this._SalesSummary = _Response.Result as any;
          return;

        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }


}
