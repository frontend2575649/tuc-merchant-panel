import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MysalesComponent } from './mysales.component';
import { RouterModule, Routes } from '@angular/router';
import { Daterangepicker } from 'ng2-daterangepicker';
import { Select2Module } from 'ng2-select2';
import { TranslateModule } from '@ngx-translate/core';
import { ChartsModule } from 'ng2-charts';

const routes: Routes = [
  { path: '', component: MysalesComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MysalesRoutingModule { }


@NgModule({
  declarations: [MysalesComponent],
  imports: [
    CommonModule,
    Daterangepicker,
    Select2Module,
    TranslateModule,
    ChartsModule,
    MysalesRoutingModule
  ]
})
export class MysalesModule { }
