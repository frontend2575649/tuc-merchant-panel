import { ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewChildren, ViewRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { BaseChartDirective, Label, Color } from 'ng2-charts';
import { Observable, Subscription } from 'rxjs';
import { DataHelperService, HelperService, OResponse, OSelect, OSalesTrend, OSalesTrendData, OLoyalityHistoryData, OLoyalityHistory } from '../../../../service/service';
declare var moment: any;
import * as Feather from "feather-icons";
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import * as cloneDeep from 'lodash/cloneDeep';
import { InputFile, InputFileComponent } from 'ngx-input-file';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
declare const window: any;

const HelpImageType: any = {
  Receipt: 'receipt',
  Terminal: 'terminal',
  Serial: 'serial'
}


@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styles: ['']
})
export class TUDashboardComponent implements OnInit, OnDestroy {
  // showDailyChart = true;
  // public LoadingChart: boolean = true;
  // lastweektext = "LAST WEEK";
  // currentweektext="CURRENT WEEK"
  // lastweekCustom = "LAST WEEK";


  ngOnDestroy(): void {
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService.Icon_Crop_Clear();
  }
  Types: any = {
    year: 'year',
    month: 'month',
    week: 'week',
    day: 'day',
    hour: 'hour'
  }

  public showGraph: boolean = true;

  public lineChartData: ChartDataSets[] = [
    { data: [87, 10, 101, 110], type: 'line', label: 'Series A', hidden: false },
    { data: [122, 10, 101, 200], type: 'line', label: 'Series A' },
    { data: [87, 10, 101, 110], type: 'bar', label: 'Series A', hidden: false },
    { data: [87, 10, 101, 110], type: 'line', label: 'Series A', hidden: false },
  ];
  public lineSalesChartColors: Color[] = [
    {
      borderColor: '#10b759',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#EE9800',
      backgroundColor: 'rgba(255, 255, 255, 0)',
    }



  ]

  public lineVisitsChartColors: Color[] = [

    {
      backgroundColor: 'rgba(0, 204, 204, 1)',
    },
    {
      backgroundColor: 'rgba(241, 8, 117, 1)',
    }

  ]

  // AgePiedata = []
  // GenderSegment = []
  // GenderLabel = ['Male', 'Female'];
  // AgeLabel;
  // public GenderRange: any = []
  // public GenderTotal: number = 0;
  // public AgeTotal: number = 0;
  // public SpenderTotal: number = 0;
  // public VisitorTotal: number = 0;

  Form_AddStorevalue;

  @ViewChildren(BaseChartDirective) components: BaseChartDirective[];
  @ViewChild("terminal")
  private InputFileComponent_Term: InputFileComponent;

  @ViewChild("cashier")
  private InputFileComponent_Cashier: InputFileComponent;
  public ShowCategorySelector: boolean = true;

  CurrentImagesCount: number = 0;
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef

  ) {
    this._HelperService.ShowDateRange = true;
  }
  // ngOnDestroy(): void {
  //     this._DateSubscription.unsubscribe();
  // }

  //#region DougnutConfigs 

  //#endregion

  public _DateSubscription: Subscription = null;

  ngOnInit() {
    Feather.replace();
    // this.TodayStartTime = moment().startOf('day');
    // this.TodayEndTime = moment().endOf('day');
    // this.initializeDatePicker('weeklydate', this._HelperService.AppConfig.DatePickerTypes.week, 'WeekSalesTrend_dropdown');
    this.TUTr_Filter_Stores_Load();
    this.InitializeDates();
    this.LoadData();
    this.Form_AddUser_Load();
    this.Form_AddCashier_Load();
    this.Form_RewardUser_Load();
    this.GetBusinessCategories();
    this.Form_AddStore_Load();
    this.TUTr_Filter_Providers_Load();
    this.TUTr_Filter_Banks_Load();
    this.TUTr_Filter_Cashiers_Load();
    this._HelperService.GetAccountCount(this._HelperService.AppConfig.ActiveOwnerKey, this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.DefaultStartTimeAll, this._HelperService.AppConfig.DefaultEndTimeToday);


  }

  public _InterSwitchAmount = 0;
  public PaymentId = 0;
  public nameValue2: any = 0;
  public RandomNumber: number = null;

  interswitch() {
    this.RandomNumber = this._HelperService.Get6DigitRandomNumber();
    this._InterSwitchAmount = null;
    this.PaymentId = this._HelperService.GetRandomNumber();
    this._HelperService.CloseModal('exampleModal');
    this._HelperService.OpenModal('Form_AddUser_InterSwitch');
  }
  timeout = null;
  startTop() {
    // this._InterSwitchAmount = 0;
    console.log("test", this.RandomNumber)
    var submitForm = document.getElementById("submit-form");
    submitForm.addEventListener("submit", (event) => {
      event.preventDefault();
      console.log("test2", this.RandomNumber)
      var merchantCode = this._HelperService.AppConfig.MerchantCode;
      var itemId = this._HelperService.AppConfig.MerchantPayMentId;
      var transRef = String(this.RandomNumber);
      var customerName = this._HelperService.AppConfig.ActiveOwnerDisplayName;
      var nameValue: any = document.getElementById("param-amount");
      this.nameValue2 = Number(nameValue.value) * 100;
      var customerId = this._HelperService.AppConfig.ActiveOwnerId;
      var mode = this._HelperService.AppConfig.MerchantPayMode;
      var redirectUrl = location.href;
      var paymentRequest = {
        merchant_code: merchantCode,
        pay_item_id: itemId,
        txn_ref: transRef,
        amount: this.nameValue2.toString(),
        currency: 566,
        cust_id: customerId,
        cust_name: customerName,
        site_redirect_url: redirectUrl,
        onComplete: (resp) => {
          // console.log(resp.resp);
          if (resp.resp != undefined && resp.resp != null && resp.resp != '') {
            if (resp.resp == "00") {
              this.CreditInterSwitchAmount();
            }
            else {
              this._HelperService.NotifyError('Payment failed');
              // this._HelperService.CloseModal('Form_AddUser_InterSwitch');  
              location.reload();

            }
          }
          else {
            this._HelperService.NotifyError('Payment failed');
            // this._HelperService.CloseModal('Form_AddUser_InterSwitch');
            location.reload();

          }

        },
        mode: mode
      };
      if (customerName != "") {
        paymentRequest.cust_name = customerName;
      }
      if (customerId != "") {
        paymentRequest.cust_id = customerId;
      }
      //  console.log("paymentRequest",paymentRequest)
      window.webpayCheckout(paymentRequest);
    });

  }
  public CreditInterSwitchAmount() {
    this._HelperService.IsFormProcessing = true;
    var PostData = {
      Task: "topupaccount",
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      Amount: null,
      merchant_code: this._HelperService.AppConfig.MerchantCode,
      pay_item_id: this._HelperService.AppConfig.MerchantPayMentId,
      txn_ref: null,
      cust_name: this._HelperService.AppConfig.ActiveOwnerDisplayName,
      cust_id: this._HelperService.AppConfig.ActiveOwnerId,
      PaymentReference: "quickteller",
      PaymentSource: "quickteller",
      TransactionId: this.PaymentId
    };
    let _OResponse: Observable<OResponse>;
    var nameValue: any = document.getElementById("param-amount");
    PostData.Amount = nameValue.value;
    PostData.txn_ref = this._HelperService.GetRandomNumber();
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Subscription, PostData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._AccountBalanceCreditAmount = 0;
          this._TransactionReference = null;
          this._HelperService.NotifySuccess("Account credited");
          this._HelperService.CloseModal('Form_AddUser_InterSwitch');
          //  this._HelperService.OpenModal('exampleModal');
          this.GetBalance();


        } else {
          this._HelperService.NotifySuccess(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }





  private InitImagePicker(InputFileComponent: InputFileComponent) {
    if (InputFileComponent != undefined) {
      this.CurrentImagesCount = 0;
      this._HelperService._InputFileComponent = InputFileComponent;
      InputFileComponent.onChange = (files: Array<InputFile>): void => {
        if (files.length >= this.CurrentImagesCount) {
          this._HelperService._SetFirstImageOrNone(InputFileComponent.files);
        }
        this.CurrentImagesCount = files.length;
      };
    }
  }
  LoadData() {
    this.GetLoyaltyOverviewLite();
    this._MonthlySalesReportGetActualData();
    this.GetLoyalityReport(moment().startOf('day'), moment().endOf('day'),
      this.LoyalityData);
  }

  RefreshData() {
    this.GetLoyaltyOverviewLite();
    this._MonthlySalesReportGetActualData();
    this.GetLoyalityReport(moment(this.CurrentDate.start).startOf('day'), moment(this.CurrentDate.end).endOf('day'), this.LoyalityData);
  }

  InitializeDates(): void {
    //#region Monthly Dates 

    this._Monthly.ActualStartDate = moment().startOf('day');
    this._Monthly.ActualEndDate = moment().endOf('day');

    // this._Monthly.CompareStartDate = moment().subtract(1, 'months').startOf('day');
    // this._Monthly.CompareEndDate = moment().subtract(1, 'months').endOf('day');

    // this.GetGenderWiseOverview();
    // this.GetGenderGroupCategories();

    //#endregion

  }

  CurrentDate: any;
  //#region DateChangeHandler
  DateChanged(event: any, Type: any): void {

    this.CurrentDate = cloneDeep(event);

    // this.TodayStartTime = moment(event.start).startOf('day');
    // this.TodayEndTime = moment(event.end).endOf('day');

    //#region Monthly 
    this._Monthly.ActualStartDate = moment(this.CurrentDate.start).startOf('day');
    this._Monthly.ActualEndDate = moment(this.CurrentDate.end).endOf('day');
    // this._Monthly.CompareStartDate = moment(ev.start).subtract(1, 'months').startOf('month').startOf('day');
    // this._Monthly.CompareEndDate = moment(ev.end).subtract(1, 'months').endOf('month').endOf('day');
    //#endregion
    this.GetLoyaltyOverviewLite();
    this._MonthlySalesReportGetActualData();
    // this.GetGenderWiseOverview();
    // this.GetGenderGroupCategories();
    this.GetLoyalityReport(moment(this.CurrentDate.start).startOf('day'), moment(this.CurrentDate.end).endOf('day'), this.LoyalityData);

  }
  //#endregion
  public BarChartOptions: any = {
    cornerRadius: 20,
    responsive: true,
    legend: {
      display: false,
      position: 'right',
    },
    ticks: {
      autoSkip: false
    },
    scales: {
      xAxes: [
        {
          gridLines: {
            stacked: true,
            display: false
          },
          ticks: {
            autoSkip: false,
            fontSize: 11
          }
        }
      ],
      yAxes: [
        {

          gridLines: {
            stacked: true,
            display: true
          },
          ticks: {
            beginAtZero: true,
            fontSize: 11
          }
        }
      ]
    },
    annotation: {
      annotations: [{
        type: 'line',
        mode: 'horizontal',
        scaleID: 'y-axis-0',
        // value: 20,
        borderColor: 'rgb(75, 192, 192)',
        borderWidth: 4,
        label: {
          enabled: false,
          content: 'Test label'
        }
      }]
    },
    plugins: {
      datalabels: {
        backgroundColor: "#ffffff47",
        color: "#798086",
        borderRadius: "2",
        borderWidth: "1",
        borderColor: "transparent",
        anchor: "end",
        align: "end",
        padding: 2,
        font: {
          size: 10,
          weight: 500
        },
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          if (label != undefined) {
            return value;
          } else {
            return value;
          }
        }
      },
    },
    emptyOverlay: {
      fillStyle: 'rgba(255,0,0,0.4)',
      fontColor: 'rgba(255,255,255,1.0)',
      fontStrokeWidth: 0,
      enabled: true
    }
  }
  public barChartLabels: Label[] = [];
  public barChartColors = [{ backgroundColor: ['#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC', '#00CCCC'] }, { backgroundColor: ['#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A', '#FFC20A'] }, { backgroundColor: ['#F10875', '#F10875', '#F10875', '#F10875', '#F10875', '#F10875', '#F10875'] }, { backgroundColor: ['#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA', '#0168FA'] }];
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartData = [
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Active', hidden: false },
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Idle', hidden: false },
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Dead', hidden: false },
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Inactive', hidden: false }
  ];
  TodayStartTime = null;
  TodayEndTime = null;
  StoreId: number = 0;
  StoreKey: string = null;

  GetLoyaltyOverviewLite() {
    this._HelperService.IsFormProcessing = true;
    var Data = {
      Task: 'getloyaltyoverview',
      StartDate: this._HelperService.DateInUTC(this._Monthly.ActualStartDate),
      EndDate: this._HelperService.DateInUTC(this._Monthly.ActualEndDate),
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      StoreReferenceId: this.StoreId,
      StoreReferenceKey: this.StoreKey,
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, Data);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._AccountOverview = _Response.Result as OAccountOverview;
          this.components.forEach(a => {
            try {
              if (a.chart) a.chart.update();
            } catch (error) {
              console.log('chartjs error');
            }
          });

        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  public MonthlybarChartLabels = [];

  public MonthlylineChartData: ChartDataSets[] = [
    { data: [22, 44, 44, 77], label: 'Series A' },
    { data: [33, 55, 77, 44], borderDash: [10, 5], label: 'Series B' },
    { data: [33, 55, 77, 99], type: 'bar', label: 'Series C' },
    { data: [33, 55, 77, 88], type: 'bar', label: 'Series D' }

  ];

  public MonthlylineChartColors: Color[] = [
    {
      borderColor: '#0168fa',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#FFC20A',
      backgroundColor: 'rgba(255, 194, 10, 00)',
    },
    {
      backgroundColor: 'red',
    },
    {
      backgroundColor: 'rgba(255, 194, 10, 00)',
    }
  ]

  //#region Monthly Sales Report 

  showMonthlyChart = true;

  public _Monthly: OSalesTrend = {
    ActualStartDate: moment(),
    ActualEndDate: moment(),
    ActualData: null,

    CompareStartDate: moment(),
    CompareEndDate: moment(),
    CompareData: null,

    ActualSalesAmount: 0,
    CompareSalesAmount: 0,
    SalesAmountDifference: 0
  }

  MonthlyDateChanged(event: any, Type: any): void {
    var ev: any = cloneDeep(event);
    this._Monthly.CompareStartDate = moment(ev.start).startOf("day");
    this._Monthly.CompareEndDate = moment(ev.end).endOf("day");

    //this.MonthlybarChartLabels = this._HelperService.CalculateIntermediateDate(moment(this._Monthly.ActualStartDate), moment(this._Monthly.ActualEndDate).endOf('month').endOf('day'));

    this._MonthlySalesReportGetActualData();
  }

  public _MonthlySalesReportReset(): void {

  }

  public _MonthlySalesReportGetActualData(): void {

    this.MonthlylineChartData[0].data = [];
    this.MonthlylineChartData[1].data = [];
    this.MonthlylineChartData[2].data = [];
    this.MonthlylineChartData[3].data = [];


    this._Monthly.ActualData = this.GetSalesReport(this._Monthly.ActualStartDate, this._Monthly.ActualEndDate, this._Monthly.ActualData, this.Types.month, 'actual');
    // this._Monthly.CompareData = this.GetSalesReport(this._Monthly.CompareStartDate, this._Monthly.CompareEndDate, this._Monthly.CompareData, this.Types.month, 'compare');

  }
  //#endregion
  private pData = {
    Task: 'getsaleshistory',
    StartDate: null,
    EndDate: null,
    AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
    AccountId: this._HelperService.AppConfig.ActiveOwnerId,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
    Type: null
  };

  GetSalesReport(StartDateTime, EndDateTime, Data: OSalesTrendData[], Type, LineType: string) {

    this._HelperService.IsFormProcessing = true;

    this.pData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pData.EndDate = this._HelperService.DateInUTC(EndDateTime);
    this.pData.Type = Type;

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, this.pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          Data = _Response.Result as OSalesTrendData[];

          var TempArray = [];
          var SalesAmount = 0;

          for (let index = 0; index < Data.length; index++) {
            const element: OSalesTrendData = Data[index];
            TempArray.push(element.TotalInvoiceAmount);

            SalesAmount = SalesAmount + element.TotalInvoiceAmount;
          }

          if (Type == this._HelperService.AppConfig.GraphTypes.month) {
            this.showMonthlyChart = false;
            if (LineType == 'actual') {
              this.MonthlylineChartData[0].data = TempArray;
              this._Monthly.ActualSalesAmount = SalesAmount;
            } else if (LineType == 'compare') {
              this.MonthlylineChartData[1].data = TempArray;
              this._Monthly.CompareSalesAmount = SalesAmount;
            }

            this.showMonthlyChart = true;
            if (!(this._ChangeDetectorRef as ViewRef).destroyed)
              this._ChangeDetectorRef.detectChanges();
          }

          return Data;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
          return Data;
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        return Data;
      });
  }


  public _AccountOverview: OAccountOverview =
    {

      RewardAmount: 0,
      RedeemAmount: 0,
      NewCustomers: 0,
      RepeatingCustomers: 0,
      TransactionInvoiceAmount: 0,
      VisitsByRepeatingCustomers: 0,
      Transaction: 0,
      RedeemTransaction: 0,
      RedeemInvoiceAmount: 0,
      TucPlusRewardAmountCommission: 0,
      TotalTucPlusRewardClaimTransactionAmount: 0,
      TucPlusRewardAmountUser: 0,
      TotalTucPlusRewardTransactionAmount: 0,
      TucPlusRewardClaimAmount: 0,
      TucRewardInvoiceAmount: 0,
      RepeatingCustomerInvoiceAmount: 0,
      NewCustomerInvoiceAmount: 0,

      RewardCommissionAmount: 0,
      RewardInvoiceAmount: 0,
      RewardTransaction: 0,
      TucPlusRewardAmount: 0,
      TucPlusRewardClaimInvoiceAmount: 0,
      TucPlusRewardClaimTransaction: 0,
      TucPlusRewardCommissionAmount: 0,
      TucPlusRewardInvoiceAmount: 0,
      TucPlusRewardTransaction: 0,
      TucRewardTransaction: 0,

    }
  Toogle_Dataset_Visibility(dataset_label: string): void {
    for (let index = 0; index < this.SalesChartData.length; index++) {
      const element = this.SalesChartData[index];

      if (element.label == dataset_label) {
        //#region Reload_UI 
        this.showSalesLoalityChart = false;
        element.hidden = !element.hidden;
        this.showSalesLoalityChart = true;
        //#endregion
        break;
      }
    }

    for (let index = 0; index < this.VisitsChartData.length; index++) {
      const element = this.VisitsChartData[index];
      if (element.label == dataset_label) {
        //#region Reload_UI 
        this.showVisitsLoalityChart = false;
        element.hidden = !element.hidden;
        this.showVisitsLoalityChart = true;
        //#endregion
        break;
      }
    }

  }

  public ToggleStoreSelect: boolean = false;
  public TUTr_Filter_Store_Selected: any;
  public TUTr_Filter_Store_Option: Select2Options;
  TUTr_Filter_Stores_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.Store
        }
      ]
    };

    var OwnerKey = this._HelperService.UserAccount.AccountId;
    if (this._HelperService.UserAccount.AccountTypeCode != this._HelperService.AppConfig.AccountType.Merchant) {
      OwnerKey = this._HelperService.UserOwner.AccountId;
    }
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, OwnerKey, '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Store_Option = {
      placeholder: 'Select Store',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TUTr_Filter_Stores_Change(event: any) {
    this.StoreId = event.data[0].ReferenceId,
      this.StoreKey = event.data[0].ReferenceKey,

      this.Form_AddCashier.patchValue(
        {
          StoreId: event.data[0].ReferenceId,
          StoreKey: event.data[0].ReferenceKey,
        }


      );
    this.Form_AddUser.patchValue(
      {
        StoreReferenceId: event.data[0].ReferenceId,
        StoreReferenceKey: event.data[0].ReferenceKey,
      }


    );

    if (event.value == this.TUTr_Filter_Store_Selected) {
      this.pData.StoreReferenceId = 0;
      this.pData.StoreReferenceKey = null;

      this.pLoyalityData.StoreReferenceId = 0;
      this.pLoyalityData.StoreReferenceKey = null;
      this.TUTr_Filter_Store_Selected = 0;
    }
    else if (event.value != this.TUTr_Filter_Store_Selected) {
      this.pData.StoreReferenceId = event.data[0].ReferenceId;
      this.pData.StoreReferenceKey = event.data[0].ReferenceKey;

      this.pLoyalityData.StoreReferenceId = event.data[0].ReferenceId;
      this.pLoyalityData.StoreReferenceKey = event.data[0].ReferenceKey;


      this.TUTr_Filter_Store_Selected = event.value;
    }
    this.LoadData();
    setTimeout(() => {
      this._HelperService.ToggleField = false;
      this.GetLoyaltyOverviewLite();
    }, 500);
  }

  DateTypeSelected(event): void {
    console.log(event);
  }

  //#region loyality History 

  //#region Sales Chart Data 
  public SalesChartData: ChartDataSets[] = [

    { data: [], label: 'Returning Sales', hidden: false },
    { data: [], borderDash: [10, 5], label: 'New Sales', hidden: false }

  ];
  public SalesChartColors: Color[] = [
    {
      borderColor: '#10b759',
      backgroundColor: 'rgba(1, 104, 250, 0.10)',
    },
    {
      borderColor: '#EE9800',
      backgroundColor: 'rgba(255, 255, 255, 0)',
    }
  ];
  public SalesChartLabels = [];
  //#endregion

  //#region Visits Chart Data 
  public VisitsChartData: ChartDataSets[] = [

    { data: [], type: 'bar', label: 'Returning Visits', hidden: false },
    { data: [], type: 'bar', label: 'New Visits', hidden: false }

  ];
  public VisitsChartColors: Color[] = [
    {
      backgroundColor: 'rgba(0, 204, 204, 1)',
    },
    {
      backgroundColor: 'rgba(0, 204, 204, .10)',
    },
  ];
  public VisitsChartLabels = [];
  //#endregion

  public _OLoyalityHistory: OLoyalityHistory = {
    NewCustomerInvoiceAmount_Sum: 0,
    NewCustomer_Sum: 0,
    RepeatingCustomerSaleAmount_Sum: 0,
    RepeatingCustomer_Sum: 0,
    TotalCustomer_Sum: 0,
    TotalInvoiceAmount_Sum: 0,
    RepeatingCustomerInvoiceAmount: 0
  };

  private pLoyalityData = {
    Task: 'getloyaltyvisithistory',
    StartDate: null,
    EndDate: null,
    AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
    AccountId: this._HelperService.AppConfig.ActiveOwnerId,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
    Type: null
  };

  LoyalityData: OLoyalityHistoryData[];

  showVisitsLoalityChart: boolean = true;
  showSalesLoalityChart: boolean = true;

  GetLoyalityReport(StartDateTime, EndDateTime, Data: OLoyalityHistoryData[]) {

    this._OLoyalityHistory = {
      NewCustomerInvoiceAmount_Sum: 0,
      NewCustomer_Sum: 0,
      RepeatingCustomerSaleAmount_Sum: 0,
      RepeatingCustomer_Sum: 0,
      TotalCustomer_Sum: 0,
      TotalInvoiceAmount_Sum: 0,
      RepeatingCustomerInvoiceAmount: 0
    };

    this._HelperService.IsFormProcessing = true;

    this.pLoyalityData.StartDate = this._HelperService.DateInUTC(StartDateTime);
    this.pLoyalityData.EndDate = this._HelperService.DateInUTC(EndDateTime);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, this.pLoyalityData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          Data = _Response.Result as OLoyalityHistoryData[];

          var ReturningSales = [];
          var NewSales = [];

          var ReturningVisits = [];
          var NewVisits = [];

          var ChartLabels: any[] = [];



          if (Data.length > 0 && this.TitleIsHour(Data[0].Title)) {

            for (let index = 0; index < 24; index++) {

              //#region current 

              var dd = " AM";
              var h = index;
              if (h >= 12) {
                h = index - 12;
                dd = " PM";
              }
              if (h == 0) {
                h = 12;
              }
              var Hour = h + ":00" + dd;
              ChartLabels.push(Hour);

              //#endregion

              var RData: any = Data.find(x => x.Title == Hour);
              if (RData != undefined && RData != null) {
                const element: any = RData;

                //#region Calculate Totals 

                this._OLoyalityHistory.NewCustomer_Sum = this._OLoyalityHistory.NewCustomer_Sum + element.NewCustomer;
                this._OLoyalityHistory.NewCustomerInvoiceAmount_Sum = this._OLoyalityHistory.NewCustomerInvoiceAmount_Sum + element.NewCustomerInvoiceAmount;
                this._OLoyalityHistory.RepeatingCustomer_Sum = this._OLoyalityHistory.RepeatingCustomer_Sum + element.RepeatingCustomer;
                this._OLoyalityHistory.RepeatingCustomerSaleAmount_Sum = this._OLoyalityHistory.RepeatingCustomerSaleAmount_Sum + element.RepeatingCustomerInvoiceAmount;
                this._OLoyalityHistory.TotalCustomer_Sum = this._OLoyalityHistory.TotalCustomer_Sum + element.NewCustomer + element.RepeatingCustomer;
                this._OLoyalityHistory.TotalInvoiceAmount_Sum = this._OLoyalityHistory.TotalInvoiceAmount_Sum + element.NewCustomerInvoiceAmount + element.RepeatingCustomerInvoiceAmount;

                //#endregion

                ReturningSales.push(element.RepeatingCustomerInvoiceAmount);
                NewSales.push(element.NewCustomerInvoiceAmount);
                ReturningVisits.push(element.VisitsByRepeatingCustomers);
                NewVisits.push(element.NewCustomer);

              }
              else {
                ReturningSales.push(0.0);
                NewSales.push(0.0);
                ReturningVisits.push(0.0);
                NewVisits.push(0.0);

              }
            }

          } else {
            this.CalculateDateRangeType(cloneDeep(StartDateTime), cloneDeep(EndDateTime));

            if (this.SelectedDateRangeType == this._HelperService.AppConfig.GraphTypes.week) {

              for (let index = 0; index < this.Intermediate.length; index++) {
                const element = this.Intermediate[index];
                ChartLabels.push(element);
                var RData: any = Data.find(x => moment(x.Title, 'DD-MM-YYYY').format('DD MMM') == element);
                if (RData != undefined && RData != null) {
                  const element: any = RData;

                  //#region Calculate Totals 

                  this._OLoyalityHistory.NewCustomer_Sum = this._OLoyalityHistory.NewCustomer_Sum + element.NewCustomer;
                  this._OLoyalityHistory.NewCustomerInvoiceAmount_Sum = this._OLoyalityHistory.NewCustomerInvoiceAmount_Sum + element.NewCustomerInvoiceAmount;
                  this._OLoyalityHistory.RepeatingCustomer_Sum = this._OLoyalityHistory.RepeatingCustomer_Sum + element.RepeatingCustomer;
                  this._OLoyalityHistory.RepeatingCustomerSaleAmount_Sum = this._OLoyalityHistory.RepeatingCustomerSaleAmount_Sum + element.RepeatingCustomerInvoiceAmount;
                  this._OLoyalityHistory.TotalCustomer_Sum = this._OLoyalityHistory.TotalCustomer_Sum + element.NewCustomer + element.RepeatingCustomer;
                  this._OLoyalityHistory.TotalInvoiceAmount_Sum = this._OLoyalityHistory.TotalInvoiceAmount_Sum + element.NewCustomerInvoiceAmount + element.RepeatingCustomerInvoiceAmount;

                  //#endregion

                  ReturningSales.push(element.RepeatingCustomerInvoiceAmount);
                  NewSales.push(element.NewCustomerInvoiceAmount);
                  ReturningVisits.push(element.VisitsByRepeatingCustomers);
                  NewVisits.push(element.NewCustomer);

                }
                else {
                  ReturningSales.push(0.0);
                  NewSales.push(0.0);
                  ReturningVisits.push(0.0);
                  NewVisits.push(0.0);

                }
              }

            } else if (this.SelectedDateRangeType == this._HelperService.AppConfig.GraphTypes.month) {

              for (let index = 0; index < this.Intermediate.length; index++) {
                const element = this.Intermediate[index];
                ChartLabels.push(element);
                var RData: any = Data.find(x => moment(x.Title, 'DD-MM-YYYY').format('DD MMM') == element);
                if (RData != undefined && RData != null) {
                  const element: any = RData;

                  //#region Calculate Totals 

                  this._OLoyalityHistory.NewCustomer_Sum = this._OLoyalityHistory.NewCustomer_Sum + element.NewCustomer;
                  this._OLoyalityHistory.NewCustomerInvoiceAmount_Sum = this._OLoyalityHistory.NewCustomerInvoiceAmount_Sum + element.NewCustomerInvoiceAmount;
                  this._OLoyalityHistory.RepeatingCustomer_Sum = this._OLoyalityHistory.RepeatingCustomer_Sum + element.RepeatingCustomer;
                  this._OLoyalityHistory.RepeatingCustomerSaleAmount_Sum = this._OLoyalityHistory.RepeatingCustomerSaleAmount_Sum + element.RepeatingCustomerInvoiceAmount;
                  this._OLoyalityHistory.TotalCustomer_Sum = this._OLoyalityHistory.TotalCustomer_Sum + element.NewCustomer + element.RepeatingCustomer;
                  this._OLoyalityHistory.TotalInvoiceAmount_Sum = this._OLoyalityHistory.TotalInvoiceAmount_Sum + element.NewCustomerInvoiceAmount + element.RepeatingCustomerInvoiceAmount;

                  //#endregion

                  ReturningSales.push(element.RepeatingCustomerInvoiceAmount);
                  NewSales.push(element.NewCustomerInvoiceAmount);
                  ReturningVisits.push(element.VisitsByRepeatingCustomers);
                  NewVisits.push(element.NewCustomer);

                }
                else {
                  ReturningSales.push(0.0);
                  NewSales.push(0.0);
                  ReturningVisits.push(0.0);
                  NewVisits.push(0.0);

                }
              }

            } else if (this.SelectedDateRangeType == this._HelperService.AppConfig.GraphTypes.year) {

              for (let index = 0; index < this.Intermediate.length; index++) {
                const element = this.Intermediate[index];
              }

            }


          }

          this.MonthlybarChartLabels = ChartLabels;

          this.showVisitsLoalityChart = false;
          this.showSalesLoalityChart = false;

          this.SalesChartData[0].data = NewSales;
          this.SalesChartData[1].data = ReturningSales;

          this.VisitsChartData[0].data = ReturningVisits;
          this.VisitsChartData[1].data = NewVisits;

          this.showVisitsLoalityChart = true;
          this.showSalesLoalityChart = true;
          if (!(this._ChangeDetectorRef as ViewRef).destroyed)
          this._ChangeDetectorRef.detectChanges();
          this._HelperService.IsFormProcessing = false;

          return Data;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
          return Data;
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        return Data;
      });
  }

  TitleIsHour(title: string): boolean {
    return (title.includes('AM') || title.includes('PM'));
  }

  SelectedDateRangeType: string = this._HelperService.AppConfig.GraphTypes.week;
  Intermediate: any[];
  CalculateDateRangeType(start, end): void {

    this.Intermediate = this._HelperService.CalculateIntermediateDate(start, end);

    if (this.Intermediate.length <= 7) {
      this.SelectedDateRangeType = this._HelperService.AppConfig.GraphTypes.week;
    } else if (this.Intermediate.length > 7 && this.Intermediate.length < 32) {
      this.SelectedDateRangeType = this._HelperService.AppConfig.GraphTypes.month;
    } else if (this.Intermediate.length > 32) {
      this.SelectedDateRangeType = this._HelperService.AppConfig.GraphTypes.year;
    }

  }

  //#endregion



  //#region Add Terminal 

  Form_AddUser: FormGroup;
  Form_AddUser_Show() {
    this._HelperService.Icon_Crop_Clear();
    this.InitImagePicker(this.InputFileComponent_Term);
    this._HelperService.OpenModal("Form_AddUser_Content");
    this._HelperService.CloseModal('exampleModal');

  }
  Form_AddUser_Close() {
    this._HelperService.CloseModal("Form_AddUser_Content");
    // this._HelperService.OpenModal('exampleModal');

  }
  Form_AddUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_AddUser = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.ThankUCash.saveterminal,
      AccountId: this._HelperService.UserAccount.AccountId,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      MerchantReferenceKey: this._HelperService.AppConfig.ActiveOwnerKey,
      MerchantReferenceId: this._HelperService.AppConfig.ActiveOwnerId,
      StoreReferenceId: [null, Validators.required],
      StoreReferenceKey: [null, Validators.required],
      ProviderReferenceId: [null, Validators.required],
      ProviderReferenceKey: [null, Validators.required],
      AcquirerReferenceId: [null, Validators.required],
      AcquirerReferenceKey: [null, Validators.required],
      // CashierReferenceId: [null, Validators.required],
      // CashierReferenceKey: [null, Validators.required],
      StatusCode: this._HelperService.AppConfig.Status.Active,
      TerminalId: [null, Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(25)])],
      SerialNumber: null,


    });
  }
  Form_AddUser_Clear() {
    this.Form_AddUser.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_AddUser_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_AddUser_Process(_FormValue: any) {
    this.ResetFilterUI();
    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;



    if (this._HelperService.AppConfig.TerminalsCount == 0) {
      _OResponse = this._HelperService.PostData(
        this._HelperService.AppConfig.NetworkLocation.V3.Account,
        _FormValue
      );
      _OResponse.subscribe(
        _Response => {
          this._HelperService.IsFormProcessing = false;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.FlashSwalSuccess("New pos terminal has been added successfully",
              "You have successfully created new POS terminal");
            this._HelperService.ObjectCreated.next(true);
            this.Form_AddUser_Clear();
            this.Form_AddUser_Close();
            if (_FormValue.OperationType == "close") {
              this.Form_AddUser_Close();
            }
            this._HelperService.GetAccountCount(this._HelperService.AppConfig.ActiveOwnerKey, this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.DefaultStartTimeAll, this._HelperService.AppConfig.DefaultEndTimeToday)

          } else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        }
      );
      return;
    }


    else if (this._HelperService.AppConfig.TerminalPermission) {
      if (this._HelperService.AppConfig.TerminalPermission.MinimumLimit <= this._HelperService.AppConfig.TerminalsCount && this._HelperService.AppConfig.TerminalPermission.MaximumLimit > this._HelperService.AppConfig.TerminalsCount) {

        _OResponse = this._HelperService.PostData(
          this._HelperService.AppConfig.NetworkLocation.V3.Account,
          _FormValue
        );
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.FlashSwalSuccess("New pos terminal has been added successfully",
                "You have successfully created new POS terminal");
              this._HelperService.ObjectCreated.next(true);
              this.Form_AddUser_Clear();
              this.Form_AddUser_Close();
              if (_FormValue.OperationType == "close") {
                this.Form_AddUser_Close();
              }
              this._HelperService.GetAccountCount(this._HelperService.AppConfig.ActiveOwnerKey, this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.DefaultStartTimeAll, this._HelperService.AppConfig.DefaultEndTimeToday)

            } else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          }
        );
      }
      else {
        this._HelperService.NotifyError('Upgrade Your Subscription')
        this.Form_AddUser_Close();
        this._Router.navigate(['m' + '/' + this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Upgrade]);
        return;
      }

    } else {
      this._HelperService.IsFormProcessing = false;
      this._HelperService.NotifyError('Terminal is disable')
      return;
    }


  }

  //#endregion

  //#region Add Cashier 
  CreateCashierRequest(_FormValue: any): void {

    // _FormValue.Address = {
    //   Latitude: this.Form_AddStore_Latitude,
    //   Longitude: this.Form_AddStore_Longitude,
    //   Address: this.Form_AddStore_Address,
    //   CityName: this._CurrentAddress.sublocality_level_2,
    //   StateName: this._CurrentAddress.sublocality_level_1,
    //   CountryName: this._CurrentAddress.country
    // }

    // _FormValue.Latitude = undefined;
    // _FormValue.Longitude = undefined;
    // _FormValue.MapAddress = undefined;

    var IconContent: any = undefined;

    if (this._HelperService._Icon_Cropper_Data.Content != null) {
      IconContent = this._HelperService._Icon_Cropper_Data;
    }

    _FormValue.IconContent = IconContent;

    return _FormValue;
  }

  Form_AddCashier: FormGroup;
  Form_AddCashier_Show() {
    this._HelperService.Icon_Crop_Clear();
    this.InitImagePicker(this.InputFileComponent_Cashier);
    this._HelperService.OpenModal("Form_AddCashier_Content");
    this._HelperService.CloseModal('exampleModal');

  }
  Form_AddCashier_Close() {
    this._HelperService.CloseModal("Form_AddCashier_Content");
    // this._HelperService.OpenModal('exampleModal');

  }
  Form_AddCashier_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_AddCashier = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.ThankUCash.SaveCashier,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      AccountId: this._HelperService.UserAccount.AccountId,
      StoreId: [null, Validators.required],
      StoreKey: [null, Validators.required],
      StatusCode: this._HelperService.AppConfig.Status.Active,
      FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],
      LastName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
      ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(18)])],
      EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
      EmployeeCode: [null],
      GenderCode: this._HelperService.AppConfig.Gender.Male,
    });
  }
  Form_AddCashier_Clear() {
    this.Form_AddCashier.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_AddCashier_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_AddCashier_Process(_FormValue: any) {
    this.ResetFilterUI();
    this._HelperService.IsFormProcessing = true;

    var Request = this.CreateCashierRequest(_FormValue);


    let _OResponse: Observable<OResponse>;

    if (this._HelperService.AppConfig.CashiersCount == 0) {
      _OResponse = this._HelperService.PostData(
        this._HelperService.AppConfig.NetworkLocation.V3.Account,
        Request
      );
      _OResponse.subscribe(
        _Response => {
          this._HelperService.IsFormProcessing = false;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.FlashSwalSuccess("New cashier has been added successfully",
              "Done! You have successfully created new Cashier");
            this._HelperService.ObjectCreated.next(true);
            this.Form_AddCashier_Clear();
            this.Form_AddCashier_Close();
            if (_FormValue.OperationType == "close") {
              this.Form_AddCashier_Close();
            }
            this._HelperService.GetAccountCount(this._HelperService.AppConfig.ActiveOwnerKey, this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.DefaultStartTimeAll, this._HelperService.AppConfig.DefaultEndTimeToday)

          } else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        }
      );

      return;
    }
    else if (this._HelperService.AppConfig.CashiersPermission) {

      if (this._HelperService.AppConfig.CashiersPermission.MinimumLimit <= this._HelperService.AppConfig.CashiersCount && this._HelperService.AppConfig.CashiersPermission.MaximumLimit > this._HelperService.AppConfig.CashiersCount) {
        _OResponse = this._HelperService.PostData(
          this._HelperService.AppConfig.NetworkLocation.V3.Account,
          Request
        );
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.FlashSwalSuccess("New cashier has been added successfully",
                "Done! You have successfully created new Cashier");
              this._HelperService.ObjectCreated.next(true);
              this.Form_AddCashier_Clear();
              this.Form_AddCashier_Close();
              if (_FormValue.OperationType == "close") {
                this.Form_AddCashier_Close();
              }
            } else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          }
        );
      }
      else {
        this._HelperService.NotifyError('Upgrade Your Subscription')
        this.Form_AddCashier_Close();
        this._Router.navigate(['m' + '/' + this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Upgrade]);
        return;
      }

    } else {
      this._HelperService.IsFormProcessing = false;
      this._HelperService.NotifyError('Cashier is disable')
      return;
    }


  }

  //#endregion

  //#region New Reward Perentage 

  RewardPerc: any = null;

  RewardList_GetData() {
    var pData = {
      Task: 'getconfiguration',
      StartDate: null,
      EndDate: null,
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      ConfigurationKey: this._HelperService.AppConfig.ConfigurationKey
    };


    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Operations, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.RewardPerc = _Response.Result.Value;
          if (this.RewardPerc != 0) {
            this._HelperService.IsRewardSet = true;
          }

        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });

  }

  //#endregion

  //#region Balance Data 
  public _TransactionReference = null;
  public _AccountBalanceCreditAmount = 0;
  generateRandomNumber() {
    var TrRef = Math.floor(100000 + Math.random() * (999999 + 1 - 100000));
    return TrRef;
  }
  OpenPaymentOptions() {
    this._AccountBalanceCreditAmount = 0;
    var Ref = this.generateRandomNumber();
    this._TransactionReference = 'Tp' + '_' + this._HelperService.UserAccount.AccountId + '_' + Ref;
    this._HelperService.OpenModal('Form_TopUp_Content');
    this._HelperService.CloseModal('exampleModal');

  }
  paymentDone(ref: any) {
    this.TransactionId = ref.trans
    if (ref != undefined && ref != null && ref != '') {
      if (ref.status == 'success') {
        this.CreditAmount();
      }
      else {
        this._HelperService.NotifyError('Payment failed');
      }
    }
    else {
      this._HelperService.NotifyError('Payment failed');
    }
    // this.title = 'Payment successfull';
    // console.log(this.title, ref);
  }

  paymentClose() {
    this._HelperService.OpenModal('exampleModal');

  }

  public TransactionId
  public CreditAmount() {
    this._HelperService.IsFormProcessing = true;
    var PostData = {
      Task: "topupaccount",
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      Amount: this._AccountBalanceCreditAmount,
      PaymentReference: this._TransactionReference,
      TransactionId: this.TransactionId
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Subscription, PostData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._AccountBalanceCreditAmount = 0;
          this._TransactionReference = null;
          this._HelperService.NotifySuccess("Account credited");
          this.GetBalance();
          this._HelperService.GetAccountCount(this._HelperService.AppConfig.ActiveOwnerKey, this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.DefaultStartTimeAll, this._HelperService.AppConfig.DefaultEndTimeToday);


        } else {
          this._HelperService.NotifySuccess(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }
  paymentCancel() {
    console.log('payment failed');
  }
  public _AccountBalance: any = {
    Credit: 0.0,
    Debit: 0.0,
    Balance: 0.0,
    LastTransactionAmount: 0.0,
    LastTopupBalance: 0.0,
    LastTransactionDate: null,
    Ratio: 0,
    IsLoaded: false,
  };

  public GetBalance() {
    this._AccountBalance.IsLoaded = false;
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: 'getaccountbalance',
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      Source: this._HelperService.AppConfig.TransactionSource.Merchant
    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Subscription, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._AccountBalance = _Response.Result;
          if (this._AccountBalance.Balance && this._AccountBalance.Balance > 0) {
            this._AccountBalance.Ratio = this._AccountBalance.Balance / this._AccountBalance.LastTopupBalance;
          }
          else {
            this._AccountBalance.Ratio = 0;
          }
          this._AccountBalance.IsLoaded = true;

          setTimeout(() => {
            setTimeout(() => {
              try {
              } catch (error) {
                console.log('Gauge Initializtion failed');
              }
            }, 200);
          }, 500);

        } else {
          this._AccountBalance.IsLoaded = true;
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  //#endregion


  //Reward
  Form_RewardUser: FormGroup;
  Form_RewardUser_Show() {
    this._HelperService.OpenModal("Form_RewardUser_Content");
    this._HelperService.CloseModal('exampleModal');
  }
  Form_RewardUser_Close() {
    // this._Router.navigate([
    //     this._HelperService.AForm_RewardUserppConfig.Pages.System.AdminUsers
    // ]);
    this._HelperService.CloseModal("Form_RewardUser_Content");
    this._HelperService.OpenModal('exampleModal');

  }
  Form_RewardUser_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_RewardUser = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.ThankUCash.saveconfiguration,
      AccountId: this._HelperService.UserAccount.AccountId,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      StatusCode: this._HelperService.AppConfig.Status.Active,
      ConfigurationKey: this._HelperService.AppConfig.ConfigurationKey,
      Value: [null, Validators.compose([Validators.required, Validators.min(0), Validators.max(100)])],
      Comment: [null, Validators.required],


    });
  }
  Form_RewardUser_Clear() {
    this.Form_RewardUser.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_RewardUser_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_RewardUser_Process(_FormValue: any) {
    this._HelperService.IsFormProcessing = true;
    if (this._HelperService.AppConfig.RewardPercentagePermission) {
      this._HelperService.IsFormProcessing = false;
      if (_FormValue.Value >= this._HelperService.AppConfig.RewardPercentagePermission.MinimumValue && _FormValue.Value <= this._HelperService.AppConfig.RewardPercentagePermission.MaximumValue) {
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(
          this._HelperService.AppConfig.NetworkLocation.V3.Operations,
          _FormValue
        );
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.NotifySuccess("Reward Percentage Updated successfully");
              this.Form_AddUser_Clear();
              this.Form_AddUser_Close();
              if (_FormValue.OperationType == "close") {
                this.Form_AddUser_Close();
              }
              this.RewardList_GetData();
            } else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          }
        );
      } else {
        this._HelperService.NotifyError('Upgrade Your Subscription')
        this.Form_AddUser_Close();
        this._Router.navigate(['m' + '/' + this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Upgrade]);
        return;
      }

    } else {
      this._HelperService.IsFormProcessing = false;
      this._HelperService.NotifyError('Reward percentage is disabled');
      return;

    }
  }


  //end


  //Store
  _CurrentAddress: any = {};
  Form_AddStore_Address: string = null;
  Form_AddStore_Latitude: number = 0;
  Form_AddStore_Longitude: number = 0;
  @ViewChild('places') places: GooglePlaceDirective;
  Form_AddStore_PlaceMarkerClick(event) {
    this.Form_AddStore_Latitude = event.coords.lat;
    this.Form_AddStore_Longitude = event.coords.lng;
  }
  public Form_AddStore_AddressChange(address: Address) {

    this.Form_AddStore_Latitude = address.geometry.location.lat();
    this.Form_AddStore_Longitude = address.geometry.location.lng();
    this.Form_AddStore_Address = address.formatted_address;

    this.Form_AddStore.controls['Latitude'].setValue(this.Form_AddStore_Latitude);
    this.Form_AddStore.controls['Longitude'].setValue(this.Form_AddStore_Longitude);
    this._CurrentAddress = this._HelperService.GoogleAddressArrayToJson(address.address_components);

  }

  CreateStoreRequest(_FormValue: any): void {
    _FormValue.ContactPerson = {
      FirstName: _FormValue.FirstName,
      LastName: _FormValue.LastName,
      MobileNumber: _FormValue.MobileNumber,
      // EmailAddress: _FormValue.EmailAddress,
      EmailAddress: _FormValue.MEmailAddress

    }

    _FormValue.Address = {
      Latitude: this.Form_AddStore_Latitude,
      Longitude: this.Form_AddStore_Longitude,
      Address: this.Form_AddStore_Address,
      CityName: this._CurrentAddress.sublocality_level_2,
      StateName: this._CurrentAddress.sublocality_level_1,
      CountryName: this._CurrentAddress.country
    }

    _FormValue.FirstName = undefined;
    _FormValue.LastName = undefined;
    _FormValue.MobileNumber = undefined;
    // _FormValue.EmailAddress = undefined;

    _FormValue.Latitude = undefined;
    _FormValue.Longitude = undefined;
    _FormValue.MapAddress = undefined;

    return _FormValue;
  }


  Form_AddStore: FormGroup;
  Form_AddStore_Show() {
    this._HelperService.OpenModal("Form_AddStore_Content");
    this._HelperService.CloseModal('exampleModal');

  }
  Form_AddStore_Close() {
    this._HelperService.CloseModal("Form_AddStore_Content");
    // this._HelperService.OpenModal('exampleModal');

  }
  Form_AddStore_Load() {
    this._HelperService._FileSelect_Icon_Data.Width = 128;
    this._HelperService._FileSelect_Icon_Data.Height = 128;

    this._HelperService._FileSelect_Poster_Data.Width = 800;
    this._HelperService._FileSelect_Poster_Data.Height = 400;

    this.Form_AddStore = this._FormBuilder.group({
      OperationType: "new",
      Task: this._HelperService.AppConfig.Api.ThankUCash.savestore,
      AccountId: this._HelperService.UserAccount.AccountId,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      DisplayName: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
      Name: [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(256)])],
      FirstName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
      LastName: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
      ContactNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(18)])],
      MobileNumber: [null, Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(14)])],
      EmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],
      MEmailAddress: [null, Validators.compose([Validators.required, Validators.email, Validators.minLength(2)])],

      MapAddress: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(256)])],
      Latitude: [null, Validators.compose([Validators.required])],
      Longitude: [null, Validators.compose([Validators.required])],
      StatusCode: this._HelperService.AppConfig.Status.Active,
    });
  }
  Form_AddStore_Clear() {
    this.Form_AddStore.reset();
    this._HelperService._FileSelect_Icon_Reset();
    this._HelperService._FileSelect_Poster_Reset();
    this.Form_AddStore_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  Form_AddStore_Process(_FormValue: any) {
    this.ResetFilterUI();

    if (this.SelectedBusinessCategories != undefined && this.SelectedBusinessCategories.length > 0) {
      _FormValue.Categories = []
      this.SelectedBusinessCategories.forEach(element => {
        _FormValue.Categories.push(
          {
            // TypeCode: this._HelperService.AppConfig.HelperTypes.MerchantCategories,
            // ReferenceKey: element.key,
            ReferenceId: element
          }
        )
      });
    }

    this._HelperService.IsFormProcessing = true;
    let _OResponse: Observable<OResponse>;

    if (this._HelperService.AppConfig.StorePermission) {
      if (this._HelperService.AppConfig.StorePermission.MinimumLimit <= this._HelperService.AppConfig.StoresCount && this._HelperService.AppConfig.StorePermission.MaximumLimit > this._HelperService.AppConfig.StoresCount) {
        var Request = this.CreateStoreRequest(_FormValue);
        _OResponse = this._HelperService.PostData(
          this._HelperService.AppConfig.NetworkLocation.V3.Account,
          Request
        );
        _OResponse.subscribe(
          _Response => {
            this._HelperService.IsFormProcessing = false;
            if (_Response.Status == this._HelperService.StatusSuccess) {
              this._HelperService.FlashSwalSuccess("Store Added successfully", "Done! New Store Has Been Added");
              this._HelperService.ObjectCreated.next(true);
              this.Form_AddStore_Clear();
              this.Form_AddStore_Close();
              if (_FormValue.OperationType == "close") {
                this.Form_AddStore_Close();

              }
              this._HelperService.GetAccountCount(this._HelperService.AppConfig.ActiveOwnerKey, this._HelperService.AppConfig.ActiveOwnerId, this._HelperService.AppConfig.DefaultStartTimeAll, this._HelperService.AppConfig.DefaultEndTimeToday)

            } else {
              this._HelperService.NotifyError(_Response.Message);
            }
          },
          _Error => {
            this._HelperService.IsFormProcessing = false;
            this._HelperService.HandleException(_Error);
          }
        );
      }
      else {
        this._HelperService.NotifyError('Upgrade Your Subscription')
        this.Form_AddStore_Close();
        this._Router.navigate(['m' + '/' + this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.Upgrade]);
        return;
      }


    } else {
      this._HelperService.IsFormProcessing = false;
      this._HelperService.NotifyError('Store is disable')
      return;
    }

  }

  public BusinessCategories = [];
  public S2BusinessCategories = [];

  GetBusinessCategories() {

    this._HelperService.ToggleField = true;
    var PData =
    {
      Task: this._HelperService.AppConfig.Api.Core.getcategories,
      SearchCondition: this._HelperService.GetSearchConditionStrict('', 'StatusCode', this._HelperService.AppConfig.DataType.Text, 'default.active', '='),
      SortExpression: 'Name asc',
      Offset: 0,
      Limit: 1000,
    }
    PData.SearchCondition = this._HelperService.GetSearchConditionStrict(
      PData.SearchCondition,
      "TypeCode",
      this._HelperService.AppConfig.DataType.Text,
      this._HelperService.AppConfig.HelperTypes.MerchantCategories,
      "="
    );
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Op, PData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          if (_Response.Result.Data != undefined) {
            this.BusinessCategories = _Response.Result.Data;

            this.ShowCategorySelector = false;
            this._ChangeDetectorRef.detectChanges();
            for (let index = 0; index < this.BusinessCategories.length; index++) {
              const element = this.BusinessCategories[index];
              this.S2BusinessCategories.push(
                {
                  id: element.ReferenceId,
                  key: element.ReferenceKey,
                  text: element.Name
                }
              );
            }
            this.ShowCategorySelector = true;
            this._ChangeDetectorRef.detectChanges();

            this._HelperService.ToggleField = false;

          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        this._HelperService.ToggleField = false;

      });
  }
  public SelectedBusinessCategories = [];
  CategoriesSelected(Items) {

    if (Items != undefined && Items.value != undefined && Items.value.length > 0) {
      this.SelectedBusinessCategories = Items.value;
    }
    else {
      this.SelectedBusinessCategories = [];
    }


  }
  SaveMerchantBusinessCategory(item) {
    if (item != '0') {
      var Setup =
      {
        Task: this._HelperService.AppConfig.Api.Core.SaveUserParameter,
        TypeCode: this._HelperService.AppConfig.HelperTypes.MerchantCategories,
        // UserAccountKey: this._UserAccount.ReferenceKey,
        CommonKey: item,
        StatusCode: 'default.active'
      };
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, Setup);
      _OResponse.subscribe(
        _Response => {
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.NotifySuccess('Business category assigned to merchant');
            // this.BusinessCategories = [];
            // this.GetMerchantBusinessCategories();
          }
          else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        });
    }
  }



  //#endregion
  //end

  //Region DropDowns


  public TerminalId: string = null;
  public TUTr_Filter_Bank_Option: Select2Options;
  TUTr_Filter_Banks_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.Acquirer
        }]
    };
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Bank_Option = {
      placeholder: 'Select Bank (Acquirer)',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TUTr_Filter_Banks_Change(event: any) {
    this.Form_AddUser.patchValue(
      {
        AcquirerReferenceId: event.data[0].ReferenceId,
        AcquirerReferenceKey: event.data[0].ReferenceKey,
      });
  }
  public TUTr_Filter_Provider_Option: Select2Options;
  TUTr_Filter_Providers_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.PosAccount
        }]
    };


    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Provider_Option = {
      placeholder: 'Select  Provider (PTSP)',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TUTr_Filter_Providers_Change(event: any) {
    this.Form_AddUser.patchValue(
      {
        ProviderReferenceId: event.data[0].ReferenceId,
        ProviderReferenceKey: event.data[0].ReferenceKey,
      });
  }
  public TUTr_Filter_Cashier_Option: Select2Options;
  TUTr_Filter_Cashiers_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetCashiers,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      AccountId: this._HelperService.UserAccount.AccountId,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "Name",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        }
        // {
        //   SystemName: "AccountTypeCode",
        //   Type: this._HelperService.AppConfig.DataType.Text,
        //   SearchCondition: "=",
        //   SearchValue: this._HelperService.AppConfig.AccountType.PosAccount
        // }
      ]
    };


    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Cashier_Option = {
      placeholder: 'Select  Cashier',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TUTr_Filter_Cashiers_Change(event: any) {
    this.Form_AddUser.patchValue(
      {
        CashierReferenceId: event.data[0].ReferenceId,
        CashierReferenceKey: event.data[0].ReferenceKey,
      });
  }



  OpenModel() {
    this._HelperService.CloseModal('Form_AddHelp_Content');

    this._HelperService.OpenModal('Form_AddUser_Content');


  }

  HelpImageUrl: string;
  ShowHelpImage(HelpImage: string): void {
    this._HelperService.CloseModal('Form_AddUser_Content');
    switch (HelpImage) {
      case HelpImageType.Receipt: {
        this.HelpImageUrl = '../../../../../assets/img/receipt.png';
        this._HelperService.OpenModal("Form_AddHelp_Content");
      }

        break;

      case HelpImageType.Terminal: {
        this.HelpImageUrl = '../../../../../assets/img/terminalid.png';
        this._HelperService.OpenModal('Form_AddHelp_Content');
      }

        break;

      case HelpImageType.Serial: {
        this.HelpImageUrl = '../../../../../assets/img/serial.png';
        this._HelperService.OpenModal('Form_AddHelp_Content');
      }

        break;

      default:
        break;
    }
  }


  public ResetFilterControls: boolean = true;
  ResetFilterUI(): void {
    setTimeout(() => {
      this.ResetFilterControls = false;
      this._ChangeDetectorRef.detectChanges();
      this.Form_AddCashier_Load();
      this.Form_AddStore_Load();
      this.Form_AddUser_Load();

      this.ResetFilterControls = true;
      this._ChangeDetectorRef.detectChanges();
    }, 500);

  }

  // public GenderWise: any = []
  // public percentage = null;
  // private pGenderWiseData = {
  //   Task: 'getcustgenders',
  //   StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
  //   EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
  //   StoreReferenceId: 0,
  //   StoreReferenceKey: null,
  // };
  // GetGenderWiseOverview() {

  //   this._HelperService.IsFormProcessing = true;
  //   this.LoadingChart = true;
  //   this.pGenderWiseData.StartDate = this._HelperService.DateInUTC(this.TodayStartTime);
  //   this.pGenderWiseData.EndDate = this._HelperService.DateInUTC(this.TodayEndTime);

  //   let _OResponse: Observable<OResponse>;
  //   _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.CustAnalytics, this.pGenderWiseData);
  //   _OResponse.subscribe(
  //     _Response => {
  //       this._HelperService.IsFormProcessing = false;
  //       if (_Response.Status == this._HelperService.StatusSuccess) {
  //         this.LoadingChart = false;
  //         this.showDailyChart = false;
  //         this._ChangeDetectorRef.detectChanges();
  //         this.GenderTotal = 0;
  //         this.VisitorTotal = 0;

  //         //#endregion

  //         this.GenderWise = _Response.Result as any;
  //         this.GenderRange = this.GenderWise;
  //         this.GenderSegment = []
  //         for (let index = 0; index < this.GenderRange.length; index++) {
  //           const element1 = this.GenderRange[index];

  //           this.GenderTotal = this.GenderTotal + element1.Count;

  //           this.GenderRange.Percentage =

  //             this.GenderSegment.push(element1.Count);
  //           switch (index) {
  //             case 0: this.GenderRange[index].text = 'text-primary';

  //               break;
  //             case 1: this.GenderRange[index].text = 'tx-verve';

  //               break;
  //               // case 2: this.GenderRange[index].text = 'tx-rubik';

  //               break;

  //             default:
  //               break;
  //           }

  //         }
  //         this.LoadingChart = false;
  //         this.showDailyChart = true;
  //         this._ChangeDetectorRef.detectChanges();


  //       }

  //       else {
  //         this._HelperService.NotifyError(_Response.Message);
  //       }
  //     },
  //     _Error => {
  //       this._HelperService.IsFormProcessing = false;
  //       this._HelperService.HandleException(_Error);
  //     });
  // }




  // public barChartOptions: ChartOptions = {
  //   responsive: true,
  //   layout: {
  //     padding: {
  //       right: 0,
  //       left: 5,
  //     },
  //   },
  //   scales: {
  //     xAxes: [{
  //       display: false,
  //       gridLines: {
  //         display: false
  //       },

  //     }],
  //     yAxes: [{
  //       display: false,
  //       gridLines: {
  //         display: false
  //       }
  //     }]
  //   }
  // };
  // public SbarChartLabels: Label[] = ['', '', '', ''];
  // public SbarChartType: ChartType = 'horizontalBar';
  // public SbarChartLegend = false;
  // public barChartPlugins = [];
  // public SbarChartData: ChartDataSets[] = [
  //   { data: [0, 0, 0, 0], label: 'Male', stack: 'a' },
  //   { data: [0, 0, 0, 0], label: 'Female', stack: 'a' }
  // ];

  // public _GetGenderGroupCategories = []
  // public GenderGroupCategotries = [
  //   { data: [0], label: '1M-2M' },
  //   { data: [0], label: '2M-3M' },
  //   { data: [0], label: '3M-4M ' },
  //   { data: [0], label: 'More Than 5' },
  // ]
  // GenderGroupElement1: any = {};
  // GenderGroupElement2: any = {};
  // GenderGroupElement3: any = {};

  // private pGenderGroupData = {
  //   Task: 'getcustagegroupsbygender',
  //   StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
  //   EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
  //   StoreReferenceId: 0,
  //   StoreReferenceKey: null,
  // };

  // GetGenderGroupCategories() {

  //   this._HelperService.IsFormProcessing = true;
  //   this.pGenderGroupData.StartDate = this._HelperService.DateInUTC(this.TodayStartTime);
  //   this.pGenderGroupData.EndDate = this._HelperService.DateInUTC(this.TodayEndTime);

  //   let _OResponse: Observable<OResponse>;
  //   _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.CustAnalytics, this.pGenderGroupData);
  //   _OResponse.subscribe(
  //     _Response => {
  //       this._HelperService.IsFormProcessing = false;
  //       if (_Response.Status == this._HelperService.StatusSuccess) {
  //         this._GetGenderGroupCategories = _Response.Result as any;

  //         this.showDailyChart = false;
  //         this._ChangeDetectorRef.detectChanges();
  //         this.GenderGroupElement1 = this._GetGenderGroupCategories[0];
  //         this.GenderGroupElement2 = this._GetGenderGroupCategories[1];
  //         this.GenderGroupElement3 = this._GetGenderGroupCategories[2];

  //         for (let i = 0; i < this._GetGenderGroupCategories.length; i++) {
  //           // this.barChartData[0].data[i] = this._LastSevenDaysData[i].Data.Total;
  //           this.SbarChartData[0].data[i] = this._GetGenderGroupCategories[i].Male;
  //           this.SbarChartData[1].data[i] = this._GetGenderGroupCategories[i].Female;
  //           this.SbarChartLabels[i] = this._GetGenderGroupCategories[i].Title;

  //         }


  //         this.showDailyChart = true;
  //         this._ChangeDetectorRef.detectChanges();

  //         return;

  //       }

  //       else {
  //         this._HelperService.NotifyError(_Response.Message);
  //       }
  //     },
  //     _Error => {
  //       this._HelperService.IsFormProcessing = false;
  //       this._HelperService.HandleException(_Error);
  //     });
  // }
  // public AgeWise: any = []
  // public AgeRange: any = []

  // private pAgeWiseData = {
  //   Task: 'getcustagegroups',
  //   StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
  //   EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
  //   StoreReferenceId: 0,
  //   StoreReferenceKey: null,
  // };
  // GetAgeWiseOverview() {

  //   this._HelperService.IsFormProcessing = true;
  //   this.LoadingChart = true;
  //   this.pAgeWiseData.StartDate = this._HelperService.DateInUTC(this.TodayStartTime);
  //   this.pAgeWiseData.EndDate = this._HelperService.DateInUTC(this.TodayEndTime);

  //   let _OResponse: Observable<OResponse>;
  //   _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.CustAnalytics, this.pAgeWiseData);
  //   _OResponse.subscribe(
  //     _Response => {
  //       this._HelperService.IsFormProcessing = false;
  //       if (_Response.Status == this._HelperService.StatusSuccess) {
  //         this.LoadingChart = false;
  //         //#endregion

  //         this.AgeWise = _Response.Result as any;
  //         this.AgeRange = this.AgeWise;
  //         // this.AgePiedata = this.AgeRange.Count;
  //         this.AgePiedata = []
  //         for (let index = 0; index < this.AgeRange.length; index++) {
  //           const element = this.AgeRange[index];
  //           this.AgeTotal = this.AgeTotal + element.Count;

  //           this.AgePiedata.push(element.Count);

  //           switch (index) {
  //             case 0: this.AgeRange[index].text = 'tx-verve';

  //               break;
  //             case 1: this.AgeRange[index].text = 'tx-MasterCard';

  //               break;
  //             case 2: this.AgeRange[index].text = 'tx-teal';

  //               break;

  //             default:
  //               break;
  //           }

  //         }


  //         this.LoadingChart = false;




  //       }

  //       else {
  //         this._HelperService.NotifyError(_Response.Message);
  //       }
  //     },
  //     _Error => {
  //       this._HelperService.IsFormProcessing = false;
  //       this._HelperService.HandleException(_Error);
  //     });
  // }


  // public _Weekly: OSalesTrend = {
  //   ActualStartDate: moment(),
  //   ActualEndDate: moment(),
  //   ActualData: null,

  //   CompareStartDate: moment(),
  //   CompareEndDate: moment(),
  //   CompareData: null,

  //   ActualSalesAmount: 0,
  //   CompareSalesAmount: 0,
  //   SalesAmountDifference: 0
  // }

  // WeelyDateChanged(event: any, Type: any): void {

  //   var ev: any = cloneDeep(event);
  //   this._Weekly.CompareStartDate = moment(ev.start).startOf("week").startOf('day');
  //   this._Weekly.CompareEndDate = moment(ev.end).endOf("week").endOf('day');


  //   this.lastweektext = this._HelperService.GetDateSByFormat(this._Weekly.CompareStartDate, 'DD MMM YY')
  //     + "-" + this._HelperService.GetDateSByFormat(this._Weekly.CompareEndDate, 'DD MMM YY');
  //   this.lastweekCustom = this._HelperService.GetDateSByFormat(this._Weekly.CompareStartDate, 'DD MMM YYYY')
  //   this.lastweekCustom = 'Custom Week';

  //   this.hideWeeklyPicker = true;
  //   this._HelperService.CloseModal('Form_DatePicker_Content');
  // }

  // CloseRowModal(index: number): void {
  //   switch (index) {

  //     case 1: {
  //       var ev: any = {
  //         start: moment().subtract(1, 'month').startOf('week').startOf('day'),
  //         end: moment().subtract(1, 'month').endOf('week').endOf('day')
  //       };
  //       this.WeelyDateChanged(ev, '');
  //       $("#WeekSalesTrend_dropdown").dropdown('toggle');
  //     }

  //       break;

  //     default:
  //       break;
  //   }
  // }

  // initializeDatePicker(pickerId: string, type: string, dropdownId: string): void {
  //   var i = '#' + pickerId;
  //   var picker = "#" + dropdownId;


  //   if (type == this._HelperService.AppConfig.DatePickerTypes.month) {
  //     $(i).datepicker(
  //       {
  //         viewMode: "months",
  //         minViewMode: "months"
  //       }
  //     );
  //   } else {
  //     $(i).datepicker();
  //   }

  //   $(i).on('changeDate', () => {
  //     switch (type) {


  //       case this._HelperService.AppConfig.DatePickerTypes.week:
  //         {
  //           this.WeelyDateChanged({
  //             start: $(i).datepicker("getDate"),
  //             end: $(i).datepicker("getDate")
  //           }, '');
  //           $(picker).dropdown('toggle');
  //         }
  //         break;



  //       default:
  //         break;
  //     }
  //   });
  // }

  // hideDailyPicker: boolean = true;
  // hideWeeklyPicker: boolean = true;
  // hideMonthlyPicker: boolean = true;

  // ShowHideCalendar(type: string) {
  //   switch (type) {
  //     case this._HelperService.AppConfig.DatePickerTypes.hour: {
  //       this.hideDailyPicker = !this.hideDailyPicker;
  //     }

  //       break;
  //     case this._HelperService.AppConfig.DatePickerTypes.week: {
  //       this.hideWeeklyPicker = !this.hideWeeklyPicker;
  //     }

  //       break;
  //     case this._HelperService.AppConfig.DatePickerTypes.month: {
  //       this.hideMonthlyPicker = !this.hideMonthlyPicker;
  //     }

  //       break;

  //     default:
  //       break;
  //   }
  // }

  //End


}

export class OAccountOverview {
  public NewCustomerInvoiceAmount: any;
  public NewCustomers: any;
  public RedeemAmount: any;
  public RedeemInvoiceAmount: any;
  public RedeemTransaction: any;
  public RepeatingCustomerInvoiceAmount: any;
  public RepeatingCustomers: any;
  public RewardAmount: any;
  public RewardCommissionAmount: any;
  public RewardInvoiceAmount: any;
  public RewardTransaction: any;
  public Transaction: any;
  public TransactionInvoiceAmount: any;
  public TucPlusRewardAmount: any;
  public TucPlusRewardClaimAmount: any;
  public TucPlusRewardClaimInvoiceAmount: any;
  public TucPlusRewardClaimTransaction: any;
  public TucPlusRewardCommissionAmount: any;
  public TucPlusRewardInvoiceAmount: any;
  public TucPlusRewardTransaction: any;
  public TucRewardTransaction: any;
  public VisitsByRepeatingCustomers: any;


  public TucPlusRewardAmountCommission: any;
  public TotalTucPlusRewardClaimTransactionAmount: any;
  public TucPlusRewardAmountUser: any;
  public TotalTucPlusRewardTransactionAmount: any;
  public TucRewardInvoiceAmount: any;

}