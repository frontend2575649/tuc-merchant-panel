import { ChangeDetectorRef, Component, OnInit, OnDestroy, ElementRef, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from "feather-icons";
import { Observable, Subscription } from "rxjs";
import { DataHelperService } from "src/app/service/datahelper.service";
import { FilterHelperService } from "src/app/service/filterhelper.service";
import { HelperService } from "src/app/service/helper.service";
import { OList, OResponse, OSelect } from "src/app/service/object.service";
import swal from "sweetalert2";
declare var $: any;
import * as cloneDeep from 'lodash/cloneDeep';
import moment from "moment";

@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.component.html',
  styles: [`

::ng-deep .select2-container--default .select2-selection--multiple .select2-selection__rendered {
    padding: 0 4px;
    display: block;
    height: 100% !important;
}
  `]
})
export class SubscriptionComponent implements OnInit, OnDestroy {

  DateRangeOptions: any;
  public ListType: number;
  public ResetFilterControls: boolean = true;
  public _ObjectSubscription: Subscription = null;
  MinAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit;
  MaxAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit;
  public Select2Options_Multiple: any = {
    multiple: true,
    placeholder: "Search & Select Stores",
    closeOnSelect: false
  };
  public SelectedBusinessCategories = [];
  public S2BusinessCategories = [];
  public ShowCategorySelector: boolean = true;
  public BusinessCategories = [];
  public _NumberOfStores = 1;
  allStores: any = [];
  @ViewChild("offCanvas") divView: ElementRef;
  CurrentData: any = {}
  public monthlyAmount = 0;
  public dateRange
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = false;
    this._HelperService.showAddNewCashierBtn = true;
    this._HelperService.showAddNewSubAccBtn = false;

    this.DateRangeOptions = cloneDeep(this._HelperService.AppConfig.DateRangeOptions);
    this.DateRangeOptions.opens = "left";

  }

  ngOnInit() {
    this._HelperService.StopClickPropogation();
    Feather.replace();
    this._HelperService.setTheme();
    this.MerchantsList_Setup();
    this.TUTr_Filter_Stores_Load();
    this.InitBackDropClickEvent();
    this.Form_EditSubscription_Load();
    this.getstorenotsubscribedlist();
    this.getAmountMonthly();
    let currentDate = new Date()
    this.dateRange =  moment(currentDate).format('DD-MMM-YYYY') + " to " +  moment(new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 0)).format('DD-MMM-YYYY') ;
  }

  InitBackDropClickEvent(): void {
    var backdrop: HTMLElement = document.getElementById("backdrop");

    backdrop.onclick = () => {
      $('.df-settings').show();
      $(this.divView.nativeElement).removeClass('show');
      backdrop.classList.remove("show");
    };
  }
  clicked() {
    $('.df-settings').hide();
    $(this.divView.nativeElement).addClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.add("show");
  }
  unclick() {
    $('.df-settings').show();
    $(this.divView.nativeElement).removeClass('show');
    var backdrop: HTMLElement = document.getElementById("backdrop");
    backdrop.classList.remove("show");
  }

  ngOnDestroy(): void {
    try {
      this._ObjectSubscription.unsubscribe();
    } catch (error) {

    }
  }

  getAmountMonthly() {
    const pData = {
      Task: 'getamountmonthly',
      AccountId: this._HelperService.UserAccount.AccountId,
      AccountKey: this._HelperService.UserAccount.AccountKey
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.AccountSubscription, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.monthlyAmount = _Response.Result.Amount
        }

      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  CategoriesSelected(Items) {
    if (Items != undefined && Items.value != undefined && Items.value.length > 0) {
      this.SelectedBusinessCategories = Items.value;
    }
    else {
      this.SelectedBusinessCategories = [];
    }
  }

  TUTr_Filter_Stores_Load2() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.Store
        }
      ]
    };

    var OwnerKey = this._HelperService.UserAccount.AccountId;
    if (this._HelperService.UserAccount.AccountTypeCode != this._HelperService.AppConfig.AccountType.Merchant) {
      OwnerKey = this._HelperService.UserOwner.AccountId;
    }
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, OwnerKey, '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    _Transport
    // this.Select2Options_Multiple = {
    //   placeholder: 'Filter Store',
    //   ajax: _Transport,
    //   multiple: true,
    //   allowClear: false,
    //   closeOnSelect: false
    // };
  }

  TUTr_Filter_Stores_Load() {
    this._HelperService.ToggleField = true;
    var OwnerKey = this._HelperService.UserAccount.AccountId;
    if (this._HelperService.UserAccount.AccountTypeCode != this._HelperService.AppConfig.AccountType.Merchant) {
      OwnerKey = this._HelperService.UserOwner.AccountId;
    }
    var PData =
    {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      SearchCondition: this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, OwnerKey, '='),
      Offset: 0,
      Limit: 1000,
    }
    var data: any[] = [];
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V2.System, PData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          if (_Response.Result.Data != undefined) {
            this.BusinessCategories = _Response.Result.Data;
            // data.push({
            //   id: '0',
            //   text: 'Select Stores',
            //   disabled: true
            // })
            this.ShowCategorySelector = false;
            this._ChangeDetectorRef.detectChanges();
            for (let index = 0; index < this.BusinessCategories.length; index++) {
              const element = this.BusinessCategories[index];
              data.push(
                {
                  id: element.ReferenceId,
                  key: element.ReferenceKey,
                  text: element.Name,
                  disabled: false
                }
              );
            }
            this.ShowCategorySelector = true;
            this.S2BusinessCategories = data;
            this._ChangeDetectorRef.detectChanges();

            this._HelperService.ToggleField = false;
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
        this._HelperService.ToggleField = false;

      });
  }

  SubscriptionList_RowSelected(ReferenceData) {
    // this.displayDetails = true
    // this._HelperService.SaveStorage(
    //   "cashoutdetails",
    //   {
    //     ReferenceKey: ReferenceData.ReferenceKey,
    //     ReferenceId: ReferenceData.ReferenceId,
    //     DisplayName: ReferenceData.DisplayName,
    //     AccountTypeCode: this._HelperService.AppConfig.AccountType.PosTerminal,
    //   }
    // );

    // this._HelperService.AppConfig.ActiveReferenceKey =
    //   ReferenceData.ReferenceKey;
    // this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;
    // this.CurrentData = ReferenceData
    // console.log("this.CurrentData", this.CurrentData);
    this.clicked()
  }

  Form_EditSubscription: FormGroup;

  Form_EditSubscription_Close() {
    // var backdrop: HTMLElement = document.getElementById("backdrop");
    // $(this.divView.nativeElement).removeClass('show');
    // backdrop.classList.remove("show");
  }
  Form_EditSubscription_Load() {
    // this.Form_EditSubscription = this._FormBuilder.group({
    //   OperationType: 'new',
    //   AccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
    //   AccountId: this._HelperService.AppConfig.ActiveReferenceId,
    //   Task: this._HelperService.AppConfig.Api.ThankUCash.updatestore,
    //   Key: [null, Validators.required],
    // });
  }

  Form_EditSubscription_Process(val) {
    const pData = {
      task: ''
    };

    // let _OResponse: Observable<OResponse>;
    // _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, pData);
    // _OResponse.subscribe(
    //   _Response => {
    //     this._HelperService.IsFormProcessing = false;
    //     if (_Response.Status == this._HelperService.StatusSuccess) {

    //     }

    //   },
    //   _Error => {
    //     this._HelperService.IsFormProcessing = false;
    //     this._HelperService.HandleException(_Error);
    //   });
  }

  openSubscriptionModal() {
    this._HelperService.OpenModal('Pay_subscription_Modal');
    this.createTransaction();
    this._NumberOfStores =1;
  }

  public MerchantsList_Config: OList;

  MerchantsList_Setup() {
    this.MerchantsList_Config = {
      Id: null,
      Task: 'getaccountsubscriptionstorewise',
      Location: this._HelperService.AppConfig.NetworkLocation.V3.AccountSubscription,
      Title: "Subscription",
      StatusType: "default",
      Type: this._HelperService.AppConfig.TerminalTypes.all,
      AccountId: this._HelperService.UserAccount.AccountId,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      DefaultSortExpression: "CreatedDate desc",
      SearchCondition: "",
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict(''),
      TableFields: [
        {
          DisplayName: "Created Date",
          SystemName: "CreatedDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          IsDateSearchField: true,
          NavigateField: "ReferenceKey",
        },
        {
          DisplayName: "Time",
          SystemName: "time",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Amount",
          SystemName: "ActualPrice",
          DataType: this._HelperService.AppConfig.DataType.Decimal,
          Class: "",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Transaction ID",
          SystemName: "transactionId",
          DataType: this._HelperService.AppConfig.DataType.Number,
          Class: "",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        }
      ],
      Sort:
      {
        SortDefaultName: null,
        SortDefaultColumn: 'CreatedDate',
        SortName: null,
        SortColumn: null,
        SortOrder: 'desc',
        SortOptions: [],
      },
    };
    this.MerchantsList_Config = this._DataHelperService.List_Initialize(
      this.MerchantsList_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Subscription,
      this.MerchantsList_Config
    );

    this.MerchantsList_GetData(true);
  }
  MerchantsList_ToggleOption(event: any, Type: any) {
    if (Type == this._HelperService.AppConfig.ListToggleOption.SalesRange) {
      event.data = {
        SalesMin: this.MinAmount,
        SalesMax: this.MaxAmount,
      }
    }
    if (event != null) {
      for (let index = 0; index < this.MerchantsList_Config.Sort.SortOptions.length; index++) {
        const element = this.MerchantsList_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.MerchantsList_Config


    );

    this.MerchantsList_Config = this._DataHelperService.List_Operations(
      this.MerchantsList_Config,
      event,
      Type
    );

    if (
      (this.MerchantsList_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.MerchantsList_GetData(true);
    }

  }
  MerchantsList_GetData(isFirstTime?: boolean) {

    var TConfig = this._DataHelperService.List_GetData(
      this.MerchantsList_Config
    );
    this.MerchantsList_Config = TConfig;

  }

  timeout = null;
  MerchantsList_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.MerchantsList_Config.Sort.SortOptions.length; index++) {
          const element = this.MerchantsList_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.MerchantsList_Config


      );

      this.MerchantsList_Config = this._DataHelperService.List_Operations(
        this.MerchantsList_Config,
        event,
        Type
      );

      if (
        (this.MerchantsList_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.MerchantsList_GetData(true);
      }
    }, this._HelperService.AppConfig.SearchInputDelay);

  }

  SetOtherFilters(): void {
    this.MerchantsList_Config.SearchBaseConditions = [];
    // this.MerchantsList_Config.SearchBaseCondition = null;

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    // if (CurrentIndex != -1) {
    //   this.MerchantsList_Filter_Stores_Selected = null;
    //   this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    // }
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {

    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.MerchantsList_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.MerchantsList_GetData(true);
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_NoSort(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.MerchantsList_Config);

    this.SetOtherFilters();

    this.MerchantsList_GetData(true);
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      // input: "text",
      html:
        '<input id="swal-input1" class="swal2-input" placeholder="filter name" class="swal2-input">' +
        '<label class="mg-x-5 mg-t-5">Private</label><input type="radio" checked name="swal-input2" id="swal-input2" class="">' +
        '<label class="mg-x-5 mg-t-5">Public</label><input type="radio" name="swal-input2" id="swal-input3" class="">',
      focusConfirm: false,
      preConfirm: () => {
        return {
          filter: document.getElementById('swal-input1')['value'],
          private: document.getElementById('swal-input2')['checked']
        }
      },
      // inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      // inputAttributes: {
      //   autocapitalize: "off",
      //   autocorrect: "off",
      //   maxLength: "4",
      //   minLength: "4",
      // },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {

        if (result.value.filter.length < 5) {
          this._HelperService.NotifyError('Enter filter name length greater than 4');
          return;
        }

        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value.filter);

        var AccessType: number = result.value.private ? 0 : 1;
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Subscription,
          AccessType
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Subscription
        );
        this._FilterHelperService.SetMerchantConfig(this.MerchantsList_Config);
        this.MerchantsList_GetData(true);

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    if (this.MinAmount && this.MaxAmount && this.MinAmount > this.MaxAmount) {
      this._HelperService.NotifyError("Minimum Amount should be less than Maximum Amount");
      return;
    }
    this.SetSearchRanges();
    this._HelperService.MakeFilterSnapPermanent();
    this.MerchantsList_GetData();

    if (ButtonType == 'Sort') {
      $("#MerchantsList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#MerchantsList_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this.MinAmount = this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit;
    this.MaxAmount = this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit;
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.MerchantsList_Config);
    this.SetOtherFilters();

    this.MerchantsList_GetData(true);

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    // this.MerchantsList_Filter_Stores_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }

  SetSalesRanges(): void {
    this.MinAmount = this.MerchantsList_Config.SalesRange.SalesMin;
    this.MaxAmount = this.MerchantsList_Config.SalesRange.SalesMax;
  }

  SetSearchRanges(): void {
    this.MerchantsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('ActualPrice', this.MerchantsList_Config.SearchBaseConditions);
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'ActualPrice', this.MinAmount, this.MaxAmount);
    if (this.MinAmount == this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit && this.MaxAmount == this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit) {
      this.MerchantsList_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.MerchantsList_Config.SearchBaseConditions);
    }
    else {
      this.MerchantsList_Config.SearchBaseConditions.push(SearchCase);
    }
  }

  GotoAddMerchant() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.UserOnboarding.Merchant
    ]);
  }

  MerchantsList_ListTypeChange(Type) {
    this.ListType = Type;
    this.MerchantsList_Setup();
  }


  public TransactionId = '';
  public PayStackKey = '';
  public _TransactionReference = null;
  public _Amount = 0;

  createTransaction() {
    const percente = (5 / 100) * Number(50000);
    this._Amount = Number(50000) + Math.round(percente);
    var Ref = this.generateRandomNumber();
    this._TransactionReference = 'Tp' + '_' + Ref;
  }

  generateRandomNumber() {
    var TrRef = Math.floor(100000 + Math.random() * (999999 + 1 - 100000));
    return TrRef;
  }

  paymentDone(ref: any) {
    this.TransactionId = ref.trans;
    if (ref != undefined && ref != null && ref != '') {
      if (ref.status == 'success') {
        this.checkoutPayment();
      }
      else {
        this._HelperService.NotifyError('Payment failed');
      }
    }
    else {
      this._HelperService.NotifyError('Payment failed');
    }
  }

  checkoutPayment() {
    this._HelperService.IsFormProcessing = true;
    var PostData = {
      Task: "updatemonthlysubscription",
      ReferenceId: this._HelperService.UserAccount.AccountId,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      PaymentReference: this._TransactionReference,
      TransactionId: this.TransactionId,
      TotalAmount: this._NumberOfStores * this.monthlyAmount,
      NoOfStores: this._NumberOfStores,
      AmountPerStore: this.monthlyAmount
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.AccountSubscription, PostData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._Amount = 0;
          this._TransactionReference = null;
          this._HelperService.NotifySuccess(_Response.Message);
          this.MerchantsList_GetData(true);
          let temp = this._HelperService.GetStorage("hca")
          temp.Subscription.IsSubscriptionActive = true
          this._HelperService.SaveStorage("hca", temp)
          this._HelperService.IsSubscriptionActive = true

        } else {
          this._HelperService.NotifySuccess(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      }
    );
  }

  paymentCancel() {
    this._HelperService.NotifyError("Payment has been cancelled.")
  }


  openAssignStoreModal() {
    this._HelperService.OpenModal('subscription_Modal');
  }

  getstorenotsubscribedlist() {
    this.allStores = [];
    const pData = {
      "Task": "getstorenotsubscribed",
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.AccountSubscription, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          if (_Response.Result && _Response.Result.Data && _Response.Result.Data.length > 0) {
            _Response.Result.Data.forEach(element => {
              this.allStores.push({ id: element.ReferenceId, text: element.DisplayName, key: element.ReferenceKey });
            });
          }
        }

      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });

  }

  storeArr: any = []
  changeValue(e, store) {
    if (e.target.checked) {
      this.storeArr.push({
        ReferenceId: store.id,
        ReferenceKey: store.key
      })
    }
    else {
      this.storeArr.splice(this.storeArr.findIndex(a => a.ReferenceId === store.id), 1)
    }
    console.log(this.storeArr);
  }

  updateSubscriptionStore() {
    if (this.MerchantsList_Config.InActiveSubscription && (this.MerchantsList_Config.InActiveSubscription < this.storeArr.length)) {
      this._HelperService.NotifyError("Please select store as per your unused subscription");
      return;
    }
    const pData = {
      Task: "updatesubscriptionstorewise",
      AccountId: this._HelperService.UserAccount.AccountId,
      AccountKey: this._HelperService.UserAccount.AccountKey,
      Stores: this.storeArr
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.AccountSubscription, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._HelperService.NotifySuccess(_Response.Message);
          this.getstorenotsubscribedlist();
          this.MerchantsList_GetData();
          this.storeArr = [];
          this._HelperService.CloseModal('subscription_Modal');
        } else {
          this._HelperService.NotifySuccess(_Response.Message);
        }

      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }
}
