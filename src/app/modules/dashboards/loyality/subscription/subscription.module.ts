import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubscriptionComponent } from './subscription.component';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { Select2Module } from 'ng2-select2';
import { NgxPaginationModule } from 'ngx-pagination';
import { Daterangepicker } from 'ng2-daterangepicker';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Angular4PaystackModule } from 'angular4-paystack';

const routes: Routes = [
  { path: '', component: SubscriptionComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubscriptionRoutingModule { }

@NgModule({
  declarations: [SubscriptionComponent],
  imports: [
    CommonModule,
    TranslateModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    FormsModule,
    SubscriptionRoutingModule,
    Angular4PaystackModule,
    ReactiveFormsModule
  ]
})
export class SubscriptionModule { }
