import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import * as Feather from "feather-icons";
import swal from "sweetalert2";
declare var $: any;

import {
  DataHelperService,
  HelperService,
  OList,
  OSelect,
  FilterHelperService,
  OResponse,
} from "../../../../service/service";
import { Observable } from 'rxjs';
import { ChangeContext } from 'ng5-slider';
import * as moment from 'moment-timezone';

@Component({
  selector: "tu-tucashiers",
  templateUrl: "./tucashiers.component.html",
})
export class TuCashiersComponent implements OnInit {
  public ResetFilterControls: boolean = true;
  public InactiveDays: string;
  public SortCashierTransaction: boolean = false
  public SortCashierSales: boolean = false
  public SortAvgTransaction: boolean = false


  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
    public _FilterHelperService: FilterHelperService
  ) {
    this._HelperService.showAddNewPosBtn = false;
    this._HelperService.showAddNewStoreBtn = true;
    this._HelperService.showAddNewCashierBtn = false;
    this._HelperService.showAddNewSubAccBtn = false;


  }



  ngOnInit() {
    this._HelperService.StopClickPropogation();
    Feather.replace();
    this.TodayStartTime = moment().startOf('day');
    this.TodayEndTime = moment().endOf('day');
    this.Cashiers_Setup();
    this.Cashiers_Filter_Owners_Load();
    this.InitColConfig();
    this.TUTr_Filter_Stores_Load();
    this.GetCashierOverviewLite();
    // this._HelperService.StopClickPropogation();

  }
  CashierReferesh() {
    this.GetCashierOverviewLite();

  }

  Form_AddUser_Open() {
    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelAcquirer
        .MerchantOnboarding,
    ]);
  }

  ToogleType(type: string): void {
    this.Cashiers_Config.Type = type;
    this.Cashiers_GetData();
  }

  //#region columnConfig

  TempColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  ColumnConfig: any = [
    {
      Name: "Status",
      Value: true,
    },
    {
      Name: "City",
      Value: true,
    },
    {
      Name: "Contact",
      Value: true,
    },
    {
      Name: "Stores",
      Value: true,
    },
    {
      Name: "Reward",
      Value: true,
    },
    {
      Name: "POS",
      Value: true,
    },
    {
      Name: "ActivePOS",
      Value: true,
    },
    {
      Name: "RM",
      Value: true,
    },
    {
      Name: "Added",
      Value: true,
    },
  ];

  InitColConfig() {
    var MerchantTableConfig = this._HelperService.GetStorage("BMerchantTable");
    var ColConfigExist: boolean =
      MerchantTableConfig != undefined && MerchantTableConfig != null;
    if (ColConfigExist) {
      this.ColumnConfig = MerchantTableConfig.config;
      this.TempColumnConfig = this._HelperService.CloneJson(
        MerchantTableConfig.config
      );
    }
  }

  OpenEditColModal() {
    this._HelperService.OpenModal("EditCol");
  }

  SaveEditCol() {
    this.ColumnConfig = this._HelperService.CloneJson(this.TempColumnConfig);
    this._HelperService.SaveStorage("BMerchantTable", {
      config: this.ColumnConfig,
    });
    this._HelperService.CloseModal("EditCol");
  }

  //#endregion

  //#region merchantlist

  public Cashiers_Config: OList;
  Cashiers_Setup() {
    this.Cashiers_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetCashiers,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      Title: "All Cashiers",
      StatusType: "default",
      Type: this._HelperService.AppConfig.CustomerTypes.all,
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      DefaultSortExpression: "CreateDate desc",
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '=='),
      TableFields: [
        {
          DisplayName: " Name",
          SystemName: "DisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Mobile No",
          SystemName: "MobileNumber",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-center",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Email Address",
          SystemName: "EmailAddress",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Total Spent",
          SystemName: "TotalInvoiceAmount",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Visits",
          SystemName: "TotalTransaction",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Rewards Earned",
          SystemName: "TucRewardAmount",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Redeemed",
          SystemName: "RedeemAmount",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: 'Last Visit',
          SystemName: "LastTransactionDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          IsDateSearchField: true,
          NavigateField: "ReferenceKey",
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: "CreateDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
      ]
    };
    this.Cashiers_Config = this._DataHelperService.List_Initialize(
      this.Cashiers_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Customer,
      this.Cashiers_Config
    );

    this.Cashiers_GetData();
  }
  Cashiers_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.Cashiers_Config.Sort.SortOptions.length; index++) {
        const element = this.Cashiers_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.Cashiers_Config


    );

    this.Cashiers_Config = this._DataHelperService.List_Operations(
      this.Cashiers_Config,
      event,
      Type
    );

    if (
      (this.Cashiers_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.Cashiers_GetData();
    }

  }
  Cashiers_GetData() {
    this.GetOverviews(this.Cashiers_Config, this._HelperService.AppConfig.Api.ThankUCash.TransactionOverviews.getcashiersoverview);
    var TConfig = this._DataHelperService.List_GetData(
      this.Cashiers_Config
    );
    this.Cashiers_Config = TConfig;
  }
  Cashiers_RowSelected(ReferenceData) {
    this._HelperService.SaveStorage(
      this._HelperService.AppConfig.Storage.ActiveCashier,
      {
        ReferenceKey: ReferenceData.ReferenceKey,
        ReferenceId: ReferenceData.ReferenceId,
        DisplayName: ReferenceData.DisplayName,
        AccountTypeCode: this._HelperService.AppConfig.AccountType.Cashier,
      }
    );

    this._HelperService.AppConfig.ActiveReferenceKey =
      ReferenceData.ReferenceKey;
    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.CashierDetails
        .Overview, ReferenceData.ReferenceKey, ReferenceData.ReferenceId
    ]);


  }

  //#endregion
  TUTr_InvoiceRangeMinAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit;
  TUTr_InvoiceRangeMaxAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit;
  TUTr_InvoiceRange_OnChange(changeContext: ChangeContext): void {
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'TotalInvoiceAmount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
    this.Cashiers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Cashiers_Config.SearchBaseConditions);
    this.TUTr_InvoiceRangeMinAmount = changeContext.value;
    this.TUTr_InvoiceRangeMaxAmount = changeContext.highValue;
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'TotalInvoiceAmount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
    if (this.TUTr_InvoiceRangeMinAmount == this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit && this.TUTr_InvoiceRangeMaxAmount == this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit) {
      this.Cashiers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Cashiers_Config.SearchBaseConditions);
    }
    else {
      this.Cashiers_Config.SearchBaseConditions.push(SearchCase);
    }
    this.Cashiers_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }

  TUTr_RedeemRangeMinAmount: number = this._HelperService.AppConfig.RangeRedeemAmountMinimumLimit;
  TUTr_RedeemRangeMaxAmount: number = this._HelperService.AppConfig.RangeRedeemAmountMaximumLimit;
  TUTr_RedeemRange_OnChange(changeContext: ChangeContext): void {
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'RedeemAmount', this.TUTr_RedeemRangeMinAmount, this.TUTr_RedeemRangeMaxAmount);
    this.Cashiers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Cashiers_Config.SearchBaseConditions);
    this.TUTr_RedeemRangeMinAmount = changeContext.value;
    this.TUTr_RedeemRangeMaxAmount = changeContext.highValue;
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'RedeemAmount', this.TUTr_RedeemRangeMinAmount, this.TUTr_RedeemRangeMaxAmount);
    if (this.TUTr_RedeemRangeMinAmount == this._HelperService.AppConfig.RangeRedeemAmountMinimumLimit && this.TUTr_RedeemRangeMaxAmount == this._HelperService.AppConfig.RangeRedeemAmountMaximumLimit) {
      this.Cashiers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Cashiers_Config.SearchBaseConditions);
    }
    else {
      this.Cashiers_Config.SearchBaseConditions.push(SearchCase);
    }
    this.Cashiers_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }

  TUTr_RewardRangeMinAmount: number = this._HelperService.AppConfig.RangeRewardAmountMinimumLimit;
  TUTr_RewardRangeMaxAmount: number = this._HelperService.AppConfig.RangeRewardAmountMaximumLimit;
  TUTr_RewardRange_OnChange(changeContext: ChangeContext
  ): void {
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'TucRewardAmount', this.TUTr_RewardRangeMinAmount, this.TUTr_RewardRangeMaxAmount);
    this.Cashiers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Cashiers_Config.SearchBaseConditions);
    this.TUTr_RewardRangeMinAmount = changeContext.value;
    this.TUTr_RewardRangeMaxAmount = changeContext.highValue;
    var SearchCase = this._HelperService.GetSearchConditionRange('', 'TucRewardAmount', this.TUTr_RewardRangeMinAmount, this.TUTr_RewardRangeMaxAmount);
    if (this.TUTr_RewardRangeMinAmount == this._HelperService.AppConfig.RangeRewardAmountMinimumLimit && this.TUTr_RewardRangeMaxAmount == this._HelperService.AppConfig.RangeRewardAmountMaximumLimit) {
      this.Cashiers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.Cashiers_Config.SearchBaseConditions);
    }
    else {
      this.Cashiers_Config.SearchBaseConditions.push(SearchCase);
    }
    this.Cashiers_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
  }


  //#region OwnerFilter

  public Cashiers_Filter_Owners_Option: Select2Options;
  public Cashiers_Filter_Owners_Selected = null;
  Cashiers_Filter_Owners_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true,
        },
      ],
    };
    // _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'AccountTypeCode', this._HelperService.AppConfig.DataType.Text,
    //   [
    //     this._HelperService.AppConfig.AccountType.Merchant,
    //     this._HelperService.AppConfig.AccountType.Acquirer,
    //     this._HelperService.AppConfig.AccountType.PGAccount,
    //     this._HelperService.AppConfig.AccountType.PosAccount
    //   ]
    //   , '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.Cashiers_Filter_Owners_Option = {
      placeholder: "Sort by Referrer",
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  Cashiers_Filter_Owners_Change(event: any) {
    this._HelperService.Update_CurrentFilterSnap(
      event,
      this._HelperService.AppConfig.ListToggleOption.Other,
      this.Cashiers_Config,
      this._HelperService.AppConfig.OtherFilters.Merchant.Owner
    );

    this.OwnerEventProcessing(event);

  }

  OwnerEventProcessing(event: any): void {
    if (event.value == this.Cashiers_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.Cashiers_Filter_Owners_Selected,
        "="
      );
      this.Cashiers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.Cashiers_Config.SearchBaseConditions
      );
      this.Cashiers_Filter_Owners_Selected = null;
    } else if (event.value != this.Cashiers_Filter_Owners_Selected) {
      var SearchCase = this._HelperService.GetSearchConditionStrict(
        "",
        "ReferenceKey",
        this._HelperService.AppConfig.DataType.Text,
        this.Cashiers_Filter_Owners_Selected,
        "="
      );
      this.Cashiers_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(
        SearchCase,
        this.Cashiers_Config.SearchBaseConditions
      );
      this.Cashiers_Filter_Owners_Selected = event.data[0].ReferenceKey;
      this.Cashiers_Config.SearchBaseConditions.push(
        this._HelperService.GetSearchConditionStrict(
          "",
          "ReferenceKey",
          this._HelperService.AppConfig.DataType.Text,
          this.Cashiers_Filter_Owners_Selected,
          "="
        )
      );
    }

    this.Cashiers_ToggleOption(
      null,
      this._HelperService.AppConfig.ListToggleOption.ResetOffset
    );
  }

  //#endregion

  SetOtherFilters(): void {
    this.Cashiers_Config.SearchBaseConditions = [];
    this.Cashiers_Config.SearchBaseCondition = null;

    var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.Merchant.Owner));
    if (CurrentIndex != -1) {
      this.Cashiers_Filter_Owners_Selected = null;
      this.OwnerEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
    }
  }

  //#region filterOperations

  Active_FilterValueChanged(event: any) {
    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetMerchantConfig(this.Cashiers_Config);

    //#region setOtherFilters
    this.SetOtherFilters();
    //#endregion

    this.Cashiers_GetData();
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Merchant(Type, index);
    this._FilterHelperService.SetMerchantConfig(this.Cashiers_Config);

    this.SetOtherFilters();

    this.Cashiers_GetData();
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      input: "text",
      inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
      inputAttributes: {
        autocapitalize: "off",
        autocorrect: "off",
        maxLength: "4",
        minLength: "4",
      },
      inputValidator: function (value) {
        if (value === '' || value.length < 4) {
          return 'Enter filter name length greater than 4!'
        }
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._FilterHelperService._BuildFilterName_Merchant(result.value);
        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.Customer
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.Customer
        );
        this._FilterHelperService.SetMerchantConfig(this.Cashiers_Config);
        this.Cashiers_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.Cashiers_GetData();

    if (ButtonType == 'Sort') {
      $("#Cashiers_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#Cashiers_fdropdown").dropdown('toggle');
    }

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.Cashiers_Config);
    this.SetOtherFilters();

    this.Cashiers_GetData();

    this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }

  //#endregion

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();

    this.Cashiers_Filter_Owners_Load();

    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }
  //#region SalesOverview 
  public OverviewData = { Total: 0, New: 0, Repeating: 0, Lost: 0 };
  GetOverviews(ListOptions: any, Task: string): any {
    this._HelperService.IsFormProcessing = true;

    ListOptions.SearchCondition = '';
    ListOptions = this._DataHelperService.List_GetSearchCondition(ListOptions);
    if (ListOptions.ActivePage == 1) {
      ListOptions.RefreshCount = true;
    }
    var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;

    if (ListOptions.Sort.SortDefaultName) {
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
    }

    if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
      if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
        SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
      }
      else {
        SortExpression = ListOptions.Sort.SortColumn + ' desc';
      }
    }


    var pData = {
      Task: Task,
      TotalRecords: ListOptions.TotalRecords,
      Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
      Limit: ListOptions.PageRecordLimit,
      RefreshCount: ListOptions.RefreshCount,
      SearchCondition: ListOptions.SearchCondition,
      SortExpression: SortExpression,
      Type: ListOptions.Type,
      ReferenceKey: ListOptions.ReferenceKey,
      StartDate: ListOptions.StartDate,
      EndDate: ListOptions.EndDate,
      ReferenceId: ListOptions.ReferenceId,
      SubReferenceId: ListOptions.SubReferenceId,
      SubReferenceKey: ListOptions.SubReferenceKey,
      AccountId: ListOptions.AccountId,
      AccountKey: ListOptions.AccountKey,
      ListType: ListOptions.ListType,
      IsDownload: false,
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.OverviewData = _Response.Result as any;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });


  }
  //#endregion
  //#region Store Filter
  public TUTr_Filter_Store_Selected: any;
  public ToggleStoreSelect: boolean = false;
  public TUTr_Filter_Store_Option: Select2Options;
  public StoreReferenceId: number;
  public StoreReferenceKey: string;
  TUTr_Filter_Stores_Load() {
    var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
    var _Select: OSelect = {
      Task: this._HelperService.AppConfig.Api.Core.GetUserAccountsLite,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SearchCondition: "",
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },
        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: "AccountTypeCode",
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: "=",
          SearchValue: this._HelperService.AppConfig.AccountType.Store
        }
      ]
    };

    var OwnerKey = this._HelperService.UserAccount.AccountId;
    if (this._HelperService.UserAccount.AccountTypeCode != this._HelperService.AppConfig.AccountType.Merchant) {
      OwnerKey = this._HelperService.UserOwner.AccountId;
    }
    _Select.SearchCondition = this._HelperService.GetSearchConditionStrict('', 'OwnerId', this._HelperService.AppConfig.DataType.Number, OwnerKey, '=');
    var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.TUTr_Filter_Store_Option = {
      placeholder: 'Select Store',
      ajax: _Transport,
      multiple: false,
      allowClear: true,
    };
  }
  TUTr_Filter_Stores_Change(event: any) {
    if (event.value == this.TUTr_Filter_Store_Selected) {
      this.StoreReferenceId = 0;
      this.StoreReferenceKey = null;
      this.TUTr_Filter_Store_Selected = 0;
    }
    else if (event.value != this.TUTr_Filter_Store_Selected) {
      this.StoreReferenceId = event.data[0].ReferenceId;
      this.StoreReferenceKey = event.data[0].ReferenceKey;

      this.TUTr_Filter_Store_Selected = event.value;
    }
    this.GetCashierOverviewLite();
    setTimeout(() => {
      this._HelperService.ToggleField = false;
    }, 500);
  }
  //#endregion
  DateChanged(event: any, Type: any): void {
    this.TodayStartTime = event.start;
    this.TodayEndTime = event.end;
    this.GetCashierOverviewLite();
  }
  TodayStartTime = null;
  TodayEndTime = null;
  HPerformance: number = 0;
  GPerformance: number = 0;
  APerformance: number = 0;
  BPerformance: number = 0;

  GetCashierOverviewLite() {
    this._HelperService.IsFormProcessing = true;
    this.HPerformance = 0;
    this.GPerformance = 0;
    this.APerformance = 0;
    this.BPerformance = 0;
    var Data = {
      Task: 'getcashiersoverview',
      StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
      EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      StoreReferenceId: this.StoreReferenceId,
      StoreReferenceKey: this.StoreReferenceKey,

    };
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Analytics, Data);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._AccountOverview = _Response.Result as OAccountOverview;



          for (let index = 0; index < this._AccountOverview.Cashiers.length; index++) {
            const element = this._AccountOverview.Cashiers[index];

            switch (element.PerformanceName) {
              case 'Below Average': this.BPerformance = this.BPerformance + 1;

                break;

              case 'Average': this.APerformance = this.APerformance + 1;

                break;

              case 'Good': this.GPerformance = this.GPerformance + 1;

                break;
              case 'Excellent': this.HPerformance = this.HPerformance + 1;

                break;

              default:
                break;
            }

          }



          // var sortable = [];
          // for (var vehicle in this._AccountOverview.Cashiers) {
          //   sortable.push([this._AccountOverview.Cashiers, this._AccountOverview.Cashiers[vehicle]]);
          // }
          // sortable.sort(function (a, b) {
          //   return a.Transactions - b.Transactions;
          // });

        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }


  sortedTransaction: any[] = [];
  sortedTransaction2: any[] = [];
  TransactionSort(): void {
    this.sortedTransaction = [];
    this.SortCashierTransaction = !this.SortCashierTransaction;

    if (this.SortCashierTransaction) {
      this.sortedTransaction = this._AccountOverview.Cashiers.sort((a, b) => {
        return b['Transactions'] - a['Transactions'];
      });
      
    } else {

      this.sortedTransaction2 = this._AccountOverview.Cashiers.sort((a, b) => {
        return a['Transactions'] - b['Transactions'];
      });

    }

  }
  sortedSales: any[] = [];
  sortedSales2: any[] = [];
  SalesSort(): void {
    this.sortedSales = [];
    this.SortCashierSales = !this.SortCashierSales;

    if (this.SortCashierSales) {
      this.sortedSales = this._AccountOverview.Cashiers.sort((a, b) => {
        return b['InvoiceAmount'] - a['InvoiceAmount'];
      });
      
    } else {

      this.sortedSales2 = this._AccountOverview.Cashiers.sort((a, b) => {
        return a['InvoiceAmount'] - b['InvoiceAmount'];
      });

    }

  }
  AvgTransaction: any[] = [];
  AvgTransaction2: any[] = [];
  AvgTransactionSort(): void {
    this.AvgTransaction = [];
    this.SortAvgTransaction = !this.SortAvgTransaction;

    if (this.SortAvgTransaction) {
      this.AvgTransaction = this._AccountOverview.Cashiers.sort((a, b) => {
        return b['AverageTransactions'] - a['AverageTransactions'];
      });
      
    } else {

      this.AvgTransaction2 = this._AccountOverview.Cashiers.sort((a, b) => {
        return a['AverageTransactions'] - b['AverageTransactions'];
      });

    }

  }


  public _AccountOverview: OAccountOverview =
    {
      Total: 0,
      Transactions: 0,
      Customers: 0,
      InvoiceAmount: 0,
      CardInvoiceAmount: 0,
      CashInvoiceAmount: 0,
      WorkingDays: 0,
      AverageCustomers: 0,
      AverageTransactions: 0,
      InactiveDays: 0,
      Cashiers: []
    }

}

export class OAccountOverview {
  public Total: any;
  public Transactions: any;
  public Customers: any;
  public InvoiceAmount: any;
  public CardInvoiceAmount: any;
  public CashInvoiceAmount: any;
  public WorkingDays: any;
  public AverageCustomers: any;
  public AverageTransactions: any;
  public InactiveDays: any;
  public Cashiers: any[];



}