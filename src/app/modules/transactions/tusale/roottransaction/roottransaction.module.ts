import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";
import { Ng2FileInputModule } from "ng2-file-input";

import { AgmCoreModule } from '@agm/core';
import { RootTransactionComponent } from './roottransaction.component';
// import { LeafletModule } from '@asymmetrik/ngx-leaflet';
const routes: Routes = [
    {
        path: "",
        component: RootTransactionComponent,
        children: [
            { path: "saleshistory", data: { permission: "getmerchant", menuoperations: "ManageMerchant",  PageName: "System.Menu.SalesHistory", accounttypecode: "merchant" }, loadChildren: "../../tusale/merchant/tusale.module#TUSaleModule" },

            // { path: "purchasehistory/:referencekey/:referenceid", data: { permission: "getmerchant", menuoperations: "ManageMerchant",  PageName: "System.Menu.DealHistory", accounttypecode: "merchant" }, loadChildren: "../../../../../../../app/Customer/modules/transactions/tusale/tudealhistory/tupayments/tupayments.module#TUPaymentsModule" },

            { path: "pendingtransaction", data: { permission: "getmerchant", menuoperations: "ManageMerchant", PageName: "System.Menu.SuspiciousTransaction",accounttypecode: "merchant" }, loadChildren: "../../../../modules/accounts/tumerchants/pendingtransaction/pendingtransaction.module#TUPendingTransactionModule" },

            
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class  RootTransactionRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        RootTransactionRoutingModule,
        // LeafletModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
    ],
    declarations: [RootTransactionComponent]
})
export class RootTransactionModule { }
