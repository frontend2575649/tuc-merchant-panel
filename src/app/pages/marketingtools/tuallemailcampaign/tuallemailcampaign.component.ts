import { Component, OnInit } from '@angular/core';
import { HelperService } from 'src/app/service/helper.service';

@Component({
  selector: 'app-tuallemailcampaign',
  templateUrl: './tuallemailcampaign.component.html',
  styleUrls: ['./tuallemailcampaign.component.css']
})
export class TuallemailcampaignComponent implements OnInit {

  public TransactionId = '';
  public PayStackKey = '';
  public _TransactionReference = null;
  public _Amount = 0;

  constructor(
    public _HelperService: HelperService,
  ) { }

  ngOnInit() {
    this.createTransaction();
  }
  createTransaction() {
    const percente = (5 / 100) * Number(5000);
    this._Amount = Number(5000) + Math.round(percente);
    var Ref = this.generateRandomNumber();
    this._TransactionReference = 'Tp' + '_' + Ref;
  }

  generateRandomNumber() {
    var TrRef = Math.floor(100000 + Math.random() * (999999 + 1 - 100000));
    return TrRef;
  }

  paymentDone(ref: any) {
    this.TransactionId = ref.trans;
    if (ref && ref.status == 'success') {
      this.checkoutPayment();
    }
    else {
      this._HelperService.NotifyError("Payment failed.")
    }
  }

  checkoutPayment() {

  }

  paymentCancel() {
    this._HelperService.NotifyError("Payment has been cancelled.")
  }
}
