import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TuallemailcampaignComponent } from './tuallemailcampaign.component';
import { Routes, RouterModule } from '@angular/router';
import { Angular4PaystackModule } from 'angular4-paystack';

const routes: Routes = [{ path: "", component: TuallemailcampaignComponent }];

@NgModule({
  declarations: [TuallemailcampaignComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    Angular4PaystackModule
  ]
})
export class TuallemailcampaignModule { }
