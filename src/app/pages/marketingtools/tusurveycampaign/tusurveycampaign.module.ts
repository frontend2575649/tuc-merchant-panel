import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TusurveycampaignComponent } from './tusurveycampaign.component';
import { RouterModule, Routes } from '@angular/router';
const routes: Routes = [{ path: "", component: TusurveycampaignComponent }];

@NgModule({
  declarations: [TusurveycampaignComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class TusurveycampaignModule { }
