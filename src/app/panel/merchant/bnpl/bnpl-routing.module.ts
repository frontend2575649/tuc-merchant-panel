import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OverviewComponent } from './overview/overview.component';
import { PendingloansComponent } from './pendingloans/pendingloans.component';
import { RedeemedloansComponent } from './redeemedloans/redeemedloans.component';
import { InvoiceComponent } from './settlements/invoice/invoice.component';

const routes: Routes = [
  { path: 'overview',data: { permission: "dashboard", PageName: "Overview" }, component: OverviewComponent },
  { path: 'pending-loans', data: { permission: "dashboard", PageName: "Pending Loans" },component: PendingloansComponent },
  { path: 'redeemed-loans', data: { permission: "dashboard", PageName: "Redeemed Loans" },component: RedeemedloansComponent },
  { path: 'settlements', data: { permission: "dashboard", PageName: "Settlements" },loadChildren:"./settlements/settlements.module#SettlementsModule" },
  { path: "invoice",  component: InvoiceComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BnplRoutingModule { }
