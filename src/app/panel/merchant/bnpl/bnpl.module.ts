import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";

import { BnplRoutingModule } from './bnpl-routing.module';
import { OverviewComponent } from './overview/overview.component';
import { PendingloansComponent } from './pendingloans/pendingloans.component';
import { RedeemedloansComponent } from './redeemedloans/redeemedloans.component';
import { SettlementsModule } from './settlements/settlements.module';
import { InvoiceComponent } from "./settlements/invoice/invoice.component";


@NgModule({
  declarations: [OverviewComponent, PendingloansComponent, RedeemedloansComponent, InvoiceComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    Select2Module,
    NgxPaginationModule,
    Daterangepicker,
    SettlementsModule,
    BnplRoutingModule,
    
  ]
})
export class BnplModule { }
