import { Component, OnInit } from '@angular/core';
import { Route, Router } from '@angular/router';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { HelperService, OResponse } from '../../../../service/service';
import * as cloneDeep from 'lodash/cloneDeep';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class OverviewComponent implements OnInit {

  public DateSelected = moment().startOf("day");
  overviewData: any = {};
  redeemedData: any = {};

  pendingLoanList: any = {};
  pendingSettlements: any;
  pendingLoansOverviewData: any;
  pendingSettle: any;
  ActualStartDate: any;
  ActualEndDate: any;

  constructor(
    public _HelperService: HelperService,
    public _Router: Router
  ) { }

  //#region DateChangeHandler
  DateChanged(event: any, Type: any): void {

    var ev: any = cloneDeep(event);
    this.DateSelected = moment(ev.start).startOf("day");
    this.ActualStartDate = moment(ev.start).startOf("day");
    this.ActualEndDate = moment(ev.end).endOf("day");
    this.getLoanOverview();
  }



  getLoanOverview() {
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.BNPL.GetLoanOverview,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      StartDate: this._HelperService.DateInUTC(this.ActualStartDate),
      EndDate: this._HelperService.DateInUTC(this.ActualEndDate),
    };


    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.bnpl.merchant, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.overviewData = _Response.Result as any;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });
  }



  getRedeemedLoans() {
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.BNPL.GetRedeemedLoansOverview,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      ReferenceId: this._HelperService.UserAccount.AccountId
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.bnpl.merchant, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.redeemedData = _Response.Result as any;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });
  }

  getPendingLoanOverview() {
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.BNPL.GetSettlementOverview,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      // ReferenceKey: 'testtucbnpl',
      // ReferenceId: 15
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.bnpl.merchant, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.pendingLoansOverviewData = _Response.Result;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });
  }

  GetPendingLoans(): any {

    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.BNPL.getPendingLoans,
      TotalRecords: 0,
      Offset: 0,
      Limit: 5,
      RefreshCount: 'true',
      SearchCondition: '',
      SortExpression: "ReferenceId desc",
      Type: null,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      SubReferenceId: 0,
      IsDownload: false
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.bnpl.merchant, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.pendingLoanList = _Response.Result.Data as any;
          // console.log(this.pendingLoanList);

        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }

  getPendingSettlements() {
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.BNPL.GetSettlementList,
      TotalRecords: 0,
      Offset: 0,
      Limit: 5,
      RefreshCount: 'true',
      SearchCondition: '',
      SortExpression: "LoanId desc",
      Type: null,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      SubReferenceId: 0,
      IsDownload: false
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.bnpl.merchant, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.pendingSettlements = _Response.Result.Data as any;
          this.pendingSettle = this.pendingSettlements.filter((item) => {
            item.StatusName != 'Successful'
          })
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }
  goToRequestPage() { }

  gotoActiveLoanPage() { }


  ngOnInit() {
    this.getLoanOverview();
    this.GetPendingLoans();
    this.getRedeemedLoans();
    this.getPendingSettlements();
    this.getPendingLoanOverview();
  }


}
