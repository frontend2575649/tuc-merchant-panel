import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import * as Feather from 'feather-icons';
import { type } from 'os';
import { Observable } from 'rxjs';
declare var $: any;
import swal from "sweetalert2";

import { DataHelperService, FilterHelperService, HelperService, OList, OOverviewArray, OResponse, OSelect } from '../../../../service/service';

@Component({
  selector: 'app-pendingloans',
  templateUrl: './pendingloans.component.html',
  styleUrls: ['./pendingloans.component.css']
})
export class PendingloansComponent implements OnInit {

  pendingLoansList = [];
  pendingLoansOverviewData: OOverviewArray;
  public ResetFilterControls: boolean = true;

  minLoanAmount = this._HelperService.AppConfig.minLoanAmount;
  maxLoanAmount = this._HelperService.AppConfig.maxLoanAmount;

  constructor(
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _FilterHelperService: FilterHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef,
  ) { }

  public TUTr_Config: OList;
  TUTr_Setup() {
    this.TUTr_Config =
    {
      Id: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.BNPL.getPendingLoans,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.bnpl.merchant,
      Title: 'Pending Loans',
      StatusType: null,
      ReferenceId:this._HelperService.AppConfig.ActiveOwnerId,
      ReferenceKey : this._HelperService.AppConfig.ActiveOwnerKey,
      Status: null,
      Type: null,
      IsDownload: false,
      DefaultSortExpression: "ReferenceId desc",
      Sort:
      {
        SortDefaultName: null,
        SortDefaultColumn: 'ReferenceId',
        SortName: null,
        SortColumn: null,
        SortOrder: 'desc',
        SortOptions: [],
      },
      TableFields: [
        {
          DisplayName: 'Loan ID',
          SystemName: 'ReferenceId',
          DataType: this._HelperService.AppConfig.DataType.Number,
          Show: true,
          Search: false,
          Sort: true,
        },
        {
          DisplayName: 'Approved on',
          SystemName: 'ApprovedOn',
          DataType: this._HelperService.AppConfig.DataType.Date,
          Show: true,
          Search: true,
          Sort: true,
          IsDateSearchField: true,
        },
        {
          DisplayName: 'Customer Details',
          SystemName: 'CustomerDisplayName',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: true,
          Sort: false,
        },
        {
          DisplayName: 'Loan Amount',
          SystemName: 'LoanAmount',
          DataType: this._HelperService.AppConfig.DataType.Decimal,
          Show: true,
          Search: false,
          Sort: true,
        },
        {
          DisplayName: 'TUC Fees',
          SystemName: 'TUCFees',
          DataType: this._HelperService.AppConfig.DataType.Decimal,
          Show: true,
          Search: false,
          Sort: true,
        },
        {
          DisplayName: 'Amount to be paid by TUC',
          SystemName: 'TUCAmount',
          DataType: this._HelperService.AppConfig.DataType.Text,
          Show: true,
          Search: false,
          Sort: false,
        },
      ]
    }

    this.TUTr_Config = this._DataHelperService.List_Initialize(this.TUTr_Config);
    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.PendingLoans,
      this.TUTr_Config
    );
    this.TUTr_GetData();

  }


  Active_FilterValueChanged(event: any) {
    if (event.data && event.data[0] && event.data[0].StatusOptions && event.data[0].StatusOptions.length > 0) {
      const el = event.data[0].StatusOptions.find(element => {
        if (element.code === event.data[0].Status) {
          return element;
        }
      });
      if (el) {
        event.data[0].Status = el.id;
      }
    }

    this._HelperService.Active_FilterValueChanged(event);
    this._FilterHelperService.SetStoreConfig(this.TUTr_Config);
    this.SetSalesRanges();
      //#endregion

    this.TUTr_GetData();
  }

  TUTr_GetData() {
    this.GetOverviews(this.TUTr_Config, this._HelperService.AppConfig.Api.ThankUCash.BNPL.getPendingLoans);
    var TConfig = this._DataHelperService.List_GetDataTur(this.TUTr_Config);
    this.TUTr_Config = TConfig;
  }

  RemoveFilterComponent(Type: string, index?: number): void {
    this._FilterHelperService._RemoveFilter_Store(Type, index);
    this._FilterHelperService.SetStoreConfig(this.TUTr_Config);

    //#region setOtherFilters
    this.SetSalesRanges();

    this.setOtherFilters();
    //#endregion

    this.TUTr_GetData();
  }

  setOtherFilters() {
    this.TUTr_Config.SearchBaseConditions = [];
    this.SetSearchRanges();
  }

  TUTr_ToggleOption_Date(event: any, Type: any) {
    this.TUTr_ToggleOption(event, Type);
  }
  TUTr_ToggleOption(event: any, Type: any) {
    if (event != null) {
      for (let index = 0; index < this.TUTr_Config.Sort.SortOptions.length; index++) {
        const element = this.TUTr_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }
    if (Type == this._HelperService.AppConfig.ListToggleOption.SalesRange) {
      event.data = {
        minLoanAmount: this.minLoanAmount,
        maxLoanAmount: this.maxLoanAmount
      }
    }
    this._HelperService.Update_CurrentFilterSnap(event, Type, this.TUTr_Config);
    this.TUTr_Config = this._DataHelperService.List_Operations(this.TUTr_Config, event, Type);
    if (
      (this.TUTr_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.TUTr_GetData();
    }
  }

  Save_NewFilter() {
    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
      text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
      // input: "text",
      html:
        '<input id="swal-input1" class="swal2-input" placeholder="filter name" class="swal2-input">' +
        '<label class="mg-x-5 mg-t-5">Private</label><input type="radio" checked name="swal-input2" id="swal-input2" class="">' +
        '<label class="mg-x-5 mg-t-5">Public</label><input type="radio" name="swal-input2" id="swal-input3" class="">',
      focusConfirm: false,
      preConfirm: () => {
        return {
          filter: document.getElementById('swal-input1')['value'],
          private: document.getElementById('swal-input2')['checked']
        }
      },
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Green,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: "Save",
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
    }).then((result) => {
      if (result.value) {

        if (result.value.filter.length < 5) {
          this._HelperService.NotifyError('Enter filter name length greater than 4');
          return;
        }

        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();


        var AccessType: number = result.value.private ? 0 : 1;
        this._FilterHelperService._BuildFilterName_MerchantSales(result.value.filter);

        this._HelperService.Save_NewFilter(
          this._HelperService.AppConfig.FilterTypeOption.PendingLoans,
          AccessType
        );

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });
  }

  Delete_Filter() {

    swal({
      position: "center",
      title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
      text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
      animation: false,
      customClass: this._HelperService.AppConfig.Alert_Animation,
      showCancelButton: true,
      confirmButtonColor: this._HelperService.AppConfig.Color_Red,
      cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
      confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
      cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

    }).then((result) => {
      if (result.value) {
        this._HelperService._RefreshUI = false;
        this._ChangeDetectorRef.detectChanges();

        this._HelperService.Delete_Filter(
          this._HelperService.AppConfig.FilterTypeOption.PendingLoans
        );
        this._FilterHelperService.SetMerchantConfig(this.TUTr_Config);
        this.TUTr_GetData();

        this._HelperService._RefreshUI = true;
        this._ChangeDetectorRef.detectChanges();
      }
    });

  }

  timeout = null;
  TUTr_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      this._HelperService.Update_CurrentFilterSnap(event, Type, this.TUTr_Config);

      this.TUTr_Config = this._DataHelperService.List_Operations(this.TUTr_Config, event, Type);
      if (event != null) {
        for (let index = 0; index < this.TUTr_Config.Sort.SortOptions.length; index++) {
          const element = this.TUTr_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }

          else {
            element.SystemActive = false;
          }

        }
      }


      if (
        (this.TUTr_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.TUTr_GetData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);

  }

  GetOverviews(ListOptions: any, Task: string): any {
    this._HelperService.IsFormProcessing = true;

    ListOptions.SearchCondition = '';
    ListOptions = this._DataHelperService.List_GetSearchCondition(ListOptions);

    if (ListOptions.ActivePage == 1) {
      ListOptions.RefreshCount = true;
    }
    var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;

    if (ListOptions.Sort.SortDefaultName) {
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
      ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
    }

    if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
      if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
        SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
      }
      else {
        SortExpression = ListOptions.Sort.SortColumn + ' desc';
      }
    }


    var pData = {
      Task: Task,
      TotalRecords: ListOptions.TotalRecords,
      Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
      Limit: ListOptions.PageRecordLimit,
      RefreshCount: ListOptions.RefreshCount,
      SearchCondition: ListOptions.SearchCondition,
      SortExpression: (SortExpression === "CreateDate desc") ? "ReferenceId desc" : SortExpression,
      Type: null,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      SubReferenceId: 0,
      IsDownload: false
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.bnpl.merchant, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.pendingLoansList = _Response.Result.Data;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });
  }

  getPendingLoanOverview() {
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.BNPL.GetPendingLoansOverview,
      ReferenceKey: this._HelperService.UserAccount.AccountKey,
      ReferenceId: this._HelperService.UserAccount.AccountId,
      // ReferenceKey: 'testtucbnpl',
      // ReferenceId: 15
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.bnpl.merchant, pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.pendingLoansOverviewData = _Response.Result;
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);

      });
  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this.SetSearchRanges();
    this._HelperService.MakeFilterSnapPermanent();
    this.TUTr_GetData();

    this.ResetFilterUI();
    this._HelperService.StopClickPropogation();

    if (ButtonType == 'Sort') {
      $("#TUTr_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#TUTr_fdropdown").dropdown('toggle');
    }
  }

  ResetFilters(event: any, Type: any): void {
    this._HelperService.ResetFilterSnap();
    this._FilterHelperService.SetMerchantConfig(this.TUTr_Config);
    this.SetSalesRanges();

    this.TUTr_GetData();
    this.ResetFilterUI();
    this._HelperService.StopClickPropogation();
  }

  ResetFilterUI(): void {
    this.ResetFilterControls = false;
    this._ChangeDetectorRef.detectChanges();
    // this is needed to set filters
    this.ResetFilterControls = true;
    this._ChangeDetectorRef.detectChanges();
  }

  SetSearchRanges(): void {
    this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('LoanAmount', this.TUTr_Config.SearchBaseConditions);

    var SearchCase = this._HelperService.GetSearchConditionRange('', 'LoanAmount', this.minLoanAmount, this.maxLoanAmount);
    if (this.minLoanAmount == this._HelperService.AppConfig.minLoanAmount && this.maxLoanAmount == this._HelperService.AppConfig.maxLoanAmount) {
      this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
    }
    else {
      this.TUTr_Config.SearchBaseConditions.push(SearchCase);
    }
  }

  SetSalesRanges(): void {
    this.minLoanAmount = this._HelperService.AppConfig.minLoanAmount
    this.maxLoanAmount = this._HelperService.AppConfig.maxLoanAmount
  }


  ngOnInit() {
    Feather.replace();
    this._HelperService.StopClickPropogation();
    this.getPendingLoanOverview();
    this.TUTr_Setup();
  }
}
