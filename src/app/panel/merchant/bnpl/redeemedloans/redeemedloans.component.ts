import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import * as Feather from 'feather-icons';
import { ChangeContext } from 'ng5-slider';
import { Observable } from 'rxjs';
import swal from 'sweetalert2';
import { DataHelperService, FilterHelperService, HelperService, OList, OOverviewArray, OResponse, OSelect } from '../../../../service/service';
declare var moment: any;
declare var $: any;

@Component({
    selector: 'app-redeemedloans',
    templateUrl: './redeemedloans.component.html',
    styleUrls: ['./redeemedloans.component.css']
})
export class RedeemedloansComponent implements OnInit {

    RedeemedLoansOverviewData: OOverviewArray;
    redeemedLoansList = [];
    minLoanAmount = this._HelperService.AppConfig.minLoanAmount;
    maxLoanAmount = this._HelperService.AppConfig.maxLoanAmount;

    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _FilterHelperService: FilterHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef,
    ) {
    }

    public ResetFilterControls: boolean = true;
    public AcquirerId = 0;
    ngOnInit() {
        Feather.replace();
        this._HelperService.StopClickPropogation();
        this.getRedeemedLoanOverview();
        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
            this._HelperService.AppConfig.ActiveReferenceId = params['referenceid'];
            if (this._HelperService.AppConfig.ActiveReferenceKey == null && this._HelperService.AppConfig.ActiveReferenceId == null) {
                if (this._HelperService.AppConfig.ActiveOwnerId != null) {
                    this.AcquirerId = this._HelperService.AppConfig.ActiveOwnerId;
                    this.TUTr_Setup();
                }
                else {
                    this._Router.navigate([this._HelperService.AppConfig.Pages.System.NotFound]);
                }
            } else {
                this.AcquirerId = this._HelperService.AppConfig.ActiveReferenceId;

                this._HelperService.Get_UserAccountDetails(true);
                this.TUTr_Setup();
            }
        });

    }
    SetSearchRanges(): void {
        this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('LoanAmount', this.TUTr_Config.SearchBaseConditions);
    
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'LoanAmount', this.minLoanAmount, this.maxLoanAmount);
        if (this.minLoanAmount == this._HelperService.AppConfig.minLoanAmount && this.maxLoanAmount == this._HelperService.AppConfig.maxLoanAmount) {
          this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
        }
        else {
          this.TUTr_Config.SearchBaseConditions.push(SearchCase);
        }
      }
    

    SetSalesRanges(): void {
        this.minLoanAmount = this._HelperService.AppConfig.minLoanAmount
        this.maxLoanAmount = this._HelperService.AppConfig.maxLoanAmount
    }


    //#region transactions 

    public TUTr_Config: OList;
    public TuTr_Columns =
        {
            Status: true,
            Date: true,
            Account: true,
            AccountName: true,
            Mob: true,
            Biller: true,
            Amount: true,
            Source: true,
            PaymentRef: true
        }
    public TuTr_Filters_List =
        {
            Date: false,
            Sort: false,
            Status: false,
            Biller: false
        }
    TUTr_Setup() {
        var SearchCondition = undefined
        // SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'StatusCode', this._HelperService.AppConfig.DataType.Number, 'dealcode.used', '=');
        // SearchCondition = this._HelperService.GetSearchConditionStrict(SearchCondition, 'MerchantId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveOwnerId, '=')
        this.TUTr_Config =
        {
            Id: null,
            Task: this._HelperService.AppConfig.Api.ThankUCash.BNPL.getRedeemedLoans,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.bnpl.merchant,
            Sort:
            {
                SortDefaultName: null,
                SortDefaultColumn: 'ReferenceId',
                SortName: null,
                SortColumn: null,
                SortOrder: 'desc',
                SortOptions: [],
            },
            Title: "Redeemed loans",
            // AccountId: this._HelperService.AppConfig.ActiveReferenceId,
            // AccountKey: this._HelperService.AppConfig.ActiveReferenceKey,
            ReferenceId:this._HelperService.AppConfig.ActiveOwnerId,
            ReferenceKey : this._HelperService.AppConfig.ActiveOwnerKey,
            StatusType: null,
            SearchBaseCondition: SearchCondition,
            TableFields: this.TuTr_Setup_Fields(),
            DefaultSortExpression: "ReferenceId desc",
            IsDownload: true
        }

        this.TUTr_Config = this._DataHelperService.List_Initialize(this.TUTr_Config);
        this._HelperService.Active_FilterInit(
            this._HelperService.AppConfig.FilterTypeOption.RedeemedLoans,
            this.TUTr_Config
        );
        this.TUTr_GetData();
    }

    TuTr_Setup_SearchCondition() {
        this.TuTr_Columns =
        {
            Status: true,
            Date: true,
            Account: true,
            AccountName: true,
            Mob: true,
            Biller: true,
            Amount: true,
            Source: true,
            PaymentRef: true
        }
        var SearchCondition = '';
        this.TuTr_Filters_List =
        {
            Date: true,
            Sort: true,
            Status: true,
            Biller: true
        }

        return SearchCondition;
    }
    TuTr_Setup_Fields() {
        var TableFields = [];

        TableFields = [
            {
                DisplayName: "Loan ID",
                SystemName: 'ReferenceId',
                DataType: this._HelperService.AppConfig.DataType.Number,
                Show: true,
                Search: false,
                Sort: true,
                ResourceId: null,
            },
            {
                DisplayName: 'Customer Details',
                SystemName: 'CustomerDisplayName',
                DataType: this._HelperService.AppConfig.DataType.Text,
                Show: true,
                Search: true,
                Sort: false,
            },
            {
                DisplayName: 'Approved',
                SystemName: 'ApprovedOn',
                DataType: this._HelperService.AppConfig.DataType.Date,
                Show: true,
                Search: false,
                Sort: true,
            },
            {
                DisplayName: 'Redeemed on',
                SystemName: 'RedeemedOn',
                DataType: this._HelperService.AppConfig.DataType.Date,
                Show: true,
                Search: false,
                Sort: true,
                IsDateSearchField: true,
            },
            {
                DisplayName: 'Loan Details',
                SystemName: 'LoanAmount',
                DataType: this._HelperService.AppConfig.DataType.Decimal,
                Show: true,
                Search: false,
                Sort: true,
            },
            {
                DisplayName: 'TUC Fees',
                SystemName: 'TUCFees',
                DataType: this._HelperService.AppConfig.DataType.Decimal,
                Show: true,
                Search: false,
                Sort: true,
            },
            {
                DisplayName: 'Settlement',
                SystemName: 'SettlementAmount',
                DataType: this._HelperService.AppConfig.DataType.Decimal,
                Show: true,
                Search: false,
                Sort: true,
            },
            {
                DisplayName: 'Settlement Date',
                SystemName: 'SettlementDate',
                DataType: this._HelperService.AppConfig.DataType.Date,
                Show: true,
                Search: false,
                Sort: false,
            },
        ]

        return TableFields;
    }

    TUTr_ToggleOption_Date(event: any, Type: any) {
        this.TUTr_ToggleOption(event, Type);
    }

    TUTr_ToggleOption(event: any, Type: any) {
        if (event != null) {
          for (let index = 0; index < this.TUTr_Config.Sort.SortOptions.length; index++) {
            const element = this.TUTr_Config.Sort.SortOptions[index];
            if (event.SystemName == element.SystemName) {
              element.SystemActive = true;
            }
            else {
              element.SystemActive = false;
            }
          }
        }
        if (Type == this._HelperService.AppConfig.ListToggleOption.SalesRange) {
          event.data = {
            minLoanAmount: this.minLoanAmount,
            maxLoanAmount: this.maxLoanAmount
          }
        }
        this._HelperService.Update_CurrentFilterSnap(event, Type, this.TUTr_Config);
        this.TUTr_Config = this._DataHelperService.List_Operations(this.TUTr_Config, event, Type);
        if (
          (this.TUTr_Config.RefreshData == true)
          && this._HelperService.DataReloadEligibility(Type)
        ) {
          this.TUTr_GetData();
        }
      }

    timeout=null;
    TUTr_ToggleOptionSearch(event: any, Type: any) {

        clearTimeout(this.timeout);
    
        this.timeout = setTimeout(() => {
          this._HelperService.Update_CurrentFilterSnap(event, Type, this.TUTr_Config);
    
          this.TUTr_Config = this._DataHelperService.List_Operations(this.TUTr_Config, event, Type);
          if (event != null) {
            for (let index = 0; index < this.TUTr_Config.Sort.SortOptions.length; index++) {
              const element = this.TUTr_Config.Sort.SortOptions[index];
              if (event.SystemName == element.SystemName) {
                element.SystemActive = true;
              }
    
              else {
                element.SystemActive = false;
              }
    
            }
          }
    
    
          if (
            (this.TUTr_Config.RefreshData == true)
            && this._HelperService.DataReloadEligibility(Type)
          ) {
            this.TUTr_GetData();
          }
        }, this._HelperService.AppConfig.SearchInputDelay);
    
      }

    TUTr_GetData() {
        this.GetOverviews(this.TUTr_Config, this._HelperService.AppConfig.Api.ThankUCash.BNPL.getRedeemedLoans);
        var TConfig = this._DataHelperService.List_GetData(this.TUTr_Config);
        this.TUTr_Config = TConfig;
    }
    TUTr_RowSelected(ReferenceData) {
        var ReferenceKey = ReferenceData.ReferenceKey;
        this._HelperService.AppConfig.ActiveReferenceKey = ReferenceKey;
    }

    //#endregion
    //#region dropdowns 

    ResetFilterUI(): void {
        this.ResetFilterControls = false;
        this._ChangeDetectorRef.detectChanges();
        // this is needed to reset filters
        this.ResetFilterControls = true;
        this._ChangeDetectorRef.detectChanges();
    }

    RouteDeal(ReferenceData) {
        this._HelperService.SaveStorage(
            this._HelperService.AppConfig.Storage.ActiveDeal,
            {
                ReferenceKey: ReferenceData.DealKey,
                ReferenceId: ReferenceData.DealId,
                AccountKey: ReferenceData.MerchantKey,
                AccountId: ReferenceData.MerchantId,
                DisplayName: ReferenceData.DisplayName,
                AccountTypeCode: this._HelperService.AppConfig.AccountType.Store,
            }
        );

        this._HelperService.AppConfig.ActiveReferenceKey =
            ReferenceData.DealKey;
        this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.DealId;
        this._HelperService.AppConfig.ActiveAccountKey =
            ReferenceData.DealKey;
        this._HelperService.AppConfig.ActiveAccountId = ReferenceData.DealId;
        // this._Router.navigate([
        //   this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Store
        //     .Dashboard,
        //   ReferenceData.ReferenceKey,
        //   ReferenceData.ReferenceId,
        // ]);

        this._Router.navigate([
            this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.Analytics,
            ReferenceData.DealKey,
            ReferenceData.DealId,
            ReferenceData.MerchantId,
            ReferenceData.MerchantKey,

        ]);
    }

    RouteMerchant(ReferenceData) {
        //#region Save Current Merchant To Storage 

        this._HelperService.SaveStorage(
            this._HelperService.AppConfig.Storage.ActiveMerchant,
            {
                ReferenceKey: ReferenceData.MerchantKey,
                ReferenceId: ReferenceData.MerchantId,
                DisplayName: ReferenceData.DisplayName,
                AccountTypeCode: this._HelperService.AppConfig.AccountType.Merchant,
            }
        );

        //#endregion

        //#region Set Active Reference Key To Current Merchant 

        this._HelperService.AppConfig.ActiveReferenceKey = ReferenceData.MerchantKey;
        this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.MerchantId;

        //#endregion

        //#region navigate 

        // this._Router.navigate([
        //     this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Accounts.RewardPercentage,
        //     ReferenceData.MerchantKey,
        //     ReferenceData.MerchantId,
        // ]);

        //#endregion
    }

    GetOverviews(ListOptions: any, Task: string): any {
        this._HelperService.IsFormProcessing = true;

        ListOptions.SearchCondition = '';
        ListOptions = this._DataHelperService.List_GetSearchCondition(ListOptions);
        if (ListOptions.ActivePage == 1) {
            ListOptions.RefreshCount = true;
        }
        var SortExpression = ListOptions.Sort.SortDefaultColumn + ' ' + ListOptions.Sort.SortDefaultOrder;;

        if (ListOptions.Sort.SortDefaultName) {
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' desc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName.replace(' asc', '');
            ListOptions.Sort.SortDefaultName = ListOptions.Sort.SortDefaultName + ' ' + ListOptions.Sort.SortDefaultOrder;
        }

        if (ListOptions.Sort.SortColumn != undefined && ListOptions.Sort.SortColumn != null && ListOptions.Sort.SortColumn != '') {
            if (ListOptions.Sort.SortOrder != undefined && ListOptions.Sort.SortOrder != null && ListOptions.Sort.SortOrder != '') {
                SortExpression = ListOptions.Sort.SortColumn + ' ' + ListOptions.Sort.SortOrder;
            }
            else {
                SortExpression = ListOptions.Sort.SortColumn + ' desc';
            }
        }


        var pData = {
            Task: Task,
            TotalRecords: ListOptions.TotalRecords,
            Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
            Limit: ListOptions.PageRecordLimit,
            RefreshCount: ListOptions.RefreshCount,
            SearchCondition: ListOptions.SearchCondition,
            SortExpression: (SortExpression === "CreateDate desc") ? "ReferenceId desc" : SortExpression,
            Type: null,
            ReferenceKey: this._HelperService.UserAccount.AccountKey,
            ReferenceId: this._HelperService.UserAccount.AccountId,
            SubReferenceId: 0,
            IsDownload: false
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.bnpl.merchant, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.redeemedLoansList = _Response.Result.Data;
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);

            });


    }

    //#region filterOperations

    Active_FilterValueChanged(event: any) {
        this._HelperService.Active_FilterValueChanged(event);
        this._FilterHelperService.SetStoreConfig(this.TUTr_Config);
        this.SetSalesRanges();
        this.TUTr_GetData();
    }

    setOtherFilters() {
        this.TUTr_Config.SearchBaseConditions = [];
        this.SetSearchRanges();
    }

    RemoveFilterComponent(Type: string, index?: number): void {
        this._FilterHelperService._RemoveFilter_Store(Type, index);
        this._FilterHelperService.SetStoreConfig(this.TUTr_Config);
        this.SetSalesRanges();
        this.setOtherFilters();
        this.TUTr_GetData();
    }

    Save_NewFilter() {
        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
            text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
            input: "text",
            inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
            inputAttributes: {
                autocapitalize: "off",
                autocorrect: "off",
                //maxLength: "4",
                minLength: "4",
            },
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Green,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: "Save",
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {
                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();

                this._FilterHelperService._BuildFilterName_MerchantSales(result.value);
                this._HelperService.Save_NewFilter(
                    this._HelperService.AppConfig.FilterTypeOption.RedeemedLoans
                );

                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    Delete_Filter() {

        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
            text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {
                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();

                this._HelperService.Delete_Filter(
                    this._HelperService.AppConfig.FilterTypeOption.RedeemedLoans
                );
                this._FilterHelperService.SetStoreConfig(this.TUTr_Config);
                this.TUTr_GetData();

                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    ApplyFilters(event: any, Type: any, ButtonType: any): void {
        this.SetSearchRanges();
        this._HelperService.MakeFilterSnapPermanent();
        this.TUTr_GetData();

        this.ResetFilterUI();
        this._HelperService.StopClickPropogation();

        if (ButtonType == 'Sort') {
            $("#TUTr_sdropdown").dropdown('toggle');
        } else if (ButtonType == 'Other') {
            $("#TUTr_fdropdown").dropdown('toggle');
        }
    }

    ResetFilters(event: any, Type: any): void {
        this._HelperService.ResetFilterSnap();
        this._FilterHelperService.SetStoreConfig(this.TUTr_Config);
        this.SetSalesRanges();

        this.TUTr_GetData();
        this.ResetFilterUI(); this._HelperService.StopClickPropogation();
    }

    //#endregion

    getRedeemedLoanOverview() {
        var pData = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.BNPL.GetRedeemedLoansOverview,
            ReferenceKey: this._HelperService.UserAccount.AccountKey,
            ReferenceId: this._HelperService.UserAccount.AccountId
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.bnpl.merchant, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.RedeemedLoansOverviewData = _Response.Result as any;
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);

            });
    }
}
