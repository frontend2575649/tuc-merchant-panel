import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClearsettlementsComponent } from './clearsettlements/clearsettlements.component';
import { PendingsettlementsComponent } from './pendingsettlements/pendingsettlements.component';
import { SettlementsComponent } from './settlements.component';
import { TranscationComponent } from './transcation/transcation.component';

const routes: Routes = [

  {
    path: "",
    component: SettlementsComponent,
    children: [
      { path: "", redirectTo: 'transaction' },
      { path: 'pending', component: PendingsettlementsComponent },
      { path: 'clear/:referencekey/:referenceid', component: ClearsettlementsComponent },
      { path: 'transaction', component: TranscationComponent }
    ]
  },


]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettlementsRoutingModule { }
