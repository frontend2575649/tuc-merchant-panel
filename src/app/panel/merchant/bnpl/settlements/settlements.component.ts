import { Component, OnInit } from '@angular/core';

import { HelperService } from '../../../../service/service';


@Component({
  selector: 'app-settlements',
  templateUrl: './settlements.component.html',
  styleUrls: ['./settlements.component.css']
})
export class SettlementsComponent implements OnInit {

  constructor(public _HelperService:HelperService) { }

  ngOnInit() {
  }

}
