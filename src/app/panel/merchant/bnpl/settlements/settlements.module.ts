import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { TranslateModule } from "@ngx-translate/core";
import { Select2Module } from "ng2-select2";
import { NgxPaginationModule } from "ngx-pagination";
import { Daterangepicker } from "ng2-daterangepicker";

import { SettlementsRoutingModule } from './settlements-routing.module';
import { PendingsettlementsComponent } from './pendingsettlements/pendingsettlements.component';
import { ClearsettlementsComponent } from './clearsettlements/clearsettlements.component';
import { SettlementsComponent } from './settlements.component';
import { TranscationComponent } from './transcation/transcation.component';

@NgModule({
  declarations: [PendingsettlementsComponent,ClearsettlementsComponent,SettlementsComponent, TranscationComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    NgxPaginationModule,
    Daterangepicker,
    Select2Module,
    SettlementsRoutingModule
  ]
})
export class SettlementsModule { }
