import { AgmCoreModule } from '@agm/core';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { AgmOverlays } from 'agm-overlays';
import { ChartsModule } from 'ng2-charts';
import { Daterangepicker } from 'ng2-daterangepicker';
import { Ng2FileInputModule } from 'ng2-file-input';
import { Select2Module } from 'ng2-select2';
import { NgxPaginationModule } from 'ngx-pagination';
import { Angular4PaystackModule } from 'angular4-paystack';
import { DashboarddealsComponent } from './dashboard-deal.component';
import { AddBussinessLocationModalModule } from 'src/app/component/app-add-bussiness-location-modal/add-bussiness-location-modal.component';
//import { DashboarddealsComponent } from './dashboarddeals.component';

const routes: Routes = [
  { path: '', component: DashboarddealsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DealsDashboardRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        DealsDashboardRoutingModule,
        AgmOverlays,
        Angular4PaystackModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
        ChartsModule,
        AddBussinessLocationModalModule
    ],
    declarations: [DashboarddealsComponent]
})
export class DashboarddealsModule { }
