import { AgmCoreModule } from '@agm/core';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { AgmOverlays } from 'agm-overlays';
import { ChartsModule } from 'ng2-charts';
import { Daterangepicker } from 'ng2-daterangepicker';
import { Ng2FileInputModule } from 'ng2-file-input';
import { Select2Module } from 'ng2-select2';
import { NgxPaginationModule } from 'ngx-pagination';
import { Angular4PaystackModule } from 'angular4-paystack';
import { DashboardloyaltyComponent } from './dashboardloyalty.component';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { HCXAddressManagerModule } from 'src/app/component/hcxaddressmanager/hcxaddressmanager.component';
import { AddBussinessLocationModalModule } from '../../../../component/app-add-bussiness-location-modal/add-bussiness-location-modal.component';

const routes: Routes = [
  { path: '', component: DashboardloyaltyComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoyaltyDashboardRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        LoyaltyDashboardRoutingModule,
        AgmOverlays,
        Angular4PaystackModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
        ChartsModule,
        GooglePlaceModule,
        HCXAddressManagerModule,
        AddBussinessLocationModalModule
    ],
    declarations: [DashboardloyaltyComponent]
})
export class DashboardloyaltyModule { }
