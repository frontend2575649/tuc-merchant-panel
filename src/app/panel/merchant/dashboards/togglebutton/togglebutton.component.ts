import { Component, Output, EventEmitter} from '@angular/core';
import { HelperService } from 'src/app/service/helper.service';

@Component({
  selector: 'toggle-button',
  template: `
    <input type="checkbox" id="toggle-button-checkbox"
      (change)="changed.emit($event.target.checked)" [checked]="_HelperService.isDisplayLoyaltyConfig">
    <label class="toggle-button-switch"  
      for="toggle-button-checkbox"></label>
    <div class="toggle-button-text">
      <div class="toggle-button-text-on">Loyalty</div>
      <div class="toggle-button-text-off">Deals</div>
    </div>
  `,
  styles: [`
    :host {
      display: block;
      position: relative;
      width: 100px;
      height: 50px;
    }
    
    input[type="checkbox"] {
      display: none; 
    }


    .toggle-button-switch {
      position: absolute;
      top: 0px;
      left: 0px;
      width: 50px;
      height: 30px;
      background-color: #fff;
      border-radius: 12px 12px 12px 12px;
      cursor: pointer;
      z-index: 100;
      transition: left 0.3s;
    }


    .toggle-button-text {
      overflow: hidden;
      background-color: blue;
      border-radius: 25px;
      box-shadow: 2px 2px 5px 0 rgba(50, 50, 50, 0.75);
      transition: background-color 0.3s;
    }


    .toggle-button-text-on,
    .toggle-button-text-off {
      float: left;
      width: 50%;
      height: 100%;
      line-height: 30px;
      font-family: Lato, sans-serif;
      font-weight: bold;
      color: #fff;
      text-align: center;
    }
    input[type="checkbox"]:checked ~ .toggle-button-switch {
      left: 52px;
    }

    input[type="checkbox"]:checked ~ .toggle-button-text {
      background-color: #3dbf87;
    }
  `]
})
export class ToggleButtonComponent  {
  @Output() changed = new EventEmitter<boolean>();
  constructor(public _HelperService: HelperService){

  }
}