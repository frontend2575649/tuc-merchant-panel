import { ChangeDetectorRef, Component, OnInit, ViewChild, OnDestroy ,ElementRef} from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router, Params } from "@angular/router";
import * as Feather from "feather-icons";
import { InputFileComponent, InputFile } from 'ngx-input-file';
import swal from "sweetalert2";
import { ImageCroppedEvent } from 'ngx-image-cropper';
declare var $: any;
declare var moment: any;
import {
    DataHelperService,
    HelperService,
    OList,
    OSelect,
    FilterHelperService,
    OResponse,
} from "../../../../service/service";
import { Observable, Subscription } from 'rxjs';
import { ChangeContext } from 'ng5-slider';
declare var moment: any;
import * as cloneDeep from 'lodash/cloneDeep';

@Component({
    selector: "tu-tuorder",
    templateUrl: "./tuorder.component.html",
})
export class TUOrderComponent implements OnInit {
    
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _FilterHelperService: FilterHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef,
    ) {

    }
    

    public ResetFilterControls: boolean = true;
    public AcquirerId = 0;

    ngOnInit() {
    Feather.replace();
        this._HelperService.StopClickPropogation();

        this.TUTr_Setup();
       // this.TUTr_Filter_Merchants_Load();
        // this.TUTr_Filter_UserAccounts_Load();
        // this.TUTr_Filter_Stores_Load();
        // this.TUTr_Filter_Providers_Load();
        // this.TUTr_Filter_Issuers_Load();
        // this.TUTr_Filter_CardBrands_Load();
        // this.TUTr_Filter_TransactionTypes_Load();
        // this.TUTr_Filter_CardBanks_Load();
        // this.TUTr_Filter_Banks_Load();
        // this.TUTr_Filter_Terminals_Load();
        // this.TUTr_Filter_Cashiers_Load();


    }

    TUTr_InvoiceRangeMinAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit;
    TUTr_InvoiceRangeMaxAmount: number = this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit;
    TUTr_InvoiceRange_OnChange(changeContext: ChangeContext): void {
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'Amount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
        this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
        this.TUTr_InvoiceRangeMinAmount = changeContext.value;
        this.TUTr_InvoiceRangeMaxAmount = changeContext.highValue;
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'Amount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
        if (this.TUTr_InvoiceRangeMinAmount == this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit && this.TUTr_InvoiceRangeMaxAmount == this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit) {
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
        }
        else {
            this.TUTr_Config.SearchBaseConditions.push(SearchCase);
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
    }

    //#region transactions 

    public TUTr_Config: OList;
    public TuTr_Columns =
        {
            Status: true,
            Date: true,
            Account: true,
            AccountName: true,
            Mob: true,
            Biller: true,
            Amount: true,
            Source: true,
            PaymentRef: true
        }
    public TuTr_Filters_List =
        {
            Search: false,
            Date: false,
            Sort: false,
            Status: false,
            Biller: false
        }
    TUTr_Setup() {

        this.TUTr_Config = {
            Id: null,
            Sort: null,
            Task: "getorders",
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment,
            Title: "",
            StatusType: "default",
            DefaultSortExpression: "CreateDate desc",
            //SearchBaseCondition: this.TuTr_Setup_SearchCondition(),
            SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'MerchantId', this._HelperService.AppConfig.DataType.Number, this._HelperService.AppConfig.ActiveOwnerId, '='),
            TableFields: [
                {
                    DisplayName: "OrderID",
                    SystemName: "ParcelId",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "",
                    Show: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey"
                },
                {
                    DisplayName: "InvoiceID",
                    SystemName: "InvoiceID",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "text-center",
                    Show: true,
                    Search: true,
                    Sort: false,
                    ResourceId: null,
                    DefaultValue: "ThankUCash",
                    NavigateField: "ReferenceKey"
                },
                {
                    DisplayName: "Placed On",
                    SystemName: "CreateDate",
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: "",
                    Show: true,
                    IsDateSearchField: true,
                    Search: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey"
                },
                {
                    // DisplayName: "Sold Date",
                    DisplayName: "PickUp Date",
                    SystemName: "PickUpDate",
                    DataType: this._HelperService.AppConfig.DataType.Date,
                    Class: "td-date text-right",
                    Show: true,
                   // IsDateSearchField: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey",
                },
                {
                    // DisplayName: "Expiry Date",
                    DisplayName: "CustomerDisplayName",
                    SystemName: "CustomerDisplayName",
                    DataType: this._HelperService.AppConfig.DataType.Text,
                    Class: "td-date text-right",
                    Show: true,
                    Sort: false,
                    ResourceId: null,
                    NavigateField: "ReferenceKey",
                },
                {
                    DisplayName: "Mobile Number",
                    // DisplayName: "Redeemed Date",
                    SystemName: "CustomerMobileNumber",
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: "td-date text-right",
                    Show: true,
                    Sort: true,
                    ResourceId: null,
                    NavigateField: "ReferenceKey",
                },
                {
                    DisplayName: "Items",
                    SystemName: "Items",
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: "text-center",
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                    DefaultValue: "ThankUCash",
                    NavigateField: "ReferenceKey"
                },
                

                {
                    DisplayName: "Total Amount",
                    SystemName: "Total Amount",
                    DataType: this._HelperService.AppConfig.DataType.Number,
                    Class: "text-center",
                    Show: true,
                    Search: false,
                    Sort: true,
                    ResourceId: null,
                    DefaultValue: "ThankUCash",
                    NavigateField: "ReferenceKey"
                },
            ]
        };

        this.TUTr_Config = this._DataHelperService.List_Initialize(this.TUTr_Config);
        this._HelperService.Active_FilterInit(
            this._HelperService.AppConfig.FilterTypeOption.SoldHistory,
            this.TUTr_Config
        );
        this.TUTr_GetData();
    }
   
    TUTr_ToggleOption_Date(event: any, Type: any) {
        this.TUTr_ToggleOption(event, Type);



    }
    TUTr_ToggleOption(event: any, Type: any) {
        if (Type == "date") {
            this._HelperService.AppConfig.DateRangeOptions.startDate = event.start;
            this._HelperService.AppConfig.DateRangeOptions.endDate = event.end;
        }
        if (Type == this._HelperService.AppConfig.ListToggleOption.SalesRange) {
            event.data = {
                SalesMin: this.TUTr_InvoiceRangeMinAmount,
                SalesMax: this.TUTr_InvoiceRangeMaxAmount,
            }
        }
        this._HelperService.Update_CurrentFilterSnap(event, Type, this.TUTr_Config);

        this.TUTr_Config = this._DataHelperService.List_Operations(this.TUTr_Config, event, Type);
        this._DataHelperService.List_OperationsForTransCounts(this.Data, event, Type);

        this.TodayStartTime = this.TUTr_Config.StartTime;
        this.TodayEndTime = this.TUTr_Config.EndTime;
        if (event != null) {
            for (let index = 0; index < this.TUTr_Config.Sort.SortOptions.length; index++) {
                const element = this.TUTr_Config.Sort.SortOptions[index];
                if (event.SystemName == element.SystemName) {
                    element.SystemActive = true;
                }
                else {
                    element.SystemActive = false;
                }
            }
        }

        if (
            (this.TUTr_Config.RefreshData == true)
            && this._HelperService.DataReloadEligibility(Type)
        ) {
            this.TUTr_GetData();
        }
    }

    timeout = null;
    TUTr_ToggleOptionSearch(event: any, Type: any) {

        clearTimeout(this.timeout);

        this.timeout = setTimeout(() => {
            if (Type == 'date') {
                event.start = this._HelperService.DateInUTC(event.start);
                event.end = this._HelperService.DateInUTC(event.end);
            }

            this._HelperService.Update_CurrentFilterSnap(event, Type, this.TUTr_Config);

            this.TUTr_Config = this._DataHelperService.List_Operations(this.TUTr_Config, event, Type);
            this._DataHelperService.List_OperationsForTransCounts(this.Data, event, Type);

            this.TodayStartTime = this.TUTr_Config.StartTime;
            this.TodayEndTime = this.TUTr_Config.EndTime;
            if (event != null) {
                for (let index = 0; index < this.TUTr_Config.Sort.SortOptions.length; index++) {
                    const element = this.TUTr_Config.Sort.SortOptions[index];
                    if (event.SystemName == element.SystemName) {
                        element.SystemActive = true;
                    }
                    else {
                        element.SystemActive = false;
                    }

                }
            }


            if (
                (this.TUTr_Config.RefreshData == true)
                && this._HelperService.DataReloadEligibility(Type)
            ) {
                this.TUTr_GetData();
            }
        }, this._HelperService.AppConfig.SearchInputDelay);


    }

    public OverviewData: any = {
        Transactions: 0,
        InvoiceAmount: 0.0
    };

    TUTr_GetData() {
       this.GetOverviews(this.TUTr_Config, "getordersovrview");
        var TConfig = this._DataHelperService.List_GetDataTur(this.TUTr_Config);
        this.TUTr_Config = TConfig;
    }
   


    //#endregion

    SetOtherFilters(): void {
        this.TUTr_Config.SearchBaseConditions = [];
       

       var CurrentIndex = this._HelperService.FilterSnap.OtherFilters.findIndex((filter) => (filter.data[0].OtherType == this._HelperService.AppConfig.OtherFilters.MerchantSales.Store));
        if (CurrentIndex != -1) {
            this.TUTr_Filter_Store_Selected = 0;
            this.StoresEventProcessing(this._HelperService.FilterSnap.OtherFilters[CurrentIndex]);
        }
    }

    //#region dropdowns 

    //#region stores 

    public TUTr_Filter_Store_Option: Select2Options;
    public TUTr_Filter_Store_Toggle = false;
    public TUTr_Filter_Store_Selected = 0;
    TUTr_Filter_Stores_Load() {
        var PlaceHolder = this._HelperService.AppConfig.CommonResource.Select;
        var _Select: OSelect = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
            AccountKey: this._HelperService.AppConfig.ActiveMerchantReferenceKey,
            AccountId: this._HelperService.AppConfig.ActiveMerchantReferenceId,
            SearchCondition: "",
            SortCondition: [],
            Fields: [
                {
                    SystemName: "ReferenceId",
                    Type: this._HelperService.AppConfig.DataType.Number,
                    Id: true,
                    Text: false,
                },
                {
                    SystemName: "DisplayName",
                    Type: this._HelperService.AppConfig.DataType.Text,
                    Id: false,
                    Text: true
                }

            ]
        };


        var _Transport = this._DataHelperService.S2_BuildList(_Select) as any;
        this.TUTr_Filter_Store_Option = {
            placeholder: 'Search by Store',
            ajax: _Transport,
            multiple: false,
            allowClear: true,
        };
    }

    TUTr_Filter_Stores_Change(event: any) {
        this._HelperService.Update_CurrentFilterSnap(
            event,
            this._HelperService.AppConfig.ListToggleOption.Other,
            this.TUTr_Config,
            this._HelperService.AppConfig.OtherFilters.MerchantSales.Store
        );
        this.StoresEventProcessing(event);
    }

    StoresEventProcessing(event: any): void {
        if (event.value == this.TUTr_Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'StoreReferenceId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Store_Selected = 0;
        }
        else if (event.value != this.TUTr_Filter_Store_Selected) {
            var SearchCase = this._HelperService.GetSearchConditionStrict('', 'StoreReferenceId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '=');
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
            this.TUTr_Filter_Store_Selected = event.value;
            this.TUTr_Config.SearchBaseConditions.push(this._HelperService.GetSearchConditionStrict('', 'StoreReferenceId', this._HelperService.AppConfig.DataType.Number, this.TUTr_Filter_Store_Selected, '='));
        }
        this.TUTr_ToggleOption(null, this._HelperService.AppConfig.ListToggleOption.ResetOffset);
        this._HelperService.ToggleField = true;
        setTimeout(() => {
            this._HelperService.ToggleField = false;
        }, 500);
    }

    //#endregion


    TodayStartTime = null;
    TodayEndTime = null;

    Data: any = {};
   

    ResetFilterUI(): void {
        this.ResetFilterControls = false;
        this._ChangeDetectorRef.detectChanges();
        this.TUTr_Filter_Stores_Load();
      

        this.ResetFilterControls = true;
        this._ChangeDetectorRef.detectChanges();

    }

    SetSearchRanges(): void {
        //#region Invoice 
        this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArrayByField('Amount', this.TUTr_Config.SearchBaseConditions);
        var SearchCase = this._HelperService.GetSearchConditionRange('', 'Amount', this.TUTr_InvoiceRangeMinAmount, this.TUTr_InvoiceRangeMaxAmount);
        if (this.TUTr_InvoiceRangeMinAmount == this._HelperService.AppConfig.RangeInvoiceAmountMinimumLimit && this.TUTr_InvoiceRangeMaxAmount == this._HelperService.AppConfig.RangeInvoiceAmountMaximumLimit) {
            this.TUTr_Config.SearchBaseConditions = this._HelperService.RemoveItemFromArray(SearchCase, this.TUTr_Config.SearchBaseConditions);
        }
        else {
            this.TUTr_Config.SearchBaseConditions.push(SearchCase);
        }

        //#endregion      

    }

    //#region SalesOverview 

    GetOverviews(ListOptions: any, Task: string): any {
        this._HelperService.IsFormProcessing = true;

        


        var pData = {
            Task: Task,
           // TotalRecords: ListOptions.TotalRecords,
           // Offset: (ListOptions.ActivePage - 1) * ListOptions.PageRecordLimit,
            Limit: ListOptions.PageRecordLimit,
           // RefreshCount: ListOptions.RefreshCount,
            AccountId: this._HelperService.AppConfig.ActiveOwnerId,
            AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Lsshipment, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.OverviewData = _Response.Result as any;
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);

            });


    }

    //#endregion

    //#region filterOperations

    Active_FilterValueChanged(event: any) {
        this._HelperService.Active_FilterValueChanged(event);
        this._FilterHelperService.SetStoreConfig(this.TUTr_Config);

        //#region setOtherFilters
        this.SetOtherFilters();
        //#endregion

        this.TUTr_GetData();
    }

    RemoveFilterComponent(Type: string, index?: number): void {
        this._FilterHelperService._RemoveFilter_Store(Type, index);
        this._FilterHelperService.SetStoreConfig(this.TUTr_Config);

        if (Type == 'Time') {
            this._HelperService.AppConfig.DateRangeOptions.startDate= new Date(2017, 0, 1, 0, 0, 0, 0);
            this._HelperService.AppConfig.DateRangeOptions.endDate=moment().endOf("day");
        }
        //#region setOtherFilters
        this.SetOtherFilters();
        //#endregion

        this.TUTr_GetData();
    }

    Save_NewFilter() {
        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.SaveFilterTitle,
            text: this._HelperService.AppConfig.CommonResource.SaveFilterHelp,
            // input: "text",
            html:
                '<input id="swal-input1" class="swal2-input" placeholder="filter name" class="swal2-input">' +
                '<label class="mg-x-5 mg-t-5">Private</label><input type="radio" checked name="swal-input2" id="swal-input2" class="">' +
                '<label class="mg-x-5 mg-t-5">Public</label><input type="radio" name="swal-input2" id="swal-input3" class="">',
            focusConfirm: false,
            preConfirm: () => {
                return {
                    filter: document.getElementById('swal-input1')['value'],
                    private: document.getElementById('swal-input2')['checked']
                }
            },
            // inputPlaceholder: this._HelperService.AppConfig.CommonResource.FilterName,
            // inputAttributes: {
            //   autocapitalize: "off",
            //   autocorrect: "off",
            //   maxLength: "4",
            //   minLength: "4",
            // },
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Green,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: "Save",
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        }).then((result) => {
            if (result.value) {

                if (result.value.filter.length < 5) {
                    this._HelperService.NotifyError('Enter filter name length greater than 4');
                    return;
                }

                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();

                this._FilterHelperService._BuildFilterName_Merchant(result.value.filter);

                var AccessType: number = result.value.private ? 0 : 1;
                this._HelperService.Save_NewFilter(
                    this._HelperService.AppConfig.FilterTypeOption.SoldHistory,
                    AccessType
                );

                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    Delete_Filter() {

        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.DeleteTitle,
            text: this._HelperService.AppConfig.CommonResource.DeleteHelp,
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {
                this._HelperService._RefreshUI = false;
                this._ChangeDetectorRef.detectChanges();

                this._HelperService.Delete_Filter(
                    this._HelperService.AppConfig.FilterTypeOption.SoldHistory
                );
                this._FilterHelperService.SetStoreConfig(this.TUTr_Config);
                this.TUTr_GetData();

                this._HelperService._RefreshUI = true;
                this._ChangeDetectorRef.detectChanges();
            }
        });
    }

    ApplyFilters(event: any, Type: any, ButtonType: any): void {
        this.SetSearchRanges();

        this._HelperService.MakeFilterSnapPermanent();
        this.TUTr_GetData();

        this.ResetFilterUI(); this._HelperService.StopClickPropogation();

        if (ButtonType == 'Sort') {
            $("#TUTr_sdropdown").dropdown('toggle');
        } else if (ButtonType == 'Other') {
            $("#TUTr_fdropdown").dropdown('toggle');
        }
    }

    ResetFilters(event: any, Type: any): void {
        this._HelperService.ResetFilterSnap();
        this._FilterHelperService.SetStoreConfig(this.TUTr_Config);

        //#region setOtherFilters
        this.SetOtherFilters();
        this.SetSalesRanges();
        //#endregion
        this.TUTr_GetData();

        this.ResetFilterUI(); this._HelperService.StopClickPropogation();
    }
    SetSalesRanges(): void {
        this.TUTr_InvoiceRangeMinAmount = this.TUTr_Config.SalesRange.SalesMin;
        this.TUTr_InvoiceRangeMaxAmount = this.TUTr_Config.SalesRange.SalesMax;

    }
  

}


// DealsList_Config


export class OAccountOverview {
  public TotalTransactions?: number;
  public TotalSale?: number;

  public DeadTerminals?: number;
  public IdleTerminals?: number;
  public TerminalStatus?: number;
  public Total?: number;
  public Idle?: number;
  public Active?: number;
  public Dead?: number;
  public Inactive?: number;
  public UnusedTerminals?: number;
  public Merchants: number;
  public ActiveMerchants: number;
  public ActiveMerchantsDiff: number;
  public Terminals: number;
  public ActiveTerminals: number;
  public ActiveTerminalsDiff: number;
  public Transactions: number;
  public TransactionsDiff: number;
  public PurchaseAmount: number;
  public PurchaseAmountDiff: number;
  public CashRewardPurchaseAmount: number;
  public CashRewardPurchaseAmountDiff: number;
  public CardRewardPurchaseAmount: number;
  public CardRewardPurchaseAmountDiff: number;
}

export class OCoreUsage {

  public UserRewardAmount: number;
  public BankAccountNumber: number;
  public Charge: number;
  public TotalAmount: number;
  public MerchantAmount: number;
  public ToAccountId: number;
  public ToAccountMobileNumber: number;
  public AccountDisplayName: string;
  public ToAccountDisplayName: string;
  public Title: string;
  public BankName: string;
  public ReferenceNumber: string;
  public CreatedByDisplayName: string;
  public BankAccountName: string;
  public ModifyByDisplayName: string;
  public SystemComment: string;
  public ProductCategoryName: string;
  public ProductItemName: string;
  public RewardAmount: number;
  public Amount: number;
  public CommisonAmount: number;
  public AccountMobileNumber: number;
  public PaymentReference: string;
  public StatusB: string;
  Coordinates?: any;
  public ReferenceId: number;
  public AccountId: number;
  public AccountKey: string;
  public Reference: string;
  public ReferenceKey: string;
  public AppOwnerKey: string;
  public AppOwnerName: string;
  public AppKey: string;
  public AppName: string;
  public ApiKey: string;
  public ApiName: string;
  public AppVersionKey: string;
  public AppVersionName: string;
  public FeatureKey: string;
  public FeatureName: string;
  public UserAccountKey: string;
  public UserAccountDisplayName: string;
  public UserAccountIconUrl: string;
  public UserAccountTypeCode: string;
  public UserAccountTypeName: string;
  public SessionId: string;
  public SessionKey: string;
  public IpAddress: string;
  public Latitude: string;
  public Longitude: string;
  public Request: string;
  public Response: string;
  public RequestTime: Date;
  public StartDate: Date;
  public ModifyDate: Date;
  public EndDate: Date;
  public CreateDate: Date;
  public ResponseTime: Date;
  public ProcessingTime: string;
  public StatusId: number;
  public StatusCode: string;
  public StatusName: string;
}