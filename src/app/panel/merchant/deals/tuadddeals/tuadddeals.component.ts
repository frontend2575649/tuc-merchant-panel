import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent } from "../../../../service/service";
import swal from 'sweetalert2';
declare var $: any;
import * as Feather from 'feather-icons';
declare var google: any;
declare var moment: any;
import * as cloneDeep from 'lodash/cloneDeep';
import { InputFileComponent, InputFile } from 'ngx-input-file';
import { ThrowStmt } from '@angular/compiler';
import { template } from '@angular/core/src/render3';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

import { Select2OptionData } from 'ng2-select2';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { FileManagerComponent } from 'src/app/modules/file-manager/file-manager.component';
import { MatDialog } from '@angular/material';
@Component({
    selector: 'app-tuadddeals',
    templateUrl: './tuadddeals.component.html',
    styleUrls: ['./tuadddeals.component.css'],
})
export class AddDealsComponent implements OnInit {
    SelectedImage:any;

    dealType: any = '';
    isStorePickup = true;
    isDeliverToAddress = false;
    Type_Data = ['box', "envelope", "soft-packaging"];
    Form_AddDeal: FormGroup;

    public ckConfig =
        {
            toolbar: ['Bold', 'Italic', 'NumberedList', 'BulletedList'],
            height: 300
        }
    public Editor = ClassicEditor;


    _ImageManager =
        {
            TCroppedImage: null,
            ActiveImage: null,
            ActiveImageName: null,
            ActiveImageSize: null,
            Option: {
                MaintainAspectRatio: "false",
                MinimumWidth: 600,
                MinimumHeight: 500,
                MaximumWidth: 600,
                MaximumHeight: 500,
                ResizeToWidth: 600,
                ResizeToHeight: 500,
                Format: "jpg",
            }
        }
    public Hours = [];
    public Days = [];
    dealsOverview: any = {};

    _DealConfig =
        {
            DefaultStartDate: null,
            DefaultEndDate: null,
            SelectedDealCodeHours: 12,
            SelectedDealCodeDays: 30,
            SelectedDealCodeEndDate: 0,
            DealCodeValidityTypeCode: 'daysafterpurchase',
            StartDate: null,
            EndDate: null,
            DealImages: [],
            SelectedTitle: 3,
            Images: [],
            StartDateConfig: {
            },
            EndDateConfig: {
            },
            DealCodeEndDateConfig: {
            }
        }
        categoryValue = null
        public BCategory_Option: Select2Options;
        ShowCategory = true
        getDealType
        dealTypeVal = ""
    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef,
        public _dialog: MatDialog,
    ) {
    }



    ngOnInit(): void {
        this.dealsOverview = this._HelperService.GetStorage(this._HelperService.AppConfig.Storage.dealOverview);
        this.getDealType = this._HelperService.GetStorage("hca").UserAccount.UserAccountTypes

        if(this.getDealType)
        {
            this.getDealType.forEach(element => {
                if(element.TypeCode == 'accounttype.thankucashdeals')
                {
                    this.dealTypeVal = element.Value
                }
                
            });
            if(this.getDealType.length == 1 && this.getDealType[0].TypeCode == 'accounttype.thankucashdeals') {
                this.dealType = this.getDealType[0].Value == '0' ? 'dealtype.product' : '';
            }
        }        
        for (let index = 1; index < 24; index++) {
            this.Hours.push(index);
        }
        for (let index = 1; index < 366; index++) {
            this.Days.push(index);
        }
        this._DealConfig.DefaultStartDate = moment();
        this._DealConfig.DefaultEndDate = moment().add(1, 'days').endOf("day");
        this._DealConfig.StartDate = this._DealConfig.DefaultStartDate;
        this._DealConfig.EndDate = this._DealConfig.DefaultEndDate;
        this._DealConfig.StartDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            parentEl: '#adddeal_startdate',
            startDate: this._HelperService.DateInUTC(moment()),
            endDate: this._HelperService.DateInUTC(moment().endOf("day")),
            minDate: moment(),
        };
        this._DealConfig.EndDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            parentEl: '#adddeal_enddate',
            startDate: this._HelperService.DateInUTC(moment()),
            endDate: this._HelperService.DateInUTC(moment().endOf("day")),
            minDate: moment(),
        };
        this._DealConfig.DealCodeEndDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            parentEl:'#DealConfig_date',
            startDate: this._DealConfig.EndDate,
            minDate: this._DealConfig.EndDate,
        };


        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveMerchantReferenceKey = params['referencekey'];
            this._HelperService.AppConfig.ActiveMerchantReferenceId = params['referenceid'];
            // this.GetStores_List();
        });
        this.Form_AddUser_Load();
        this.GetDealCategories_List();
        this.Form_AddDeal_Load();

        this.BCategory_Option = {
            placeholder: "Select Category",
            multiple: false,
            allowClear: false,
        };
    }

    Form_AddDeal_Load() {

        this.Form_AddDeal = this._FormBuilder.group({
            Title: [
                null,
                Validators.compose([
                    Validators.required,
                ])
            ],
            Description: [
                null,
                Validators.compose([
                    Validators.required,
                    Validators.minLength(30),
                    Validators.maxLength(60)
                ])
            ],
            Type: [
                null,
                Validators.compose([
                    Validators.required,
                ])
            ],
            Height: [
                null,
                Validators.compose([
                    Validators.required,
                ])
            ],
            Weight: [
                null,
                Validators.compose([
                    Validators.required,
                ])
            ],
            Length: [
                null,
                Validators.compose([
                    Validators.required,
                ])
            ],
            Width: [
                null,
                Validators.compose([
                    Validators.required,
                ])
            ],
        });
    }

    //#region  Form Manager
    Form_AddUser_Images = [];
    Form_AddUser: FormGroup;
    Form_AddUser_Load() {
        this._HelperService._Icon_Cropper_Data.Width = 128;
        this._HelperService._Icon_Cropper_Data.Height = 128;
        this.Form_AddUser = this._FormBuilder.group({
            SubCategoryKey: [null, Validators.required],
            TitleTypeId: ["3", Validators.required], //transient
            TitleContent: ['', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
            Description: [null, Validators.compose([Validators.required, Validators.minLength(80), Validators.maxLength(5048)])],
            Terms: [null],
            MaximumUnitSale: [null, Validators.compose([Validators.required, Validators.min(1)])],
            MaximumUnitSalePerPerson: [null, Validators.compose([Validators.required, Validators.min(0)])],
            ActualPrice: [null, Validators.compose([Validators.required, Validators.min(1)])],
            SellingPrice: [null, Validators.compose([Validators.required, Validators.min(1)])],
            TUCPercentage: null,
            StartDate: this._DealConfig.DefaultStartDate.format('DD-MM-YYYY hh:mm a'),
            EndDate: this._DealConfig.DefaultEndDate.format('DD-MM-YYYY hh:mm a'),
            CodeUsageTypeCode: "daysafterpurchase", //transient
            TImage: null,
            CodeValidityEndDate: this._DealConfig.DefaultEndDate.format('DD-MM-YYYY hh:mm a'),
        });
    }
    Form_AddUser_Process(_FormValue: any, IsDraft: boolean) {
        if (this._SelectedSubCategory == undefined || this._SelectedSubCategory == null || this._SelectedSubCategory.ReferenceId == undefined || this._SelectedSubCategory.ReferenceId == null || this._SelectedSubCategory.ReferenceId == 0) {
            this._HelperService.NotifyError("Select deal category");
        }
        else if (_FormValue.ActualPrice == undefined || _FormValue.ActualPrice == null || _FormValue.ActualPrice == undefined || _FormValue.ActualPrice == 0) {
            this._HelperService.NotifyError("Enter original price");
        }
        else if (_FormValue.SellingPrice == undefined || _FormValue.SellingPrice == null || _FormValue.SellingPrice == undefined || _FormValue.SellingPrice == 0) {
            this._HelperService.NotifyError("Enter selling price");
        }
        else if (_FormValue.SellingPrice <= 0) {
            this._HelperService.NotifyError("Actual price must be greater than 0");
        }
        else if (_FormValue.ActualPrice <= 0) {
            this._HelperService.NotifyError("Selling price must be greater than or equal 0");
        }
        else if (_FormValue.MaximumUnitSalePerPerson > _FormValue.MaximumUnitSale) {
            this._HelperService.NotifyError("Maximum purchase per person must be less than maximum purchase");
        }
        // else if (parseFloat(_FormValue.SellingPrice) > parseFloat(_FormValue.ActualPrice)) {
        //     this._HelperService.NotifyError("Selling price must be less than or equal to actual price");
        // }
        // else if (_FormValue.SellingPrice < _FormValue.ActualPrice) {
        //     this._HelperService.NotifyError("Selling price must be less than or equal to actual price");
        // }
        else if (this._DealConfig.DealImages == undefined || this._DealConfig.DealImages == null || this._DealConfig.DealImages.length == 0) {
            this._HelperService.NotifyError("Atleast 1 image is required for adding deal");
        }
        else if (this._DealConfig.StartDate == undefined || this._DealConfig.StartDate == null) {
            this._HelperService.NotifyError("Deal start date required");
        }
        else if (this._DealConfig.EndDate == undefined || this._DealConfig.EndDate == null) {
            this._HelperService.NotifyError("Deal end date required");
        }
        else {
            var Title = this._Titles.Title3;
            if (this._DealConfig.SelectedTitle == 1) {
                Title = this._Titles.Title1;
            }
            else if (this._DealConfig.SelectedTitle == 2) {
                Title = this._Titles.Title2;
            }
            else {
                Title = this._Titles.Title3;
            }
            var _PostData :any =
            {
                Task: "savedeal",
                AccountId: this._HelperService.AppConfig.ActiveOwnerId,
                AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
                Title: Title,
                TitleContent: _FormValue.TitleContent,
                TitleTypeId: _FormValue.TitleTypeId,
                Description: _FormValue.Description,
                Terms: _FormValue.Terms,

                StartDate: this._HelperService.HCXConvertDate(this._DealConfig.StartDate),
                EndDate: this._HelperService.HCXConvertDate(this._DealConfig.EndDate),
                CodeUsageTypeCode: _FormValue.CodeUsageTypeCode,
                CodeValidityDays: this._DealConfig.SelectedDealCodeDays,
                CodeValidityHours: this._DealConfig.SelectedDealCodeHours,
                CodeValidityEndDate: null,
                ActualPrice: _FormValue.ActualPrice,
                SellingPrice: _FormValue.SellingPrice,
                MaximumUnitSale: _FormValue.MaximumUnitSale,
                MaximumUnitSalePerPerson: _FormValue.MaximumUnitSalePerPerson,
                SubCategoryKey: this._SelectedSubCategory.ReferenceKey,
                StatusCode: 'deal.draft',
                SendNotification: false,
                TUCPercentage: this._AmountDistribution.TUCPercentage,
                Images: [],
                Locations: [],
                Shedule: [],
                DealTypeCode: this.dealType
            };
            if (this.dealType === 'dealtype.product') {

                if (this.isStorePickup && this.isDeliverToAddress) {
                    _PostData.DeliveryTypeCode = 'deliverytype.instoreanddelivery'
                }
                if (this.isDeliverToAddress) {
                    _PostData.DeliveryTypeCode = 'deliverytype.delivery'
                }
                if (this.isStorePickup) {
                    _PostData.DeliveryTypeCode = 'deliverytype.instore'
                }
            }
            if (this.dealType === 'dealtype.product' && this.isDeliverToAddress) {
                _PostData.Packaging = {};
                _PostData.Packaging.Name = this.Form_AddDeal.value.Title;
                _PostData.Packaging.ParcelDescription = this.Form_AddDeal.value.Description;
                _PostData.Packaging.Type = this.Form_AddDeal.value.Type;
                _PostData.Packaging.Height = this.Form_AddDeal.value.Height;
                _PostData.Packaging.Weight = this.Form_AddDeal.value.Weight;
                _PostData.Packaging.Length = this.Form_AddDeal.value.Length;
                _PostData.Packaging.Width = this.Form_AddDeal.value.Width;
                _PostData.Packaging.Size_unit = 'cm';
                _PostData.Packaging.Weight_unit = 'kg';
            }
            if (_PostData.CodeUsageTypeCode == 'dealenddate') {
                _PostData.CodeValidityEndDate = this._DealConfig.EndDate;
            }
            if (_PostData.CodeUsageTypeCode == 'date') {
                _PostData.CodeValidityEndDate = this._HelperService.HCXConvertDate(this._DealConfig.SelectedDealCodeEndDate);
            }
            this._DealConfig.DealImages.forEach(element => {
                element.OriginalContent = null;
                _PostData.Images.push(element);
            });
            if (IsDraft) {
                _PostData.StatusCode = 'deal.draft';
            } else {
                _PostData.StatusCode = 'deal.approvalpending';
            }
            this._HelperService.IsFormProcessing = true;
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Deals, _PostData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.IsFormProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        if (IsDraft) {
                            this._HelperService.NotifySuccess('Deal has been saved as draft.');
                        }
                        else if (this.dealsOverview && this.dealsOverview.Total == 0) {
                            // this._HelperService.NotifySuccess('Deals uploaded successfully, and it is pending approval. Our deals team will get back to you within 6 hours');
                            this._DataHelperService.dealsOverview.subscribe(data => {
                                this._DataHelperService.changedealsOverview({ ...data, firstDealUploaded: true });
                            })
                        } else {
                            this._HelperService.NotifySuccess('Deal sent for approval.');
                        }
                        this.Form_AddUser.reset();
                        this._DealConfig =
                        {
                            DefaultStartDate: null,
                            DefaultEndDate: null,
                            SelectedDealCodeHours: 12,
                            SelectedDealCodeDays: 30,
                            SelectedDealCodeEndDate: 0,
                            DealCodeValidityTypeCode: 'daysafterpurchase',
                            StartDate: null,
                            EndDate: null,
                            DealImages: [],
                            SelectedTitle: 3,
                            Images: [],
                            StartDateConfig: {
                            },
                            EndDateConfig: {
                            },
                            DealCodeEndDateConfig: {
                            }
                        }
                        this._ImageManager =
                        {
                            TCroppedImage: null,
                            ActiveImage: null,
                            ActiveImageName: null,
                            ActiveImageSize: null,
                            Option: {
                                MaintainAspectRatio: "false",
                                MinimumWidth: 700,
                                MinimumHeight: 500,
                                MaximumWidth: 700,
                                MaximumHeight: 500,
                                ResizeToWidth: 700,
                                ResizeToHeight: 500,
                                Format: "jpg",
                            }
                        }
                        this.ngOnInit();
                        this.Form_AddUser_Close();
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HandleException(_Error);
                });
        }
    }
    Form_AddUser_Close() {
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.Deals]);
    }
    //#endregion
    //#region Amount Distribution
    _AmountDistribution =
        {
            ActualPrice: null,
            SellingPrice: null,
            SellingPricePercentage: null,
            SellingPriceDifference: null,
            TUCPercentage: null,
            TUCAmount: null,
            MerchantPercentage: null,
            MerchantAmount: null,
        };
    ProcessAmounts() {
        //var tAmount: any = this._HelperService.HCXGetNumberComponent(this._AmountDistribution);
        var tAmount = this._AmountDistribution;
        var SellingPrice = parseFloat(this._AmountDistribution.SellingPrice);
        var ActualPrice = parseFloat(this._AmountDistribution.ActualPrice);
        if (tAmount.ActualPrice != undefined && tAmount.ActualPrice != null && isNaN(tAmount.ActualPrice) == false && tAmount.ActualPrice >= 0) {
            if (tAmount.SellingPrice != undefined && tAmount.SellingPrice != null && isNaN(tAmount.SellingPrice) == false && tAmount.SellingPrice >= 0) {
                if (tAmount.ActualPrice >= tAmount.SellingPrice) {
                    tAmount.SellingPriceDifference = this._HelperService.GetFixedDecimalNumber(tAmount.ActualPrice - tAmount.SellingPrice);
                    tAmount.SellingPricePercentage = this._HelperService.GetPercentageFromNumber(tAmount.SellingPriceDifference, tAmount.ActualPrice);
                    tAmount.TUCAmount = this._HelperService.GetAmountFromPercentage(tAmount.SellingPrice, tAmount.TUCPercentage);
                    tAmount.MerchantAmount = this._HelperService.GetFixedDecimalNumber(tAmount.SellingPrice - tAmount.TUCAmount);
                    tAmount.MerchantPercentage = this._HelperService.GetPercentageFromNumber(tAmount.MerchantAmount, tAmount.SellingPrice);
                    this._AmountDistribution =
                    {
                        ActualPrice: tAmount.ActualPrice,
                        SellingPrice: tAmount.SellingPrice,
                        SellingPricePercentage: tAmount.SellingPricePercentage,
                        SellingPriceDifference: tAmount.SellingPriceDifference,
                        TUCPercentage: this._AmountDistribution.TUCPercentage,
                        TUCAmount: tAmount.TUCAmount,
                        MerchantPercentage: tAmount.MerchantPercentage,
                        MerchantAmount: tAmount.MerchantAmount,
                    };
                }
                else {
                    if (parseFloat(this._AmountDistribution.SellingPrice) >= parseFloat(this._AmountDistribution.ActualPrice)) {
                        setTimeout(() => {
                            this._AmountDistribution =
                            {
                                ActualPrice: this._AmountDistribution.ActualPrice,
                                SellingPrice: null,
                                SellingPricePercentage: null,
                                SellingPriceDifference: null,
                                TUCPercentage: this._AmountDistribution.TUCPercentage,
                                TUCAmount: null,
                                MerchantPercentage: null,
                                MerchantAmount: null,
                            };
                        }, 200);
                    }
                }
            }
        }
        this.ProcessTitle();
    }
    //#endregion

    //#region Title Manager
    _Titles =
        {
            OriginalTitle: "",
            Title1: this._HelperService.UserCountry.CurrencyNotation + " X deal title for " + this._HelperService.UserCountry.CurrencyNotation + " Y",
            Title2: this._HelperService.UserCountry.CurrencyNotation + " Y off on deal title",
            Title3: "x% off on deal title"
        }
    ProcessTitle() {
        this._Titles.Title1 = this._HelperService.UserCountry.CurrencyNotation + " " + Math.round(this._AmountDistribution.ActualPrice) + " " + this._Titles.OriginalTitle + " for " + this._HelperService.UserCountry.CurrencyNotation + " " + Math.round(this._AmountDistribution.SellingPrice);
        this._Titles.Title2 = this._HelperService.UserCountry.CurrencyNotation + " " + Math.round(this._AmountDistribution.SellingPriceDifference) + " off on " + this._Titles.OriginalTitle;
        this._Titles.Title3 = Math.round(this._AmountDistribution.SellingPricePercentage) + "% off on " + this._Titles.OriginalTitle;
    }
    //#endregion
    //#region Deal Shedule Manager 
    ScheduleStartDateRangeChange(value) {
        console.log(value);
        this._DealConfig.EndDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            startDate: value.start,
            minDate: value.start,
        };
        this._DealConfig.StartDate = value.start;
        this._DealConfig.EndDate = value.start;
        this._DealConfig.SelectedDealCodeEndDate = value.start;
        this.Form_AddUser.patchValue(
            {
                StartDate: value.start.format('DD-MM-YYYY hh:mm a'),
                EndDate: value.start.format('DD-MM-YYYY hh:mm a'),
                CodeValidityEndDate: value.start.format('DD-MM-YYYY hh:mm a'),
            }
        );
    }
    ScheduleEndDateRangeChange(value) {
        this._DealConfig.DealCodeEndDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            parentEl:'#DealConfig_date',
            startDate: value.start,
            minDate: value.start,
        };
        this._DealConfig.EndDate = value.start;
        this._DealConfig.SelectedDealCodeEndDate = value.start;
        this.Form_AddUser.patchValue(
            {
                EndDate: value.start.format('DD-MM-YYYY hh:mm a'),
                CodeValidityEndDate: value.start.format('DD-MM-YYYY hh:mm a'),
            }
        );
    }
    ScheduleCodeEndDateRangeChange(value) {
        this._DealConfig.SelectedDealCodeEndDate = value.start;
        this.Form_AddUser.patchValue(
            {
                CodeValidityEndDate: value.start.format('DD-MM-YYYY hh:mm a'),
            }
        );
    }
    Icon_Crop_Loaded() {
        this._HelperService.OpenModal('_Icon_Cropper_Modal_Deal');
    }
    //#endregion

    //#region Image Manager
    onImageAccept(value) {
        setTimeout(() => {
            this.Form_AddUser.patchValue(
                {
                    TImage: null,
                }
            );
        }, 300);
        this._ImageManager.ActiveImage = value;
        this._ImageManager.ActiveImageName = value.file.name;
        this._ImageManager.ActiveImageName = value.file.size;
    }
    onImageRejected(value) {
        console.log(value);
    }
    Icon_B64Cropped(base64: string) {
        this._ImageManager.TCroppedImage = base64;
    }
    Icon_B64CroppedDone() {
        var ImageDetails = this._HelperService.GetImageDetails(this._ImageManager.TCroppedImage);
        var ImageItem =
        {
            OriginalContent: this._ImageManager.TCroppedImage,
            Name: this._ImageManager.ActiveImageName,
            Size: this._ImageManager.ActiveImageSize,
            Extension: ImageDetails.Extension,
            Content: ImageDetails.Content
        };
        if (this._DealConfig.DealImages.length == 0) {
            this._DealConfig.DealImages.push(
                {
                    ImageContent: ImageItem,
                    IsDefault: 1,
                }
            );
        }
        else {
            this._DealConfig.DealImages.push(
                {
                    ImageContent: ImageItem,
                    IsDefault: 0,
                }
            );
        }
        this._ImageManager.TCroppedImage = null;
        this._ImageManager.ActiveImage = null;
        this._ImageManager.ActiveImageName = null;
        this._ImageManager.ActiveImageSize = null;
        console.log(this._DealConfig.DealImages);
    }
    Icon_Crop_Clear() {
        this._ImageManager.TCroppedImage = null;
        this._ImageManager.ActiveImage = null;
        this._ImageManager.ActiveImageName = null;
        this._ImageManager.ActiveImageSize = null;
        this._HelperService.CloseModal('_Icon_Cropper_Modal_Deal');
    }


    RemoveImage(Item) {
        this._DealConfig.DealImages = this._DealConfig.DealImages.filter(x => x != Item);
    }

    openFilemanager() {
        let dialogRef = this._dialog.open(FileManagerComponent, {
            width: '1200px',
            height: '750px',
            maxWidth: '1200px',
            maxHeight: '800px',
            disableClose: true,
            data: {
                merchantId: this._HelperService.AppConfig.ActiveOwnerKey
            }
        });
        dialogRef.afterClosed().subscribe(res => {
            if (res.data) {
                this._DealConfig.DealImages.push(
                    {
                        reference: res.data.reference,
                        url: res.data.url,
                    }
                );
            }
        });
    }

    drop(event: CdkDragDrop<any>): void {
        moveItemInArray(
            this._DealConfig.DealImages,
            event.previousContainer.data,
            event.container.data
        );
    }
    public GetMerchants_Option: Select2Options;
    public GetMerchants_Transport: any;
    public SelectedMerchant: any = {};
    GetMerchants_ListChange(event: any) {
        this.SelectedMerchant = event.data[0];
        this.Form_AddUser.patchValue(
            {
                AccountKey: event.data[0].ReferenceKey,
                AccountId: event.data[0].ReferenceId
            }
        );
        this.GetStores_List();
    }
    //#endregion

    //#region   List Helpers
    public _S2Categories_Data: Array<Select2OptionData>;
    GetDealCategories_List() {
        this.ShowCategory = false;
        var pData = {
            Task: this._HelperService.AppConfig.Api.Core.getallcategories,
            Offset: 0,
            Limit: 1000,
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.System2, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    var _Data = _Response.Result.Data as any[];
                    if (_Data.length > 0) {
                        var parentCategories = [];
                        _Data.forEach(element => {
                            var itemIndex = parentCategories.findIndex(x => x.ParentCategoryId == element.ParentCategoryId);
                            if (itemIndex == -1) {
                                var categories = _Data.filter(x => x.ParentCategoryId == element.ParentCategoryId);
                                parentCategories.push(
                                    {
                                        ParentCategoryId: element.ParentCategoryId,
                                        ParentCategoryKey: element.ParentCategoryKey,
                                        ParentCategoryName: element.ParentCategoryName,
                                        Categories: categories,
                                    }
                                );
                            }
                        });
                        var finalCat = [];
                        // finalCat.push(
                        //     {
                        //         id: "0",
                        //         text: "Select category",
                        //     }
                        // )

                        parentCategories.forEach(element => {
                            var Item = {
                                id: element.ParentCategoryId,
                                text: element.ParentCategoryName,
                                children: [],
                                additional: element,
                            };
                            element.Categories.forEach(CategoryItem => {
                                var cItem = {
                                    id: CategoryItem.ReferenceId,
                                    text: CategoryItem.Name + " (" + CategoryItem.Fees + "%)",
                                    additional: CategoryItem,
                                };
                                Item.children.push(cItem);
                            });
                            finalCat.push(Item);
                        });
                        this.BCategory_Option = {
                            placeholder: "Select Category",
                            multiple: false,
                            allowClear: false,
                          };
                        this._S2Categories_Data = finalCat;
                        this.ShowCategory = true;        
                    }
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }
    _SelectedSubCategory =
        {
            ReferenceId: 0,
            ReferenceKey: null,
            Fee: 5,
            Name: null,
            IconUrl: null,

            ParentCategoryId: 0,
            ParentCategoryKey: 0,
            ParentCategoryName: 0
        };
    GetDealCategories_Selected(event: any) {
        if (event.value != "0") {
            this._SelectedSubCategory.ReferenceId = event.data[0].additional.ReferenceId;
            this._SelectedSubCategory.ReferenceKey = event.data[0].additional.ReferenceKey;
            this._SelectedSubCategory.Name = event.data[0].additional.Name;
            this._SelectedSubCategory.IconUrl = event.data[0].additional.IconUrl;
            this._SelectedSubCategory.Fee = event.data[0].additional.Fees;
            this._AmountDistribution.TUCPercentage = event.data[0].additional.Fees;
            this._SelectedSubCategory.ParentCategoryId = event.data[0].additional.ParentCategoryId;
            this._SelectedSubCategory.ParentCategoryKey = event.data[0].additional.ParentCategoryKey;
            this._SelectedSubCategory.ParentCategoryName = event.data[0].additional.ParentCategoryName;
            this.Form_AddUser.patchValue(
                {
                    SubCategoryKey: this._SelectedSubCategory.ParentCategoryKey
                }
            );
            this.ProcessAmounts();
        }else{
            this._SelectedSubCategory.ReferenceId = 0;
        }
    }

    // public GetMerchants_Option: Select2Options;
    // public GetMerchants_Transport: any;
    // public SelectedMerchant: any = {};
    GetStores_List() {
        var pData = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Accounts,
            Offset: 0,
            Limit: 1000,
            SearchCondition: this._HelperService.GetSearchCondition("", "MerchantReferenceId", "number", this._HelperService.AppConfig.ActiveMerchantReferenceId),
        };
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Accounts, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    // console.log("Stores", _Response);
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }
    //#endregion
}
