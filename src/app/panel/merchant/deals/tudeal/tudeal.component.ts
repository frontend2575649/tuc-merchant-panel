import { ChangeDetectorRef, Component, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { BaseChartDirective, Label } from 'ng2-charts';
import { Observable } from 'rxjs';
import { DataHelperService, HelperService, OList, OResponse, OSelect } from '../../../../service/service';
declare var moment: any;
declare var $: any;
import * as pluginEmptyOverlay from "chartjs-plugin-empty-overlay";
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import swal from 'sweetalert2';
import * as Feather from "feather-icons";
import * as cloneDeep from 'lodash/cloneDeep';
import { InputFileComponent } from 'ngx-input-file';
import { InputFile } from 'ngx-input-file';
import { Select2OptionData } from 'ng2-select2';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';

@Component({
  selector: 'tudeal',
  templateUrl: './tudeal.component.html',
  styles: [`
    agm-map {
      height: 300px;
    }
`]
})
export class TUDealComponent implements OnInit {

  public ckConfig =
    {
      toolbar: ['Bold', 'Italic', 'NumberedList', 'BulletedList'],
      height: 300
    }
  public Editor = ClassicEditor;

  showDailyChart: boolean = true;
  IsNoDaySelected: boolean = false;
  showMore = false;
  Piechartlable = ['Available Deals', 'Sold Deals', 'Redeemed Deals'];
  public TodayDate: any;
  Piedata = ['0', '0', '0']
  public dougnut: any = {
    type: 'doughnut',
    data: {
      labels: ["Red", "Orange", "Green"],
      datasets: [{
        label: '# of Votes',
        data: [33, 33, 33],
        backgroundColor: [
          'rgba(231, 76, 60, 1)',
          'rgba(255, 164, 46, 1)',
          'rgba(46, 204, 113, 1)'
        ],
        borderColor: [
          'rgba(255, 255, 255 ,1)',
          'rgba(255, 255, 255 ,1)',
          'rgba(255, 255, 255 ,1)'
        ],
        borderWidth: 5
      }]

    },
    options: {
      rotation: 1 * Math.PI,
      circumference: 1 * Math.PI,
      legend: {
        display: false
      },
      tooltip: {
        enabled: false
      },
      cutoutPercentage: 95
    }
  }
  @ViewChildren(BaseChartDirective) components: BaseChartDirective[];


  _DealConfig =
    {
      DefaultStartDate: null,
      DefaultEndDate: null,
      SelectedDealCodeHours: 12,
      SelectedDealCodeDays: 30,
      SelectedDealCodeEndDate: 0,
      DealCodeValidityTypeCode: 'daysafterpurchase',
      StartDate: null,
      EndDate: null,
      DealImages: [],
      SavedDealImages: [],
      SelectedTitle: 3,
      Images: [],
      StartDateConfig: {
      },
      EndDateConfig: {
      },
      DealCodeEndDateConfig: {
      }
    };

  public Hours = [];
  public Days = [];
  constructor(
    public _Router: Router,
    public _ActivatedRoute: ActivatedRoute,
    public _FormBuilder: FormBuilder,
    public _HelperService: HelperService,
    public _DataHelperService: DataHelperService,
    public _ChangeDetectorRef: ChangeDetectorRef
  ) {
  }

  // ngOnInit() {
  //   this.TodayStartTime = this._HelperService.AppConfig.DefaultStartTimeAll;
  //   this.TodayEndTime = this._HelperService.AppConfig.DefaultEndTimeToday;
  //   Feather.replace();


  //   this._DealConfig.DefaultStartDate = moment().startOf('day');
  //       this._DealConfig.DefaultEndDate = moment().add(1, 'days').endOf("day");
  //       this._DealConfig.StartDate = this._DealConfig.DefaultStartDate;
  //       this._DealConfig.EndDate = this._DealConfig.DefaultEndDate;
  //       this._DealConfig.StartDateConfig = {
  //           autoUpdateInput: false,
  //           singleDatePicker: true,
  //           timePicker: true,
  //           locale: { format: "DD-MM-YYYY" },
  //           alwaysShowCalendars: false,
  //           showDropdowns: true,
  //           startDate: this._HelperService.DateInUTC(moment().startOf("day")),
  //           endDate: this._HelperService.DateInUTC(moment().endOf("day")),
  //           minDate: moment(),
  //       };
  //       this._DealConfig.EndDateConfig = {
  //           autoUpdateInput: false,
  //           singleDatePicker: true,
  //           timePicker: true,
  //           locale: { format: "DD-MM-YYYY" },
  //           alwaysShowCalendars: false,
  //           showDropdowns: true,
  //           startDate: this._HelperService.DateInUTC(moment().startOf("day")),
  //           endDate: this._HelperService.DateInUTC(moment().endOf("day")),
  //           minDate: moment(),
  //       };
  //       this._DealConfig.DealCodeEndDateConfig = {
  //           autoUpdateInput: false,
  //           singleDatePicker: true,
  //           timePicker: true,
  //           locale: { format: "DD-MM-YYYY" },
  //           alwaysShowCalendars: false,
  //           showDropdowns: true,
  //           startDate: this._DealConfig.EndDate,
  //           minDate: this._DealConfig.EndDate,
  //       };
  //       this._ActivatedRoute.params.subscribe((params: Params) => {
  //           this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
  //           this._HelperService.AppConfig.ActiveReferenceId = params["referenceid"];

  //           this._HelperService.AppConfig.ActiveAccountKey = params["accountkey"];
  //           this._HelperService.AppConfig.ActiveAccountId = params["accountid"];

  //       });



  //   setTimeout(() => {
  //     this._HelperService.ValidateDataPermission();

  //   }, 500);

  //   this._HelperService.FullContainer = false;
  //   this._HelperService.FullContainer = false;
  //   this._ActivatedRoute.params.subscribe((params: Params) => {
  //     this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
  //     this.pData.ReferenceKey = params["referencekey"];
  //     // this.pReviewData.ReferenceKey = params["referencekey"];
  //     this.pDealPurchaseData.ReferenceKey = params["referencekey"];

  //     this._HelperService.AppConfig.ActiveReferenceId = params["referenceid"];
  //     this.pData.ReferenceId = params["referenceid"];
  //     // this.pReviewData.ReferenceId = params["referenceid"];
  //     this.pDealPurchaseData.ReferenceId = params["referenceid"];


  //     this._HelperService.AppConfig.ActiveAccountKey = params["accountkey"];
  //     this.pData.AccountKey = params["accountkey"];
  //     // this.pReviewData.AccountKey = params["accountkey"];
  //     this.pDealPurchaseData.AccountKey = params["accountkey"];


  //     this._HelperService.AppConfig.ActiveAccountId = params["accountid"];
  //     this.pData.AccountId = params["accountid"];
  //     // this.pReviewData.AccountId = params["accountid"];
  //     this.pDealPurchaseData.AccountId = params["accountid"];

  //     this.Form_EditUser_Load();
  //     this.GetDealCategories_List();

  //     if (this._HelperService.AppConfig.ActiveReferenceKey == null) {
  //       this._Router.navigate([
  //         this._HelperService.AppConfig.Pages.System.NotFound
  //       ]);
  //     } else {
  //       this._HelperService.ResetDateRange();
  //       this.GetAccountDetails();
  //       this.GetStores_List();
  //       this.PurchaseHistory_Setup();
  //       this.GetSalesOverview();
  //       this.ReviewHistory_Setup();

  //     }


  //   });



  // }


  ngOnInit() {
    this._HelperService.StopClickPropogation();
    Feather.replace();
    for (let index = 1; index < 24; index++) {
      this.Hours.push(index);
    }
    for (let index = 1; index < 366; index++) {
      this.Days.push(index);
    }
    this.GetMerchants_List();
    this._DealConfig.DefaultStartDate = moment().startOf('day');
    this._DealConfig.DefaultEndDate = moment().add(1, 'days').endOf("day");
    this._DealConfig.StartDate = this._DealConfig.DefaultStartDate;
    this._DealConfig.EndDate = this._DealConfig.DefaultEndDate;
    this._DealConfig.StartDateConfig = {
      autoUpdateInput: false,
      singleDatePicker: true,
      timePicker: true,
      locale: { format: "DD-MM-YYYY" },
      alwaysShowCalendars: false,
      showDropdowns: true,
      startDate: this._HelperService.DateInUTC(moment().startOf("day")),
      endDate: this._HelperService.DateInUTC(moment().endOf("day")),
      minDate: moment(),
    };
    this._DealConfig.EndDateConfig = {
      autoUpdateInput: false,
      singleDatePicker: true,
      timePicker: true,
      locale: { format: "DD-MM-YYYY" },
      alwaysShowCalendars: false,
      showDropdowns: true,
      startDate: this._HelperService.DateInUTC(moment().startOf("day")),
      endDate: this._HelperService.DateInUTC(moment().endOf("day")),
      minDate: moment(),
    };
    this._DealConfig.DealCodeEndDateConfig = {
      autoUpdateInput: false,
      singleDatePicker: true,
      timePicker: true,
      locale: { format: "DD-MM-YYYY" },
      alwaysShowCalendars: false,
      showDropdowns: true,
      startDate: this._DealConfig.EndDate,
      minDate: this._DealConfig.EndDate,
    };
    this._ActivatedRoute.params.subscribe((params: Params) => {
      this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
      this._HelperService.AppConfig.ActiveReferenceId = params["referenceid"];

      this._HelperService.AppConfig.ActiveAccountKey = params["accountkey"];
      this._HelperService.AppConfig.ActiveAccountId = params["accountid"];

    });
    this.Form_EditUser_Load();
    this.GetDealCategories_List();
    this.GetAccountDetails();
    this.PurchaseHistory_Setup();


    this._HelperService.ResetDateRange();
    //this.GetAccountDetails();
    this.GetStores_List();
    // this.PurchaseHistory_Setup();
    this.GetSalesOverview();
    this.ReviewHistory_Setup();
  }


  public _UserAccount: any =
    {
      MerchantDisplayName: null,
      SecondaryEmailAddress: null,
      BankDisplayName: null,
      BankKey: null,
      OwnerName: null,
      SubOwnerAddress: null,
      SubOwnerLatitude: null,
      SubOwnerDisplayName: null,
      SubOwnerKey: null,
      SubOwnerLongitude: null,
      AccessPin: null,
      LastLoginDateS: null,
      AppKey: null,
      AppName: null,
      AppVersionKey: null,
      CreateDate: null,
      CreateDateS: null,
      CreatedByDisplayName: null,
      CreatedByIconUrl: null,
      CreatedByKey: null,
      Description: null,
      IconUrl: null,
      ModifyByDisplayName: null,
      ModifyByIconUrl: null,
      ModifyByKey: null,
      ModifyDate: null,
      ModifyDateS: null,
      PosterUrl: null,
      ReferenceKey: null,
      StatusCode: null,
      StatusI: null,
      StatusId: null,
      StatusB: null,
      StatusName: null,
      AccountCode: null,
      AccountOperationTypeCode: null,
      AccountOperationTypeName: null,
      AccountTypeCode: null,
      AccountTypeName: null,
      Address: null,
      AppVersionName: null,
      ApplicationStatusCode: null,
      ApplicationStatusName: null,
      AverageValue: null,
      CityAreaKey: null,
      CityAreaName: null,
      CityKey: null,
      CityName: null,
      ContactNumber: null,
      CountValue: null,
      CountryKey: null,
      CountryName: null,
      DateOfBirth: null,
      DisplayName: null,
      EmailAddress: null,
      EmailVerificationStatus: null,
      EmailVerificationStatusDate: null,
      FirstName: null,
      GenderCode: null,
      GenderName: null,
      LastLoginDate: null,
      LastName: null,
      Latitude: null,
      Longitude: null,
      MobileNumber: null,
      Name: null,
      NumberVerificationStatus: null,
      NumberVerificationStatusDate: null,
      OwnerDisplayName: null,
      OwnerKey: null,
      Password: null,
      Reference: null,
      ReferralCode: null,
      ReferralUrl: null,
      RegionAreaKey: null,
      RegionAreaName: null,
      RegionKey: null,
      RegionName: null,
      RegistrationSourceCode: null,
      RegistrationSourceName: null,
      RequestKey: null,
      RoleKey: null,
      RoleName: null,
      SecondaryPassword: null,
      SystemPassword: null,
      UserName: null,
      WebsiteUrl: null,

      StateKey: null,
      StateName: null

    }
  toogleIsFormProcessing(value: boolean): void {
    this._HelperService.IsFormProcessing = value;
    //    this._ChangeDetectorRef.detectChanges();
  }
  public _Address: any = {};
  public _ContactPerson: any = {};
  timeRemaining = "--";
  isTimeLessThanDay: boolean = false;
  timerConfig: any = { leftTime: 30 };

  VarAccordion: boolean = false;
  VarAccordion1: boolean = false;
  VarAccordion1F() {
    this.VarAccordion1 = !this.VarAccordion1;

  }

  VarAccordionF() {
    this.VarAccordion = !this.VarAccordion;

  }
  ShowAllLocations(): void {
    this._HelperService.OpenModal("AllLocation");
  }
  IsDetailsLoaded = false;
  GetAccountDetails() {
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetDeal,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveAccountId,
      AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
      // Reference: this._HelperService.GetSearchConditionStrict(
      //   "",
      //   "ReferenceKey",
      //   this._HelperService.AppConfig.DataType.Text,
      //   this._HelperService.AppConfig.ActiveReferenceKey,
      //   "="
      // ),
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Deals, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.IsDetailsLoaded = true;
          this.toogleIsFormProcessing(false);
          this._UserAccount = this._HelperService.HCXGetDateComponent(_Response.Result);
          this._Address = this._UserAccount.Address;
          this._ContactPerson = this._UserAccount.ContactPerson;
          var minutesDifference = moment(this._UserAccount.EndDate).diff(moment(), 'minutes');
          var difference = minutesDifference / 60;

          //#region RelocateMarker 

          if (_Response.Result.Latitude != undefined && _Response.Result.Longitude != undefined) {
            this._HelperService._UserAccount.Latitude = _Response.Result.Latitude;
            this._HelperService._UserAccount.Longitude = _Response.Result.Longitude;
          } else {
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Latitude;
            this._HelperService._UserAccount.Longitude = this._HelperService._UserAccount.Longitude;
          }

          if (difference < 0) {
            this.timeRemaining = "Deal No Longer Available";
            this.isTimeLessThanDay = false;
          } else if (difference >= 0 && difference <= 24) {
            this.isTimeLessThanDay = true;
            this.timerConfig.leftTime = (difference * 60 * 60);
          } else if (difference > 24 && difference >= 48) {
            this.timeRemaining = Math.round(difference / 24) + " Day Remaining";
            this.isTimeLessThanDay = false;
          } else {
            this.timeRemaining = Math.round(difference / 24) + " Days Remaining";
            this.isTimeLessThanDay = false;
          }
          // this.FormC_EditUser_Latitude = this._UserAccount.Latitude;
          // this.FormC_EditUser_Longitude = this._UserAccount.Longitude;

          // this._HelperService._ReLocate();

          //#endregion

          //#region DatesAndStatusInit 
          this._UserAccount.CodeValidityStartDate = this._HelperService.GetDateS(
            this._UserAccount.CodeValidityStartDate
          );
          this._UserAccount.CodeValidityEndDate = this._HelperService.GetDateS(
            this._UserAccount.CodeValidityEndDate
          );
          this._UserAccount.StartDateS = this._HelperService.GetDateS(
            this._UserAccount.StartDate
          );
          this._UserAccount.EndDateS = this._HelperService.GetDateS(
            this._UserAccount.EndDate
          );
          this._UserAccount.CreateDateS = this._HelperService.GetDateTimeS(
            this._UserAccount.CreateDate
          );
          this._UserAccount.ModifyDateS = this._HelperService.GetDateTimeS(
            this._UserAccount.ModifyDate
          );
          this._UserAccount.StatusI = this._HelperService.GetStatusIcon(
            this._UserAccount.StatusCode
          );
          this._UserAccount.StatusB = this._HelperService.GetStatusBadge(
            this._UserAccount.StatusCode
          );
          this._UserAccount.StatusC = this._HelperService.GetStatusColor(
            this._UserAccount.StatusCode
          );


          //#endregion

          this._ChangeDetectorRef.detectChanges();
        }
        else {
          this.toogleIsFormProcessing(false);
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }


  public RedeemHistory_Config : OList
  public PurchaseHistory_Config: OList;
  PurchaseHistory_Setup() {
    this.PurchaseHistory_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetPurchaseHistory,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Deals,
      Title: "Purchase History",
      StatusType: "default",
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveAccountId,
      AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
      // Type: this._HelperService.AppConfig.ListType.SubOwner,
      DefaultSortExpression: "CreateDate desc",
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '=='),
      TableFields: [
        {
          DisplayName: " Name",
          SystemName: "DisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Contact No",
          SystemName: "ContactNumber",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-center",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Email Address",
          SystemName: "EmailAddress",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },

        {
          DisplayName: "Terminals",
          SystemName: "Terminals",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Cashiers",
          SystemName: "Cashiers",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
          //   .PanelAcquirer.Merchant.Dashboard,
        }, {
          DisplayName: "Manager",
          SystemName: "ManagerName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: "CreateDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          IsDateSearchField: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
      ]
    };
    this.PurchaseHistory_Config = this._DataHelperService.List_Initialize(
      this.PurchaseHistory_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Stores,
      this.PurchaseHistory_Config
    );

    this.PurchaseHistory_GetData();
  }
  PurchaseHistory_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.PurchaseHistory_Config.Sort.SortOptions.length; index++) {
        const element = this.PurchaseHistory_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.PurchaseHistory_Config
    );

    this.PurchaseHistory_Config = this._DataHelperService.List_Operations(
      this.PurchaseHistory_Config,
      event,
      Type
    );

    if (
      (this.PurchaseHistory_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.PurchaseHistory_GetData();
    }

  }
  timeout = null;
  PurchaseHistory_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.PurchaseHistory_Config.Sort.SortOptions.length; index++) {
          const element = this.PurchaseHistory_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.PurchaseHistory_Config
      );

      this.PurchaseHistory_Config = this._DataHelperService.List_Operations(
        this.PurchaseHistory_Config,
        event,
        Type
      );

      if (
        (this.PurchaseHistory_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.PurchaseHistory_GetData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);
  }
  PurchaseHistory_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.PurchaseHistory_Config
    );
    this.PurchaseHistory_Config = TConfig;
    this.RedeemHistory_Config = TConfig
  }
  //Below method (Fetch_RedeemHistoy) help us to fiter the RedeemHistory Data
  RedeemHist_Details =[]
  Fetch_RedeemHistoy(){
   this.RedeemHist_Details =[]
    for(let i=0; i<this.RedeemHistory_Config.Data.length; i++){
      if(this.RedeemHistory_Config.Data[i].LastUseDate){
       this.RedeemHist_Details.push(this.RedeemHistory_Config.Data[i])
      }
    }
  }
  PurchaseHistory_RowSelected(ReferenceData) {
    // this._HelperService.SaveStorage(
    //   this._HelperService.AppConfig.Storage.ActiveDeal,
    //   {
    //     ReferenceKey: ReferenceData.ReferenceKey,
    //     ReferenceId: ReferenceData.ReferenceId,
    //     DisplayName: ReferenceData.DisplayName,
    //     AccountTypeCode: this._HelperService.AppConfig.AccountType.Store,
    //   }
    // );

    // this._HelperService.AppConfig.ActiveReferenceKey =
    //   ReferenceData.ReferenceKey;
    // this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;
    // this._HelperService.AppConfig.ActiveAccountKey =
    //   ReferenceData.ReferenceKey;
    // this._HelperService.AppConfig.ActiveAccountId = ReferenceData.ReferenceId;

    // this._Router.navigate([
    //   this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.Deal,
    //   ReferenceData.ReferenceKey,
    //   ReferenceData.ReferenceId,
    //   ReferenceData.AccountId,
    //   ReferenceData.AccountKey,

    // ]);


  }

  public _GetoverviewSummary: any = {};
  public TodayStartTime = null;
  public TodayEndTime = null;
  private pData = {
    Task: 'getdealsoverview',
    StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
    EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
    ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
    ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
    AccountId: this._HelperService.AppConfig.ActiveAccountId,
    AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
  };
  GetSalesOverview() {
    this.showDailyChart = false;
    this._HelperService.IsFormProcessing = true;

    this.pData.StartDate = this._HelperService.DateInUTC(this.TodayStartTime);
    this.pData.EndDate = this._HelperService.DateInUTC(this.TodayEndTime);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Deals, this.pData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._GetoverviewSummary = _Response.Result as any;
          // this.Piedata[0] = this._GetoverviewSummary.Total;
          this.showDailyChart = true;
          this._ChangeDetectorRef.detectChanges();
          this.GetDealPurchaseOverview();

          return;

        }

        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }


  public _GetPurchaseoverviewSummary: any = {};
  TotalCountDeal: any = null;
  private pDealPurchaseData = {
    Task: 'getdealspurchaseoverview',
    StartDate: this._HelperService.DateInUTC(this.TodayStartTime),
    EndDate: this._HelperService.DateInUTC(this.TodayEndTime),
    ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
    ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
    AccountId: this._HelperService.AppConfig.ActiveAccountId,
    AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
    StoreReferenceId: 0,
    StoreReferenceKey: null,
  };
  GetDealPurchaseOverview() {
    this.showDailyChart = false;
    this._ChangeDetectorRef.detectChanges();

    this._HelperService.IsFormProcessing = true;

    this.pData.StartDate = this._HelperService.DateInUTC(this.TodayStartTime);
    this.pData.EndDate = this._HelperService.DateInUTC(this.TodayEndTime);

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Deals, this.pDealPurchaseData);
    _OResponse.subscribe(
      _Response => {
        this._HelperService.IsFormProcessing = false;
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this._GetPurchaseoverviewSummary = _Response.Result as any;
          this.Piedata[1] = this._GetPurchaseoverviewSummary.Total;
          this.Piedata[2] = this._GetPurchaseoverviewSummary.Used;
          this.TotalCountDeal = this._GetPurchaseoverviewSummary.MaxLimit - this._GetPurchaseoverviewSummary.Total
          this.Piedata[0] = this.TotalCountDeal;

          this.showDailyChart = true;
          this._ChangeDetectorRef.detectChanges();

          return;

        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.IsFormProcessing = false;
        this._HelperService.HandleException(_Error);
      });
  }


  public ReviewHistory_Config: OList;
  ReviewHistory_Setup() {
    this.ReviewHistory_Config = {
      Id: null,
      Sort: null,
      Task: this._HelperService.AppConfig.Api.ThankUCash.getdealreviews,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Deals,
      Title: "Purchase History",
      StatusType: "default",
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveAccountId,
      AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
      // Type: this._HelperService.AppConfig.ListType.SubOwner,
      DefaultSortExpression: "CreateDate desc",
      // SearchBaseCondition: this._HelperService.GetSearchConditionStrict('', 'StatusId', this._HelperService.AppConfig.DataType.Number, 2, '=='),
      TableFields: [
        {
          DisplayName: " Name",
          SystemName: "AccountDisplayName",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Contact No",
          SystemName: "AccountMobileNumber",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-center",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          DefaultValue: "ThankUCash",
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "Rating",
          SystemName: "Rating",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "",
          Show: true,
          Search: false,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },

        {
          DisplayName: "Review",
          SystemName: "Review",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: true,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey"
        },
        {
          DisplayName: "IsWorking",
          SystemName: "IsWorking",
          DataType: this._HelperService.AppConfig.DataType.Text,
          Class: "text-right",
          Show: true,
          Search: false,
          Sort: false,
          ResourceId: null,
          NavigateField: "ReferenceKey",
          // NavigateLink: this._HelperService.AppConfig.Pages.ThankUCash
          //   .PanelAcquirer.Merchant.Dashboard,
        },
        {
          DisplayName: this._HelperService.AppConfig.CommonResource.CreateDate,
          SystemName: "CreateDate",
          DataType: this._HelperService.AppConfig.DataType.Date,
          Class: "td-date text-right",
          Show: true,
          IsDateSearchField: true,
          Sort: true,
          ResourceId: null,
          NavigateField: "ReferenceKey",
        },
      ]
    };
    this.ReviewHistory_Config = this._DataHelperService.List_Initialize(
      this.ReviewHistory_Config
    );

    this._HelperService.Active_FilterInit(
      this._HelperService.AppConfig.FilterTypeOption.Stores,
      this.ReviewHistory_Config
    );

    this.ReviewHistory_GetData();
  }
  ReviewHistory_ToggleOption(event: any, Type: any) {

    if (event != null) {
      for (let index = 0; index < this.ReviewHistory_Config.Sort.SortOptions.length; index++) {
        const element = this.ReviewHistory_Config.Sort.SortOptions[index];
        if (event.SystemName == element.SystemName) {
          element.SystemActive = true;
        }
        else {
          element.SystemActive = false;
        }
      }
    }

    this._HelperService.Update_CurrentFilterSnap(
      event,
      Type,
      this.ReviewHistory_Config
    );

    this.ReviewHistory_Config = this._DataHelperService.List_Operations(
      this.ReviewHistory_Config,
      event,
      Type
    );

    if (
      (this.ReviewHistory_Config.RefreshData == true)
      && this._HelperService.DataReloadEligibility(Type)
    ) {
      this.ReviewHistory_GetData();
    }

  }
  ReviewHistory_ToggleOptionSearch(event: any, Type: any) {

    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      if (event != null) {
        for (let index = 0; index < this.ReviewHistory_Config.Sort.SortOptions.length; index++) {
          const element = this.ReviewHistory_Config.Sort.SortOptions[index];
          if (event.SystemName == element.SystemName) {
            element.SystemActive = true;
          }
          else {
            element.SystemActive = false;
          }
        }
      }

      this._HelperService.Update_CurrentFilterSnap(
        event,
        Type,
        this.ReviewHistory_Config
      );

      this.ReviewHistory_Config = this._DataHelperService.List_Operations(
        this.ReviewHistory_Config,
        event,
        Type
      );

      if (
        (this.ReviewHistory_Config.RefreshData == true)
        && this._HelperService.DataReloadEligibility(Type)
      ) {
        this.ReviewHistory_GetData();
      }
    }, this._HelperService.AppConfig.SearchInputDelay);
  }
  ReviewHistory_GetData() {
    var TConfig = this._DataHelperService.List_GetData(
      this.ReviewHistory_Config
    );
    this.ReviewHistory_Config = TConfig;
  }
  // ReviewHistory_RowSelected(ReferenceData) {
  //    this._HelperService.SaveStorage(
  //      this._HelperService.AppConfig.Storage.ActiveDeal,
  //      {
  //        ReferenceKey: ReferenceData.ReferenceKey,
  //        ReferenceId: ReferenceData.ReferenceId,
  //        DisplayName: ReferenceData.DisplayName,
  //        AccountTypeCode: this._HelperService.AppConfig.AccountType.Store,
  //      }
  //    );

  //    this._HelperService.AppConfig.ActiveReferenceKey =
  //      ReferenceData.ReferenceKey;
  //    this._HelperService.AppConfig.ActiveReferenceId = ReferenceData.ReferenceId;
  //    this._HelperService.AppConfig.ActiveAccountKey =
  //      ReferenceData.ReferenceKey;
  //    this._HelperService.AppConfig.ActiveAccountId = ReferenceData.ReferenceId;


  //    this._Router.navigate([
  //      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.Deal,
  //      ReferenceData.ReferenceKey,
  //      ReferenceData.ReferenceId,
  //      ReferenceData.AccountId,
  //      ReferenceData.AccountKey,

  //    ]);



  //  }
  UpdateStatusArray: any = ['deal.draft', 'deal.approvalpending', 'deal.approved', 'deal.published', 'deal.paused', 'deal.expired',];
  StatusUpdate(ReferenceData, i) {
    if (i == 4) {
      swal({
        title: "Pause Deal?",
        text: this._HelperService.AppConfig.CommonResource.ContinueUpdateHelp,
        showCancelButton: true,
        position: this._HelperService.AppConfig.Alert_Position,
        animation: this._HelperService.AppConfig.Alert_AllowAnimation,
        customClass: this._HelperService.AppConfig.Alert_Animation,
        allowOutsideClick: true,
        allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
        confirmButtonColor: this._HelperService.AppConfig.Color_Red,
        cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
        confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
        cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        // input: 'password',
        // inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
        // inputAttributes: {
        //   autocapitalize: 'off',
        //   autocorrect: 'off',
        //   maxLength: "4",
        //   minLength: "4"
        // },

        // inputValidator: function (value) {
        //   if (value === '' || value.length < 4) {
        //     return 'Enter your 4 digit pin!'
        //   }
        // },

      }).then((result) => {
        if (result.value) {
          //   if (result.value.length < 4) {
          //     this._HelperService.NotifyError('Pin length must be 4 digits');
          //     return;
          // }

          this._HelperService.IsFormProcessing = true;
          var P1Data =
          {
            Task: this._HelperService.AppConfig.Api.ThankUCash.updatedealstatus,
            ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
            ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
            AccountId: this._HelperService.AppConfig.ActiveAccountId,
            AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
            StatusCode: this.UpdateStatusArray[i],
            // AuthPin: result.value,
          }

          let _OResponse: Observable<OResponse>;
          _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Deals, P1Data);
          _OResponse.subscribe(
            _Response => {
              if (_Response.Status == this._HelperService.StatusSuccess) {
                this._HelperService.NotifySuccess("Status Updated successfully");
                this.GetAccountDetails();
                this._HelperService.IsFormProcessing = false;
              }
              else {
                this._HelperService.NotifyError(_Response.Message);
              }
            }
            ,
            _Error => {
              this._HelperService.IsFormProcessing = false;
              this._HelperService.HandleException(_Error);
              this._HelperService.ToggleField = false;
            });

        }
      });
    }
    else {
      swal({
        title: "Resume Deal?",
        text: this._HelperService.AppConfig.CommonResource.ContinueUpdateHelp,
        showCancelButton: true,
        position: this._HelperService.AppConfig.Alert_Position,
        animation: this._HelperService.AppConfig.Alert_AllowAnimation,
        customClass: this._HelperService.AppConfig.Alert_Animation,
        allowOutsideClick: true,
        allowEscapeKey: this._HelperService.AppConfig.Alert_AllowEscapeKey,
        confirmButtonColor: this._HelperService.AppConfig.Color_Red,
        cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
        confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
        cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel,
        // input: 'password',
        // inputPlaceholder: this._HelperService.AppConfig.CommonResource.AccessPin,
        // inputAttributes: {
        //   autocapitalize: 'off',
        //   autocorrect: 'off',
        //   maxLength: "4",
        //   minLength: "4"
        // },

        // inputValidator: function (value) {
        //   if (value === '' || value.length < 4) {
        //     return 'Enter your 4 digit pin!'
        //   }
        // },

      }).then((result) => {
        if (result.value) {
          //   if (result.value.length < 4) {
          //     this._HelperService.NotifyError('Pin length must be 4 digits');
          //     return;
          // }

          this._HelperService.IsFormProcessing = true;
          var P1Data =
          {
            Task: this._HelperService.AppConfig.Api.ThankUCash.updatedealstatus,
            ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
            ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
            AccountId: this._HelperService.AppConfig.ActiveAccountId,
            AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
            StatusCode: this.UpdateStatusArray[i],
            // AuthPin: result.value,
          }

          let _OResponse: Observable<OResponse>;
          _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Deals, P1Data);
          _OResponse.subscribe(
            _Response => {
              if (_Response.Status == this._HelperService.StatusSuccess) {
                this._HelperService.NotifySuccess("Status Updated successfully");
                this.GetAccountDetails();
                this._HelperService.IsFormProcessing = false;
              }
              else {
                this._HelperService.NotifyError(_Response.Message);
              }
            }
            ,
            _Error => {
              this._HelperService.IsFormProcessing = false;
              this._HelperService.HandleException(_Error);
              this._HelperService.ToggleField = false;
            });

        }
      });
    }



  }

  ApplyFilters(event: any, Type: any, ButtonType: any): void {
    this._HelperService.MakeFilterSnapPermanent();
    this.ReviewHistory_GetData();

    if (ButtonType == 'Sort') {
      $("#DealsList_sdropdown").dropdown('toggle');
    } else if (ButtonType == 'Other') {
      $("#DealsList_fdropdown").dropdown('toggle');
    }
    this._HelperService.StopClickPropogation();
    // this.ResetFilterUI(); this._HelperService.StopClickPropogation();
  }


  tabSwitched(arg1: any): void { }
  ResetFilters(arg1: any, arg2: any): void { }
  OpenEditColModal(): void { }


  //form edit user--
  IsStartDateAfterEnd: boolean = false;
  Form_EditUser: FormGroup;

  Form_EditUser_Images = [];



  Form_EditUser_Address: string = null;
  Form_EditUser_Latitude: number = 0;
  Form_EditUser_Longitude: number = 0;
  _CurrentAddress: any = {};

  public NiraOrPercent: any = {
    DiscountAmount: "1",
    CommissionAmount: "1"
  }

  ValidTimings: any = {
    Start: new Date(),
    End: new Date()
  }

  DealTimings: any = {
    Start: new Date(),
    End: new Date()
  }

  Expiry: any = {
    Hours: 0,
    Days: 0,
    Date: new Date()
  }

  public WeekDays: any[] = [
    { name: 'Monday', selected: true },
    { name: 'Tuesday', selected: true },
    { name: 'Wednesday', selected: true },
    { name: 'Thursday', selected: true },
    { name: 'Friday', selected: true },
    { name: 'Saturday', selected: true },
    { name: 'Sunday', selected: true }
  ]

  @ViewChild('places') places: GooglePlaceDirective;
  Form_EditUser_PlaceMarkerClick(event) {
    this.Form_EditUser_Latitude = event.coords.lat;
    this.Form_EditUser_Longitude = event.coords.lng;
  }
  public Form_EditUser_AddressChange(address: Address) {
    this.Form_EditUser_Latitude = address.geometry.location.lat();
    this.Form_EditUser_Longitude = address.geometry.location.lng();
    this.Form_EditUser_Address = address.formatted_address;
    this.Form_EditUser.controls['Latitude'].setValue(this.Form_EditUser_Latitude);
    this.Form_EditUser.controls['Longitude'].setValue(this.Form_EditUser_Longitude);
    this._CurrentAddress = this._HelperService.GoogleAddressArrayToJson(address.address_components);
    this.Form_EditUser.controls['Address'].setValue(address.name + ',' + address.formatted_address);
  }
  Form_EditUser_Show() {
  }
  Form_EditUser_Close() {
    this._HelperService.CloseModal('Account_Edit_Content_approve');
  }


  Form_EditUser_Load() {
    this._HelperService._Icon_Cropper_Data.Width = 128;
    this._HelperService._Icon_Cropper_Data.Height = 128;
    this.Form_EditUser = this._FormBuilder.group({
      AccountId: [null, Validators.required],
      AccountKey: [null, Validators.required],
      SubCategoryKey: [null, Validators.required],
      TitleTypeId: [null, Validators.required], //transient
      TitleContent: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
      Description: [null, Validators.compose([Validators.required, Validators.minLength(80), Validators.maxLength(5048)])],
      Terms: [null, Validators.required],
      MaximumUnitSale: [null, Validators.compose([Validators.required, Validators.min(1)])],
      MaximumUnitSalePerPerson: [null, Validators.compose([Validators.required, Validators.min(0)])],
      ActualPrice: [null, Validators.compose([Validators.required, Validators.min(1)])],
      SellingPrice: [null, Validators.compose([Validators.required, Validators.min(1)])],
      StartDate: this._DealConfig.DefaultStartDate.format('DD-MM-YYYY hh:mm a'),
      EndDate: this._DealConfig.DefaultEndDate.format('DD-MM-YYYY hh:mm a'),
      CodeUsageTypeCode: "daysafterpurchase", //transient
      TImage: null,
      CodeValidityEndDate: this._DealConfig.DefaultEndDate.format('DD-MM-YYYY hh:mm a'),
    });
  }
  Form_EditUser_Clear() {
    this._HelperService.ToggleField = true;
    setTimeout(() => {
      this._HelperService.ToggleField = false;
    }, 300);
    this.Form_EditUser_Latitude = 0;
    this.Form_EditUser_Longitude = 0;
    this.Form_EditUser.reset();
    this._HelperService.Icon_Crop_Clear();
    this.Form_EditUser_Load();
    this._HelperService.GetRandomNumber();
    this._HelperService.GeneratePassoword();
  }
  //form edit user--

  //get merchant and store--
  public GetMerchants_Option: Select2Options;
  public GetMerchants_Transport: any;
  public SelectedMerchant: any = {};
  public merchantPlaceholder: string = "Select Merchant";
  public categoryPlaceholder: string = "Select Category";
  GetMerchants_List() {
    var PlaceHolder = "Select Merchant";
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },

        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        {
          SystemName: 'StatusCode',
          Type: this._HelperService.AppConfig.DataType.Text,
          SearchCondition: '=',
          SearchValue: this._HelperService.AppConfig.Status.Active,
        }
      ]
    }

    this.GetMerchants_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetMerchants_Option = {
      placeholder: PlaceHolder,
      ajax: this.GetMerchants_Transport,
      multiple: false,
    };
  }
  GetMerchants_ListChange(event: any) {

    this.SelectedMerchant = event.data[0];
    this.Form_EditUser.patchValue(
      {
        AccountKey: event.data[0].ReferenceKey,
        AccountId: event.data[0].ReferenceId
      }
    );



  }

  public GetCategories_Option: Select2Options;
  public GetCategories_Transport: any;
  GetCategories_List() {
    var PlaceHolder = this.categoryPlaceholder;
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.Core.GetCoreCommons,
      Location: this._HelperService.AppConfig.NetworkLocation.V2.System,
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceKey",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: true,
          Text: false,
        },

        {
          SystemName: "Name",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        // {
        //     SystemName: 'StatusCode',
        //     Type: this._HelperService.AppConfig.DataType.Text,
        //     SearchCondition: '=',
        //     SearchValue: this._HelperService.AppConfig.Status.Active,
        // }
      ]
    }


    _Select.SearchCondition = this._HelperService.GetSearchConditionStrictFromArray('', 'TypeCode', this._HelperService.AppConfig.DataType.Text,
      [
        this._HelperService.AppConfig.HelperTypes.MerchantCategories,

      ]
      , '=');
    this.GetCategories_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetCategories_Option = {
      placeholder: PlaceHolder,
      ajax: this.GetCategories_Transport,
      multiple: false,
    };


  }
  GetCategories_ListChange(event: any) {
    this.Form_EditUser.patchValue(
      {
        CategoryKey: event.data[0].ReferenceKey

      }
    );
  }

  public GetStores_Option: Select2Options;
  public GetStores_Transport: any;
  public StoresList: any = []
  GetStores_List() {
    var PlaceHolder = "Select Stores";
    var _Select: OSelect =
    {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
      Location: this._HelperService.AppConfig.NetworkLocation.V3.Account,
      AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
      AccountId: this._HelperService.AppConfig.ActiveOwnerId,
      SortCondition: [],
      Fields: [
        {
          SystemName: "ReferenceId",
          Type: this._HelperService.AppConfig.DataType.Number,
          Id: true,
          Text: false,
        },

        {
          SystemName: "DisplayName",
          Type: this._HelperService.AppConfig.DataType.Text,
          Id: false,
          Text: true
        },
        // {
        //     SystemName: 'StatusCode',
        //     Type: this._HelperService.AppConfig.DataType.Text,
        //     SearchCondition: '=',
        //     SearchValue: this._HelperService.AppConfig.Status.Active,
        // }
      ]
    }


    this.GetStores_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    this.GetStores_Option = {
      placeholder: PlaceHolder,
      ajax: this.GetStores_Transport,
      multiple: true,
    };
  }
  GetStores_ListChange(event: any) {
    this.StoresList = event.data
  }

  ScheduleStartDateRangeChange_Prev(value) {

    this.IsStartDateAfterEnd = moment(this.Form_EditUser.controls['StartDate'].value).isBefore(moment(value.start));

    // this.Form_EditUser.patchValue(
    //     {
    //         StartDate: moment(value.start).format('DD-MM-YYYY') + ' - ' + moment(value.end).format('DD-MM-YYYY'),
    //     }
    // );
    this.Form_EditUser.patchValue(
      {
        StartDate: value.end,
      }
    );
  }


  ScheduleEndDateRangeChange_Prev(value) {

    this.IsStartDateAfterEnd = moment(this.Form_EditUser.controls['EndDate'].value).isBefore(moment(value.start));

    this.Form_EditUser.patchValue(
      {
        EndDate: value.end,
      }
    );
  }

  ScheduleEndDateRangeChange_Modal(value) {

    this.IsStartDateAfterEnd = moment(moment(value.start)).isBefore(this.Form_EditUser.controls['StartDate'].value);

    // this.Form_EditUser.patchValue(
    //     {
    //         EndDate: moment(value.start).format('DD-MM-YYYY') + ' - ' + moment(value.end).format('DD-MM-YYYY'),
    //     }
    // );
    this.Form_EditUser.patchValue(
      {
        EndDate: value.end,
      }
    );

  }
  //end merchant and store--


  RouteEditDealDetails() {
    // this.DuplicateDealDetails[number].ReferenceKey = number.ReferenceKey
    // this.DuplicateDealDetails[number].ReferenceId = number.ReferenceId
    // this._HelperService.SaveStorage(
    //   this._HelperService.AppConfig.Storage.ActiveDeal,
    //   {
    //     ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
    //     ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
    //     AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
    //     AccountId: this._HelperService.AppConfig.ActiveAccountId,
    //   }
    // );


    // this._HelperService.CloseModal('exampleModal');

    this._Router.navigate([
      this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.EditDeal,
      this._HelperService.AppConfig.ActiveReferenceKey,
      this._HelperService.AppConfig.ActiveReferenceId,
      this._HelperService.AppConfig.ActiveAccountId,
      this._HelperService.AppConfig.ActiveAccountKey,
    ]);

    // this._Router.navigate([
    //   this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.EditDuplicateDeals,
    //   this._HelperService.AppConfig.ActiveReferenceKey,
    //   this._HelperService.AppConfig.ActiveReferenceId,

    //   this._HelperService.AppConfig.ActiveAccountId,
    //   this._HelperService.AppConfig.ActiveAccountKey,

    // ]);
    //this._HelperService.OpenModal('Account_Edit_Content');
    // console.log("edit deal", this._UserAccount.StatusCode)
    // if (this._UserAccount.StatusCode == 'deal.rejected') {
    //   this._HelperService.OpenModal('Account_Edit_Content_approve');
    // }
    // else {
    //   this._HelperService.OpenModal('Account_Edit_Content');
    // }
    // this.Form_EditUser_Show();
  }

  IsRunAllTime: boolean = true;
  RunAllTimeToogle(): void {
    this.IsRunAllTime = !(this.IsRunAllTime);
  }

  //#endregion

  //#region Redeem Time 

  IsValidAllTime: boolean = true;
  ValidAllTimeToogle(): void {
    this.IsValidAllTime = !(this.IsValidAllTime);
  }

  IsAllStores: boolean = true;
  AllStoresToogle(): void {
    this.IsAllStores = !(this.IsAllStores);
  }

  AKAmount: number = 0;

  Demo() {
    setTimeout(() => {
      var formValue: any = cloneDeep(this.Form_EditUser.value);
      var formRequest: any = {
        'OperationType': 'new',
        'Task': 'savedeal',
        "AccountId": formValue.AccountId,
        "AccountKey": formValue.AccountKey,
        "TypeCode": "deal",
        "Title": formValue.Title,
        "Description": formValue.Description,
        "Terms": formValue.Terms,
        "StartDate": formValue.StartDate,
        "EndDate": formValue.EndDate,

        CodeValidityDays: "10",
        // CodeValidityStartDate: "2020-11-04 00:00:00",
        CodeValidityEndDate: "2020-11-14 00:00:00",
        UsageTypeCode: "hour",

        "ActualPrice": formValue.ActualPrice,
        "SellingPrice": formValue.Amount,
        "DiscountAmount": formValue.DiscountAmount,

        "DiscountPercentage": 0.0,

        "Amount": formValue.Amount,
        "Charge": 0.0,
        "CommissionAmount": formValue.CommissionAmount,
        "TotalAmount": 0.0,

        "MaximumUnitSale": formValue.MaximumUnitSale,
        "MaximumUnitSalePerDay": formValue.MaximumUnitSalePerDay,
        "MaximumUnitSalePerPerson": formValue.MaximumUnitSalePerPerson,
        "CategoryKey": formValue.CategoryKey,
        "Schedule": [
        ]
      };
      if (this.NiraOrPercent.DiscountAmount == '0') { // percentage
        formRequest.DiscountPercentage = formValue.DiscountAmount;
        formRequest.DiscountAmount = (formValue.DiscountAmount / 100) * formValue.ActualPrice;
        formRequest.SellingPrice = formValue.ActualPrice - formRequest.DiscountAmount;
        formRequest.Amount = formRequest.SellingPrice;
        this.Form_EditUser.patchValue({ SellingPrice: (formValue.ActualPrice - formRequest.DiscountAmount) });
        this.Form_EditUser.patchValue({ Amount: (formValue.ActualPrice - formRequest.DiscountAmount) });
       // console.log('0', formRequest.DiscountAmount)
      } else if (this.NiraOrPercent.DiscountAmount == '1' && formValue.ActualPrice != 0.0) { // naira
        formRequest.DiscountPercentage = Math.round((formValue.DiscountAmount / formValue.ActualPrice) * 100);
        formRequest.DiscountAmount = formRequest.DiscountAmount;
        formRequest.Amount = (formValue.ActualPrice - formRequest.DiscountAmount);
        formRequest.SellingPrice = (formValue.ActualPrice - formRequest.DiscountAmount);
        this.Form_EditUser.patchValue({ SellingPrice: (formValue.ActualPrice - formRequest.DiscountAmount) });
        this.Form_EditUser.patchValue({ Amount: (formValue.ActualPrice - formRequest.DiscountAmount) });
       // console.log('1', formRequest.DiscountAmount)
      }

      if (this.NiraOrPercent.CommissionAmount == '0') {
        formRequest.CommissionAmount = (formValue.CommissionAmount / 100) * formValue.ActualPrice;

      }
      this.AKAmount = formRequest.Amount - formRequest.CommissionAmount;

    },

      100);

  }

  _ImageManager =
    {
      TCroppedImage: null,
      ActiveImage: null,
      ActiveImageName: null,
      ActiveImageSize: null,
      Option: {
        MaintainAspectRatio: "true",
        MinimumWidth: 800,
        MinimumHeight: 400,
        MaximumWidth: 800,
        MaximumHeight: 400,
        ResizeToWidth: 800,
        ResizeToHeight: 400,
        Format: "jpg",
      }
    }

  RemoveImage(Item) {
    this._DealConfig.DealImages = this._DealConfig.DealImages.filter(x => x != Item);
  }

  public SaveAccountRequest: any;
  public value: any;
  public IsDealSold: boolean = false;

  ShowDatePickers: boolean = true;
  @ViewChild(InputFileComponent)
  private InputFileComponent_Term: InputFileComponent;

  Form_EditUser_Process(_FormValue: any, IsDraft: boolean) {


    if (this.SelectedMerchant == undefined || this.SelectedMerchant == null) {
      this._HelperService.NotifyError("Select deal merchant");
    }
    else if (this._SelectedSubCategory == undefined || this._SelectedSubCategory == null || this._SelectedSubCategory.ReferenceId == undefined || this._SelectedSubCategory.ReferenceId == null || this._SelectedSubCategory.ReferenceId == 0) {
      this._HelperService.NotifyError("Select deal category");
    }
    else if (_FormValue.ActualPrice == undefined || _FormValue.ActualPrice == null || _FormValue.ActualPrice == undefined || _FormValue.ActualPrice == 0) {
      this._HelperService.NotifyError("Enter original price");
    }
    else if (_FormValue.SellingPrice == undefined || _FormValue.SellingPrice == null || _FormValue.SellingPrice == undefined || _FormValue.SellingPrice == 0) {
      this._HelperService.NotifyError("Enter selling price");
    }
    else if (_FormValue.SellingPrice <= 0) {
      this._HelperService.NotifyError("Actual price must be greater than 0");
    }
    else if (_FormValue.ActualPrice <= 0) {
      this._HelperService.NotifyError("Selling price must be greater than or equal 0");
    }
    else if (_FormValue.MaximumUnitSalePerPerson > _FormValue.MaximumUnitSale) {
      this._HelperService.NotifyError("Maximum purchase per person must be less than maximum purchase");
    }
    else if (_FormValue.SellingPrice > _FormValue.ActualPrice) {
      this._HelperService.NotifyError("Selling price must be less than or equal to actual price");
    }
    // else if (this._DealConfig.DealImages == undefined || this._DealConfig.DealImages == null || this._DealConfig.DealImages.length == 0) {
    //     this._HelperService.NotifyError("Atleast 1 image is required for adding deal");
    // }
    else if (this._DealConfig.StartDate == undefined || this._DealConfig.StartDate == null) {
      this._HelperService.NotifyError("Deal start date required");
    }
    else if (this._DealConfig.EndDate == undefined || this._DealConfig.EndDate == null) {
      this._HelperService.NotifyError("Deal end date required");
    }
    else if (this._DealConfig.EndDate < moment()) {
      this._HelperService.NotifyError("Deal end Date is greater than current date");
    }
    else {
      var Title = this._Titles.Title3;
      if (this._DealConfig.SelectedTitle == 1) {
        Title = this._Titles.Title1;
      }
      else if (this._DealConfig.SelectedTitle == 2) {
        Title = this._Titles.Title2;
      }
      else {
        Title = this._Titles.Title3;
      }
      var _PostData =
      {
        Task: "updatedeal",
        ReferenceId: this._DealDetails.ReferenceId,
        ReferenceKey: this._DealDetails.ReferenceKey,
        AccountId: this.SelectedMerchant.ReferenceId,
        AccountKey: this.SelectedMerchant.ReferenceKey,
        Title: Title,
        TitleContent: _FormValue.TitleContent,
        TitleTypeId: _FormValue.TitleTypeId,
        Description: _FormValue.Description,
        Terms: _FormValue.Terms,

        StartDate: moment(this._DealConfig.StartDate).format('YYYY-MM-DD HH:mm'),
        EndDate: moment(this._DealConfig.EndDate).format('YYYY-MM-DD HH:mm'),
        CodeUsageTypeCode: _FormValue.CodeUsageTypeCode,
        CodeValidityDays: this._DealConfig.SelectedDealCodeDays,
        CodeValidityHours: this._DealConfig.SelectedDealCodeHours,
        CodeValidityEndDate: null,
        ActualPrice: _FormValue.ActualPrice,
        SellingPrice: _FormValue.SellingPrice,
        MaximumUnitSale: _FormValue.MaximumUnitSale,
        MaximumUnitSalePerPerson: _FormValue.MaximumUnitSalePerPerson,
        SubCategoryKey: this._SelectedSubCategory.ReferenceKey,
        StatusCode: 'deal.draft',
        SendNotification: false,
        Images: [],
        Locations: [],
        Shedule: []
      };
      if (this._DealConfig.SelectedDealCodeEndDate != null) {
        _PostData.CodeValidityEndDate = moment(this._DealConfig.SelectedDealCodeEndDate).format('YYYY-MM-DD HH:mm')
      }
      this._DealConfig.DealImages.forEach(element => {
        element.OriginalContent = null;
        _PostData.Images.push(element);
      });
      if (IsDraft) {
        _PostData.StatusCode = 'deal.draft';
      } else {
        _PostData.StatusCode = 'deal.approvalpending';
      }
    //  console.log(_PostData);
      this._HelperService.IsFormProcessing = true;
      let _OResponse: Observable<OResponse>;
      _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Deals, _PostData);
      _OResponse.subscribe(
        _Response => {
          this._HelperService.IsFormProcessing = false;
          if (_Response.Status == this._HelperService.StatusSuccess) {
            this._HelperService.NotifySuccess(_Response.Message);
            this.Form_EditUser_Close();
            this.GetAccountDetails();
            // this.Form_AddUser.reset();
            // this._DealConfig =
            // {
            //     DefaultStartDate: null,
            //     DefaultEndDate: null,
            //     SelectedDealCodeHours: 12,
            //     SelectedDealCodeDays: 30,
            //     SelectedDealCodeEndDate: 0,
            //     DealCodeValidityTypeCode: 'dealenddate',
            //     StartDate: null,
            //     EndDate: null,
            //     DealImages: [],
            //     SelectedTitle: 3,
            //     Images: [],
            //     StartDateConfig: {
            //     },
            //     EndDateConfig: {
            //     },
            //     DealCodeEndDateConfig: {
            //     }
            // }
            // this._ImageManager =
            // {
            //     TCroppedImage: null,
            //     ActiveImage: null,
            //     ActiveImageName: null,
            //     ActiveImageSize: null,
            //     Option: {
            //         MaintainAspectRatio: "true",
            //         MinimumWidth: 800,
            //         MinimumHeight: 400,
            //         MaximumWidth: 800,
            //         MaximumHeight: 400,
            //         ResizeToWidth: 800,
            //         ResizeToHeight: 400,
            //         Format: "jpg",
            //     }
            // }
            // this.ngOnInit();
            // this.Form_AddUser_Close();
          }
          else {
            this._HelperService.NotifyError(_Response.Message);
          }
        },
        _Error => {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.HandleException(_Error);
        });
    }
  }

  ReFormat_RequestBody(): void {
    var formValue: any = cloneDeep(this.Form_EditUser.value);
    var formRequest: any = {
      'OperationType': 'new',
      'Task': 'updatedeal',
      "AccountId": formValue.AccountId,
      "AccountKey": formValue.AccountKey,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      "TypeCode": "deal",
      "Title": formValue.Title,
      "Description": formValue.Description,
      "Terms": formValue.Terms,
      "StartDate": formValue.StartDate,
      "EndDate": formValue.EndDate,

      CodeValidityDays: "10",
      // CodeValidityStartDate: "2020-11-04 00:00:00",
      CodeValidityEndDate: "2020-11-14 00:00:00",
      UsageTypeCode: "hour",

      "ActualPrice": formValue.ActualPrice,
      "SellingPrice": formValue.Amount,
      "DiscountAmount": formValue.DiscountAmount,

      "DiscountPercentage": 0,

      "Amount": formValue.Amount,
      "Charge": 0.0,
      "CommissionAmount": formValue.CommissionAmount,
      "TotalAmount": 0.0,

      "MaximumUnitSale": formValue.MaximumUnitSale,
      "MaximumUnitSalePerDay": formValue.MaximumUnitSalePerDay,
      "MaximumUnitSalePerPerson": formValue.MaximumUnitSalePerPerson,
      "CategoryKey": formValue.CategoryKey,
      "Schedule": [

      ]
    };

    if (this.NiraOrPercent.DiscountAmount == '0') {
      formRequest.DiscountPercentage = formValue.DiscountAmount;
      formRequest.DiscountAmount = (formValue.DiscountAmount / 100) * formValue.ActualPrice;
    } else if (this.NiraOrPercent.DiscountAmount == '1' && formValue.ActualPrice != 0.0) {
      formRequest.DiscountPercentage = (formValue.DiscountAmount / formValue.ActualPrice) * 100;
    }

    if (this.NiraOrPercent.CommissionAmount == '0') {
      formRequest.CommissionAmount = (formValue.CommissionAmount / 100) * formValue.ActualPrice;
    }

    //#region set image 

    if (this._HelperService._Icon_Cropper_Data.Content != null) {
      formRequest.Images = [
        {
          "ImageContent": this._HelperService._Icon_Cropper_Data,
          "IsDefault": 1
        }
      ];
    }

    //#endregion

    //#region Set Title (not required)

    // switch (formValue.DealTitle) {
    //     case '0': {
    //         formRequest.Title =
    //             formValue.ActualPrice + " " + formRequest.Title + ' for ' +
    //             formValue.SellingPrice + ' at ' + this.SelectedMerchant.DisplayName;
    //     }

    //         break;
    //     case '1': {
    //         formRequest.Title = formValue.DiscountAmount + ' off on ' + formRequest.Title;
    //     }

    //         break;
    //     case '2': {
    //         formRequest.Title = ((formValue.DiscountAmount / formValue.ActualPrice) * 100) + '% off on ' + formRequest.Title;
    //     }

    //         break;

    //     default:
    //         break;
    // }

    //#endregion 

    //#region Add store locations 

    if (!this.IsAllStores) {

      formRequest.Locations = [
      ];

      for (let index = 0; index < this.StoresList.length; index++) {
        const element = this.StoresList[index];

        formRequest.Locations.push({
          ReferenceId: element.ReferenceId,
          ReferenceKey: element.ReferenceKey
        });
      }
    }

    //#endregion

    //#region Add Redeem schedule days 

    for (let index = 0; index < this.WeekDays.length; index++) {
      const element = this.WeekDays[index];

      if (element.selected) {
        // formRequest.Schedule[0].DayOfWeek.push(element.name);
        formRequest.Schedule.push({ DayOfWeek: index });
      }

    }

    //#endregion

    //#region Set Redeem Schedule Timing 

    for (let index = 0; index < formRequest.Schedule.length; index++) {

      if (this.IsValidAllTime) {
        formRequest.Schedule[index].StartHour = '00:00';
        formRequest.Schedule[index].EndHour = '23:59';
        formRequest.Schedule[index].Type = 'dealredeemshedule';
      } else {
        formRequest.Schedule[index].StartHour = moment(this.ValidTimings.Start).format('hh:mm');
        formRequest.Schedule[index].EndHour = moment(this.ValidTimings.End).format('hh:mm');
        formRequest.Schedule[index].Type = 'dealredeemshedule';
      }
    }

    //#endregion

    //#region Set Schedule 

    for (let index = 0; index < 7; index++) {

      if (this.IsRunAllTime) {
        formRequest.Schedule.push({
          DayOfWeek: index,
          StartHour: '00:00',
          EndHour: '23:59',
          Type: 'dealshedule'
        });
      } else {
        formRequest.Schedule.push({
          DayOfWeek: index,
          StartHour: moment(this.DealTimings.Start).format('hh:mm'),
          EndHour: moment(this.DealTimings.End).format('hh:mm'),
          Type: 'dealshedule'
        });

      }
    }

    //#endregion

    //#region Set All Stores 



    //#endregion

    //#region Set Coupon Validity 

    switch (formValue.CouponValidity) {
      case '0': {
        formRequest.CodeValidityDays = this.Expiry.Days * 24;
        formRequest.UsageTypeCode = 'hour'
      }

        break;
      case '1': {
        formRequest.CodeValidityDays = this.Expiry.Hours;
        formRequest.UsageTypeCode = 'hour'
      }

        break;
      case '2': {
        formRequest.CodeValidityEndDate = moment(formRequest.EndDate);
        formRequest.UsageTypeCode = 'date';
      }

        break;
      case '3': {
        formRequest.CodeValidityEndDate = moment(this.Expiry.Date);
        formRequest.UsageTypeCode = 'date';
      }

        break;

      default:
        break;
    }

    //#endregion

    // console.log(formRequest);
    return formRequest;

  }


  //new code---
  public _S2Categories_Data: Array<Select2OptionData>;
  _SelectedSubCategory =
    {
      ReferenceId: 0,
      ReferenceKey: null,
      Fee: 5,
      Name: null,
      IconUrl: null,

      ParentCategoryId: 0,
      ParentCategoryKey: 0,
      ParentCategoryName: 0
    };
  GetDealCategories_List() {
    var pData = {
      Task: this._HelperService.AppConfig.Api.Core.getallcategories,
      Offset: 0,
      Limit: 1000,
    };

    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.System2, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
         // console.log(_Response);
          var _Data = _Response.Result.Data as any[];
          if (_Data.length > 0) {
            var parentCategories = [];
            _Data.forEach(element => {
              var itemIndex = parentCategories.findIndex(x => x.ParentCategoryId == element.ParentCategoryId);
              if (itemIndex == -1) {
                var categories = _Data.filter(x => x.ParentCategoryId == element.ParentCategoryId);
                parentCategories.push(
                  {
                    ParentCategoryId: element.ParentCategoryId,
                    ParentCategoryKey: element.ParentCategoryKey,
                    ParentCategoryName: element.ParentCategoryName,
                    Categories: categories,
                  }
                );
              }
            });
            var finalCat = [];
            parentCategories.forEach(element => {
              var Item = {
                id: element.ParentCategoryId,
                text: element.ParentCategoryName,
                children: [],
                additional: element,
              };
              element.Categories.forEach(CategoryItem => {
                var cItem = {
                  id: CategoryItem.ReferenceId,
                  text: CategoryItem.Name + " (" + CategoryItem.Fees + "%)",
                  additional: CategoryItem,
                };
                Item.children.push(cItem);
              });
              finalCat.push(Item);
            });
            this._S2Categories_Data = finalCat;
            this.GetDeal();
          }
        }
        else {
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }

  GetDealCategories_Selected(event: any) {
   // console.log(event);
    if (event.value != "0") {
      this._SelectedSubCategory.ReferenceId = event.data[0].additional.ReferenceId;
      this._SelectedSubCategory.ReferenceKey = event.data[0].additional.ReferenceKey;
      this._SelectedSubCategory.Name = event.data[0].additional.Name;
      this._SelectedSubCategory.IconUrl = event.data[0].additional.IconUrl;
      this._SelectedSubCategory.Fee = event.data[0].additional.Fees;
      this._AmountDistribution.TUCPercentage = event.data[0].additional.Fees;
      this._SelectedSubCategory.ParentCategoryId = event.data[0].additional.ParentCategoryId;
      this._SelectedSubCategory.ParentCategoryKey = event.data[0].additional.ParentCategoryKey;
      this._SelectedSubCategory.ParentCategoryName = event.data[0].additional.ParentCategoryName;
      this.Form_EditUser.patchValue(
        {
          SubCategoryKey: this._SelectedSubCategory.ParentCategoryKey
        }
      );
      this.ProcessAmounts();
    }
  }



  onImageAccept(value) {
    setTimeout(() => {
      this.Form_EditUser.patchValue(
        {
          TImage: null,
        }
      );
    }, 300);
   // console.log(value);
   // console.log(value.file);
    this._ImageManager.ActiveImage = value;
    this._ImageManager.ActiveImageName = value.file.name;
    this._ImageManager.ActiveImageName = value.file.size;
  }
  IsDealLoaded = false;
  public _DealDetails: any =
    {
    };
  GetDeal() {
    this.IsDealLoaded = false;
    this._HelperService.IsFormProcessing = true;
    var pData = {
      Task: this._HelperService.AppConfig.Api.ThankUCash.GetDeal,
      ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
      ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
      AccountId: this._HelperService.AppConfig.ActiveAccountId,
      AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
    }
    let _OResponse: Observable<OResponse>;
    _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Deals, pData);
    _OResponse.subscribe(
      _Response => {
        if (_Response.Status == this._HelperService.StatusSuccess) {
          this.IsDealLoaded = true;
        //  console.log(_Response);
          this._HelperService.IsFormProcessing = false;
          this._DealDetails = _Response.Result;
          this.Form_EditUser.controls['AccountKey'].setValue(this._DealDetails.AccountKey);
          this.Form_EditUser.controls['AccountId'].setValue(this._DealDetails.AccountId);
          this.Form_EditUser.controls['SubCategoryKey'].setValue(this._DealDetails.SubCategoryKey);
          // this.MerchantPlaceHolder = this._DealDetails.AccountDisplayName;
          this.GetMerchants_List();

          this.Form_EditUser.controls['TitleTypeId'].setValue(this._DealDetails.TitleTypeId);
          this.Form_EditUser.controls['TitleContent'].setValue(this._DealDetails.TitleContent);
          this.Form_EditUser.controls['Description'].setValue(this._DealDetails.Description);
          this.Form_EditUser.controls['Terms'].setValue(this._DealDetails.Terms);
          this.Form_EditUser.controls['MaximumUnitSale'].setValue(this._DealDetails.MaximumUnitSale);
          this.Form_EditUser.controls['MaximumUnitSalePerPerson'].setValue(this._DealDetails.MaximumUnitSalePerPerson);
          this.Form_EditUser.controls['Terms'].setValue(this._DealDetails.Terms);
          this.Form_EditUser.controls['ActualPrice'].setValue(this._DealDetails.ActualPrice);
          this.Form_EditUser.controls['SellingPrice'].setValue(this._DealDetails.SellingPrice);
          this.Form_EditUser.controls['StartDate'].setValue(moment(this._DealDetails.StartDate).format('DD-MM-YYYY hh:mm a'));
          this.Form_EditUser.controls['EndDate'].setValue(moment(this._DealDetails.EndDate).format('DD-MM-YYYY hh:mm a'));

          if (this._DealDetails.Images != undefined && this._DealDetails.Images != null && this._DealDetails.Images.length > 0) {
            this._DealConfig.SavedDealImages = this._DealDetails.Images;
          }
          this.SelectedMerchant =
          {
            ReferenceId: this._DealDetails.AccountId,
            ReferenceKey: this._DealDetails.AccountKey,
            DisplayName: this._DealDetails.AccountDisplayName,
          };
          this._Titles.OriginalTitle = this._DealDetails.TitleContent;
          this._SelectedSubCategory =
          {
            ReferenceId: this._DealDetails.SubCategoryId,
            ReferenceKey: this._DealDetails.SubCategoryKey,
            Fee: this._DealDetails.SubCategoryFee,
            Name: this._DealDetails.SubCategoryName,
            IconUrl: null,

            ParentCategoryId: this._DealDetails.CategoryId,
            ParentCategoryKey: this._DealDetails.CategoryKey,
            ParentCategoryName: this._DealDetails.CategoryName,
          };
          this._AmountDistribution =
          {
            ActualPrice: this._DealDetails.ActualPrice,
            SellingPrice: this._DealDetails.SellingPrice,
            SellingPricePercentage: this._DealDetails.DiscountPercentage,
            SellingPriceDifference: this._DealDetails.DiscountAmount,
            TUCPercentage: this._HelperService.GetPercentageFromNumber(this._DealDetails.CommissionAmount, this._DealDetails.SellingPrice),
            TUCAmount: this._DealDetails.CommissionAmount,
            MerchantAmount: this._HelperService.GetFixedDecimalNumber((this._DealDetails.SellingPrice - this._DealDetails.CommissionAmount)),
            MerchantPercentage: this._HelperService.GetPercentageFromNumber(this._DealDetails.MerchantAmount, this._DealDetails.SellingPrice),
          };
          var tStartDate = moment(this._DealDetails.StartDate);
          var tEndDate = moment(this._DealDetails.EndDate);

          this._DealConfig.SelectedTitle = this._DealDetails.TitleTypeId;
          if (this._DealDetails.CodeValidityEndDate != undefined && this._DealDetails.CodeValidityEndDate != null) {
            this._DealConfig.SelectedDealCodeEndDate = this._DealDetails.CodeValidityEndDate;
            this.Form_EditUser.controls['CodeValidityEndDate'].setValue(moment(this._DealDetails.CodeValidityEndDate).format('DD-MM-YYYY hh:mm a'));
          }
          this._DealConfig.DealCodeValidityTypeCode = this._DealDetails.UsageTypeCode;
          this.Form_EditUser.controls['CodeUsageTypeCode'].setValue(this._DealDetails.UsageTypeCode);
          if (this._DealConfig.DealCodeValidityTypeCode == "hour") {
            this._DealConfig.SelectedDealCodeHours = this._DealDetails.CodeValidityDays;
          }
          if (this._DealConfig.DealCodeValidityTypeCode == "daysafterpurchase") {
            this._DealConfig.SelectedDealCodeDays = Math.round(this._DealDetails.CodeValidityDays / 24);
          }
          this._DealConfig.StartDate = tStartDate;
          this._DealConfig.EndDate = tEndDate;
          this._DealConfig.DefaultStartDate = moment().startOf('day');
          this._DealConfig.DefaultEndDate = moment().add(1, 'days').endOf("day");

          this._DealConfig.StartDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            startDate: this._HelperService.DateInUTC(moment().startOf("day")),
            endDate: this._HelperService.DateInUTC(moment().endOf("day")),
            minDate: moment(),
          };
          this._DealConfig.EndDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            startDate: this._HelperService.DateInUTC(moment().startOf("day")),
            endDate: this._HelperService.DateInUTC(moment().endOf("day")),
            minDate: moment(),
          };
          this._DealConfig.DealCodeEndDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            startDate: this._DealConfig.EndDate,
            minDate: this._DealConfig.EndDate,
          };
          // this._DealConfig =
          // {
          //     DealImages: [],
          //     Images: [],
          //     DealCodeEndDateConfig: {
          //     }
          // };
          // this.startdate = this._HelperService.GetDateS(this._DealDetails.StartDate);
          // this.enddate = this._HelperService.GetDateS(this._DealDetails.EndDate);
          // this.catfees=this._DealDetails.CategoryFee;
          // this.categoryPlaceholder = this._DealDetails.SubCategoryName; 
          this.ProcessAmounts();
        }
        else {
          this._HelperService.IsFormProcessing = false;
          this._HelperService.NotifyError(_Response.Message);
        }
      },
      _Error => {
        this._HelperService.HandleException(_Error);
      }
    );
  }


  //#region Amount Distribution
  _AmountDistribution =
    {
      ActualPrice: null,
      SellingPrice: null,
      SellingPricePercentage: null,
      SellingPriceDifference: null,
      TUCPercentage: null,
      TUCAmount: null,
      MerchantPercentage: null,
      MerchantAmount: null,
    };
  ProcessAmounts() {
    var tAmount = this._AmountDistribution;
    if (tAmount.ActualPrice != undefined && tAmount.ActualPrice != null && isNaN(tAmount.ActualPrice) == false && tAmount.ActualPrice >= 0) {
      if (tAmount.SellingPrice != undefined && tAmount.SellingPrice != null && isNaN(tAmount.SellingPrice) == false && tAmount.SellingPrice >= 0) {
        if (tAmount.ActualPrice >= tAmount.SellingPrice) {
          tAmount.SellingPriceDifference = this._HelperService.GetFixedDecimalNumber(tAmount.ActualPrice - tAmount.SellingPrice);
          tAmount.SellingPricePercentage = this._HelperService.GetPercentageFromNumber(tAmount.SellingPriceDifference, tAmount.ActualPrice);
          tAmount.TUCAmount = this._HelperService.GetAmountFromPercentage(tAmount.SellingPrice, tAmount.TUCPercentage);
          tAmount.MerchantAmount = this._HelperService.GetFixedDecimalNumber(tAmount.SellingPrice - tAmount.TUCAmount);
          tAmount.MerchantPercentage = this._HelperService.GetPercentageFromNumber(tAmount.MerchantAmount, tAmount.SellingPrice);
        }
        else {
          if (parseFloat(this._AmountDistribution.SellingPrice) >= parseFloat(this._AmountDistribution.ActualPrice)) {
            setTimeout(() => {
              this._AmountDistribution =
              {
                ActualPrice: this._AmountDistribution.ActualPrice,
                SellingPrice: null,
                SellingPricePercentage: null,
                SellingPriceDifference: null,
                TUCPercentage: this._AmountDistribution.TUCPercentage,
                TUCAmount: null,
                MerchantPercentage: null,
                MerchantAmount: null,
              };
              tAmount.SellingPriceDifference = this._HelperService.GetFixedDecimalNumber(tAmount.ActualPrice - 0);
              tAmount.SellingPricePercentage = this._HelperService.GetPercentageFromNumber(tAmount.SellingPriceDifference, tAmount.ActualPrice);
              tAmount.TUCAmount = this._HelperService.GetAmountFromPercentage(0, tAmount.TUCPercentage);
              tAmount.MerchantAmount = this._HelperService.GetFixedDecimalNumber(0 - tAmount.TUCAmount);
              tAmount.MerchantPercentage = this._HelperService.GetPercentageFromNumber(tAmount.MerchantAmount, 0);
            }, 200);
          }
        }
      }
    }
    //console.log(this._AmountDistribution);
    this.ProcessTitle();
  }
  //#endregion

  //#region Title Manager
  _Titles =
    {
      OriginalTitle: "",
      Title1: this._HelperService.UserCountry.CurrencyCode + " X deal title for " + this._HelperService.UserCountry.CurrencyCode + "Y",
      Title2: this._HelperService.UserCountry.CurrencyCode + " Y off on deal title",
      Title3: "x% off on deal title"
    }
  ProcessTitle() {
    // if (this._AmountDistribution.ActualPrice == undefined) {
    //     this._AmountDistribution.ActualPrice = 0;
    // }
    // if (this._AmountDistribution.SellingPrice == undefined) {
    //     this._AmountDistribution.SellingPrice = 0;
    // }
    // if (this._AmountDistribution.SellingPricePercentage == undefined) {
    //     this._AmountDistribution.SellingPricePercentage = 0;
    // }
    // if (this._AmountDistribution.SellingPriceDifference == undefined) {
    //     this._AmountDistribution.SellingPriceDifference = 0;
    // }
    // if (this._AmountDistribution.TUCPercentage == undefined) {
    //     this._AmountDistribution.TUCPercentage = 0;
    // }
    // if (this._AmountDistribution.TUCAmount == undefined) {
    //     this._AmountDistribution.TUCAmount = 0;
    // }
    // if (this._AmountDistribution.MerchantPercentage == undefined) {
    //     this._AmountDistribution.MerchantPercentage = 0;
    // }
    // if (this._AmountDistribution.MerchantAmount == undefined) {
    //     this._AmountDistribution.MerchantAmount = 0;
    // }
    // this._Titles.Title1 = this._HelperService.UserCountry.CurrencyCode + " " + Math.round(this._AmountDistribution.ActualPrice) + " " + this._Titles.OriginalTitle + " for " + this._HelperService.UserCountry.CurrencyCode + " " + Math.round(this._AmountDistribution.SellingPrice);
    // this._Titles.Title2 = this._HelperService.UserCountry.CurrencyCode + " " + Math.round(this._AmountDistribution.SellingPriceDifference) + " off on " + this._Titles.OriginalTitle;
    // this._Titles.Title3 = Math.round(this._AmountDistribution.SellingPricePercentage) + "% off on " + this._Titles.OriginalTitle;

    //  this._Titles.Title1 = "N " + Math.round(this._AmountDistribution.ActualPrice) + " " + this._Titles.OriginalTitle + " for N " + Math.round(this._AmountDistribution.SellingPrice);
    //  this._Titles.Title2 = "N " + Math.round(this._AmountDistribution.SellingPriceDifference) + " off on " + this._Titles.OriginalTitle;
    //  this._Titles.Title3 = Math.round(this._AmountDistribution.SellingPricePercentage) + "% off on " + this._Titles.OriginalTitle;

    this._Titles.Title1 = this._HelperService.UserCountry.CurrencyNotation + " " + Math.round(this._AmountDistribution.ActualPrice) + " " + this._Titles.OriginalTitle + " for " + this._HelperService.UserCountry.CurrencyNotation + " " + Math.round(this._AmountDistribution.SellingPrice);
    this._Titles.Title2 = this._HelperService.UserCountry.CurrencyNotation + " " + Math.round(this._AmountDistribution.SellingPriceDifference) + " off on " + this._Titles.OriginalTitle;
    this._Titles.Title3 = Math.round(this._AmountDistribution.SellingPricePercentage) + "% off on " + this._Titles.OriginalTitle;

    // if (this._Titles.OriginalTitle != undefined && this._Titles.OriginalTitle != null && this._Titles.OriginalTitle != "") {
    //     this._Titles.Title1 = this._HelperService.UserCountry.CurrencyCode + " " + this._AmountDistribution.ActualPrice + " " + this._Titles.OriginalTitle + " for " + this._HelperService.UserCountry.CurrencyCode + " " + this._AmountDistribution.SellingPrice;
    //     this._Titles.Title2 = this._HelperService.UserCountry.CurrencyCode + " " + this._AmountDistribution.SellingPriceDifference + " off on " + this._Titles.OriginalTitle;
    //     this._Titles.Title3 = this._AmountDistribution.SellingPricePercentage + "% off on " + this._Titles.OriginalTitle;
    // }
    // else {
    //     // this._Titles.Title1 = this._HelperService.UserCountry.CurrencyCode + " X deal title for " + this._HelperService.UserCountry.CurrencyCode + "Y";
    //     // this._Titles.Title2 = this._HelperService.UserCountry.CurrencyCode + " Y off on deal title";
    //     // this._Titles.Title3 = "x% off on deal title";
    // }
   // console.log(this._Titles);
  }
  //#endregion


  //#region Deal Shedule Manager 
  ScheduleStartDateRangeChange(value) {
  //  console.log(value);
    this._DealConfig.EndDateConfig = {
      autoUpdateInput: false,
      singleDatePicker: true,
      timePicker: true,
      locale: { format: "DD-MM-YYYY" },
      alwaysShowCalendars: false,
      showDropdowns: true,
      startDate: value.start,
      minDate: value.start,
    };
    this._DealConfig.StartDate = value.start;
    this._DealConfig.EndDate = value.start;
    this._DealConfig.SelectedDealCodeEndDate = value.start;
    this.Form_EditUser.patchValue(
      {
        StartDate: value.start.format('DD-MM-YYYY hh:mm a'),
        EndDate: value.start.format('DD-MM-YYYY hh:mm a'),
        CodeValidityEndDate: value.start.format('DD-MM-YYYY hh:mm a'),
      }
    );
  }
  ScheduleEndDateRangeChange(value) {
    this._DealConfig.DealCodeEndDateConfig = {
      autoUpdateInput: false,
      singleDatePicker: true,
      timePicker: true,
      locale: { format: "DD-MM-YYYY" },
      alwaysShowCalendars: false,
      showDropdowns: true,
      startDate: value.start,
      minDate: value.start,
    };
    this._DealConfig.EndDate = value.start;
    this._DealConfig.SelectedDealCodeEndDate = value.start;
    this.Form_EditUser.patchValue(
      {
        EndDate: value.start.format('DD-MM-YYYY hh:mm a'),
        CodeValidityEndDate: value.start.format('DD-MM-YYYY hh:mm a'),
      }
    );
  }
  ScheduleCodeEndDateRangeChange(value) {
    this._DealConfig.SelectedDealCodeEndDate = value.start;
    this.Form_EditUser.patchValue(
      {
        CodeValidityEndDate: value.start.format('DD-MM-YYYY hh:mm a'),
      }
    );
  }
  //#endregion

}

