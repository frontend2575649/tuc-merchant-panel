import { AgmCoreModule } from '@agm/core';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { AgmOverlays } from 'agm-overlays';
import { ChartsModule } from 'ng2-charts';
import { Daterangepicker } from 'ng2-daterangepicker';
import { Ng2FileInputModule } from 'ng2-file-input';
import { Select2Module } from 'ng2-select2';
import { NgxPaginationModule } from 'ngx-pagination';
import { TUDealComponent } from './tudeal.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { TimepickerModule } from 'ngx-bootstrap';
import { CountdownModule } from 'ngx-countdown';
import { InputFileConfig, InputFileModule } from 'ngx-input-file';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

const config: InputFileConfig = {
    fileAccept: '*',
    fileLimit: 3,
};
const routes: Routes = [
    { path: '', component: TUDealComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TUDealRoutingModule { }

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        Select2Module,
        NgxPaginationModule,
        Daterangepicker,
        Ng2FileInputModule,
        TUDealRoutingModule,
        TimepickerModule.forRoot(),
        AgmOverlays,
        CountdownModule,
        InputFileModule.forRoot(config),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB9DMDX78ll840bMxjGL4WUfjAYXfi8vZo'
        }),
        ChartsModule,
        ImageCropperModule,
        CKEditorModule
    ],
    declarations: [TUDealComponent]
})
export class TUDealModule {

} 
