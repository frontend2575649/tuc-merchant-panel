import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { OSelect, OList, DataHelperService, HelperService, OResponse, OStorageContent } from "../../../../service/service";
import swal from 'sweetalert2';
declare var $: any;
import * as Feather from 'feather-icons';
declare var google: any;
declare var moment: any;
import * as cloneDeep from 'lodash/cloneDeep';
import { InputFileComponent, InputFile } from 'ngx-input-file';
import * as _ from 'lodash';
import { Select2OptionData } from 'ng2-select2';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { MatDialog } from '@angular/material';
import { FileManagerComponent } from 'src/app/modules/file-manager/file-manager.component';

// 
@Component({
    selector: 'app-tueditdeal',
    templateUrl: './tueditdeal.component.html'
})
export class EditDealComponent implements OnInit {

    dealType: any = '';
    isStorePickup = false;
    isDeliverToAddress = false;
    Type_Data = ['box', "envelope", "soft-packaging"];
    Form_AddDeal: FormGroup;

    public ckConfig =
        {
            toolbar: ['Bold', 'Italic', 'NumberedList', 'BulletedList'],
            height: 300
        }
    public Editor = ClassicEditor;
    public data = "";
    _ImageManager =
        {
            TCroppedImage: null,
            ActiveImage: null,
            ActiveImageName: null,
            ActiveImageSize: null,
            Option: {
                MaintainAspectRatio: "true",
                MinimumWidth: 700,
                MinimumHeight: 550,
                MaximumWidth: 700,
                MaximumHeight: 550,
                ResizeToWidth: 700,
                ResizeToHeight: 550,
                Format: "jpg",
            }
        }
    public Hours = [];
    public Days = [];


    _DealConfig =
        {
            DefaultStartDate: null,
            DefaultEndDate: null,
            SelectedDealCodeHours: 12,
            SelectedDealCodeDays: 30,
            SelectedDealCodeEndDate: 0,
            DealCodeValidityTypeCode: 'daysafterpurchase',
            StartDate: null,
            EndDate: null,
            DealImages: [],
            SavedDealImages: [],
            SelectedTitle: 3,
            Images: [],
            StartDateConfig: {
            },
            EndDateConfig: {
            },
            DealCodeEndDateConfig: {
            }
        };

    constructor(
        public _Router: Router,
        public _ActivatedRoute: ActivatedRoute,
        public _FormBuilder: FormBuilder,
        public _HelperService: HelperService,
        public _DataHelperService: DataHelperService,
        public _ChangeDetectorRef: ChangeDetectorRef,
        public _dialog: MatDialog
    ) {
    }



    ngOnInit(): void {
        for (let index = 1; index < 24; index++) {
            this.Hours.push(index);
        }
        for (let index = 1; index < 366; index++) {
            this.Days.push(index);
        }
        // this.GetMerchants_List();
        this._DealConfig.DefaultStartDate = moment().startOf('day');
        this._DealConfig.DefaultEndDate = moment().add(1, 'weeks').endOf("day");
        this._DealConfig.StartDate = this._DealConfig.DefaultStartDate;
        this._DealConfig.EndDate = this._DealConfig.DefaultEndDate;
        this._DealConfig.StartDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            startDate: this._HelperService.DateInUTC(moment().startOf("day")),
            endDate: this._HelperService.DateInUTC(moment().endOf("day")),
            minDate: moment(),
        };
        this._DealConfig.EndDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            startDate: this._HelperService.DateInUTC(moment().startOf("day")),
            endDate: this._HelperService.DateInUTC(moment().endOf("day")),
            minDate: moment(),
        };
        this._DealConfig.DealCodeEndDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            startDate: this._DealConfig.EndDate,
            minDate: this._DealConfig.EndDate,
        };
        this._ActivatedRoute.params.subscribe((params: Params) => {
            this._HelperService.AppConfig.ActiveReferenceKey = params["referencekey"];
            this._HelperService.AppConfig.ActiveReferenceId = params["referenceid"];

            this._HelperService.AppConfig.ActiveAccountKey = params["accountkey"];
            this._HelperService.AppConfig.ActiveAccountId = params["accountid"];

        });
        this.Form_AddUser_Load();
        this.GetDealCategories_List();
        this.Form_AddDeal_Load();
    }
    IsDealLoaded = false;
    public _DealDetails: any =
        {
        };

    Form_AddDeal_Load() {

        this.Form_AddDeal = this._FormBuilder.group({
            Title: [
                null,
                Validators.compose([
                    Validators.required,
                ])
            ],
            Description: [
                null,
                Validators.compose([
                    Validators.required,
                    Validators.minLength(30),
                    Validators.maxLength(60)
                ])
            ],
            Type: [
                null,
                Validators.compose([
                    Validators.required,
                ])
            ],
            Height: [
                null,
                Validators.compose([
                    Validators.required,
                ])
            ],
            Weight: [
                null,
                Validators.compose([
                    Validators.required,
                ])
            ],
            Length: [
                null,
                Validators.compose([
                    Validators.required,
                ])
            ],
            Width: [
                null,
                Validators.compose([
                    Validators.required,
                ])
            ],
        });
    }

    GetDeal() {
        this.IsDealLoaded = false;
        this._HelperService.IsFormProcessing = true;
        var pData = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetDeal,
            ReferenceId: this._HelperService.AppConfig.ActiveReferenceId,
            ReferenceKey: this._HelperService.AppConfig.ActiveReferenceKey,
            AccountId: this._HelperService.AppConfig.ActiveAccountId,
            AccountKey: this._HelperService.AppConfig.ActiveAccountKey,
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.IsDealLoaded = true;
                    this._HelperService.IsFormProcessing = false;
                    this._DealDetails = this._HelperService.HCXGetDateComponent(_Response.Result);
                    this.Form_AddUser.controls['AccountKey'].setValue(this._DealDetails.AccountKey);
                    this.Form_AddUser.controls['AccountId'].setValue(this._DealDetails.AccountId);
                    this.Form_AddUser.controls['SubCategoryKey'].setValue(this._DealDetails.SubCategoryKey);
                    this.Form_AddUser.controls['TitleTypeId'].setValue(this._DealDetails.TitleTypeId);
                    this.Form_AddUser.controls['TitleContent'].setValue(this._DealDetails.TitleContent);
                    this.Form_AddUser.controls['Description'].setValue(this._DealDetails.Description);
                    this.Form_AddUser.controls['Terms'].setValue(this._DealDetails.Terms);
                    this.Form_AddUser.controls['MaximumUnitSale'].setValue(this._DealDetails.MaximumUnitSale);
                    this.Form_AddUser.controls['MaximumUnitSalePerPerson'].setValue(this._DealDetails.MaximumUnitSalePerPerson);
                    this.Form_AddUser.controls['Terms'].setValue(this._DealDetails.Terms);
                    this.Form_AddUser.controls['ActualPrice'].setValue(this._DealDetails.ActualPrice);
                    this.Form_AddUser.controls['SellingPrice'].setValue(this._DealDetails.SellingPrice);
                    this.Form_AddUser.controls['TUCPercentage'].setValue(this._DealDetails.CommissionPercentage);
                    this.Form_AddUser.controls['StartDate'].setValue(this._DealDetails.StartDatePart.DateTime);
                    this.Form_AddUser.controls['EndDate'].setValue(this._DealDetails.EndDatePart.DateTime);
                    this.dealType = this._DealDetails.DealTypeCode;

                    if (this._DealDetails.DeliveryTypeCode === 'deliverytype.instoreanddelivery') {
                        this.isDeliverToAddress = true;
                        this.isStorePickup = true;
                    }
                    if (this._DealDetails.DeliveryTypeCode === 'deliverytype.instore') {
                        this.isStorePickup = true;
                    }

                    if (this._DealDetails.DeliveryTypeCode === 'deliverytype.delivery') {
                        this.isDeliverToAddress = true;
                    }

                    if (this._DealDetails.Packaging) {
                        this.Form_AddDeal.controls['Title'].setValue(this._DealDetails.Packaging.Name);
                        this.Form_AddDeal.controls['Description'].setValue(this._DealDetails.Packaging.ParcelDescription);
                        this.Form_AddDeal.controls['Type'].setValue(this._DealDetails.Packaging.Type);
                        this.Form_AddDeal.controls['Height'].setValue(this._DealDetails.Packaging.Height);
                        this.Form_AddDeal.controls['Weight'].setValue(this._DealDetails.Packaging.Weight);
                        this.Form_AddDeal.controls['Length'].setValue(this._DealDetails.Packaging.Length);
                        this.Form_AddDeal.controls['Width'].setValue(this._DealDetails.Packaging.Width);
                    }
                    if (this._DealDetails.Images != undefined && this._DealDetails.Images != null && this._DealDetails.Images.length > 0) {
                        this._DealConfig.SavedDealImages = this._DealDetails.Images;
                    }
                    this.SelectedMerchant =
                    {
                        ReferenceId: this._DealDetails.AccountId,
                        ReferenceKey: this._DealDetails.AccountKey,
                        DisplayName: this._DealDetails.AccountDisplayName,
                    };
                    this._Titles.OriginalTitle = this._DealDetails.TitleContent;
                    this._SelectedSubCategory =
                    {
                        ReferenceId: this._DealDetails.SubCategoryId,
                        ReferenceKey: this._DealDetails.SubCategoryKey,
                        Fee: this._DealDetails.SubCategoryFee,
                        Name: this._DealDetails.SubCategoryName,
                        IconUrl: null,

                        ParentCategoryId: this._DealDetails.CategoryId,
                        ParentCategoryKey: this._DealDetails.CategoryKey,
                        ParentCategoryName: this._DealDetails.CategoryName,
                    };
                    this._AmountDistribution =
                    {
                        ActualPrice: this._DealDetails.ActualPrice,
                        SellingPrice: this._DealDetails.SellingPrice,
                        SellingPricePercentage: this._DealDetails.DiscountPercentage,
                        SellingPriceDifference: this._DealDetails.DiscountAmount,
                        TUCPercentage: this._DealDetails.CommissionPercentage,
                        OrignalTUCPercentage: this._DealDetails.CommissionPercentage,
                        TUCAmount: this._DealDetails.CommissionAmount,
                        MerchantAmount: this._HelperService.GetFixedDecimalNumber((this._DealDetails.SellingPrice - this._DealDetails.CommissionAmount)),
                        MerchantPercentage: this._HelperService.GetPercentageFromNumber(this._DealDetails.MerchantAmount, this._DealDetails.SellingPrice),
                    };
                    this._DealConfig.SelectedTitle = this._DealDetails.TitleTypeId;
                    this._DealConfig.StartDate = this._DealDetails.StartDatePart.Object;
                    this._DealConfig.EndDate = this._DealDetails.EndDatePart.Object;
                    this._DealConfig.DefaultStartDate = this._DealConfig.StartDate;
                    this._DealConfig.DefaultEndDate = this._DealConfig.EndDate;
                    this._DealConfig.StartDateConfig = {
                        autoUpdateInput: false,
                        singleDatePicker: true,
                        timePicker: true,
                        locale: { format: "DD-MM-YYYY" },
                        alwaysShowCalendars: false,
                        showDropdowns: true,
                        startDate: this._DealConfig.StartDate,
                        endDate: this._DealConfig.StartDate,
                        minDate: moment(),
                    };
                    this._DealConfig.EndDateConfig = {
                        autoUpdateInput: false,
                        singleDatePicker: true,
                        timePicker: true,
                        locale: { format: "DD-MM-YYYY" },
                        alwaysShowCalendars: false,
                        showDropdowns: true,
                        startDate: this._DealConfig.EndDate,
                        endDate: this._DealConfig.EndDate,
                        minDate: moment(),
                    };
                    if (this._DealDetails.CodeValidityEndDate != undefined && this._DealDetails.CodeValidityEndDate != null) {
                        this._DealConfig.SelectedDealCodeEndDate = this._DealDetails.CodeValidityEndDatePart.Object;
                        this.Form_AddUser.controls['CodeValidityEndDate'].setValue(this._DealDetails.CodeValidityEndDatePart.DateTime);
                        this._DealConfig.DealCodeEndDateConfig = {
                            autoUpdateInput: false,
                            singleDatePicker: true,
                            timePicker: true,
                            locale: { format: "DD-MM-YYYY" },
                            alwaysShowCalendars: false,
                            showDropdowns: true,
                            startDate: this._DealDetails.CodeValidityEndDatePart.Object,
                            minDate: this._DealDetails.CodeValidityEndDatePart.Object,
                        };
                    }
                    else {
                        this._DealConfig.DealCodeEndDateConfig = {
                            autoUpdateInput: false,
                            singleDatePicker: true,
                            timePicker: true,
                            locale: { format: "DD-MM-YYYY" },
                            alwaysShowCalendars: false,
                            showDropdowns: true,
                            startDate: this._DealConfig.EndDate,
                            minDate: this._DealConfig.EndDate,
                        };
                    }
                    this._DealConfig.DealCodeValidityTypeCode = this._DealDetails.UsageTypeCode;
                    this.Form_AddUser.controls['CodeUsageTypeCode'].setValue(this._DealDetails.UsageTypeCode);
                    if (this._DealConfig.DealCodeValidityTypeCode == "hour") {
                        this._DealConfig.SelectedDealCodeHours = this._DealDetails.CodeValidityDays;
                    }
                    if (this._DealConfig.DealCodeValidityTypeCode == "daysafterpurchase") {
                        this._DealConfig.SelectedDealCodeDays = Math.round(this._DealDetails.CodeValidityDays / 24);
                    }
                    console.log(this._DealDetails);
                    console.log(this._DealConfig);

                    // var tStartDate = moment(this._DealDetails.StartDate);
                    // var tEndDate = moment(this._DealDetails.EndDate);
                    // if (this._DealDetails.CodeValidityEndDate != undefined && this._DealDetails.CodeValidityEndDate != null) {
                    //     this._DealConfig.SelectedDealCodeEndDate = this._DealDetails.CodeValidityEndDate;
                    //     this.Form_AddUser.controls['CodeValidityEndDate'].setValue(moment(this._DealDetails.CodeValidityEndDate).format('DD-MM-YYYY hh:mm a'));
                    // }
                    // this._DealConfig.DealCodeValidityTypeCode = this._DealDetails.UsageTypeCode;
                    // this.Form_AddUser.controls['CodeUsageTypeCode'].setValue(this._DealDetails.UsageTypeCode);
                    // if (this._DealConfig.DealCodeValidityTypeCode == "hour") {
                    //     this._DealConfig.SelectedDealCodeHours = this._DealDetails.CodeValidityDays;
                    // }
                    // if (this._DealConfig.DealCodeValidityTypeCode == "daysafterpurchase") {
                    //     this._DealConfig.SelectedDealCodeDays = Math.round(this._DealDetails.CodeValidityDays / 24);
                    // }
                    // this._DealConfig.StartDate = tStartDate;
                    // this._DealConfig.EndDate = tEndDate;
                    // this._DealConfig.DefaultStartDate = moment().startOf('day');
                    // this._DealConfig.DefaultEndDate = moment().add(1, 'days').endOf("day");

                    // this._DealConfig.StartDateConfig = {
                    //     autoUpdateInput: false,
                    //     singleDatePicker: true,
                    //     timePicker: true,
                    //     locale: { format: "DD-MM-YYYY" },
                    //     alwaysShowCalendars: false,
                    //     showDropdowns: true,
                    //     startDate: this._HelperService.DateInUTC(moment().startOf("day")),
                    //     endDate: this._HelperService.DateInUTC(moment().endOf("day")),
                    //     minDate: moment(),
                    // };
                    // this._DealConfig.EndDateConfig = {
                    //     autoUpdateInput: false,
                    //     singleDatePicker: true,
                    //     timePicker: true,
                    //     locale: { format: "DD-MM-YYYY" },
                    //     alwaysShowCalendars: false,
                    //     showDropdowns: true,
                    //     startDate: this._HelperService.DateInUTC(moment().startOf("day")),
                    //     endDate: this._HelperService.DateInUTC(moment().endOf("day")),
                    //     minDate: moment(),
                    // };
                    // this._DealConfig.DealCodeEndDateConfig = {
                    //     autoUpdateInput: false,
                    //     singleDatePicker: true,
                    //     timePicker: true,
                    //     locale: { format: "DD-MM-YYYY" },
                    //     alwaysShowCalendars: false,
                    //     showDropdowns: true,
                    //     startDate: this._DealConfig.EndDate,
                    //     minDate: this._DealConfig.EndDate,
                    // };
                    // this._DealConfig =
                    // {
                    //     DealImages: [],
                    //     Images: [],
                    //     DealCodeEndDateConfig: {
                    //     }
                    // };
                    // this.startdate = this._HelperService.GetDateS(this._DealDetails.StartDate);
                    // this.enddate = this._HelperService.GetDateS(this._DealDetails.EndDate);
                    // this.catfees=this._DealDetails.CategoryFee;
                    // this.categoryPlaceholder = this._DealDetails.SubCategoryName; 
                    this.ProcessAmounts();
                }
                else {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }

    //#region  Form Manager
    Form_AddUser_Images = [];
    Form_AddUser: FormGroup;
    Form_AddUser_Load() {
        this._HelperService._Icon_Cropper_Data.Width = 128;
        this._HelperService._Icon_Cropper_Data.Height = 128;
        this.Form_AddUser = this._FormBuilder.group({
            AccountId: [null, Validators.required],
            AccountKey: [null, Validators.required],
            SubCategoryKey: [null, Validators.required],
            TitleTypeId: [null, Validators.required], //transient
            TitleContent: [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(128)])],
            Description: [null, Validators.compose([Validators.required, Validators.minLength(80), Validators.maxLength(5048)])],
            Terms: [null],
            MaximumUnitSale: [null, Validators.compose([Validators.required, Validators.min(1)])],
            MaximumUnitSalePerPerson: [null, Validators.compose([Validators.required, Validators.min(0)])],
            ActualPrice: [null, Validators.compose([Validators.required, Validators.min(1)])],
            SellingPrice: [null, Validators.compose([Validators.required, Validators.min(1)])],
            StartDate: this._DealConfig.DefaultStartDate.format('DD-MM-YYYY hh:mm a'),
            EndDate: this._DealConfig.DefaultEndDate.format('DD-MM-YYYY hh:mm a'),
            CodeUsageTypeCode: "daysafterpurchase", //transient
            TImage: null,
            TUCPercentage: null,
            CodeValidityEndDate: this._DealConfig.DefaultEndDate.format('DD-MM-YYYY hh:mm a'),
        });
    }

    public packagingData: any = {};

    Form_AddUser_Process(_FormValue: any, IsDraft: boolean) {
        // if (this.SelectedMerchant == undefined || this.SelectedMerchant == null) {
        //     this._HelperService.NotifyError("Select deal merchant");
        // }
        if (this._SelectedSubCategory == undefined || this._SelectedSubCategory == null || this._SelectedSubCategory.ReferenceId == undefined || this._SelectedSubCategory.ReferenceId == null || this._SelectedSubCategory.ReferenceId == 0) {
            this._HelperService.NotifyError("Select deal category");
        }
        else if (_FormValue.ActualPrice == undefined || _FormValue.ActualPrice == null || _FormValue.ActualPrice == undefined || _FormValue.ActualPrice == 0) {
            this._HelperService.NotifyError("Enter original price");
        }
        else if (_FormValue.SellingPrice == undefined || _FormValue.SellingPrice == null || _FormValue.SellingPrice == undefined || _FormValue.SellingPrice == 0) {
            this._HelperService.NotifyError("Enter selling price");
        }
        else if (_FormValue.SellingPrice <= 0) {
            this._HelperService.NotifyError("Actual price must be greater than 0");
        }
        else if (_FormValue.ActualPrice <= 0) {
            this._HelperService.NotifyError("Selling price must be greater than or equal 0");
        }
        else if (_FormValue.MaximumUnitSalePerPerson > _FormValue.MaximumUnitSale) {
            this._HelperService.NotifyError("Deals per person should be less than total deals available");
        }
        else if (_FormValue.SellingPrice > _FormValue.ActualPrice) {
            this._HelperService.NotifyError("Selling price must be less than or equal to actual price");
        }
        // else if (this._DealConfig.DealImages == undefined || this._DealConfig.DealImages == null || this._DealConfig.DealImages.length == 0) {
        //     this._HelperService.NotifyError("Atleast 1 image is required for adding deal");
        // }
        else if (this._DealConfig.StartDate == undefined || this._DealConfig.StartDate == null) {
            this._HelperService.NotifyError("Deal start date required");
        }
        else if (this._DealConfig.EndDate == undefined || this._DealConfig.EndDate == null) {
            this._HelperService.NotifyError("Deal end date required");
        }
        else if (this._DealConfig.EndDate < moment()) {
            this._HelperService.NotifyError("Deal end Date is greater than current date");
        }
        else {
            var Title = this._Titles.Title3;
            if (this._DealConfig.SelectedTitle == 1) {
                Title = this._Titles.Title1;
            }
            else if (this._DealConfig.SelectedTitle == 2) {
                Title = this._Titles.Title2;
            }
            else {
                Title = this._Titles.Title3;
            }
            var _PostData: any =
            {
                Task: "updatedeal",
                ReferenceId: this._DealDetails.ReferenceId,
                ReferenceKey: this._DealDetails.ReferenceKey,
                AccountId: this.SelectedMerchant.ReferenceId,
                AccountKey: this.SelectedMerchant.ReferenceKey,
                Title: Title,
                TitleContent: _FormValue.TitleContent,
                TitleTypeId: _FormValue.TitleTypeId,
                Description: _FormValue.Description,
                Terms: _FormValue.Terms,

                StartDate: moment(this._DealConfig.StartDate).format('YYYY-MM-DD HH:mm'),
                EndDate: moment(this._DealConfig.EndDate).format('YYYY-MM-DD HH:mm'),
                CodeUsageTypeCode: _FormValue.CodeUsageTypeCode,
                CodeValidityDays: this._DealConfig.SelectedDealCodeDays,
                CodeValidityHours: this._DealConfig.SelectedDealCodeHours,
                CodeValidityEndDate: null,
                ActualPrice: _FormValue.ActualPrice,
                SellingPrice: _FormValue.SellingPrice,
                MaximumUnitSale: _FormValue.MaximumUnitSale,
                MaximumUnitSalePerPerson: _FormValue.MaximumUnitSalePerPerson,
                SubCategoryKey: this._SelectedSubCategory.ReferenceKey,
                StatusCode: 'deal.draft',
                SendNotification: false,
                Images: [],
                Locations: [],
                Shedule: [],
                DealTypeCode: this.dealType
            };
            if (this.dealType === 'dealtype.product') {

                if (this.isStorePickup && this.isDeliverToAddress) {
                    _PostData.DeliveryTypeCode = 'deliverytype.instoreanddelivery'
                }
                if (this.isDeliverToAddress) {
                    _PostData.DeliveryTypeCode = 'deliverytype.delivery'
                }
                if (this.isStorePickup) {
                    _PostData.DeliveryTypeCode = 'deliverytype.instore'
                }
            }
            if (this.dealType === 'dealtype.product' && this.isDeliverToAddress) {
                _PostData.Packaging = {};
                _PostData.Packaging.Name = this.packagingData.Title;
                _PostData.Packaging.ParcelDescription = this.packagingData.Description;
                _PostData.Packaging.Type = this.packagingData.Type;
                _PostData.Packaging.Height = this.packagingData.Height;
                _PostData.Packaging.Weight = this.packagingData.Weight;
                _PostData.Packaging.Length = this.packagingData.Length;
                _PostData.Packaging.Width = this.packagingData.Width;
                _PostData.Packaging.Size_unit = 'cm';
                _PostData.Packaging.Weight_unit = 'kg';
            }
            if (this._DealConfig.SelectedDealCodeEndDate != null) {
                _PostData.CodeValidityEndDate = moment(this._DealConfig.SelectedDealCodeEndDate).format('YYYY-MM-DD HH:mm')
            }
            this._DealConfig.DealImages.forEach(element => {
                element.OriginalContent = null;
                _PostData.Images.push(element);
            });
            if (this._AmountDistribution.TUCPercentage != this._AmountDistribution.OrignalTUCPercentage) {
                _PostData.TUCPercentage = this._AmountDistribution.TUCPercentage;
            }
            if (IsDraft) {
                _PostData.StatusCode = 'deal.draft';
            } else {
                // _PostData.StatusCode = 'deal.published';
                _PostData.StatusCode = 'deal.approvalpending'
            }
            this._HelperService.IsFormProcessing = true;
            let _OResponse: Observable<OResponse>;
            _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Deals, _PostData);
            _OResponse.subscribe(
                _Response => {
                    this._HelperService.IsFormProcessing = false;
                    if (_Response.Status == this._HelperService.StatusSuccess) {
                        // this._HelperService.NotifySuccess(_Response.Message);
                        if (_Response.Result.StatusCode == 'deal.draft') {
                            this._HelperService.NotifySuccess('Deal has been saved as a draft.');
                        }
                        else {
                            this._HelperService.NotifySuccess("Deal Published Successfully");
                        }


                        this.Form_AddUser.reset();
                        // this._DealConfig =
                        // {
                        //     DefaultStartDate: null,
                        //     DefaultEndDate: null,
                        //     SelectedDealCodeHours: 12,
                        //     SelectedDealCodeDays: 30,
                        //     SelectedDealCodeEndDate: 0,
                        //     DealCodeValidityTypeCode: 'daysafterpurchase',
                        //     StartDate: null,
                        //     EndDate: null,
                        //     DealImages: [],
                        //     SelectedTitle: 3,
                        //     Images: [],
                        //     StartDateConfig: {
                        //     },
                        //     EndDateConfig: {
                        //     },
                        //     DealCodeEndDateConfig: {
                        //     }
                        // }
                        this._ImageManager =
                        {
                            TCroppedImage: null,
                            ActiveImage: null,
                            ActiveImageName: null,
                            ActiveImageSize: null,
                            Option: {
                                MaintainAspectRatio: "true",
                                MinimumWidth: 800,
                                MinimumHeight: 400,
                                MaximumWidth: 800,
                                MaximumHeight: 400,
                                ResizeToWidth: 800,
                                ResizeToHeight: 400,
                                Format: "jpg",
                            }
                        }
                        //this.ngOnInit();
                        this.Form_AddUser_Close();
                    }
                    else {
                        this._HelperService.NotifyError(_Response.Message);
                    }
                },
                _Error => {
                    this._HelperService.IsFormProcessing = false;
                    this._HelperService.HandleException(_Error);
                });
        }
    }
    Form_AddUser_Close() {
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelConsole.Deals.Deals]);
    }
    //#endregion
    //#region Amount Distribution
    _AmountDistribution =
        {
            ActualPrice: null,
            SellingPrice: null,
            SellingPricePercentage: null,
            SellingPriceDifference: null,
            TUCPercentage: null,
            OrignalTUCPercentage: null,
            TUCAmount: null,
            MerchantPercentage: null,
            MerchantAmount: null,
        };
    ProcessAmounts() {
        var tAmount = this._AmountDistribution;
        if (tAmount.ActualPrice != undefined && tAmount.ActualPrice != null && isNaN(tAmount.ActualPrice) == false && tAmount.ActualPrice >= 0) {
            if (tAmount.SellingPrice != undefined && tAmount.SellingPrice != null && isNaN(tAmount.SellingPrice) == false && tAmount.SellingPrice >= 0) {
                if (tAmount.ActualPrice >= tAmount.SellingPrice) {
                    tAmount.SellingPriceDifference = this._HelperService.GetFixedDecimalNumber(tAmount.ActualPrice - tAmount.SellingPrice);
                    tAmount.SellingPricePercentage = this._HelperService.GetPercentageFromNumber(tAmount.SellingPriceDifference, tAmount.ActualPrice);
                    tAmount.TUCAmount = this._HelperService.GetAmountFromPercentage(tAmount.SellingPrice, tAmount.TUCPercentage);
                    tAmount.MerchantAmount = this._HelperService.GetFixedDecimalNumber(tAmount.SellingPrice - tAmount.TUCAmount);
                    tAmount.MerchantPercentage = this._HelperService.GetPercentageFromNumber(tAmount.MerchantAmount, tAmount.SellingPrice);
                }
                else {
                    if (parseFloat(this._AmountDistribution.SellingPrice) >= parseFloat(this._AmountDistribution.ActualPrice)) {
                        setTimeout(() => {
                            this._AmountDistribution =
                            {
                                ActualPrice: this._AmountDistribution.ActualPrice,
                                SellingPrice: null,
                                SellingPricePercentage: null,
                                SellingPriceDifference: null,
                                TUCPercentage: this._AmountDistribution.TUCPercentage,
                                OrignalTUCPercentage: this._AmountDistribution.OrignalTUCPercentage,
                                TUCAmount: null,
                                MerchantPercentage: null,
                                MerchantAmount: null,
                            };
                            tAmount.SellingPriceDifference = this._HelperService.GetFixedDecimalNumber(tAmount.ActualPrice - 0);
                            tAmount.SellingPricePercentage = this._HelperService.GetPercentageFromNumber(tAmount.SellingPriceDifference, tAmount.ActualPrice);
                            tAmount.TUCAmount = this._HelperService.GetAmountFromPercentage(0, tAmount.TUCPercentage);
                            tAmount.MerchantAmount = this._HelperService.GetFixedDecimalNumber(0 - tAmount.TUCAmount);
                            tAmount.MerchantPercentage = this._HelperService.GetPercentageFromNumber(tAmount.MerchantAmount, 0);
                        }, 200);
                    }
                }
            }
        }
        this.ProcessTitle();
    }
    //#endregion

    //#region Title Manager
    _Titles =
        {
            OriginalTitle: "",
            Title1: this._HelperService.UserCountry.CurrencyCode + " X deal title for " + this._HelperService.UserCountry.CurrencyCode + "Y",
            Title2: this._HelperService.UserCountry.CurrencyCode + " Y off on deal title",
            Title3: "x% off on deal title"
        }
    ProcessTitle() {
        // if (this._AmountDistribution.ActualPrice == undefined) {
        //     this._AmountDistribution.ActualPrice = 0;
        // }
        // if (this._AmountDistribution.SellingPrice == undefined) {
        //     this._AmountDistribution.SellingPrice = 0;
        // }
        // if (this._AmountDistribution.SellingPricePercentage == undefined) {
        //     this._AmountDistribution.SellingPricePercentage = 0;
        // }
        // if (this._AmountDistribution.SellingPriceDifference == undefined) {
        //     this._AmountDistribution.SellingPriceDifference = 0;
        // }
        // if (this._AmountDistribution.TUCPercentage == undefined) {
        //     this._AmountDistribution.TUCPercentage = 0;
        // }
        // if (this._AmountDistribution.TUCAmount == undefined) {
        //     this._AmountDistribution.TUCAmount = 0;
        // }
        // if (this._AmountDistribution.MerchantPercentage == undefined) {
        //     this._AmountDistribution.MerchantPercentage = 0;
        // }
        // if (this._AmountDistribution.MerchantAmount == undefined) {
        //     this._AmountDistribution.MerchantAmount = 0;
        // }
        // this._Titles.Title1 = this._HelperService.UserCountry.CurrencyCode + " " + Math.round(this._AmountDistribution.ActualPrice) + " " + this._Titles.OriginalTitle + " for " + this._HelperService.UserCountry.CurrencyCode + " " + Math.round(this._AmountDistribution.SellingPrice);
        // this._Titles.Title2 = this._HelperService.UserCountry.CurrencyCode + " " + Math.round(this._AmountDistribution.SellingPriceDifference) + " off on " + this._Titles.OriginalTitle;
        // this._Titles.Title3 = Math.round(this._AmountDistribution.SellingPricePercentage) + "% off on " + this._Titles.OriginalTitle;

        this._Titles.Title1 = "N " + Math.round(this._AmountDistribution.ActualPrice) + " " + this._Titles.OriginalTitle + " for N " + Math.round(this._AmountDistribution.SellingPrice);
        this._Titles.Title2 = "N " + Math.round(this._AmountDistribution.SellingPriceDifference) + " off on " + this._Titles.OriginalTitle;
        this._Titles.Title3 = Math.round(this._AmountDistribution.SellingPricePercentage) + "% off on " + this._Titles.OriginalTitle;

        // if (this._Titles.OriginalTitle != undefined && this._Titles.OriginalTitle != null && this._Titles.OriginalTitle != "") {
        //     this._Titles.Title1 = this._HelperService.UserCountry.CurrencyCode + " " + this._AmountDistribution.ActualPrice + " " + this._Titles.OriginalTitle + " for " + this._HelperService.UserCountry.CurrencyCode + " " + this._AmountDistribution.SellingPrice;
        //     this._Titles.Title2 = this._HelperService.UserCountry.CurrencyCode + " " + this._AmountDistribution.SellingPriceDifference + " off on " + this._Titles.OriginalTitle;
        //     this._Titles.Title3 = this._AmountDistribution.SellingPricePercentage + "% off on " + this._Titles.OriginalTitle;
        // }
        // else {
        //     // this._Titles.Title1 = this._HelperService.UserCountry.CurrencyCode + " X deal title for " + this._HelperService.UserCountry.CurrencyCode + "Y";
        //     // this._Titles.Title2 = this._HelperService.UserCountry.CurrencyCode + " Y off on deal title";
        //     // this._Titles.Title3 = "x% off on deal title";
        // }
    }
    //#endregion
    //#region Deal Shedule Manager 
    ScheduleStartDateRangeChange(value) {
        this._DealConfig.EndDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            startDate: value.start,
            minDate: value.start,
        };
        this._DealConfig.StartDate = value.start;
        this._DealConfig.EndDate = value.start;
        this._DealConfig.SelectedDealCodeEndDate = value.start;
        this.Form_AddUser.patchValue(
            {
                StartDate: value.start.format('DD-MM-YYYY hh:mm a'),
                EndDate: value.start.format('DD-MM-YYYY hh:mm a'),
                CodeValidityEndDate: value.start.format('DD-MM-YYYY hh:mm a'),
            }
        );
    }
    ScheduleEndDateRangeChange(value) {
        this._DealConfig.DealCodeEndDateConfig = {
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            startDate: value.start,
            minDate: value.start,
        };
        this._DealConfig.EndDate = value.start;
        this._DealConfig.SelectedDealCodeEndDate = value.start;
        this.Form_AddUser.patchValue(
            {
                EndDate: value.start.format('DD-MM-YYYY hh:mm a'),
                CodeValidityEndDate: value.start.format('DD-MM-YYYY hh:mm a'),
            }
        );
    }
    ScheduleCodeEndDateRangeChange(value) {
        this._DealConfig.SelectedDealCodeEndDate = value.start;
        this.Form_AddUser.patchValue(
            {
                CodeValidityEndDate: value.start.format('DD-MM-YYYY hh:mm a'),
            }
        );
    }
    //#endregion

    //#region Image Manager
    onImageAccept(value) {
        setTimeout(() => {
            this.Form_AddUser.patchValue(
                {
                    TImage: null,
                }
            );
        }, 300);
        this._ImageManager.ActiveImage = value;
        this._ImageManager.ActiveImageName = value.file.name;
        this._ImageManager.ActiveImageName = value.file.size;
    }
    Icon_B64Cropped(base64: string) {
        this._ImageManager.TCroppedImage = base64;
    }
    Icon_B64CroppedDone() {
        var ImageDetails = this._HelperService.GetImageDetails(this._ImageManager.TCroppedImage);
        var ImageItem =
        {
            OriginalContent: this._ImageManager.TCroppedImage,
            Name: this._ImageManager.ActiveImageName,
            Size: this._ImageManager.ActiveImageSize,
            Extension: ImageDetails.Extension,
            Content: ImageDetails.Content
        };
        if (this._DealConfig.DealImages.length == 0) {
            this._DealConfig.DealImages.push(
                {
                    ImageUrl: this._ImageManager.TCroppedImage,
                    ImageContent: ImageItem,
                    IsDefault: 1,
                }
            );
        }
        else {
            this._DealConfig.DealImages.push(
                {
                    ImageUrl: this._ImageManager.TCroppedImage,
                    ImageContent: ImageItem,
                    IsDefault: 0,
                }
            );
        }
        this._ImageManager.TCroppedImage = null;
        this._ImageManager.ActiveImage = null;
        this._ImageManager.ActiveImageName = null;
        this._ImageManager.ActiveImageSize = null;
    }
    Icon_Crop_Clear() {
        this._ImageManager.TCroppedImage = null;
        this._ImageManager.ActiveImage = null;
        this._ImageManager.ActiveImageName = null;
        this._ImageManager.ActiveImageSize = null;
        this._HelperService.CloseModal('_Icon_Cropper_Modal');
    }
    RemoveImage(Item) {
        this._DealConfig.DealImages = this._DealConfig.DealImages.filter(x => x != Item);
    }

    openFilemanager() {
        let dialogRef = this._dialog.open(FileManagerComponent, {
            width: '1200px',
            height: '750px',
            maxWidth: '1200px',
            maxHeight: '800px',
            disableClose: true,
            data: {
                merchantId: this.SelectedMerchant.ReferenceKey
            }
        });
        dialogRef.afterClosed().subscribe(res => {
            if (res.data) {
                console.log("choosen img", res.data.url);
                this._DealConfig.DealImages.push(
                    {
                        reference: res.data.reference,
                        url: res.data.url,
                    }
                );
            }
        });
    }

    RemoveDealImage(Item) {
        swal({
            position: "center",
            title: "Remove deal image?",
            text: "Image cannot be recovered once deleted. Do you want to continue?",
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: this._HelperService.AppConfig.CommonResource.Continue,
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {

                this._HelperService.IsFormProcessing = true;
                var PData =
                {
                    Task: this._HelperService.AppConfig.Api.ThankUCash.deletedealimage,
                    ReferenceId: Item.ReferenceId,
                    ReferenceKey: Item.ReferenceKey,
                }

                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Deals, PData);
                _OResponse.subscribe(
                    _Response => {
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess(_Response.Message);
                            this._DealConfig.SavedDealImages = this._DealConfig.SavedDealImages.filter(x => x != Item);
                            // this._HelperService.CloseModal('FlashDeal');
                            this._HelperService.IsFormProcessing = false;
                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    }
                    ,
                    _Error => {
                        this._HelperService.IsFormProcessing = false;
                        this._HelperService.HandleException(_Error);
                        this._HelperService.ToggleField = false;
                    });

            }
        });

    }
    //#endregion

    //#region   List Helpers
    public _S2Categories_Data: Array<Select2OptionData>;
    _SelectedSubCategory =
        {
            ReferenceId: 0,
            ReferenceKey: null,
            Fee: 5,
            Name: null,
            IconUrl: null,

            ParentCategoryId: 0,
            ParentCategoryKey: 0,
            ParentCategoryName: 0
        };
    GetDealCategories_List() {
        var pData = {
            Task: this._HelperService.AppConfig.Api.Core.getallcategories,
            Offset: 0,
            Limit: 1000,
        };

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.System2, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    var _Data = _Response.Result.Data as any[];
                    if (_Data.length > 0) {
                        var parentCategories = [];
                        _Data.forEach(element => {
                            var itemIndex = parentCategories.findIndex(x => x.ParentCategoryId == element.ParentCategoryId);
                            if (itemIndex == -1) {
                                var categories = _Data.filter(x => x.ParentCategoryId == element.ParentCategoryId);
                                parentCategories.push(
                                    {
                                        ParentCategoryId: element.ParentCategoryId,
                                        ParentCategoryKey: element.ParentCategoryKey,
                                        ParentCategoryName: element.ParentCategoryName,
                                        Categories: categories,
                                    }
                                );
                            }
                        });
                        var finalCat = [];
                        parentCategories.forEach(element => {
                            var Item = {
                                id: element.ParentCategoryId,
                                text: element.ParentCategoryName,
                                children: [],
                                additional: element,
                            };
                            element.Categories.forEach(CategoryItem => {
                                var cItem = {
                                    id: CategoryItem.ReferenceId,
                                    text: CategoryItem.Name + " (" + CategoryItem.Fees + "%)",
                                    additional: CategoryItem,
                                };
                                Item.children.push(cItem);
                            });
                            finalCat.push(Item);
                        });
                        this._S2Categories_Data = finalCat;
                        this.GetDeal();
                    }
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }

    GetDealCategories_Selected(event: any) {
        if (event.value != "0") {
            this._SelectedSubCategory.ReferenceId = event.data[0].additional.ReferenceId;
            this._SelectedSubCategory.ReferenceKey = event.data[0].additional.ReferenceKey;
            this._SelectedSubCategory.Name = event.data[0].additional.Name;
            this._SelectedSubCategory.IconUrl = event.data[0].additional.IconUrl;
            this._SelectedSubCategory.Fee = event.data[0].additional.Fees;
            this._AmountDistribution.TUCPercentage = event.data[0].additional.Fees;
            this._AmountDistribution.OrignalTUCPercentage = event.data[0].additional.Fees;
            this._SelectedSubCategory.ParentCategoryId = event.data[0].additional.ParentCategoryId;
            this._SelectedSubCategory.ParentCategoryKey = event.data[0].additional.ParentCategoryKey;
            this._SelectedSubCategory.ParentCategoryName = event.data[0].additional.ParentCategoryName;
            this.Form_AddUser.patchValue(
                {
                    SubCategoryKey: this._SelectedSubCategory.ParentCategoryKey
                }
            );
            this.ProcessAmounts();
        }
    }

    public GetMerchants_Option: Select2Options;
    public GetMerchants_Transport: any;
    public SelectedMerchant: any = {};
    public MerchantPlaceHolder = "Select Merchant";
    // GetMerchants_List() {
    //     var PlaceHolder = this.MerchantPlaceHolder;
    //     var _Select: OSelect =
    //     {
    //         Task: this._HelperService.AppConfig.Api.ThankUCash.GetMerchants,
    //         Location: this._HelperService.AppConfig.NetworkLocation.Console.V3.Accounts,
    //         SortCondition: [],
    //         Fields: [
    //             {
    //                 SystemName: "ReferenceId",
    //                 Type: this._HelperService.AppConfig.DataType.Number,
    //                 Id: true,
    //                 Text: false,
    //             },

    //             {
    //                 SystemName: "DisplayName",
    //                 Type: this._HelperService.AppConfig.DataType.Text,
    //                 Id: false,
    //                 Text: true
    //             },
    //             {
    //                 SystemName: 'StatusCode',
    //                 Type: this._HelperService.AppConfig.DataType.Text,
    //                 SearchCondition: '=',
    //                 SearchValue: this._HelperService.AppConfig.Status.Active,
    //             }
    //         ]
    //     }
    //     this.GetMerchants_Transport = this._DataHelperService.S2_BuildList(_Select) as any;
    //     this.GetMerchants_Option = {
    //         placeholder: PlaceHolder,
    //         ajax: this.GetMerchants_Transport,
    //         multiple: false,
    //     };
    // }
    // GetMerchants_ListChange(event: any) {
    //     this.SelectedMerchant = event.data[0];
    //     this.Form_AddUser.patchValue(
    //         {
    //             AccountKey: event.data[0].ReferenceKey,
    //             AccountId: event.data[0].ReferenceId
    //         }
    //     );
    //     this.GetStores_List();
    // }
    GetStores_List() {
        var pData = {
            Task: this._HelperService.AppConfig.Api.ThankUCash.GetStores,
            Location: this._HelperService.AppConfig.NetworkLocation.V3.Accounts,
            Offset: 0,
            Limit: 1000,
            SearchCondition: this._HelperService.GetSearchCondition("", "MerchantReferenceId", "number", this.SelectedMerchant.ReferenceId),
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Accounts, pData);
        _OResponse.subscribe(
            _Response => {
                if (_Response.Status == this._HelperService.StatusSuccess) {
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            }
        );
    }


    //#endregion
}
