import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HelperService } from 'src/app/service/helper.service';

const routes: Routes = [{
  path: "",
  canActivateChild: [HelperService],
  children: [
    { path: "dashboard", data: { permission: "dashboard", PageName: "System.Menu.DealsDashboard" }, loadChildren: "./../../../panel/merchant/dashboards/dashboarddeals/dashboarddeals.module#DashboarddealsModule" },
    { path: "ongoingorders", data: { permission: "account", PageName: "System.Menu.Orders" }, loadChildren: "../../../panel/merchant/tucmall/tuongoingorders/tuorders.module#TUOrdersModule" },
    { path: "orders", data: { permission: "account", PageName: "System.Menu.Orders" }, loadChildren: "../../../panel/merchant/tucmall/tuorders/tuorders.module#TUOrdersModule" },
    { path: "orderhistory/:referenceid/:referencekey/:accountid/:accountkey", data: { permission: "account", PageName: "System.Menu.OrderHistory" }, loadChildren: "../../../panel/merchant/tucmall/tuorder/tuorder.module#TUOrderModule" },
    { path: 'deals', data: { 'permission': 'deals', PageName: 'System.Menu.Deals' }, loadChildren: '../../../panel/merchant/deals/tudeals/tudeals.module#TUDealsModule' },
    { path: 'deals/:id', data: { 'permission': 'deals', PageName: 'System.Menu.Deals' }, loadChildren: '../../../panel/merchant/deals/tudeals/tudeals.module#TUDealsModule' },
    { path: 'flashdeals', data: { 'permission': 'deals', PageName: 'System.Menu.FlashDeals' }, loadChildren: '../../../panel/merchant/deals/tuflashdeals/tuflashdeals.module#TUFlashDealsModule' },
    { path: 'adddeals', data: { 'permission': 'deals', PageName: 'System.Menu.Deals' }, loadChildren: '../../../panel/merchant/deals/tuadddeals/tuadddeals.module#AddDealsModule' },
    { path: 'editdeal/:referencekey/:referenceid/:accountid/:accountkey', data: { 'permission': 'deals', PageName: 'System.Menu.Deals' }, loadChildren: '../../../panel/merchant/deals/tuedit/tueditdeal.module#EditDealModule' },
    { path: 'dealsoverview', data: { 'permission': 'deals', PageName: 'System.Menu.Deals' }, loadChildren: '../../../panel/merchant/deals/tudealspurchaseoverview/tuoverview.module#TuOverviewTrendModule' },
    { path: 'soldhistory', data: { 'permission': 'deals', PageName: 'System.Menu.Deals' }, loadChildren: '../../../panel/merchant/deals/tusoldhistory/tusoldhistory.module#TUSoldHistoryModule' },
    { path: 'dealredeemhistory', data: { 'permission': 'deals', PageName: 'System.Menu.Deals' }, loadChildren: '../../../panel/merchant/deals/turedeemhistory/turedeemhistory.module#TURedeemHistoryModule' },
    { path: "allpickup", data: { permission: "deals", PageName: "System.Menu.Deals" }, loadChildren: "../../../panel/merchant/deals/tupickups/rootpickup.module#RootPickupRoutingModule" },
    { path: "redeemedorders", data: { permission: "deals", PageName: "System.Menu.Deals" }, loadChildren: "../../../panel/merchant/deals/turedeemedorder/turedeemedorder.module#TURedeemedOrderModule" },
    { path: "cancelledorders", data: { permission: "deals", PageName: "System.Menu.Deals" }, loadChildren: "../../../panel/merchant/deals/tucancelledorders/tucancelledorder.module#TUCancelledOrderModule" },
    { path: 'dealsorders', data: { 'permission': 'deals', PageName: 'System.Menu.Deals' }, loadChildren: '../../../panel/merchant/dealdelivery/tuorder/tuorder.module#TUOrderModule' },
    { path: 'dealsovervieww', data: { 'permission': 'deals', PageName: 'System.Menu.Deals' }, loadChildren: '../../../panel/merchant/dealdelivery/tudealdeliveryoverview/tuovervieww.module#TuOverviewwTrendModule' },
    { path: ':referencekey/:referenceid/:accountid/:accountkey', data: { 'permission': 'deals', PageName: 'System.Menu.Deals' }, loadChildren: '../../../panel/merchant/deals/tudeal/tudeal.module#TUDealModule' },
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DealsRoutingModule { }
