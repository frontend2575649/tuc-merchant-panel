import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DealsRoutingModule } from './deals-routing.module';
import { MerchantguardGuard } from 'src/app/service/guard/merchantguard.guard';
import { HelperService } from "../../../service/helper.service";
import { Routes } from '@angular/router';

const routes: Routes = [
  {
    path: "deal",
    canActivateChild: [HelperService],
    children: [
      // { path: "dashboard", data: { permission: "dashboard", PageName: "System.Menu.Dashboard" }, loadChildren: "./../../panel/merchant/dashboards/root/dashboard.module#TUDashboardModule" },
      { path: "dashboard", data: { permission: "dashboard", PageName: "System.Menu.DealsDashboard" }, loadChildren: "./../../panel/merchant/dashboards/dashboarddeals/dashboarddeals.module#DashboarddealsModule"},
      // { path: "settings", data: { permission: "account", PageName: "System.Menu.AccountSettings" }, loadChildren: "../../modules/accountdetails/tumerchant/tusettings/tusettings.module#TUSettingsModule" },
      // { path: "store", data: { permission: "account", PageName: "System.Menu.Store" }, loadChildren: "../../modules/accountdetails/tumerchant/tustore/tustore.module#TUStoreModule" },
      // { path: "terminal", data: { permission: "account", PageName: "System.Menu.Terminal" }, loadChildren: "../../modules/accountdetails/tumerchant/tuterminal/tuterminal.module#TUTerminalModule" },
      // { path: "cashier", data: { permission: "account", PageName: "System.Menu.Cashier" }, loadChildren: "../../modules/accountdetails/tumerchant/tucashier/tucashier.module#TUCashierModule" },
      // { path: "subaccount", data: { permission: "account", PageName: "System.Menu.Cashier" }, loadChildren: "../../modules/accountdetails/tumerchant/tusubaccount/tusubaccount.module#TUSubAccountModule" },
      // { path: "bank", data: { permission: "account", PageName: "System.Menu.BankReconcialiation" }, loadChildren: "../../modules/accounts/tumerchants/bankreconciliation/bank.module#TUBankModule" },
      // { path: "bulkterminals", data: { permission: "account", PageName: "System.Menu.BulkTerminal" }, loadChildren: "../../modules/accounts/tumerchants/bulkterminalupload/tuterminals.module#TUBulkTerminalsModule" },
      // { path: "bulkterminalslist", data: { permission: "account", PageName: "System.Menu.BulkTerminal" }, loadChildren: "../../modules/accounts/tumerchants/bulkterminallist/tuterminals.module#TUBulkTerminalsModule" },
      // { path: "upgrade", data: { permission: "account", PageName: "System.Menu.Upgrade" }, loadChildren: "../../modules/accounts/tumerchants/NoAccess/topup.module#TUTopUpModule" },



      { path: "customer", data: { permission: "account", PageName: "System.Menu.Customer" }, loadChildren: "../../modules/accountdetails/tumerchant/tucustomer/tucustomer.module#TUCustomerModule" },
      { path: "campaign", data: { permission: "account", PageName: "System.Menu.Campaign" }, loadChildren: "../../modules/accountdetails/tumerchant/tucampaign/tucampaign.module#TUCampaignModule" },
      { path: 'cashiers', data: { 'permission': 'cashiers', PageName: 'System.Menu.Cashier' }, loadChildren: '../../modules/accounts/tumerchants/cashiers/tucashiers.module#TUCashiersModule' },

      // { path: "profile", data: { permission: "customers", PageName: "System.Menu.Profile" }, loadChildren: "./../../pages/hcprofile/hcprofile.module#HCProfileModule" },
      {
        path: "terminallive",
        data: { permission: "dashboard", PageName: "System.Menu.Dashboard" },
        loadChildren:
          "./../../pages/dashboard/tumerchantterminallive/tumerchantterminallive.module#TUMerchantTerminalsLiveModule"
      },
      {
        path: "merchantupload",
        data: { permission: "stores", PageName: "System.Menu.BulkUplaod" },
        loadChildren:
          "./../../pages/useronboarding/bulkmerchant/merchantupload.module#TUMerchantUploadModule"
      },

      //TUCMALl
      { path: "deal/ongoingorders", data: { permission: "account", PageName: "System.Menu.Orders" }, loadChildren: "../../panel/merchant/tucmall/tuongoingorders/tuorders.module#TUOrdersModule" },
      { path: "orders", data: { permission: "account", PageName: "System.Menu.Orders" }, loadChildren: "../../panel/merchant/tucmall/tuorders/tuorders.module#TUOrdersModule" },
      { path: "orderhistory/:referenceid/:referencekey/:accountid/:accountkey", data: { permission: "account", PageName: "System.Menu.OrderHistory" }, loadChildren: "../../panel/merchant/tucmall/tuorder/tuorder.module#TUOrderModule" },
      { path: "products", data: { permission: "account", PageName: "System.Menu.Products" }, loadChildren: "../../panel/merchant/tucmall/tuproducts/tuproducts.module#TUProductsModule" },
      { path: "product", data: { permission: "account", PageName: "System.Menu.Products" }, loadChildren: "../../modules/accountdetails/tumerchant/tuproduct/tuproduct.module#TUProductModule" },
      { path: "addproduct", data: { permission: "account", PageName: "System.Menu.AddProduct" }, loadChildren: "../../panel/merchant/tucmall/tuaddproduct/tuaddproduct.module#TUAddProductsModule" },
      { path: "editproduct", data: { permission: "account", PageName: "System.Menu.EditProduct" }, loadChildren: "../../panel/merchant/tucmall/tueditproduct/tueditproduct.module#TUEditProductsModule" },

      // GiftPoints
      { path: "giftpoints", data: { permission: "dashboard", PageName: "System.Menu.GiftPoints" }, loadChildren: "../../modules/dashboards/giftpoints/tugiftpoints.module#TuGiftPointsModule" },
      { path: "giftpointsales", data: { permission: "dashboard", PageName: "System.Menu.Home" }, loadChildren: "../../modules/transactions/tusale/GiftPoints/home/home.module#TUHomeModule" },
      // { path: "Reward", data: { permission: "dashboard", PageName: "System.Menu.Home" }, loadChildren: "../../modules/transactions/tusale/GiftPoints/Reward/tusale.module#TUSaleModule" },

      // GiftCard
      { path: "giftcard", data: { permission: "dashboard", PageName: "System.Menu.GiftCard" }, loadChildren: "../../modules/dashboards/giftcard/tugiftcard.module#TuGiftCardModule" },
      { path: "giftcardsales", data: { permission: "dashboard", PageName: "System.Menu.Home" }, loadChildren: "../../modules/transactions/tusale/giftcard/home/home.module#TUHomeModule" },

      //My Buissness 
      { path: "sales/saleshistory", data: { permission: "dashboard", PageName: "System.Menu.SalesHistory" }, loadChildren: "../../modules/transactions/tusale/merchant/tusale.module#TUSaleModule" },
      { path: "salestrend", data: { permission: "dashboard", PageName: "System.Menu.SalesTrend" }, loadChildren: "../../modules/dashboards/mybuisness/tusalestrends/tusalestrends.module#TuSalesTrendModule" },
      { path: "salesummary", data: { permission: "dashboard", PageName: "System.Menu.SaleSummary" }, loadChildren: "../../modules/dashboards/mybuisness/tusalesummary/tusalesummary.module#TUSaleSummaryModule" },
      { path: "cashieranalyis", data: { permission: "dashboard", PageName: "System.Menu.CashierAnalytics" }, loadChildren: "../../modules/dashboards/mybuisness/cashier/tucashiers.module#TUCashiersModule" },
      { path: "sales/pendingtransaction", data: { permission: "dashboard", PageName: "System.Menu.SuspiciousTransaction" }, loadChildren: "../../modules/accounts/tumerchants/pendingtransaction/pendingtransaction.module#TUPendingTransactionModule" },

      { path: "transaction", data: { permission: "dashboard", PageName: "System.Menu.Transaction" }, loadChildren: "../../modules/transactions/tusale/roottransaction/roottransaction.module#RootTransactionModule" },

      //Reward N Redeem

      { path: "sales/rewardclaim", data: { permission: "dashboard", PageName: "System.Menu.RewardClaim" }, loadChildren: "../../modules/transactions/tusale/turewardclaim/turewardclaim.module#TURewardsClaimModule" },


      //Loyality
      { path: "overview", canActivateChild: [MerchantguardGuard], data: { permission: "dashboard", PageName: "System.Menu.Overview" }, loadChildren: "../../modules/dashboards/loyality/overview/dashboard.module#TUDashboardModule" },
      { path: "campaigns", data: { permission: "dashboard", PageName: "System.Menu.Campaigns" }, loadChildren: "../../modules/dashboards/loyality/campaigns/tucampaigns.module#TUCampaignsModule" },
      { path: "downloads", data: { permission: "dashboard", PageName: "System.Menu.Downloads" }, loadChildren: "../../modules/accounts/tumerchants/tudownloads/tudownloads.module#TUDownloadsModule" },

      //Deals
      { path: 'deal/deals', data: { 'permission': 'deals', PageName: 'System.Menu.Deals' }, loadChildren: '../../panel/merchant/deals/tudeals/tudeals.module#TUDealsModule' },
      { path: 'deals/:id', data: { 'permission': 'deals', PageName: 'System.Menu.Deals' }, loadChildren: '../../panel/merchant/deals/tudeals/tudeals.module#TUDealsModule' },
      { path: 'flashdeals', data: { 'permission': 'deals', PageName: 'System.Menu.FlashDeals' }, loadChildren: '../../panel/merchant/deals/tuflashdeals/tuflashdeals.module#TUFlashDealsModule' },
      { path: 'deal/adddeals', data: { 'permission': 'deals', PageName: 'System.Menu.Deals' }, loadChildren: '../../panel/merchant/deals/tuadddeals/tuadddeals.module#AddDealsModule' },
      { path: 'editdeal/:referencekey/:referenceid/:accountid/:accountkey', data: { 'permission': 'deals', PageName: 'System.Menu.Deals' }, loadChildren: '../../panel/merchant/deals/tuedit/tueditdeal.module#EditDealModule' },
      { path: 'deal/:referencekey/:referenceid/:accountid/:accountkey', data: { 'permission': 'deals', PageName: 'System.Menu.Deals' }, loadChildren: '../../panel/merchant/deals/tudeal/tudeal.module#TUDealModule' },
      { path: 'deal/dealsoverview', data: { 'permission': 'deals', PageName: 'System.Menu.Deals' }, loadChildren: '../../panel/merchant/deals/tudealspurchaseoverview/tuoverview.module#TuOverviewTrendModule' },
      { path: 'deal/soldhistory', data: { 'permission': 'deals', PageName: 'System.Menu.Deals' }, loadChildren: '../../panel/merchant/deals/tusoldhistory/tusoldhistory.module#TUSoldHistoryModule' },
      { path: 'deal/dealredeemhistory', data: { 'permission': 'deals', PageName: 'System.Menu.Deals' }, loadChildren: '../../panel/merchant/deals/turedeemhistory/turedeemhistory.module#TURedeemHistoryModule' },
      { path: "allpickup", data: { permission: "deals", PageName: "System.Menu.Deals" }, loadChildren: "../../panel/merchant/deals/tupickups/rootpickup.module#RootPickupRoutingModule" },
      { path: "redeemedorders", data: { permission: "deals", PageName: "System.Menu.Deals" }, loadChildren: "../../panel/merchant/deals/turedeemedorder/turedeemedorder.module#TURedeemedOrderModule" },
      // { path: "returnedorders", data: { permission: "deals", PageName: "System.Menu.Deals" }, loadChildren: "../../panel/merchant/deals/tupickups/rootpickup.module#TURedeemOrderModule" },
      { path: "cancelledorders", data: { permission: "deals", PageName: "System.Menu.Deals" }, loadChildren: "../../panel/merchant/deals/tucancelledorders/tucancelledorder.module#TUCancelledOrderModule" },
      { path: 'deal/dealsorders', data: { 'permission': 'deals', PageName: 'System.Menu.Deals' }, loadChildren: '../../panel/merchant/dealdelivery/tuorder/tuorder.module#TUOrderModule' },
      { path: 'deal/dealsovervieww', data: { 'permission': 'deals', PageName: 'System.Menu.Deals' }, loadChildren: '../../panel/merchant/dealdelivery/tudealdeliveryoverview/tuovervieww.module#TuOverviewwTrendModule' },
      

      // FAQs
      { path: 'faqcategories', data: { 'permission': 'administration', PageName: 'System.Menu.Administration' }, loadChildren: '../../modules/accounts/tumerchants/tufaqcategories/tufaqcategories.module#TUFAQCategoriesModule' },
      { path: 'faqs/:referencekey/:referenceid', data: { 'permission': 'administration', PageName: 'System.Menu.Administration' }, loadChildren: '../../modules/accounts/tumerchants/tufaqs/tufaqs.module#TUFAQsModule' },
      // Customers
      { path: "customerrewards/:referenceid", data: { permission: "terminals", PageName: "System.Menu.CustomerRewards" }, loadChildren: "./../../modules/accounts/tumerchants/bulkcustomerrewards/bulkcustomerrewards.module#TUCBulkCustomerRewardListModule" },
      { path: "customers/:referenceid", data: { permission: "terminals", PageName: "System.Menu.Customers" }, loadChildren: "./../../modules/accounts/tumerchants/bulkcustomersfiles/customerupload.module#TUCustomerUploadModule" },
      { path: "allcustomers", data: { permission: "terminals", PageName: "System.Menu.Customers" }, loadChildren: "./../../modules/accounts/tumerchants/tucustomers/tucustomers.module#TUCustomersModule" },
      { path: "allmcustomers", data: { permission: "terminals", PageName: "System.Menu.Customers" }, loadChildren: "./../../modules/accounts/tumerchants/tuallcustomers/tuallcustomers.module#TUAllCustomerssModule" },

      {
        path: "bulkcustomerrewards",
        data: { permission: "acquirer", PageName: "System.Menu.BulkRewards" },
        loadChildren:
          "./../../pages/useronboarding/bulkreward/bulkreward.module#TUCBulkCustomerRewardModule"
      },
      {
        path: "bulkcustomerupload",
        data: { permission: "acquirer", PageName: "System.Menu.CustomerUpload" },
        loadChildren:
          "./../../pages/useronboarding/bulkreward/bulkreward.module#TUCBulkCustomerRewardModule"
      },



      { path: "bankmanager", data: { permission: "terminals", PageName: "System.Menu.Bank" }, loadChildren: "./../../modules/accounts/tumerchants/bankmanager/bankmanager.module#TUBankManagerModule" },
      { path: "banks", data: { permission: "terminals", PageName: "System.Menu.Bank" }, loadChildren: "./../../modules/accounts/tumerchants/bankmanager/banks/banks.module#TUBanksModule" },

      //  CashOuts

      { path: "cashouts", data: { permission: "terminals", PageName: "System.Menu.CashOuts" }, loadChildren: "./../../modules/accounts/tumerchants/tucashouts/tucashouts.module#TUCashoutsModule" },
      { path: "bnpl", loadChildren: "../../panel/merchant/bnpl/bnpl.module#BnplModule"},
      { path: "wallet", data: { PageName: "System.Menu.Wallet" }, loadChildren: "./../../modules/accounts/tumerchants/wallet/wallet.module#WalletModule" },
      { path: "dealswallet", data: { PageName: "System.Menu.Wallet" }, loadChildren: "./../../modules/accounts/tumerchants/dealswallet/dealswallet.module#DealswalletModule" },

    ]
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    DealsRoutingModule
  ]
})
export class DealsModule { }
