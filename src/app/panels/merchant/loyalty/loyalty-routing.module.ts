import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MerchantguardGuard } from 'src/app/service/guard/merchantguard.guard';
import { HelperService } from 'src/app/service/helper.service';

const routes: Routes = [
  {
    path: "",
    canActivateChild: [HelperService],
    children: [
      { path: "dashboard", data: { permission: "dashboard", PageName: "System.Menu.LoyaltyDashboard" }, loadChildren: "./../../../panel/merchant/dashboards/dashboardloyalty/dashboardloyalty.module#DashboardloyaltyModule" },
      { path: "customers", data: { permission: "account", PageName: "System.Menu.Customer" }, loadChildren: "../../../modules/dashboards/loyality/customers/tucustomers.module#TUCustomersModule" },
      { path: "visits", data: { permission: "dashboard", PageName: "System.Menu.Visits" }, loadChildren: "../../../modules/dashboards/loyality/visits/dashboard.module#TUDashboardModule" },

      { path: "sales/rewardhistory", data: { permission: "dashboard", PageName: "System.Menu.RewardHistory" }, loadChildren: "../../../modules/transactions/tusale/rewardhistory/tusale.module#TUSaleModule" },
      { path: "sales/pendingrewardhistory", data: { permission: "dashboard", PageName: "System.Menu.PendingRewardHistory" }, loadChildren: "../../../modules/transactions/tusale/pendingrewardhistory/tusale.module#TUSaleModule" },
      { path: "sales/redeemhistory", data: { permission: "dashboard", PageName: "System.Menu.RedeemHistory" }, loadChildren: "../../../modules/transactions/tusale/redeemhistory/tusale.module#TUSaleModule" },

      { path: "smscampaigns", data: { PageName: "SMS Marketing" }, loadChildren: "../../../pages/marketingtools/tuallsmscampaign/tuallsmscampaign.module#TUAllSMSCampaignModule" },
      { path: "emailcampaigns", data: { PageName: "Email Marketing" }, loadChildren: "../../../pages/marketingtools/tuallemailcampaign/tuallemailcampaign.module#TuallemailcampaignModule" },
      { path: "addsmscampaigns", data: { PageName: "Add SMS Campaign" }, loadChildren: "../../../pages/marketingtools/tuaddcampaign/tuaddcampaign.module#AddCampaignModule" },
      { path: "surveycampaigns", data: { PageName: "Survey Campaign" }, loadChildren: "../../../pages/marketingtools/tusurveycampaign/tusurveycampaign.module#TusurveycampaignModule" },
      { path: "mysales", data: { permission: "dashboard", PageName: "My Sales" }, loadChildren: "../../../modules/dashboards/loyality/mysales/mysales.module#MysalesModule" },
      { path: "subscription", data: { permission: "dashboard", PageName: "Subscription" }, loadChildren: "../../../modules/dashboards/loyality/subscription/subscription.module#SubscriptionModule" },
      { path: "customer", data: { permission: "account", PageName: "System.Menu.Customer" }, loadChildren: "../../../modules/accountdetails/tumerchant/tucustomer/tucustomer.module#TUCustomerModule" },

      //   //Loyality
      //   { path: "overview", canActivateChild: [MerchantguardGuard], data: { permission: "dashboard", PageName: "System.Menu.Overview" }, loadChildren: "../../modules/dashboards/loyality/overview/dashboard.module#TUDashboardModule" },
      //   { path: "campaigns", data: { permission: "dashboard", PageName: "System.Menu.Campaigns" }, loadChildren: "../../modules/dashboards/loyality/campaigns/tucampaigns.module#TUCampaignsModule" },
      //   { path: "downloads", data: { permission: "dashboard", PageName: "System.Menu.Downloads" }, loadChildren: "../../modules/accounts/tumerchants/tudownloads/tudownloads.module#TUDownloadsModule" },
      //   //  Marketing Tools
      //   { path: "smscampaign", data: { PageName: "SMS Marketing" }, loadChildren: "../../pages/marketingtools/tusmscampaign/tusmscampaign.module#TUSmscampaignDModule" },
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoyaltyRoutingModule { }
