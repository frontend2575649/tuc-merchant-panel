import { Component, OnInit, Renderer2 } from '@angular/core';
import { HelperService, OResponse, OOverview, DataHelperService, OList, OSelect } from '../../service/service';
import { Observable } from 'rxjs';
import { NavigationEnd, Router } from '@angular/router';
import swal from 'sweetalert2';
declare var moment: any;
declare var $: any;
declare var PerfectScrollbar: any;
import * as Feather from 'feather-icons';
import * as Chart from "../../../../node_modules/chart.js/dist/Chart.js";
import * as Cookies from '../../../assets/js/js.cookie.js'
import * as introJs from 'intro.js';
@Component({
    selector: 'merchant-root',
    templateUrl: './merchant.component.html',
})
export class TUMerchantComponent implements OnInit {
    introJS = introJs.default();
    public _Store_Option: Select2Options;
    public isAccountSettingActive: boolean = false;
    public alertMessages: any = { firstDealApproved: false, firstDealReject: false, askToAppDownload: false, firstDealUploaded: false };
    rejectedDeals: any;
    constructor(
        public _DataHelperService: DataHelperService,
        public _Router: Router,
        public _HelperService: HelperService,
        private renderer2: Renderer2
    ) {

    }

    dismissAlert() {
        $('alert').hide();
    }
    ngAfterViewInit(): void {
    }
    ngOnInit() {

        this._HelperService.IsSubscriptionActive = this._HelperService.GetStorage("hca").Subscription.IsSubscriptionActive;
        this._HelperService.IsGroupLoyalty = this._HelperService.GetStorage("hca").UserAccount.IsGroupLoyalty;
        this._HelperService.IsOpenCloseLoyalty = this._HelperService.GetStorage("hca").UserAccount.IsOpenCloseLoyalty;

        if (this._HelperService.GetStorage("toggleDB")) {
            this._HelperService.DisplayToggleDashboard = true
        }
        // this.isAccountSettingActive = location.href.includes('/settings');
        // const conditions = ["/settings", "store/", "terminal/", "cashier/", "subaccounts/", "app/"];

        // this._HelperService.isAccountSettingActive = conditions.some(el => location.href.includes(el))
        this._DataHelperService.dealsOverview.subscribe(data => this.alertMessages = data);
        this._HelperService.isDisplayLoyaltyConfig = this._HelperService.GetStorage("hcaDisplayLoyalty")
        // this.isAccountSettingActive = location.href.includes('/settings');
        // this._HelperService.hideAccountSetting = location.href.includes('/settings/');
        // this._HelperService.DisplayDealsDashboard = this._HelperService.GetStorage("hcaDisplayLoyalty")
        // this._HelperService.DisplayLoyaltyDashboard = this._HelperService.GetStorage("hcaDisplayLoyalty")
        // if (this._HelperService.DisplayLoyaltyDashboard || this._HelperService.DisplayDealsDashboard) {

        //     this._HelperService.isDisplayLoyaltyConfig = this._HelperService.DisplayLoyaltyDashboard
        // }

        this._Router.events.subscribe((events: any) => {
            if (events instanceof NavigationEnd) {
                // this._HelperService.hideAccountSetting = events.url.includes('/settings/');
                // this._HelperService.isAccountSettingActive = conditions.some(el => events.url.includes(el))
                // this.isAccountSettingActive = events.url.includes('/settings');
                // this._HelperService.DisplayDealsDashboard = !this._HelperService.GetStorage("hcaDisplayLoyalty")
                // this._HelperService.DisplayLoyaltyDashboard = this._HelperService.GetStorage("hcaDisplayLoyalty")
                // if (this._HelperService.DisplayLoyaltyDashboard || this._HelperService.DisplayDealsDashboard) {

                //     this._HelperService.isDisplayLoyaltyConfig = this._HelperService.DisplayLoyaltyDashboard
                // }
                this._HelperService.isDisplayLoyaltyConfig = this._HelperService.GetStorage("hcaDisplayLoyalty")
                if (this._HelperService.isDisplayLoyaltyConfig) {
                    this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.Merchant = this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.LoyaltyDashboard
                } else {
                    this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.Merchant = this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.DealsDashboard
                }
            }
        })
        setTimeout(() => {
            this._HelperService.ValidateDataPermission();
        }, 500);
        this._HelperService.darkStyle = Cookies.get("mapColor");
        // Chart.defaults.global.defaultFontColor = 'white';
        // Chart.defaults.global.defaultFontSize = '16';

        $("li").click(() => {

            var backdrop: HTMLElement = document.getElementById("backdrop");
            backdrop.classList.remove("show");

            var modal_backdrop: HTMLCollectionOf<Element> = document.getElementsByClassName("modal");
            if (modal_backdrop.length > 0) {
                const element = modal_backdrop.item(0);
                this._HelperService.CloseModal(element.id);
            }

            var modal_backdrop: HTMLCollectionOf<Element> = document.getElementsByClassName("modal-backdrop");
            if (modal_backdrop.length > 0) {
                const element = modal_backdrop.item(0);
                element.classList.remove("modal-backdrop");
            }

        });

        $('body').on('click', '.df-mode', (e) => {
            e.preventDefault();

            for (let index = 0; index < $('.df-mode').length; index++) {
                const element = $('.df-mode')[index];
                setTimeout(() => {
                    if (element.classList.length == 3) {
                        if (element.getAttribute('data-title') === 'dark') {
                            this._HelperService.CheckMode = 'dark';
                            this._HelperService.darkStyle = [
                                {
                                    elementType: "geometry",
                                    stylers: [
                                        {
                                            color: "#242f3e"
                                        }
                                    ]
                                },
                                {
                                    elementType: "labels.text.stroke",
                                    stylers: [
                                        {
                                            color: "#242f3e"
                                        }
                                    ]
                                },
                                {
                                    elementType: "labels.text.fill",
                                    stylers: [
                                        {
                                            color: "#746855"
                                        }
                                    ]
                                },
                                {
                                    featureType: "administrative.locality",
                                    elementType: "labels.text.fill",
                                    stylers: [
                                        {
                                            color: "#d59563"
                                        }
                                    ]
                                },
                                {
                                    featureType: "poi",
                                    elementType: "labels.text.fill",
                                    stylers: [
                                        {
                                            color: "#d59563"
                                        }
                                    ]
                                },
                                {
                                    featureType: "poi.park",
                                    elementType: "geometry",
                                    stylers: [
                                        {
                                            color: "#263c3f"
                                        }
                                    ]
                                },
                                {
                                    featureType: "poi.park",
                                    elementType: "labels.text.fill",
                                    stylers: [
                                        {
                                            color: "#6b9a76"
                                        }
                                    ]
                                },
                                {
                                    featureType: "road",
                                    elementType: "geometry",
                                    stylers: [
                                        {
                                            color: "#38414e"
                                        }
                                    ]
                                },
                                {
                                    featureType: "road",
                                    elementType: "geometry.stroke",
                                    stylers: [
                                        {
                                            color: "#212a37"
                                        }
                                    ]
                                },
                                {
                                    featureType: "road",
                                    elementType: "labels.text.fill",
                                    stylers: [
                                        {
                                            color: "#9ca5b3"
                                        }
                                    ]
                                },
                                {
                                    featureType: "road.highway",
                                    elementType: "geometry",
                                    stylers: [
                                        {
                                            color: "#746855"
                                        }
                                    ]
                                },
                                {
                                    featureType: "road.highway",
                                    elementType: "geometry.stroke",
                                    stylers: [
                                        {
                                            color: "#1f2835"
                                        }
                                    ]
                                },
                                {
                                    featureType: "road.highway",
                                    elementType: "labels.text.fill",
                                    stylers: [
                                        {
                                            color: "#f3d19c"
                                        }
                                    ]
                                },
                                {
                                    featureType: "transit",
                                    elementType: "geometry",
                                    stylers: [
                                        {
                                            color: "#2f3948"
                                        }
                                    ]
                                },
                                {
                                    featureType: "transit.station",
                                    elementType: "labels.text.fill",
                                    stylers: [
                                        {
                                            color: "#d59563"
                                        }
                                    ]
                                },
                                {
                                    featureType: "water",
                                    elementType: "geometry",
                                    stylers: [
                                        {
                                            color: "#17263c"
                                        }
                                    ]
                                },
                                {
                                    featureType: "water",
                                    elementType: "labels.text.fill",
                                    stylers: [
                                        {
                                            color: "#515c6d"
                                        }
                                    ]
                                },
                                {
                                    featureType: "water",
                                    elementType: "labels.text.stroke",
                                    stylers: [
                                        {
                                            color: "#17263c"
                                        }
                                    ]
                                }
                            ];


                            Cookies.set('mapColor', this._HelperService.darkStyle);
                        } else {
                            this._HelperService.CheckMode = 'light';
                            this._HelperService.darkStyle = [];
                            Cookies.set('mapColor', this._HelperService.darkStyle);
                        }
                    }
                }, 50);
            }
        });

        setTimeout(() => {
            var hasMode = Cookies.get('df-mode');

            this._HelperService.CheckMode = (hasMode === 'dark') ? 'dark' : 'light';
            if (hasMode == 'dark') {
                this._HelperService.darkStyle = [
                    {
                        elementType: "geometry",
                        stylers: [
                            {
                                color: "#242f3e"
                            }
                        ]
                    },
                    {
                        elementType: "labels.text.stroke",
                        stylers: [
                            {
                                color: "#242f3e"
                            }
                        ]
                    },
                    {
                        elementType: "labels.text.fill",
                        stylers: [
                            {
                                color: "#746855"
                            }
                        ]
                    },
                    {
                        featureType: "administrative.locality",
                        elementType: "labels.text.fill",
                        stylers: [
                            {
                                color: "#d59563"
                            }
                        ]
                    },
                    {
                        featureType: "poi",
                        elementType: "labels.text.fill",
                        stylers: [
                            {
                                color: "#d59563"
                            }
                        ]
                    },
                    {
                        featureType: "poi.park",
                        elementType: "geometry",
                        stylers: [
                            {
                                color: "#263c3f"
                            }
                        ]
                    },
                    {
                        featureType: "poi.park",
                        elementType: "labels.text.fill",
                        stylers: [
                            {
                                color: "#6b9a76"
                            }
                        ]
                    },
                    {
                        featureType: "road",
                        elementType: "geometry",
                        stylers: [
                            {
                                color: "#38414e"
                            }
                        ]
                    },
                    {
                        featureType: "road",
                        elementType: "geometry.stroke",
                        stylers: [
                            {
                                color: "#212a37"
                            }
                        ]
                    },
                    {
                        featureType: "road",
                        elementType: "labels.text.fill",
                        stylers: [
                            {
                                color: "#9ca5b3"
                            }
                        ]
                    },
                    {
                        featureType: "road.highway",
                        elementType: "geometry",
                        stylers: [
                            {
                                color: "#746855"
                            }
                        ]
                    },
                    {
                        featureType: "road.highway",
                        elementType: "geometry.stroke",
                        stylers: [
                            {
                                color: "#1f2835"
                            }
                        ]
                    },
                    {
                        featureType: "road.highway",
                        elementType: "labels.text.fill",
                        stylers: [
                            {
                                color: "#f3d19c"
                            }
                        ]
                    },
                    {
                        featureType: "transit",
                        elementType: "geometry",
                        stylers: [
                            {
                                color: "#2f3948"
                            }
                        ]
                    },
                    {
                        featureType: "transit.station",
                        elementType: "labels.text.fill",
                        stylers: [
                            {
                                color: "#d59563"
                            }
                        ]
                    },
                    {
                        featureType: "water",
                        elementType: "geometry",
                        stylers: [
                            {
                                color: "#17263c"
                            }
                        ]
                    },
                    {
                        featureType: "water",
                        elementType: "labels.text.fill",
                        stylers: [
                            {
                                color: "#515c6d"
                            }
                        ]
                    },
                    {
                        featureType: "water",
                        elementType: "labels.text.stroke",
                        stylers: [
                            {
                                color: "#17263c"
                            }
                        ]
                    }
                ];
                Cookies.set('mapColor', this._HelperService.darkStyle);
            }
            else {
                this._HelperService.darkStyle = [];
                Cookies.set('mapColor', this._HelperService.darkStyle);
            }
        }, 50);
        const s = this.renderer2.createElement('script');
        s.type = 'text/javascript';
        s.src = '../../../assets/js/dashforge.aside.js';
        s.text = ``;
        this.renderer2.appendChild(document.body, s);

        const s2 = this.renderer2.createElement('script');
        s2.type = 'text/javascript';
        s2.src = '../../../../assets/js/dashforge.settings.js';
        s2.text = ``;
        this.renderer2.appendChild(document.body, s2);
        Feather.replace();

        // this.GetDealsOverview();
        // this.GetRejectedDealData()

    }
    GetRejectedDealData() {
        var pData = {
            "Task": "getdeals",
            "TotalRecords": 0,
            "Offset": 0,
            "Limit": 1,
            "RefreshCount": true,
            "SearchCondition": `(StatusCode = \"deal.rejected\" ) AND (AccountId == \"${this._HelperService.UserAccount.AccountId}\" )`,
            "SortExpression": "CreateDate desc",
            "Type": null,
            "ReferenceKey": null,
            "ReferenceId": 0,
            "SubReferenceId": 0,
            "ListType": 0,
            "IsDownload": false
        }
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                this.rejectedDeals = [];
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this.rejectedDeals = _Response.Result.Data;
                    if (this.rejectedDeals.length > 0) {
                        // this._DataHelperService.dealsOverview.subscribe(data => {
                        this.alertMessages.firstDealReject = true;
                        this._DataHelperService.changedealsOverview(this.alertMessages);
                        // })
                    }
                }
                else {
                    this.rejectedDeals = [];
                }
            },
            _Error => {
                this._HelperService.HandleException(_Error);
            });
    }
    GetDealsOverview() {
        let pData = {
            Task: 'getdealsoverview',
            StartDate: this._HelperService.DateInUTC(this._HelperService.AppConfig.DefaultStartTimeAll),
            EndDate: this._HelperService.DateInUTC(this._HelperService.AppConfig.DefaultEndTimeToday),
            AccountId: this._HelperService.AppConfig.ActiveOwnerId,
            AccountKey: this._HelperService.AppConfig.ActiveOwnerKey,
            StoreReferenceId: 0,
            StoreReferenceKey: null,
        };
        this._HelperService.IsFormProcessing = true;

        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.Deals, pData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    this._HelperService.SaveStorage(this._HelperService.AppConfig.Storage.dealOverview, _Response.Result);
                    // this.alertMessages  = { firstDealApproved:false, firstDealReject:false, askToAppDownload: true, firstDealUploaded: false }
                    if (_Response.Result.Total == 1 && (_Response.Result.Approved == 1 || _Response.Result.Upcoming == 1)) {
                        this.alertMessages.firstDealApproved = true;
                    } if (!_Response.Result.AppDownloaded) {
                        this.alertMessages.askToAppDownload = true;
                    }
                    // this._DataHelperService.dealsOverview.pipe(distinct()).subscribe((data:any) => {
                    this._DataHelperService.changedealsOverview(this.alertMessages);
                    // })
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }

    // MoveToAccountSetting() {
    //     $('.accountsettingtt').tooltip("hide");
    //     this._Router.navigateByUrl(this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.AccountSetting)
    //     setTimeout(() => {
    //         this.ngOnInit();
    //     }, 500);
    // }

    MoveToAccountSetting(route) {
        var TAccessKey = this._HelperService.AccessKey;
        var TPublicKey = this._HelperService.PublicKey;
        var Key = btoa(TAccessKey + "|" + TPublicKey + "|" + route);

        window.location.href = this._HelperService.AppConfig.AccountPanelURL + '/account/auth/' + Key, '_blank';
    }

    backToDashboard() {
        if (this._HelperService.isDisplayLoyaltyConfig) {
            this._Router.navigateByUrl(this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.LoyaltyDashboard)
            setTimeout(() => {
                this.ngOnInit();
            }, 500);
        } else {
            this._Router.navigateByUrl(this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.DealsDashboard)
            setTimeout(() => {
                this.ngOnInit();
            }, 500);
        }
    }

    // MoveToCashiers() {
    //     this._Router.navigateByUrl("m/" + this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.AccountSettings.Cashiers)
    // }
    SkipTour() {
        $("#taketour").modal("hide");
    }

    StartIntroLoyalty() {
        this.SkipTour();
        this._Router.navigateByUrl(this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.LoyaltyDashboard)
        // introJs().start()
        if (this._HelperService.GetStorage("toggleDB")) {
            this.introJS.setOptions({
                showBullets: false,
                nextLabel: "Next",
                prevLabel: "Previous",
                skipLabel: "",
                disableInteraction: true,
                steps: [
                    {
                        title: '1/12',
                        element: '#WalletBalance',
                        intro: '<b>WALLET BALANCE</b><br><br>This is the total amount of funds stored in your account to reward your customers. You can also click on the plus button to top-up your wallet.To see the history of your wallet, click on this icon'

                    },
                    {
                        title: '2/12',
                        element: '#bentodashboard',
                        intro: '<b>DASHBOARD</b><br><br>This gives you the flexibility to switch between your loyalty and deals dashboard when you click on this icon.You can easily manage and see the performance of each of your products or services running under our loyalty and deals platform.'
                    },
                    {
                        title: '3/12',
                        element: '#RewardPercentage',
                        intro: '<b>REWARD PERCENTAGE</b><br><br>This allows you to set and change the reward percentage you are given to your customers.You can also click on this icon to see your reward history given to customers.'
                    },
                    {
                        title: '4/12',
                        element: '#SaleAmount',
                        intro: '<b>SALE AMOUNT</b><br><br>Total sales tells you the total amount of goods sold in your business or services provided over a period of time. You can also click on this icon to see all the history of your sales transactions.'
                    },
                    {
                        title: '5/12',
                        element: '#CustomerVisits',
                        intro: '<b>CUSTOMER VISITS</b><br><br>Customer visit gives you real-time information and overview of new versus returning customers buying from your store.You can also click on this icon to see a list of all your new and returning customers.'
                    },
                    {
                        title: '6/12',
                        element: '#CustomerChart',
                        intro: '<b>CUSTOMER CHART</b><br><br>This chart gives you the summary of your returning customers,new customers and dormant customers.You can click on this icon to get more information.'
                    },
                    {
                        title: '7/12',
                        element: '#RewardChart',
                        position: 'bottom',
                        intro: '<b>REWARD CHART</b><br><br>This chart gives you the summary of all the rewards given to your customers, including unclaimed and pending rewards.You can click on this icon to get more information.'
                    },
                    {
                        title: '8/12',
                        element: '#Transactions',
                        intro: '<b>RECENT TRANSACTIONS</b><br><br>This section allows you to see all your last and recent transactions done within the last one week.You can click on this icon to get more information.'
                    },
                    {
                        title: '9/12',
                        element: '#SalesPerformance',
                        intro: '<b>RECENT SALES PERFORMANCE</b><br><br>This chart gives you a quick overview and summary of how much revenue and sales your business has made in a given period. It also shows how your business is performing within a given period. You can click on this icon to get more information.'
                    },
                    {
                        title: '10/12',
                        element: '#FilterStore',
                        intro: '<b>FILTER STORE</b><br><br>This is for merchants who have more than one or two stores. It allows you to quickly search and have a quick view of how your stores are performing.'
                    },
                    {
                        title: '11/12',
                        element: '#FilterPeriod',
                        intro: '<b>FILTER PERIOD</b><br><br>This filter allows you to select or choose the date period you want information about your business on. '
                    },
                    {
                        title: '12/12',
                        element: '#Help',
                        intro: '<b>HELP</b><br><br>This allows you to learn more about each of the functionalities on this page. You can click on the “i” Icon at the top right to start learning more about each feature.'
                    }
                ]
            });
        }
        else {

            this.introJS.setOptions({
                showBullets: false,
                nextLabel: "Next",
                prevLabel: "Previous",
                skipLabel: "",
                disableInteraction: true,
                steps: [
                    {
                        title: '1/12',
                        element: '#WalletBalance',
                        intro: '<b>WALLET BALANCE</b><br><br>This is the total amount of funds stored in your account to reward your customers. You can also click on the plus button to top-up your wallet.To see the history of your wallet, click on this icon'

                    },
                    {
                        title: '2/12',
                        element: '#bentoicon',
                        intro: '<b>ACCOUNT & PREFERENCE</b><br><br>This allows you to manage your account. You can also explore other products.'
                    },
                    {
                        title: '3/12',
                        element: '#RewardPercentage',
                        intro: '<b>REWARD PERCENTAGE</b><br><br>This allows you to set and change the reward percentage you are given to your customers.You can also click on this icon to see your reward history given to customers.'
                    },
                    {
                        title: '4/12',
                        element: '#SaleAmount',
                        intro: '<b>SALE AMOUNT</b><br><br>Total sales tells you the total amount of goods sold in your business or services provided over a period of time. You can also click on this icon to see all the history of your sales transactions.'
                    },
                    {
                        title: '5/12',
                        element: '#CustomerVisits',
                        intro: '<b>CUSTOMER VISITS</b><br><br>Customer visit gives you real-time information and overview of new versus returning customers buying from your store.You can also click on this icon to see a list of all your new and returning customers.'
                    },
                    {
                        title: '6/12',
                        element: '#CustomerChart',
                        intro: '<b>CUSTOMER CHART</b><br><br>This chart gives you the summary of your returning customers,new customers and dormant customers.You can click on this icon to get more information.'
                    },
                    {
                        title: '7/12',
                        element: '#RewardChart',
                        position: 'bottom',
                        intro: '<b>REWARD CHART</b><br><br>This chart gives you the summary of all the rewards given to your customers, including unclaimed and pending rewards.You can click on this icon to get more information.'
                    },
                    {
                        title: '8/12',
                        element: '#Transactions',
                        intro: '<b>RECENT TRANSACTIONS</b><br><br>This section allows you to see all your last and recent transactions done within the last one week.You can click on this icon to get more information.'
                    },
                    {
                        title: '9/12',
                        element: '#SalesPerformance',
                        intro: '<b>RECENT SALES PERFORMANCE</b><br><br>This chart gives you a quick overview and summary of how much revenue and sales your business has made in a given period. It also shows how your business is performing within a given period. You can click on this icon to get more information.'
                    },
                    {
                        title: '10/12',
                        element: '#FilterStore',
                        intro: '<b>FILTER STORE</b><br><br>This is for merchants who have more than one or two stores. It allows you to quickly search and have a quick view of how your stores are performing.'
                    },
                    {
                        title: '11/12',
                        element: '#FilterPeriod',
                        intro: '<b>FILTER PERIOD</b><br><br>This filter allows you to select or choose the date period you want information about your business on. '
                    },
                    {
                        title: '12/12',
                        element: '#Help',
                        intro: '<b>HELP</b><br><br>This allows you to learn more about each of the functionalities on this page. You can click on the “i” Icon at the top right to start learning more about each feature.'
                    }
                ]
            });
        }

        setTimeout(() => {
            this.introJS.start();
        }, 500);
        // this.introJS.onbeforechange((targetElement) => {
        //   console.log(targetElement.id);
        // //  var boxArrow = document.getElementsByClassName('introjs-arrow top');      
        //   // if(targetElement.id=="Transactions"){
        //   //   setTimeout(function(){
        //   //     var element = document.getElementsByClassName('introjs-tooltip');
        //   //     $('.introjs-tooltip').css({top:'80px',left:'27px'});
        //   //   },600)  
        //   // }     
        // });  
    }

    StartIntroDeal() {
        this.SkipTour();
        this._Router.navigateByUrl(this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.DealsDashboard)
        // $(".introjs-helperLayer").removeAttr("style");
        // introJs().start()

        if (this._HelperService.GetStorage("toggleDB")) {
            this.introJS.setOptions({
                showBullets: false,
                nextLabel: "Next",
                prevLabel: "Previous",
                skipLabel: "",
                steps: [
                    {
                        title: '1/11',
                        element: '#WalletBalance',
                        intro: '<b>WALLET BALANCE</b><br><br>This is the total amount of funds stored in your account to reward your customers. You can also click on the plus button to top-up your wallet.To see the history of your wallet, click on this icon'

                    },
                    {
                        title: '2/11',
                        element: '#bentodashboard',
                        intro: '<b>DASHBOARD</b><br><br>This gives you the flexibility to switch between your loyalty and deals dashboard when you click on this icon.You can easily manage and see the performance of each of your products or services running under our loyalty and deals platform.'
                    },
                    {
                        title: '3/11',
                        element: '#DealsSold',
                        intro: '<b>DEALS SOLD</b><br><br>This are the deals you have sold to customers over a filter period.'
                    },
                    {
                        title: '4/11',
                        element: '#SaleAmount',
                        intro: '<b>SALE AMOUNT</b><br><br>Total sales tells you the total amount of goods sold in your business or services provided over a period of time. You can also click on this icon to see all the history of your sales transactions.'
                    },
                    {
                        title: '5/11',
                        element: '#ReedemedDeals',
                        intro: '<b>REDEEMED DEALS</b><br><br>This are the deals your customer have redeemed over a selected period.'
                    },
                    {
                        title: '6/11',
                        element: '#SalesReport',
                        intro: '<b>DEAL SALES REPORT</b><br><br>This are the deals your customer have redeemed over a selected period.'
                    },
                    {
                        title: '7/11',
                        element: '#ReedemedDealsReport',
                        intro: '<b>DEALS REDEEMED REPORT</b><br><br>See all revenue from deals redeemed between yesterday and today.'
                    },
                    {
                        title: '8/11',
                        element: '#RevenueFromDeals',
                        intro: '<b>REVENUE FROM DEALS</b><br><br>See all revenue from deals sold between yesterday and today.'
                    },
                    {
                        title: '9/11',
                        element: '#Transactions',
                        intro: '<b>RECENT TRANSACTIONS</b><br><br>This section allows you to see all your last and recent transactions done within the last one week.You can click on this icon to get more information.'
                    },
                    {
                        title: '10/11',
                        element: '#FilterPeriod',
                        intro: '<b>FILTER PERIOD</b><br><br>This filter allows you to select or choose the date period you want information about your business on. '
                    },
                    {
                        title: '11/11',
                        element: '#Help',
                        intro: '<b>HELP</b><br><br>This allows you to learn more about each of the functionalities on this page. You can click on the “i” Icon at the top right to start learning more about each feature.'
                    }
                ]
            });
        }
        else {
            this.introJS.setOptions({
                showBullets: false,
                nextLabel: "Next",
                prevLabel: "Previous",
                skipLabel: "",
                steps: [
                    {
                        title: '1/11',
                        element: '#WalletBalance',
                        intro: '<b>WALLET BALANCE</b><br><br>This is the total amount of funds stored in your account to reward your customers. You can also click on the plus button to top-up your wallet.To see the history of your wallet, click on this icon'

                    },
                    {
                        title: '2/11',
                        element: '#bentoicon',
                        intro: '<b>ACCOUNT & PREFERENCE</b><br><br>This allows you to manage your account. You can also explore other products.'
                    },
                    {
                        title: '3/11',
                        element: '#DealsSold',
                        intro: '<b>DEALS SOLD</b><br><br>This are the deals you have sold to customers over a filter period.'
                    },
                    {
                        title: '4/11',
                        element: '#SaleAmount',
                        intro: '<b>SALE AMOUNT</b><br><br>Total sales tells you the total amount of goods sold in your business or services provided over a period of time. You can also click on this icon to see all the history of your sales transactions.'
                    },
                    {
                        title: '5/11',
                        element: '#ReedemedDeals',
                        intro: '<b>REDEEMED DEALS</b><br><br>This are the deals your customer have redeemed over a selected period.'
                    },
                    {
                        title: '6/11',
                        element: '#SalesReport',
                        intro: '<b>DEAL SALES REPORT</b><br><br>This are the deals your customer have redeemed over a selected period.'
                    },
                    {
                        title: '7/11',
                        element: '#ReedemedDealsReport',
                        intro: '<b>DEALS REDEEMED REPORT</b><br><br>See all revenue from deals redeemed between yesterday and today.'
                    },
                    {
                        title: '8/11',
                        element: '#RevenueFromDeals',
                        intro: '<b>REVENUE FROM DEALS</b><br><br>See all revenue from deals sold between yesterday and today.'
                    },
                    {
                        title: '9/11',
                        element: '#Transactions',
                        intro: '<b>RECENT TRANSACTIONS</b><br><br>This section allows you to see all your last and recent transactions done within the last one week.You can click on this icon to get more information.'
                    },
                    {
                        title: '10/11',
                        element: '#FilterPeriod',
                        intro: '<b>FILTER PERIOD</b><br><br>This filter allows you to select or choose the date period you want information about your business on. '
                    },
                    {
                        title: '11/11',
                        element: '#Help',
                        intro: '<b>HELP</b><br><br>This allows you to learn more about each of the functionalities on this page. You can click on the “i” Icon at the top right to start learning more about each feature.'
                    }
                ]
            });
        }
        setTimeout(() => {
            this.introJS.start();
        }, 500);
        this.introJS.onbeforechange((targetElement) => {
            if (targetElement.id == "ReedemedDealsReport" || targetElement.id == "RevenueFromDeals") {
                setTimeout(function () {
                    var boxArrow = document.getElementsByClassName('introjs-arrow top');
                    boxArrow.item(0).setAttribute("style", "display: block");

                }, 600)
            }
        });
        this.introJS.refresh();
    }

    loadSubMenu() {
        setTimeout(() => {
            $('.submenuLabel').on('click', function (e) {
                e.preventDefault();
                $(this).parent().toggleClass('show');
            })
        }, 1000);
    }

    NavigateToLoyalty() {
        this._HelperService.SaveStorage("hcaDisplayLoyalty", true)
        this.loadSubMenu()
        setTimeout(() => {
            this._HelperService.ValidateDataPermission();
        }, 100);
    }

    NavigateToDeals() {
        this._HelperService.SaveStorage("hcaDisplayLoyalty", false)
        this.loadSubMenu()
        setTimeout(() => {
            this._HelperService.ValidateDataPermission();
        }, 100);

    }

    displayLoyaltyModal() {
        $(".loyaltyModal").modal("show");
        $(".custom-dropdown-menu").hide()
    }

    displayDealsModal() {
        $(".dealsModal").modal("show");
        $(".custom-dropdown-menu").hide()
    }

    displayDropDown() {
        $(".custom-dropdown-menu").show()
    }
    AddDealsAccountType() {
        var _PostData = {
            Task: "ob_merchant_requestaccountconfiguration",
            ReferenceId: this._HelperService.GetStorage("hca").UserAccount.AccountId,
            ReferenceKey: this._HelperService.GetStorage("hca").UserAccount.AccountKey,
            AccountTypes: [{ AccountTypeCodes: "thankucashdeals", Value: "2" }]
        }
        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.MOnBoardV2, _PostData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    $(".dealsModal").modal("hide");
                    this._HelperService.SaveStorage("hcaDisplayLoyalty", false)
                    this.loadSubMenu()
                    this._HelperService.SaveStorage("toggleDB", true)
                    this._HelperService.DisplayToggleDashboard = true
                    this._HelperService.DeleteStorage("hcuat");
                    this._HelperService.SaveStorage("hcuat", [{ "TypeId": 795, "TypeCode": "accounttype.thankucashdeals" }, { "TypeId": 793, "TypeCode": "accounttype.thankucashloyalty" }])
                    setTimeout(() => {
                        this._HelperService.ValidateDataPermission();
                    }, 10);
                    this._Router.navigateByUrl(this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.DealsDashboard)
                    this._HelperService.isDisplayLoyaltyConfig = false
                    this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.Merchant = this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.DealsDashboard
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }

    AddLoyaltyAccountType() {
        var _PostData = {
            Task: "ob_merchant_requestaccountconfiguration",
            ReferenceId: this._HelperService.GetStorage("hca").UserAccount.AccountId,
            ReferenceKey: this._HelperService.GetStorage("hca").UserAccount.AccountKey,
            AccountTypes: [{
                AccountTypeCodes: "thankucashloyalty", ConfigurationValue: "closedloyaltymodel",
                Value: null
            }]
        }
        this._HelperService.IsFormProcessing = true;
        let _OResponse: Observable<OResponse>;
        _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V3.MOnBoardV2, _PostData);
        _OResponse.subscribe(
            _Response => {
                this._HelperService.IsFormProcessing = false;
                if (_Response.Status == this._HelperService.StatusSuccess) {
                    $(".loyaltyModal").modal("hide");
                    this._HelperService.SaveStorage("hcaDisplayLoyalty", true)
                    this.loadSubMenu()
                    this._HelperService.SaveStorage("toggleDB", true)
                    this._HelperService.DisplayToggleDashboard = true
                    this._HelperService.DeleteStorage("hcuat");
                    this._HelperService.SaveStorage("hcuat", [{ "TypeId": 795, "TypeCode": "accounttype.thankucashdeals" }, { "TypeId": 793, "TypeCode": "accounttype.thankucashloyalty" }])
                    setTimeout(() => {
                        this._HelperService.ValidateDataPermission();
                    }, 10);
                    this._Router.navigateByUrl(this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.LoyaltyDashboard)
                    this._HelperService.isDisplayLoyaltyConfig = true
                    this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.Merchant = this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.LoyaltyDashboard
                }
                else {
                    this._HelperService.NotifyError(_Response.Message);
                }
            },
            _Error => {
                this._HelperService.IsFormProcessing = false;
                this._HelperService.HandleException(_Error);
            });
    }


    checked(e) {
        if (e) {
            //   console.log('navigate to loyalty');
            this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.LoyaltyDashboard])
        } else {
            //   console.log('navigate to deals');
            this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.Dashboard.DealsDashboard])
        }
    }
    NavigateCampaigns() {
        // console.log("helper key", this._HelperService.AccessKey)
        // var TAccessKey = this._HelperService.AccessKey;
        // var TPublicKey = this._HelperService.PublicKey;
        // var Key = btoa(TAccessKey + "|" + TPublicKey);
        // console.log("Mkey", Key)
        // if (this._HelperService.AppConfig.HostType == HostType.Live) {

        //     window.open("https://campaigns.thankucash.com/account/auth/" + Key, '_blank').focus();
        //     // window.open("https://campaigns.thankucash.com/account/auth/" + TAccessKey + "/" + TPublicKey, '_blank').focus();
        // }
        // else if (this._HelperService.AppConfig.HostType == HostType.Test) {
        //     window.open("https://testcampaigns.thankucash.com/account/auth/" + Key, '_blank').focus();
        // }
        // else {
        //     window.open("https://testcampaigns.thankucash.com/account/auth/" + TAccessKey + "/" + TPublicKey, '_blank').focus();
        // }
    }

    Upgrade() {
        this._Router.navigate([this._HelperService.AppConfig.Pages.ThankUCash.PanelMerchant.AllPlans])
    }

    ProcessLogout() {

        swal({
            position: "center",
            title: this._HelperService.AppConfig.CommonResource.LogoutTitle,
            text: "Click on Logout button to confirm Logout",
            animation: false,
            customClass: this._HelperService.AppConfig.Alert_Animation,
            showCancelButton: true,
            confirmButtonColor: this._HelperService.AppConfig.Color_Red,
            cancelButtonColor: this._HelperService.AppConfig.Color_Grey,
            confirmButtonText: "Logout",
            cancelButtonText: this._HelperService.AppConfig.CommonResource.Cancel

        }).then((result) => {
            if (result.value) {
                var pData = {
                    Task: this._HelperService.AppConfig.Api.Logout
                };
                let _OResponse: Observable<OResponse>;
                _OResponse = this._HelperService.PostData(this._HelperService.AppConfig.NetworkLocation.V1.System, pData);
                _OResponse.subscribe(
                    _Response => {
                        if (_Response.Status == this._HelperService.StatusSuccess) {
                            this._HelperService.NotifySuccess(_Response.Message);
                            this._HelperService.DeleteStorage('templocmerchant');

                            this._HelperService.templocmerchant = false;

                        }
                        else {
                            this._HelperService.NotifyError(_Response.Message);
                        }
                    },
                    _Error => {
                        console.log(_Error);
                    });
                this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Account);
                this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Location);
                this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.OReqH);
                this._HelperService.DeleteStorage(this._HelperService.AppConfig.Storage.Permissions);
                localStorage.clear()
                this._Router.navigate([this._HelperService.AppConfig.Pages.System.Login]);
            }
            else { }
        });



    }
}

enum HostType {
    Live,
    Test,
    Tech,
    Dev
}  