import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { TUMerchantComponent } from './merchant.component';
import { TUMerchantRoutingModule } from './merchant.routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ToggleButtonComponent } from '../../panel/merchant/dashboards/togglebutton/togglebutton.component';

@NgModule({
    declarations: [
        TUMerchantComponent,
        ToggleButtonComponent
    ],
    imports: [
        TUMerchantRoutingModule,
        TranslateModule,
        CommonModule,
        ReactiveFormsModule,
        
    ],
    providers: [],
    bootstrap: [TUMerchantComponent]
})
export class TUMerchantModule { }
