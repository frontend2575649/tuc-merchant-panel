import { NgModule } from "@angular/core";
import { Routes, RouterModule, CanActivateChild } from "@angular/router";
import { TUMerchantComponent } from "./merchant.component";
import { HelperService } from "../../service/helper.service";
import { MerchantguardGuard } from 'src/app/service/guard/merchantguard.guard';
import { } from './loyalty/loyalty.module'

const routes: Routes = [
  {
    path: "m",
    component: TUMerchantComponent,
    canActivateChild: [HelperService],
    children: [

      { path: 'loyalty', data: { 'permission': 'administration', PageName: 'System.Menu.Administration' }, loadChildren: './loyalty/loyalty.module#LoyaltyModule' },
      { path: 'deal', data: { 'permission': 'administration', PageName: 'System.Menu.Administration' }, loadChildren: './deals/deals.module#DealsModule' },

      { path: "bulkterminals", data: { permission: "account", PageName: "System.Menu.BulkTerminal" }, loadChildren: "../../modules/accounts/tumerchants/bulkterminalupload/tuterminals.module#TUBulkTerminalsModule" },
      { path: "bulkterminalslist", data: { permission: "account", PageName: "System.Menu.BulkTerminal" }, loadChildren: "../../modules/accounts/tumerchants/bulkterminallist/tuterminals.module#TUBulkTerminalsModule" },
      // { path: "upgrade", data: { permission: "account", PageName: "System.Menu.Upgrade" }, loadChildren: "../../modules/accounts/tumerchants/NoAccess/topup.module#TUTopUpModule" },
      { path: "campaign", data: { permission: "account", PageName: "System.Menu.Campaign" }, loadChildren: "../../modules/accountdetails/tumerchant/tucampaign/tucampaign.module#TUCampaignModule" },
      { path: 'cashiers', data: { 'permission': 'cashiers', PageName: 'System.Menu.Cashier' }, loadChildren: '../../modules/accounts/tumerchants/cashiers/tucashiers.module#TUCashiersModule' },

      {
        path: "terminallive",
        data: { permission: "dashboard", PageName: "System.Menu.Dashboard" },
        loadChildren:
          "./../../pages/dashboard/tumerchantterminallive/tumerchantterminallive.module#TUMerchantTerminalsLiveModule"
      },
      {
        path: "merchantupload",
        data: { permission: "stores", PageName: "System.Menu.BulkUplaod" },
        loadChildren:
          "./../../pages/useronboarding/bulkmerchant/merchantupload.module#TUMerchantUploadModule"
      },

      //   //TUCMALl
      { path: "orders", data: { permission: "account", PageName: "System.Menu.Orders" }, loadChildren: "../../panel/merchant/tucmall/tuorders/tuorders.module#TUOrdersModule" },
      { path: "orderhistory/:referenceid/:referencekey/:accountid/:accountkey", data: { permission: "account", PageName: "System.Menu.OrderHistory" }, loadChildren: "../../panel/merchant/tucmall/tuorder/tuorder.module#TUOrderModule" },
      { path: "products", data: { permission: "account", PageName: "System.Menu.Products" }, loadChildren: "../../panel/merchant/tucmall/tuproducts/tuproducts.module#TUProductsModule" },
      { path: "product", data: { permission: "account", PageName: "System.Menu.Products" }, loadChildren: "../../modules/accountdetails/tumerchant/tuproduct/tuproduct.module#TUProductModule" },
      { path: "addproduct", data: { permission: "account", PageName: "System.Menu.AddProduct" }, loadChildren: "../../panel/merchant/tucmall/tuaddproduct/tuaddproduct.module#TUAddProductsModule" },
      { path: "editproduct", data: { permission: "account", PageName: "System.Menu.EditProduct" }, loadChildren: "../../panel/merchant/tucmall/tueditproduct/tueditproduct.module#TUEditProductsModule" },

      // GiftPoints
      { path: "giftpoints", data: { permission: "dashboard", PageName: "System.Menu.GiftPoints" }, loadChildren: "../../modules/dashboards/giftpoints/tugiftpoints.module#TuGiftPointsModule" },
      { path: "giftpointsales", data: { permission: "dashboard", PageName: "System.Menu.Home" }, loadChildren: "../../modules/transactions/tusale/GiftPoints/home/home.module#TUHomeModule" },
      // { path: "Reward", data: { permission: "dashboard", PageName: "System.Menu.Home" }, loadChildren: "../../modules/transactions/tusale/GiftPoints/Reward/tusale.module#TUSaleModule" },

      // GiftCard
      { path: "giftcard", data: { permission: "dashboard", PageName: "System.Menu.GiftCard" }, loadChildren: "../../modules/dashboards/giftcard/tugiftcard.module#TuGiftCardModule" },
      { path: "giftcardsales", data: { permission: "dashboard", PageName: "System.Menu.Home" }, loadChildren: "../../modules/transactions/tusale/giftcard/home/home.module#TUHomeModule" },

      //My Buissness 
      { path: "sales/saleshistory", data: { permission: "dashboard", PageName: "System.Menu.SalesHistory" }, loadChildren: "../../modules/transactions/tusale/merchant/tusale.module#TUSaleModule" },
      { path: "salestrend", data: { permission: "dashboard", PageName: "System.Menu.SalesTrend" }, loadChildren: "../../modules/dashboards/mybuisness/tusalestrends/tusalestrends.module#TuSalesTrendModule" },
      { path: "salesummary", data: { permission: "dashboard", PageName: "System.Menu.SaleSummary" }, loadChildren: "../../modules/dashboards/mybuisness/tusalesummary/tusalesummary.module#TUSaleSummaryModule" },
      { path: "cashieranalyis", data: { permission: "dashboard", PageName: "System.Menu.CashierAnalytics" }, loadChildren: "../../modules/dashboards/mybuisness/cashier/tucashiers.module#TUCashiersModule" },
      { path: "sales/pendingtransaction", data: { permission: "dashboard", PageName: "System.Menu.SuspiciousTransaction" }, loadChildren: "../../modules/accounts/tumerchants/pendingtransaction/pendingtransaction.module#TUPendingTransactionModule" },

      { path: "transaction", data: { permission: "dashboard", PageName: "System.Menu.Transaction" }, loadChildren: "../../modules/transactions/tusale/roottransaction/roottransaction.module#RootTransactionModule" },

      //Reward N Redeem


      { path: "sales/rewardclaim", data: { permission: "dashboard", PageName: "System.Menu.RewardClaim" }, loadChildren: "../../modules/transactions/tusale/turewardclaim/turewardclaim.module#TURewardsClaimModule" },

      // Customers
      { path: "customerrewards/:referenceid", data: { permission: "terminals", PageName: "System.Menu.CustomerRewards" }, loadChildren: "./../../modules/accounts/tumerchants/bulkcustomerrewards/bulkcustomerrewards.module#TUCBulkCustomerRewardListModule" },
      { path: "customers/:referenceid", data: { permission: "terminals", PageName: "System.Menu.Customers" }, loadChildren: "./../../modules/accounts/tumerchants/bulkcustomersfiles/customerupload.module#TUCustomerUploadModule" },
      { path: "allcustomers", data: { permission: "terminals", PageName: "System.Menu.Customers" }, loadChildren: "./../../modules/accounts/tumerchants/tucustomers/tucustomers.module#TUCustomersModule" },
      { path: "allmcustomers", data: { permission: "terminals", PageName: "System.Menu.Customers" }, loadChildren: "./../../modules/accounts/tumerchants/tuallcustomers/tuallcustomers.module#TUAllCustomerssModule" },

      {
        path: "bulkcustomerrewards",
        data: { permission: "acquirer", PageName: "System.Menu.BulkRewards" },
        loadChildren:
          "./../../pages/useronboarding/bulkreward/bulkreward.module#TUCBulkCustomerRewardModule"
      },
      {
        path: "bulkcustomerupload",
        data: { permission: "acquirer", PageName: "System.Menu.CustomerUpload" },
        loadChildren:
          "./../../pages/useronboarding/bulkreward/bulkreward.module#TUCBulkCustomerRewardModule"
      },



      { path: "bankmanager", data: { permission: "terminals", PageName: "System.Menu.Bank" }, loadChildren: "./../../modules/accounts/tumerchants/bankmanager/bankmanager.module#TUBankManagerModule" },
      { path: "banks", data: { permission: "terminals", PageName: "System.Menu.Bank" }, loadChildren: "./../../modules/accounts/tumerchants/bankmanager/banks/banks.module#TUBanksModule" },

      //  CashOuts

      { path: "cashouts", data: { permission: "terminals", PageName: "System.Menu.CashOuts" }, loadChildren: "./../../modules/accounts/tumerchants/tucashouts/tucashouts.module#TUCashoutsModule" },
      { path: "bnpl", loadChildren: "../../panel/merchant/bnpl/bnpl.module#BnplModule" },
      { path: "wallet", data: { PageName: "System.Menu.Wallet" }, loadChildren: "./../../modules/accounts/tumerchants/wallet/wallet.module#WalletModule" },
      { path: "dealswallet", data: { PageName: "System.Menu.Wallet" }, loadChildren: "./../../modules/accounts/tumerchants/dealswallet/dealswallet.module#DealswalletModule" },
      { path: "bnplwallet", data: { PageName: "System.Menu.Wallet" }, loadChildren: "./../../modules/accounts/tumerchants/bnplwallet/bnplwallet.module#BnplwalletModule" },
      { path: 'faqcategories', data: { 'permission': 'administration', PageName: 'System.Menu.Administration' }, loadChildren: '../../modules/accounts/tumerchants/tufaqcategories/tufaqcategories.module#TUFAQCategoriesModule' },
      { path: 'faqs/:referencekey/:referenceid', data: { 'permission': 'administration', PageName: 'System.Menu.Administration' }, loadChildren: '../../modules/accounts/tumerchants/tufaqs/tufaqs.module#TUFAQsModule' },
      { path: "downloads", data: { permission: "dashboard", PageName: "System.Menu.Downloads" }, loadChildren: "../../modules/accounts/tumerchants/tudownloads/tudownloads.module#TUDownloadsModule" },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TUMerchantRoutingModule { }
